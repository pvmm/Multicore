----------------------------------------------------------
--  zx01.vhd
--		ZX01 top level
--		==============
--
--  12/15/01	Daniel Wallner : Rewrite of Bodo Wenzels zx97 to SOC
--  02/23/02	Daniel Wallner : Changed to the synchronous t80s
--  03/04/02	Daniel Wallner : Connected INT_n, synchronized reset and added tape_out
--  08/14/02	Daniel Wallner : Changed for xilinx XST
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- the pads ----------------------------------------------

entity zx01 is
	port (
		clock			: in    std_ulogic;							-- Clock 6.5MHz
		ophi			: out   std_ulogic;							-- Saida clock/2
		ext_reset_n	: in    std_ulogic;							-- Entrada /RESET externo
		int_reset_n	: out   std_logic;							-- Saida /RESET interno
		videoRGB		: out   std_ulogic;							-- Saida 1bit video para RGB
		videoCVBS	: out   std_ulogic;							-- Saida 1bit video para CVBS
		ohsync		: out   std_ulogic;							-- Sincronismo horizontal
		ovsync		: out   std_ulogic;							-- Sincronismo vertical
		oblank		: out   std_ulogic;							-- Blank
		tape_in		: in    std_ulogic;							-- Entrada Tape
		tape_out		: out   std_ulogic;							-- Saida Tape

		psg_en		: in    std_logic;							-- Habilita PSG
		usa_uk		: in    std_ulogic;							-- Modelo USA ou UK
		chr13			: in    std_logic;							-- Modo UDG ou Hi-res
		v_inv			: in    std_logic;							-- Video inverso
		config_o		: out   std_logic_vector(3 downto 0);	-- Saida para indicar configuracoes

		cpu_a			: out   std_logic_vector(15 downto 0);	-- CPU Addr

		ram_A			: out   std_logic_vector(15 downto 0);	-- SRAM Addr (64K)
		ram_Din		: out   std_logic_vector(7 downto 0);	-- SRAM Data in
		ram_Dout		: in    std_logic_vector(7 downto 0);	-- SRAM Data out
		ram_CE_n		: out   std_logic;							-- SRAM /CS
		ram_OE_n		: out   std_logic;							-- SRAM /OE
		ram_WE_n		: out   std_logic;							-- SRAM /WE

		rom_A			: out   std_logic_vector(12 downto 0);	-- ROM Addr (8K)
		rom_D			: in    std_logic_vector(7 downto 0);	-- ROM Data
		rom_CE_n		: out   std_logic;							-- ROM /CS

		ramUDG_A		: out   std_logic_vector(10 downto 0);	-- RAM UDG Addr (2K)
		ramUDG_Din	: out   std_logic_vector(7 downto 0);	-- RAM UDG Data in
		ramUDG_Dout	: in    std_logic_vector(7 downto 0);	-- RAM UDG Data out
		ramUDG_CE_n	: out   std_logic;							-- RAM UDG /CS
		ramUDG_OE_n	: out   std_logic;							-- RAM UDG /OE
		ramUDG_WE_n	: out   std_logic;							-- RAM UDG /WE

		kbd_col		: in    std_logic_vector(4 downto 0);	-- Colunas teclado
		psg_chMix	: out   std_logic_vector(7 downto 0);	-- Saida PSG
		
		Kolour_o		: out   std_logic_vector(7 downto 0);	

		sdbg			: out   std_logic_vector(7 downto 0) := (others => '0')
	);
end;

-- the top level ------------------------------

architecture rtl of zx01 is

	signal i_phi				: std_ulogic;
	signal clk_psg				: std_logic;
	signal a_mem_l				: std_ulogic_vector(8 downto 0);
	signal a_mem				: std_logic_vector(15 downto 0);
	signal d_ram				: std_logic_vector(7 downto 0);
	signal d_rom				: std_logic_vector(7 downto 0);
	signal d_ramUDG			: std_logic_vector(7 downto 0);
	signal n_romcs				: std_ulogic;
	signal n_ramcs				: std_ulogic;
	signal n_ramUDGcs			: std_ulogic;
	signal psg_din				: std_logic_vector(7 downto 0);
	signal psg_addr			: std_logic;
	signal psg_we				: std_logic;
	signal psg_re				: std_logic;
	signal a_cpu				: std_logic_vector(15 downto 0);
	signal n_m1					: std_ulogic;
	signal n_mreq				: std_ulogic;
	signal n_iorq				: std_ulogic;
	signal n_wr					: std_ulogic;
	signal n_rd					: std_ulogic;
	signal n_rfsh				: std_ulogic;
	signal n_nmi				: std_ulogic;
	signal n_halt				: std_ulogic;
	signal n_wait				: std_ulogic;
	signal i_n_modes			: std_ulogic;
	signal d_mem_i				: std_ulogic_vector(7 downto 0);
	signal d_cpu_i				: std_logic_vector(7 downto 0);
	signal d_cpu_o				: std_ulogic_vector(7 downto 0);
	signal kbd_mode			: std_logic_vector(4 downto 0);
	signal i_video				: std_ulogic;
	signal s_hsync				: std_ulogic;
	signal s_vsync				: std_ulogic;
	signal s_hblank			: std_logic;
	signal i_n_sync			: std_ulogic;
	signal reset_interno_n	: std_ulogic;
	signal reset_cpu_n		: std_ulogic;
	signal mode_reset			: boolean;
	signal mode_v_inv			: boolean;
	signal mode_chr13			: std_ulogic;
	signal mode_ram			: std_ulogic_vector(1 downto 0);
	signal vsync				: boolean;
	signal nmi_enable			: boolean;
	signal fake_cpu			: boolean;
	signal video_addr			: std_ulogic_vector(8 downto 0);
	signal video_mem			: boolean;
	signal d_kbd				: std_ulogic_vector(7 downto 0);
	signal d_kbd_enable		: boolean;

begin

	----------------
	-- Reset e Clock
	----------------
	c_res_clk : entity work.res_clk
	port map (
		clock			=> clock,									-- in
		mode_reset	=> mode_reset,								-- in
		ext_reset_n	=> ext_reset_n,							-- in
		n_reset		=> reset_interno_n,						-- out
		n_modes		=> i_n_modes,								-- out
		o_phi			=> i_phi,									-- out
		clk_psg		=> clk_psg									-- out
	);

	--------------
	-- Config 
	--------------
	c_modes97 : entity work.modes97
	port map (
		n_reset			=> reset_interno_n,					-- in
		n_modes			=> i_n_modes,							-- in
		phi				=> i_phi,								-- in
		v_inv				=> std_ulogic(v_inv),				-- in
		chr13				=> std_ulogic(chr13),				-- in
		addr				=> std_ulogic_vector(a_cpu),		-- in
		data				=> std_ulogic_vector(d_cpu_i),	-- in
		n_mreq			=> n_mreq,								-- in
		n_wr				=> n_wr,									-- in
		mode_reset		=> mode_reset,							-- out
		mode_v_inv		=> mode_v_inv,							-- out
		mode_chr13		=> mode_chr13,							-- out
		Kolour_o			=> Kolour_o
	);

	---------------
	-- Video
	---------------
	c_video81 : entity work.video81
	generic map (
		synchronous	=> true
	)
	port map (
		clock				=> clock,								-- in
		phi				=> i_phi,								-- in
		nmi_enable		=> nmi_enable,							-- in
		n_nmi				=> n_nmi,								-- out
		n_halt			=> n_halt,								-- in
		n_wait			=> n_wait,								-- out
		n_m1				=> n_m1,									-- in
		n_mreq			=> n_mreq,								-- in
		n_iorq			=> n_iorq,								-- in
		vsync				=> vsync,								-- in
		a_cpu				=> std_ulogic_vector(a_cpu)(15 downto 13),	-- in
		d_mem				=> d_mem_i,								-- in
		fake_cpu			=> fake_cpu,							-- out
		mode_chr13		=> mode_chr13,							-- in
		video_addr		=> video_addr,							-- out
		video_mem		=> video_mem,							-- out
		mode_v_inv		=> mode_v_inv,							-- in
		video				=> i_video,								-- out
		ohsync			=> s_hsync,								-- out
		ovsync			=> s_vsync,								-- out
		n_sync			=> i_n_sync,
		ohblank			=> s_hblank
	);

	-----------
	-- Sound
	-----------
	psg : entity work.YM2149
	port map (
		CLK				=> clock,						-- Clock alto (> 6 MHz)
		ENA				=> clk_psg,						-- Sinal de enable a 1.75 MHz
		RESET_L			=> reset_interno_n,
		I_SEL_L			=> '1', 							-- /SEL is high for AY-3-8912 compatibility

		I_DA				=> psg_din,						-- Entrada de dados
		O_DA				=> open,							-- Saida de dados

		busctrl_addr	=> psg_addr,					-- 1 grava o endereco do registrador
		busctrl_we		=> psg_we,						-- 1 grava o valor no registrador setado anteriormente
		busctrl_re		=> '0',							-- 1 le o valor do registrador setado anteriormente
		ctrl_aymode		=> '1',							-- 0 = YM, 1 = AY

		O_AUDIO_A		=> open,							-- Sai­da do canal A
		O_AUDIO_B		=> open,							-- Sai­da do canal B
		O_AUDIO_C		=> open,							-- Sai­da do canal C
		O_AUDIO			=> psg_chMix					-- Saida mixada dos 3 canais
	);

	-------------
	-- I/O Ports
	-------------
	c_io81 : entity work.io81
	port map (
		n_reset			=> reset_interno_n,					-- in
		addr				=> std_ulogic_vector(a_cpu)(7 downto 0),	-- in
		n_iorq			=> n_iorq,								-- in
		n_wr				=> n_wr,									-- in
		n_rd				=> n_rd,									-- in
		vsync				=> vsync,								-- out
		nmi_enable		=> nmi_enable,							-- out
		kbd_col			=> std_ulogic_vector(kbd_col),	-- in
		usa_uk			=> usa_uk,								-- in
		tape_in			=> tape_in,								-- in
		d_kbd				=> d_kbd,								-- out
		d_kbd_enable	=> d_kbd_enable,						-- out
		psg_en			=> psg_en,								-- in
		psg_addr			=> psg_addr,							-- out
		psg_we			=> psg_we								-- out
	);

	---------------
	-- Bus Control
	---------------
 	c_busses: entity work.busses
	port map (
		a_cpu				=> std_ulogic_vector(a_cpu),		-- in
		video_addr		=> video_addr,							-- in
		video_mem		=> video_mem,							-- in
		a_mem_l			=> a_mem_l,								-- out
		fake_cpu			=> fake_cpu,							-- in
		d_kbd				=> d_kbd,								-- in
		d_kbd_enable	=> d_kbd_enable,						-- in
		d_mem_i			=> d_mem_i,								-- in
		d_cpu_o			=> d_cpu_o,								-- out
		n_m1				=> n_m1,									-- in
		n_mreq			=> n_mreq,								-- in
		n_iorq			=> n_iorq,								-- in
		n_wr				=> n_wr,									-- in
		n_rd				=> n_rd,									-- in
		n_rfsh			=> n_rfsh,								-- in
		n_romcs			=> n_romcs,								-- out
		n_ramUDGcs		=> n_ramUDGcs,							-- out
		n_ramcs			=> n_ramcs								-- out
	);

	--------------
	-- CPU Z80
	--------------
	c_Z80: entity work.T80s
	generic map (
		Mode => 0
	)
	port map (
		M1_n		=> n_m1,
		MREQ_n	=> n_mreq,
		IORQ_n	=> n_iorq,
		RD_n		=> n_rd,
		WR_n		=> n_wr,
		RFSH_n	=> n_rfsh,
		HALT_n	=> n_halt,
		WAIT_n	=> n_wait,
		INT_n		=> a_cpu(6),
		NMI_n		=> n_nmi,
		RESET_n	=> reset_cpu_n,
		BUSRQ_n	=> '1',
		BUSAK_n	=> open,
		CLK_n		=> i_phi,
		A			=> a_cpu,
		DI			=> std_logic_vector(d_cpu_o),
		DO			=> d_cpu_i
	);

	-- Glue

	-- Reset CPU sincrono
	process (reset_interno_n, i_phi)
	begin
		if reset_interno_n = '0' then
			reset_cpu_n <= '0';
		elsif rising_edge(i_phi) then
			reset_cpu_n <= '1';
		end if;
	end process;
	
	cpu_a			<= a_cpu;
	ophi 			<= i_phi;
	int_reset_n	<= std_logic(reset_interno_n);


	-- RAM e ROM
   ram_A    	<= a_mem(15 downto 0);
	ram_Din  	<= d_cpu_i;
   d_ram    	<= ram_Dout;
	ram_CE_n 	<= n_ramcs;
	ram_OE_n 	<= n_rd and n_rfsh;--'0' when (n_ramcs = '0' and n_rd = '0') 		else '1';
   ram_WE_n 	<= n_wr;--'0' when (n_ramcs = '0' and n_wr = '0')		else '1';

	rom_A			<= a_mem(12 downto 0);
	d_rom			<= rom_D;
	rom_CE_n		<= n_romcs;

	ramUDG_A		<= a_mem(10 downto 0);
	ramUDG_Din 	<= d_cpu_i;
   d_ramUDG   	<= ramUDG_Dout;
	ramUDG_CE_n	<= n_ramUDGcs;
	ramUDG_OE_n <= '0' when (n_ramUDGcs = '0' and n_rd = '0')	else '1';
   ramUDG_WE_n <= '0' when (n_ramUDGcs = '0' and n_wr = '0')	else '1';

	a_mem(15)				<= '1' when a_cpu(15) = '1' and (a_cpu(14) = '0' or n_m1 = '1')	else '0';	-- Se for fetch no end >= $C000 fazer a_mem = a_mem - $8000
	a_mem(14 downto 9)	<= a_cpu(14 downto 9);
	a_mem(8 downto 0)		<= std_logic_vector(a_mem_l);

	-- PSG
	psg_din	<= d_cpu_i;

	-- Mux entrada dados CPU
	d_mem_i <= std_ulogic_vector(d_ram)			when n_ramcs = '0'		else		-- RAM
	           std_ulogic_vector(d_ramUDG)		when n_ramUDGcs = '0'	else		-- RAM UDG
	           std_ulogic_vector(d_rom)			when n_romcs = '0'		else		-- ROM
				  (others => '1');


  tape_out <= i_n_sync;
--	video <= '0' when i_n_sync='0'
--	    else 'Z' when i_video='0'
--	    else '1';
	
	videoRGB  <= '0'	when s_hblank = '1' or i_n_sync = '0'	else i_video;

	videoCVBS <= '0'	when s_hblank = '1' or i_n_sync = '0'	else 
	             'Z'	when i_video = '0'							else i_video;
	
	ohsync <= s_hsync;
	ovsync <= s_vsync;
	oblank <= s_hblank;

	config_o(3)	<= psg_en;
	config_o(2)	<= usa_uk;
	config_o(1)	<= '1'	when mode_chr13 = '1'	else '0';
	config_o(0)	<= '1'	when mode_v_inv			else '0';
	
end;

-- end ---------------------------------------------------
