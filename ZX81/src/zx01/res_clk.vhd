----------------------------------------------------------
--  res_clk.vhd
--			Reset and Clock
--			===============
--
--  04/28/97	Bodo Wenzel	Got from old top.vhd
--  11/27/97	Bodo Wenzel	Some polish
--  03/20/98	Bodo Wenzel	Reset per software and
--				reading of initial modes
--  01/26/99	Bodo Wenzel	Reduced clock
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- the inputs and outputs --------------------------------

entity res_clk is
	port (
		clock			: in  std_ulogic;				-- Entrada 6.5MHz
		mode_reset	: in  boolean;					-- Soft-reset
		ext_reset_n	: in  std_logic;				-- Entrada reset externo
		n_reset		: out std_ulogic;				-- Saida reset interno
		n_modes		: out std_ulogic;				-- Ler config
		o_phi			: out std_ulogic;				-- Saida CPU 3.250 MHz
		clk_psg		: out std_logic				-- Saida PSG 1.625 MHz
	);
end;

-- the description of the logic --------------------------

architecture beh of res_clk is

  constant RES_READ: natural := 4;
  constant RES_MAX:  natural := 8-1;

  signal timer			: natural range 0 to RES_MAX := 0;	-- The initialization is important!
  signal i_reset		: boolean;
  signal i_phi			: std_logic;
  signal clk_psg_s	: std_logic;

begin

	-- Divide clock por 2 para gerar o clock do CPU
	process (clock)
	begin
		if rising_edge(clock) then
			if i_phi = '0' then
				i_phi <= '1';
			else
				i_phi <= '0';
			end if;
		end if;
	end process;

	-- Divide clock por 2 para gerar o clock do PSG
	process (i_phi)
	begin
		if rising_edge(i_phi) then
			if clk_psg_s = '0' then
				clk_psg_s <= '1';
			else
				clk_psg_s <= '0';
			end if;
		end if;
	end process;

	-- Contador do power-on
	process (i_phi)
	begin
		if rising_edge(i_phi) then
			if ext_reset_n = '0' then
				timer	<= 0;
			elsif mode_reset then
				timer <= RES_READ;
			elsif i_reset then
				timer <= timer+1;
			end if;
		end if;
	end process;


	n_modes	<= '0'						when (timer < RES_READ)	else '1';
	i_reset	<= (timer /= RES_MAX);
	n_reset	<= '0'						when i_reset				else '1';
	o_phi		<= std_ulogic(i_phi);
	clk_psg	<= clk_psg_s;

end;

-- end ---------------------------------------------------
