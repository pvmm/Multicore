----------------------------------------------------------
--  io81.vhd
--			ZX81 Input and Output
--			=====================
--
--  04/28/97	Bodo Wenzel	Got from old top.vhd
--  11/14/97	Bodo Wenzel	Some polish
--  11/25/97	Bodo Wenzel	Correcting errors
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- the inputs and outputs --------------------------------

entity io81 is
	port (
		n_reset			: in  std_ulogic;
		addr				: in  std_ulogic_vector(7 downto 0);
		n_iorq			: in  std_ulogic;
		n_wr				: in  std_ulogic;
		n_rd				: in  std_ulogic;
		vsync				: out boolean;
		nmi_enable		: out boolean;
		kbd_col			: in  std_ulogic_vector(4 downto 0);
		usa_uk			: in  std_ulogic;
		tape_in			: in  std_ulogic;
		d_kbd				: out std_ulogic_vector(7 downto 0);
		d_kbd_enable	: out boolean;
		psg_en			: in  std_logic;
		psg_addr			: out std_logic;
		psg_we			: out std_logic
	);
end;

-- the description of the logic --------------------------

architecture beh of io81 is

	signal iowr				: std_ulogic;
	signal iord				: std_ulogic;
	signal i_nmi_enable	: boolean;

begin

	iowr <= not n_iorq and not n_wr;							-- 1 = Escrita em alguma porta I/O
	iord <= not n_iorq and not n_rd;							-- 1 = Leitura em alguma porta I/O

	process (n_reset, iowr)
	begin
		if n_reset = '0' then
			i_nmi_enable <= FALSE;
		elsif rising_edge(iowr) then
			if addr(0) = '0' then								-- Escrita porta I/O par [254]:
				i_nmi_enable <= TRUE;							--   habilita geracao NMI 
			elsif addr(1) = '0' then							-- Escrita porta I/O impar a cada 4 enderecos (1, 5, 9, 13... [253] )
				i_nmi_enable <= FALSE;							--  desabilita geracao NMI
			end if;
		end if;
	end process;

	process (iowr, iord)
	begin
		if iowr = '1' then
			vsync <= FALSE;										-- Escrita em qualquer porta [255], desativa V-Sync
		elsif rising_edge(iord) then
			if addr(0) = '0' and not i_nmi_enable then	-- Leitura porta I/O par [254] quando geracao NMI desativado => ativa V-Sync
				vsync <= TRUE;
			end if;
		end if;
	end process;

	nmi_enable		<= i_nmi_enable;
	d_kbd				<= tape_in & usa_uk & '0' & kbd_col;
	d_kbd_enable	<= n_iorq = '0' and n_rd = '0' and addr(0) = '0';		-- Leitura porta I/O par

	psg_addr			<= '1' when psg_en = '1' and iowr = '1' and addr(7 downto 5) = "110" and addr(3 downto 0) = X"F"	else '0';		-- Porta 0xCF ou 0xDF
	psg_we			<= '1' when psg_en = '1' and iowr = '1' and addr = X"0F"															else '0';		-- Porta 0x0F

end;

-- end ---------------------------------------------------
