----------------------------------------------------------
--  busses.vhd
--			Switching the busses
--			====================
--
--  04/29/97	Bodo Wenzel	Got from old top.vhd
--  11/17/97	Bodo Wenzel	Cut down to ordinary ZX81
--  12/02/97	Bodo Wenzel	ROM select
--  03/23/98	Bodo Wenzel	Paging of memory
--  01/28/99	Bodo Wenzel	New banking
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- the inputs and outputs --------------------------------

entity busses is
	port (
		a_cpu				: in  std_ulogic_vector(15 downto 0);
		video_addr		: in  std_ulogic_vector(8 downto 0);
		video_mem		: in  boolean;
		a_mem_l			: out std_ulogic_vector(8 downto 0);
		fake_cpu			: in  boolean;
		d_kbd				: in  std_ulogic_vector(7 downto 0);
		d_kbd_enable	: in  boolean;
		d_mem_i			: in  std_ulogic_vector(7 downto 0);
		d_cpu_o			: out std_ulogic_vector(7 downto 0);
		n_m1				: in  std_ulogic;
		n_mreq			: in  std_ulogic;
		n_iorq			: in  std_ulogic;
		n_wr				: in  std_ulogic;
		n_rd				: in  std_ulogic;
		n_rfsh			: in  std_ulogic;
		n_romcs			: out std_ulogic;
		n_ramUDGcs		: out std_ulogic;
		n_ramcs			: out std_ulogic
	);
end;

-- the description of the logic --------------------------

architecture beh of busses is
begin

	process (a_cpu, n_mreq, n_m1, n_rd, n_rfsh)
	begin
		n_romcs		<= '1';
		n_ramUDGcs	<= '1';
		n_ramcs		<= '1';

		if n_mreq = '0' then
			case a_cpu(15 downto 13) is
				when "000" =>									-- 0000-1FFF (somente leitura) [ROM]
					if n_rd = '0' or n_rfsh = '0' then	-- Habilita ROM no Refresh para circuito de video
						n_romcs <= '0';
					end if;
				when "001" =>									-- 2000-3FFF (R/W) [RAM UDG]
					n_ramUDGcs	<= '0';
				when "010" =>									-- 4000-5FFF (R/W) [RAM]
					n_ramcs <= '0';
				when "011" =>									-- 6000-7FFF (R/W) [RAM]
					n_ramcs <= '0';
				when "100" =>									-- 8000-9FFF (R/W) [RAM]
					n_ramcs <= '0';
				when "101" =>									-- A000-BFFF (R/W) [RAM]
					n_ramcs <= '0';
				when "110" =>									-- C000-DFFF (R/W) [RAM]
					n_ramcs <= '0';
				when "111" =>									-- E000-FFFF (R/W) [RAM]
					n_ramcs <= '0';
				when others =>
					null;
			end case;
		end if;
	end process;

	a_mem_l <= video_addr		when video_mem 						else a_cpu(8 downto 0);

	d_cpu_o <= (others => '0')	when fake_cpu							else		-- NOPs na geracao do video
	           d_kbd          	when d_kbd_enable						else		-- Leitura I/O porta par
	           (others => '1')	when n_m1 = '0' and n_iorq = '0'	else		-- Tudo 1 no reconhecimento de IRQs
	           d_mem_i;																	-- Memoria

end;

-- end ---------------------------------------------------
