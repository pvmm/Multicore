-- Abstracao do audio para chip WM8731

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Audio_WM8731 is
	port (
		reset_n		: in    std_logic;							-- Reset geral
		clock			: in    std_logic;							-- Clock 24 MHz
		K7feedback	: in    std_logic;
		psg			: in    std_logic_vector(7 downto 0);	-- Entrada 8 bits mono para o PSG
		tapeout		: out   std_logic;							-- Saida 1 bit K7

		i2s_xck		: out   std_logic;							-- Ligar nos pinos do TOP
		i2s_bclk		: out   std_logic;
		i2s_adclrck	: out   std_logic;
		i2s_adcdat	: in    std_logic;
		i2s_daclrck	: out   std_logic;
		i2s_dacdat	: out   std_logic;
		
		i2c_sda		: inout std_logic;							-- Ligar no pino I2C SDA
		i2c_scl		: inout std_logic								-- Ligar no pino I2C SCL
	);
end entity;

architecture Behavior of Audio_WM8731 is

	signal pcm_lrclk			: std_logic;
	signal pcm_outl			: std_logic_vector(15 downto 0);
	signal pcm_outr			: std_logic_vector(15 downto 0);
	signal pcm_inl				: std_logic_vector(15 downto 0);

	signal psg_s				: std_logic_vector(15 downto 0);
	signal tape_s				: std_logic_vector(15 downto 0);
	signal tape					: std_logic;

begin

	i2s: entity work.i2s_intf
	generic map (
		mclk_rate	=> 12000000,		-- 24 MHz / 2
		sample_rate	=> 48000,
		preamble		=>  1, -- I2S
		word_length	=> 16
	)
	port map (
		-- 2x MCLK in (e.g. 24 MHz for WM8731 USB mode)
		CLK				=> clock,
		nRESET			=> reset_n,
		-- Parallel IO
		PCM_INL			=> pcm_inl,
		PCM_INR			=> open,
		PCM_OUTL			=> pcm_outl,
		PCM_OUTR			=> pcm_outr,
		-- Codec interface (right justified mode)
		-- MCLK is generated at half of the CLK input
		I2S_MCLK			=> i2s_xck,
		-- LRCLK is equal to the sample rate and is synchronous to
		-- MCLK.  It must be related to MCLK by the oversampling ratio
		-- given in the codec datasheet.
		I2S_LRCLK		=> pcm_lrclk,
		-- Data is shifted out on the falling edge of BCLK, sampled
		-- on the rising edge.  The bit rate is determined such that
		-- it is fast enough to fit preamble + word_length bits into
		-- each LRCLK half cycle.  The last cycle of each word may be 
		-- stretched to fit to LRCLK.  This is OK at least for the 
		-- WM8731 codec.
		-- The first falling edge of each timeslot is always synchronised
		-- with the LRCLK edge.
		I2S_BCLK			=> i2s_bclk,
		-- Output bitstream
		I2S_DOUT			=> i2s_dacdat,
		-- Input bitstream
		I2S_DIN			=> i2s_adcdat
	);
	i2s_adclrck <= pcm_lrclk;
	i2s_daclrck <= pcm_lrclk;
	 

	i2c: entity work.i2c_loader 
	generic map (
		device_address	=> 16#1a#,		-- Address of slave to be loaded
		num_retries		=> 1,				-- Number of retries to allow before stopping
		-- Length of clock divider in bits.  Resulting bus frequency is
		-- CLK/2^(log2_divider + 2)
		log2_divider	=> 7
	)
	port map (
		CLK				=> clock,
		nRESET			=> reset_n,
		I2C_SCL			=> i2c_scl,
		I2C_SDA			=> i2c_sda,
		IS_DONE			=> open,
		IS_ERROR			=> open
	);

	psg_s 	<= "0" & psg & "0000000";
	tape_s	<= (11 => tape, others => '0') when K7feedback = '1'	else (others => '0');

	pcm_outl <= std_logic_vector(unsigned(tape_s) + unsigned(psg_s));
	pcm_outr <= std_logic_vector(unsigned(tape_s) + unsigned(psg_s));

	tapeout	<= tape;

	-- Hysteresis for EAR input (should help reliability)
	process (reset_n, clock)
		variable in_val : integer;
	begin
		if reset_n = '0' then
			tape	<= '0';
		elsif rising_edge(clock) then
			in_val := to_integer(signed(pcm_inl));
			if in_val < 15 then
				tape <= '0';
			elsif in_val > 45 then
				tape <= '1';
			end if;
		end if;
	end process;

end architecture;