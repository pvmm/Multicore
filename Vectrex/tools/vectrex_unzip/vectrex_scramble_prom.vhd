library ieee;
use ieee.std_logic_1164.all,ieee.numeric_std.all;

entity vectrex_scramble_prom is
port (
	clk  : in  std_logic;
	addr : in  std_logic_vector(11 downto 0);
	data : out std_logic_vector(7 downto 0)
);
end entity;

architecture prom of vectrex_scramble_prom is
	type rom is array(0 to  4095) of std_logic_vector(7 downto 0);
	signal rom_data: rom := (
		X"67",X"20",X"47",X"43",X"45",X"20",X"31",X"39",X"38",X"32",X"80",X"FD",X"D3",X"F8",X"50",X"20",
		X"C8",X"53",X"43",X"52",X"41",X"4D",X"42",X"4C",X"45",X"80",X"00",X"BD",X"F1",X"8B",X"BD",X"F1",
		X"AF",X"CC",X"02",X"03",X"BD",X"F7",X"A9",X"0A",X"79",X"0A",X"7A",X"0A",X"7A",X"86",X"01",X"0F",
		X"21",X"0F",X"22",X"20",X"41",X"8E",X"C8",X"A6",X"C6",X"35",X"BD",X"F5",X"50",X"8E",X"C9",X"4D",
		X"6F",X"80",X"EC",X"81",X"8C",X"C9",X"65",X"26",X"F7",X"CC",X"01",X"DF",X"DD",X"C9",X"86",X"10",
		X"97",X"FD",X"CC",X"28",X"A0",X"DD",X"C4",X"8E",X"C8",X"80",X"CC",X"B6",X"00",X"ED",X"81",X"8C",
		X"C8",X"A4",X"26",X"F9",X"96",X"A4",X"48",X"48",X"48",X"48",X"97",X"A3",X"BD",X"F5",X"17",X"84",
		X"06",X"8B",X"0E",X"97",X"C3",X"39",X"86",X"05",X"97",X"FE",X"0D",X"79",X"26",X"02",X"86",X"FF",
		X"97",X"FF",X"0F",X"E5",X"8E",X"C9",X"3A",X"BD",X"F8",X"4F",X"30",X"14",X"BD",X"F8",X"4F",X"30",
		X"1C",X"CC",X"F8",X"30",X"ED",X"84",X"ED",X"0C",X"86",X"7F",X"ED",X"0E",X"C6",X"B0",X"ED",X"02",
		X"6F",X"1F",X"6F",X"0B",X"CC",X"0F",X"01",X"DD",X"F9",X"86",X"02",X"97",X"F4",X"97",X"F5",X"D7",
		X"F3",X"BD",X"0E",X"C7",X"0F",X"F6",X"D6",X"F3",X"5C",X"C4",X"01",X"0D",X"79",X"26",X"01",X"5F",
		X"D7",X"F3",X"4F",X"1F",X"01",X"E6",X"89",X"C8",X"F4",X"D7",X"A4",X"5A",X"5A",X"1F",X"02",X"EC",
		X"A9",X"0A",X"3A",X"DD",X"FB",X"EC",X"A9",X"0A",X"48",X"DD",X"EF",X"0F",X"C7",X"0F",X"E4",X"6A",
		X"89",X"C8",X"FE",X"2A",X"0E",X"DC",X"FE",X"84",X"80",X"C4",X"80",X"10",X"83",X"80",X"80",X"26",
		X"C0",X"20",X"83",X"BD",X"00",X"35",X"0C",X"6A",X"0D",X"F6",X"27",X"0E",X"BD",X"F6",X"87",X"0D",
		X"56",X"26",X"0A",X"0F",X"F6",X"BD",X"0E",X"C7",X"20",X"03",X"BD",X"0E",X"E2",X"BD",X"02",X"87",
		X"0D",X"DB",X"2B",X"0E",X"0D",X"E5",X"2B",X"0A",X"D6",X"11",X"C4",X"F0",X"C1",X"F0",X"26",X"02",
		X"0A",X"E5",X"DC",X"1B",X"1E",X"89",X"DD",X"EA",X"0D",X"DB",X"2A",X"08",X"96",X"C7",X"80",X"02",
		X"84",X"0E",X"97",X"C7",X"D6",X"F3",X"4F",X"1F",X"02",X"96",X"A3",X"44",X"44",X"44",X"44",X"84",
		X"FE",X"97",X"A4",X"A7",X"A9",X"C8",X"F4",X"7E",X"03",X"04",X"DC",X"C4",X"9E",X"C9",X"27",X"06",
		X"0D",X"EA",X"27",X"0C",X"2A",X"04",X"4A",X"4A",X"20",X"06",X"81",X"5E",X"2C",X"02",X"4C",X"4C",
		X"0D",X"EB",X"27",X"0F",X"2A",X"08",X"C1",X"A0",X"2D",X"09",X"5A",X"5A",X"20",X"05",X"5D",X"2A",
		X"02",X"5C",X"5C",X"DD",X"C4",X"1F",X"01",X"86",X"0D",X"BD",X"02",X"6F",X"10",X"8E",X"C8",X"CB",
		X"EC",X"A1",X"C1",X"70",X"2E",X"1D",X"CB",X"04",X"ED",X"3E",X"1F",X"01",X"86",X"FF",X"97",X"A5",
		X"BD",X"08",X"23",X"24",X"09",X"96",X"A5",X"2B",X"0A",X"BD",X"09",X"5A",X"20",X"05",X"BD",X"02",
		X"F9",X"20",X"05",X"CC",X"80",X"80",X"ED",X"3E",X"10",X"8C",X"C8",X"D3",X"26",X"D2",X"D6",X"11",
		X"C4",X"0A",X"27",X"1C",X"8E",X"C8",X"CC",X"E6",X"81",X"C1",X"80",X"26",X"0E",X"DC",X"C4",X"CB",
		X"0E",X"ED",X"1D",X"96",X"67",X"8A",X"20",X"97",X"67",X"20",X"05",X"8C",X"C8",X"D4",X"26",X"E7",
		X"10",X"8E",X"C8",X"D3",X"EC",X"A1",X"C1",X"90",X"2D",X"24",X"80",X"02",X"6D",X"22",X"27",X"09",
		X"6A",X"22",X"E6",X"22",X"4C",X"54",X"54",X"EB",X"3F",X"ED",X"3E",X"1F",X"01",X"0F",X"A5",X"BD",
		X"08",X"23",X"25",X"07",X"86",X"0B",X"BD",X"02",X"6F",X"20",X"08",X"BD",X"09",X"5A",X"CC",X"80",
		X"80",X"ED",X"3E",X"10",X"8C",X"C8",X"D7",X"26",X"CB",X"D6",X"11",X"C4",X"05",X"27",X"20",X"8E",
		X"C8",X"D3",X"EC",X"81",X"81",X"80",X"26",X"12",X"DC",X"C4",X"CB",X"08",X"ED",X"1E",X"86",X"08",
		X"A7",X"02",X"96",X"67",X"8A",X"08",X"97",X"67",X"20",X"05",X"8C",X"C8",X"D7",X"26",X"E3",X"CE",
		X"08",X"1D",X"DC",X"C4",X"0D",X"7A",X"2B",X"02",X"AB",X"43",X"EB",X"C4",X"1F",X"01",X"86",X"FF",
		X"97",X"A5",X"BD",X"08",X"23",X"24",X"0B",X"96",X"A5",X"2B",X"03",X"BD",X"09",X"5A",X"BD",X"08",
		X"0C",X"39",X"33",X"41",X"11",X"83",X"08",X"20",X"26",X"D8",X"39",X"8E",X"00",X"10",X"5F",X"AD",
		X"9F",X"C8",X"EC",X"39",X"97",X"C6",X"D6",X"C7",X"DA",X"C8",X"1F",X"01",X"8D",X"54",X"39",X"8D",
		X"51",X"34",X"36",X"10",X"8E",X"0D",X"6B",X"48",X"31",X"A6",X"AE",X"A4",X"8D",X"73",X"C6",X"1F",
		X"D7",X"04",X"BD",X"F4",X"10",X"20",X"5C",X"BD",X"F1",X"92",X"8E",X"C8",X"00",X"CE",X"C8",X"3F",
		X"86",X"0D",X"E6",X"C0",X"BD",X"F2",X"59",X"4A",X"2A",X"F8",X"BD",X"F1",X"F8",X"BD",X"F1",X"BA",
		X"BD",X"F2",X"A5",X"BD",X"F1",X"AF",X"39",X"34",X"76",X"8E",X"C9",X"2E",X"0D",X"F3",X"27",X"02",
		X"30",X"0C",X"BD",X"F8",X"7C",X"35",X"F6",X"34",X"36",X"8D",X"36",X"8D",X"2B",X"CC",X"F3",X"73",
		X"20",X"18",X"34",X"36",X"8D",X"2B",X"8D",X"20",X"20",X"04",X"34",X"36",X"8D",X"23",X"CC",X"F3",
		X"12",X"20",X"07",X"34",X"36",X"8D",X"1A",X"CC",X"F3",X"DF",X"FD",X"C8",X"E8",X"EC",X"62",X"AD",
		X"9F",X"C8",X"E8",X"BD",X"F1",X"AF",X"35",X"B6",X"BD",X"F3",X"54",X"86",X"CE",X"B7",X"D0",X"0C",
		X"39",X"BD",X"F1",X"AA",X"86",X"7F",X"97",X"04",X"39",X"34",X"36",X"8D",X"F4",X"8D",X"E9",X"CC",
		X"F2",X"C3",X"20",X"D6",X"CC",X"02",X"D3",X"DD",X"EC",X"10",X"8E",X"C8",X"80",X"EC",X"A1",X"84",
		X"F0",X"C4",X"0F",X"D7",X"B1",X"BD",X"02",X"64",X"9F",X"DE",X"10",X"8C",X"C8",X"82",X"26",X"06",
		X"8E",X"00",X"F0",X"BD",X"02",X"CA",X"D6",X"B1",X"C1",X"04",X"27",X"14",X"A6",X"3F",X"2B",X"0B",
		X"C6",X"08",X"A6",X"A4",X"84",X"F0",X"BD",X"09",X"B8",X"20",X"05",X"BD",X"02",X"5B",X"20",X"F2",
		X"96",X"B1",X"27",X"1E",X"81",X"06",X"2A",X"1A",X"6D",X"3F",X"2A",X"13",X"9E",X"DE",X"BD",X"02",
		X"C2",X"10",X"8C",X"C8",X"82",X"27",X"06",X"8E",X"00",X"10",X"BD",X"02",X"CA",X"96",X"B1",X"BD",
		X"02",X"71",X"10",X"8C",X"C8",X"82",X"27",X"A5",X"96",X"C8",X"8B",X"10",X"97",X"C8",X"81",X"80",
		X"26",X"9B",X"96",X"DB",X"2A",X"05",X"BD",X"01",X"4A",X"20",X"48",X"81",X"3E",X"2D",X"04",X"C6",
		X"40",X"D7",X"67",X"44",X"25",X"02",X"0C",X"C5",X"CE",X"00",X"10",X"9E",X"C4",X"BD",X"02",X"C2",
		X"86",X"3F",X"90",X"DB",X"48",X"B7",X"D0",X"04",X"BD",X"F1",X"AA",X"B6",X"C8",X"E7",X"31",X"C6",
		X"EC",X"A9",X"0D",X"B5",X"BD",X"F3",X"12",X"BD",X"F1",X"AF",X"1F",X"30",X"1E",X"89",X"9B",X"E6",
		X"81",X"15",X"2B",X"04",X"80",X"05",X"20",X"F8",X"BD",X"02",X"71",X"33",X"41",X"11",X"83",X"00",
		X"15",X"26",X"C8",X"96",X"A4",X"85",X"04",X"26",X"3E",X"96",X"C7",X"81",X"08",X"26",X"38",X"10",
		X"8E",X"C8",X"BF",X"EC",X"A1",X"10",X"83",X"80",X"80",X"27",X"08",X"10",X"8C",X"C8",X"C3",X"26",
		X"F2",X"20",X"24",X"D6",X"C3",X"8E",X"C8",X"82",X"3A",X"EC",X"80",X"C4",X"0F",X"C1",X"02",X"26",
		X"16",X"E6",X"84",X"CB",X"04",X"E7",X"84",X"84",X"F0",X"8B",X"08",X"D6",X"C3",X"58",X"58",X"58",
		X"CB",X"90",X"ED",X"A3",X"BD",X"00",X"6C",X"10",X"8E",X"C8",X"BF",X"EC",X"A1",X"10",X"83",X"80",
		X"80",X"27",X"2D",X"8B",X"02",X"81",X"70",X"27",X"22",X"0D",X"DB",X"2A",X"13",X"C0",X"02",X"0D",
		X"7A",X"2B",X"09",X"85",X"03",X"26",X"05",X"D1",X"C5",X"2D",X"01",X"5A",X"C1",X"88",X"2D",X"0B",
		X"ED",X"3E",X"1F",X"01",X"86",X"06",X"BD",X"02",X"6F",X"20",X"05",X"CC",X"80",X"80",X"ED",X"3E",
		X"10",X"8C",X"C8",X"C3",X"26",X"C5",X"0D",X"DB",X"2A",X"1B",X"CE",X"C8",X"BF",X"CC",X"00",X"80",
		X"DD",X"F7",X"EC",X"C1",X"BE",X"0D",X"A7",X"BD",X"04",X"DB",X"1F",X"12",X"BD",X"07",X"CA",X"11",
		X"83",X"C8",X"C3",X"26",X"ED",X"96",X"A4",X"81",X"04",X"10",X"26",X"00",X"E3",X"10",X"8E",X"C8",
		X"A6",X"4F",X"E6",X"28",X"1F",X"01",X"EC",X"A1",X"10",X"83",X"80",X"80",X"27",X"37",X"AB",X"89",
		X"0D",X"7D",X"EB",X"89",X"04",X"9F",X"ED",X"3E",X"1F",X"01",X"86",X"0F",X"BD",X"02",X"6F",X"A6",
		X"3E",X"E6",X"26",X"84",X"FE",X"2B",X"0C",X"81",X"28",X"25",X"0E",X"CA",X"02",X"20",X"0A",X"00",
		X"FC",X"00",X"FC",X"81",X"E8",X"22",X"02",X"C4",X"FD",X"84",X"20",X"27",X"04",X"CA",X"01",X"20",
		X"02",X"C4",X"FE",X"E7",X"26",X"10",X"8C",X"C8",X"AE",X"26",X"B6",X"0D",X"DB",X"2A",X"59",X"CE",
		X"C8",X"A6",X"CC",X"01",X"00",X"DD",X"F7",X"EC",X"C1",X"BE",X"0D",X"A9",X"8D",X"0D",X"1F",X"12",
		X"BD",X"07",X"CA",X"11",X"83",X"C8",X"AE",X"26",X"EE",X"20",X"3D",X"34",X"36",X"10",X"8E",X"C8",
		X"CB",X"AE",X"A1",X"8C",X"80",X"80",X"27",X"17",X"10",X"8C",X"C8",X"D3",X"25",X"04",X"C1",X"20",
		X"2E",X"0D",X"34",X"26",X"1F",X"02",X"EC",X"66",X"BD",X"F8",X"FF",X"35",X"26",X"25",X"08",X"10",
		X"8C",X"C8",X"D7",X"26",X"DC",X"35",X"B6",X"BD",X"09",X"5A",X"CC",X"80",X"80",X"ED",X"A3",X"ED",
		X"5E",X"DC",X"F7",X"BD",X"02",X"A7",X"35",X"B6",X"D6",X"A7",X"C4",X"FC",X"C1",X"80",X"26",X"05",
		X"CC",X"80",X"80",X"DD",X"A6",X"0A",X"FD",X"26",X"27",X"86",X"20",X"97",X"FD",X"10",X"8E",X"C8",
		X"A6",X"A6",X"2A",X"A7",X"28",X"EC",X"22",X"ED",X"A1",X"10",X"8C",X"C8",X"AC",X"26",X"F2",X"BD",
		X"F5",X"17",X"84",X"0F",X"1F",X"89",X"CB",X"70",X"8B",X"10",X"DD",X"AC",X"86",X"01",X"97",X"B4",
		X"96",X"A4",X"81",X"06",X"26",X"59",X"CE",X"C8",X"B7",X"EC",X"C1",X"C4",X"F8",X"C1",X"80",X"26",
		X"07",X"CC",X"80",X"80",X"ED",X"5E",X"20",X"1A",X"EC",X"5E",X"C0",X"08",X"D0",X"7A",X"ED",X"5E",
		X"0D",X"DB",X"2A",X"07",X"10",X"BE",X"0D",X"A9",X"BD",X"07",X"CA",X"AE",X"5E",X"86",X"0E",X"BD",
		X"02",X"6F",X"11",X"83",X"C8",X"BF",X"26",X"D1",X"D6",X"BE",X"C1",X"40",X"2C",X"21",X"10",X"8E",
		X"C8",X"B9",X"EC",X"A4",X"ED",X"A3",X"31",X"24",X"10",X"8C",X"C8",X"C3",X"26",X"F4",X"BD",X"F5",
		X"17",X"84",X"F8",X"81",X"60",X"2E",X"F7",X"81",X"D8",X"2D",X"F3",X"C6",X"7F",X"DD",X"BD",X"BD",
		X"09",X"86",X"96",X"80",X"48",X"48",X"48",X"48",X"BD",X"02",X"64",X"8E",X"00",X"F0",X"BD",X"02",
		X"CA",X"10",X"8E",X"C8",X"82",X"A6",X"A0",X"48",X"48",X"48",X"48",X"97",X"B1",X"91",X"C6",X"26",
		X"09",X"81",X"60",X"26",X"05",X"CC",X"02",X"CA",X"20",X"03",X"CC",X"02",X"D3",X"DD",X"EC",X"96",
		X"B1",X"E6",X"A0",X"2A",X"07",X"BD",X"02",X"5B",X"96",X"B1",X"20",X"02",X"C6",X"08",X"BD",X"09",
		X"B8",X"96",X"B1",X"97",X"C6",X"10",X"8C",X"C8",X"A4",X"26",X"CA",X"DC",X"D3",X"10",X"83",X"80",
		X"80",X"26",X"0E",X"DC",X"D5",X"10",X"83",X"80",X"80",X"26",X"06",X"96",X"67",X"8A",X"10",X"97",
		X"67",X"4F",X"D6",X"7A",X"CB",X"04",X"DD",X"DC",X"DC",X"C9",X"0D",X"DB",X"2A",X"0D",X"0D",X"C7",
		X"26",X"09",X"93",X"DC",X"2A",X"03",X"CC",X"00",X"00",X"DD",X"C9",X"10",X"83",X"00",X"77",X"24",
		X"06",X"96",X"6A",X"84",X"08",X"27",X"20",X"8E",X"98",X"B0",X"BD",X"02",X"C2",X"96",X"C9",X"27",
		X"06",X"8E",X"00",X"50",X"BD",X"02",X"D3",X"D6",X"CA",X"54",X"54",X"D7",X"EE",X"54",X"54",X"DB",
		X"EE",X"4F",X"1F",X"01",X"BD",X"02",X"D3",X"96",X"67",X"9E",X"C9",X"8C",X"00",X"77",X"22",X"04",
		X"8A",X"02",X"20",X"02",X"8A",X"01",X"97",X"67",X"8E",X"C9",X"29",X"CE",X"C8",X"FE",X"6D",X"84",
		X"26",X"19",X"A6",X"06",X"81",X"31",X"26",X"13",X"6C",X"84",X"6C",X"C4",X"34",X"40",X"BD",X"0E",
		X"C7",X"CE",X"FF",X"8F",X"BD",X"F6",X"8D",X"0C",X"F6",X"35",X"40",X"30",X"0C",X"33",X"41",X"11",
		X"83",X"C9",X"00",X"26",X"D9",X"CC",X"FC",X"38",X"DD",X"2A",X"0D",X"F3",X"27",X"07",X"8E",X"88",
		X"44",X"D6",X"FF",X"20",X"05",X"8E",X"88",X"A0",X"D6",X"FE",X"2B",X"12",X"86",X"68",X"34",X"16",
		X"BD",X"02",X"F1",X"BD",X"02",X"E8",X"35",X"16",X"BD",X"F3",X"93",X"BD",X"F1",X"AF",X"0D",X"DB",
		X"10",X"2A",X"00",X"9E",X"96",X"C7",X"10",X"26",X"00",X"98",X"D6",X"F0",X"54",X"25",X"06",X"CC",
		X"00",X"10",X"BD",X"02",X"A7",X"8E",X"C8",X"82",X"EC",X"84",X"ED",X"83",X"30",X"04",X"8C",X"C8",
		X"A4",X"26",X"F5",X"DE",X"FB",X"9E",X"EF",X"30",X"01",X"A6",X"89",X"0B",X"63",X"85",X"10",X"26",
		X"0E",X"33",X"41",X"11",X"83",X"00",X"FC",X"26",X"06",X"CE",X"00",X"ED",X"8E",X"01",X"EB",X"DF",
		X"FB",X"9F",X"EF",X"E6",X"89",X"0B",X"63",X"A6",X"C9",X"0A",X"66",X"DD",X"A2",X"C4",X"E0",X"C1",
		X"A0",X"26",X"11",X"0D",X"7A",X"27",X"02",X"2A",X"0B",X"1F",X"89",X"C4",X"0F",X"C1",X"05",X"24",
		X"03",X"4C",X"97",X"A2",X"D6",X"A3",X"C5",X"08",X"27",X"20",X"C4",X"F7",X"D7",X"A3",X"96",X"E4",
		X"4C",X"84",X"01",X"97",X"E4",X"27",X"13",X"D6",X"A4",X"5A",X"5A",X"4F",X"1F",X"01",X"EC",X"89",
		X"0A",X"56",X"DD",X"FB",X"EC",X"89",X"0A",X"5E",X"DD",X"EF",X"CC",X"01",X"6F",X"10",X"93",X"EF",
		X"26",X"10",X"0D",X"E4",X"27",X"0C",X"0F",X"E4",X"CC",X"00",X"D0",X"DD",X"FB",X"CC",X"01",X"78",
		X"DD",X"EF",X"CE",X"C9",X"2A",X"BD",X"02",X"B7",X"0D",X"79",X"27",X"06",X"CE",X"C9",X"36",X"BD",
		X"02",X"B7",X"0D",X"DB",X"2B",X"41",X"0A",X"DB",X"26",X"3D",X"DC",X"FE",X"4A",X"10",X"2A",X"F9",
		X"30",X"5A",X"10",X"2A",X"F9",X"2B",X"8E",X"C9",X"2E",X"8D",X"10",X"8E",X"C9",X"3A",X"8D",X"0B",
		X"9E",X"F9",X"30",X"1E",X"9F",X"F9",X"2A",X"09",X"7E",X"F0",X"00",X"CE",X"CB",X"EB",X"7E",X"F8",
		X"D8",X"96",X"11",X"84",X"0F",X"10",X"26",X"F8",X"CD",X"0C",X"DB",X"CE",X"07",X"BA",X"0D",X"E5",
		X"2A",X"02",X"33",X"48",X"BD",X"02",X"B7",X"7E",X"00",X"F6",X"FC",X"38",X"28",X"F8",X"45",X"4E",
		X"44",X"80",X"FC",X"38",X"28",X"F8",X"50",X"41",X"4E",X"80",X"34",X"26",X"10",X"8E",X"08",X"1D",
		X"10",X"9F",X"F1",X"DC",X"C4",X"EB",X"A4",X"0D",X"7A",X"2B",X"02",X"AB",X"23",X"1F",X"01",X"EC",
		X"E4",X"C1",X"14",X"2C",X"2E",X"1F",X"02",X"EC",X"62",X"BD",X"F8",X"FF",X"24",X"22",X"D6",X"A4",
		X"C1",X"06",X"27",X"08",X"CC",X"80",X"80",X"ED",X"5E",X"BD",X"09",X"5A",X"86",X"3F",X"97",X"DB",
		X"BD",X"F5",X"17",X"84",X"0F",X"97",X"E7",X"44",X"97",X"E6",X"35",X"A6",X"34",X"26",X"20",X"EC",
		X"10",X"9E",X"F1",X"31",X"21",X"10",X"8C",X"08",X"20",X"26",X"B5",X"35",X"A6",X"0C",X"F8",X"FE",
		X"00",X"04",X"FC",X"34",X"70",X"1F",X"10",X"97",X"B6",X"1F",X"98",X"C4",X"F0",X"D7",X"B5",X"84",
		X"0F",X"90",X"C7",X"2A",X"08",X"C0",X"10",X"96",X"B5",X"80",X"10",X"97",X"B5",X"CB",X"80",X"54",
		X"54",X"54",X"4F",X"1F",X"02",X"DD",X"E0",X"EC",X"A9",X"C8",X"82",X"97",X"D8",X"D7",X"B1",X"84",
		X"F0",X"D6",X"C7",X"DA",X"B5",X"DD",X"DE",X"DD",X"E2",X"A6",X"A9",X"C8",X"84",X"97",X"DA",X"0D",
		X"B1",X"2B",X"1A",X"D6",X"DA",X"C4",X"F0",X"96",X"D8",X"84",X"F0",X"9A",X"C7",X"BD",X"09",X"E6",
		X"D7",X"EE",X"1F",X"10",X"91",X"EE",X"2E",X"0B",X"1A",X"01",X"7E",X"09",X"4C",X"D6",X"D8",X"C4",
		X"F0",X"20",X"ED",X"0D",X"A5",X"27",X"30",X"96",X"D8",X"48",X"48",X"48",X"48",X"97",X"D8",X"D6",
		X"DA",X"58",X"58",X"58",X"58",X"D7",X"DA",X"10",X"83",X"60",X"60",X"27",X"1A",X"0D",X"B1",X"2B",
		X"12",X"9A",X"C7",X"BD",X"09",X"E6",X"D7",X"EE",X"1F",X"10",X"91",X"EE",X"2D",X"09",X"1A",X"01",
		X"7E",X"09",X"4C",X"D6",X"D8",X"20",X"EF",X"96",X"B1",X"84",X"0F",X"97",X"B1",X"27",X"04",X"81",
		X"06",X"2B",X"05",X"1C",X"FE",X"7E",X"09",X"4C",X"D6",X"E2",X"CB",X"10",X"D1",X"B6",X"2D",X"F3",
		X"CE",X"0D",X"95",X"48",X"33",X"C6",X"10",X"AE",X"C4",X"EC",X"22",X"1F",X"23",X"10",X"9E",X"E2",
		X"BD",X"F8",X"E5",X"24",X"67",X"D6",X"B1",X"C1",X"05",X"26",X"2C",X"34",X"36",X"CE",X"FD",X"D3",
		X"BD",X"F6",X"8D",X"86",X"01",X"97",X"F6",X"CE",X"00",X"02",X"DF",X"FB",X"DF",X"EF",X"CE",X"01",
		X"DF",X"DF",X"C9",X"0C",X"7A",X"8E",X"C8",X"B7",X"C6",X"08",X"BD",X"F5",X"50",X"8E",X"C8",X"A6",
		X"C6",X"08",X"BD",X"F5",X"50",X"35",X"36",X"58",X"4F",X"1F",X"02",X"EC",X"A9",X"09",X"4E",X"BD",
		X"02",X"A7",X"96",X"B1",X"81",X"01",X"26",X"0F",X"9E",X"C9",X"C6",X"40",X"3A",X"8C",X"01",X"DF",
		X"2D",X"03",X"8E",X"01",X"DF",X"9F",X"C9",X"81",X"04",X"27",X"0F",X"10",X"9E",X"E0",X"A6",X"A9",
		X"C8",X"83",X"84",X"F0",X"A7",X"A9",X"C8",X"83",X"0F",X"A5",X"1A",X"01",X"35",X"F0",X"00",X"00",
		X"01",X"50",X"00",X"50",X"02",X"00",X"00",X"00",X"08",X"00",X"34",X"26",X"0F",X"B5",X"10",X"8E",
		X"C9",X"4D",X"A6",X"A4",X"27",X"12",X"91",X"B5",X"2A",X"03",X"10",X"9F",X"DE",X"31",X"23",X"10",
		X"8C",X"C9",X"65",X"26",X"ED",X"10",X"9E",X"DE",X"86",X"0F",X"A7",X"A0",X"AF",X"A4",X"96",X"67",
		X"8A",X"04",X"97",X"67",X"35",X"A6",X"34",X"36",X"10",X"8E",X"C9",X"4D",X"A6",X"A4",X"27",X"1E",
		X"84",X"01",X"8B",X"07",X"AE",X"21",X"BD",X"02",X"6F",X"6A",X"A4",X"EC",X"21",X"0D",X"DB",X"2A",
		X"0B",X"C0",X"02",X"C1",X"81",X"2E",X"05",X"6F",X"A4",X"CC",X"80",X"80",X"ED",X"21",X"31",X"23",
		X"10",X"8C",X"C9",X"65",X"26",X"D6",X"35",X"B6",X"34",X"16",X"D6",X"C6",X"47",X"57",X"D7",X"C6",
		X"90",X"C6",X"E6",X"61",X"10",X"83",X"00",X"00",X"27",X"1A",X"1F",X"01",X"84",X"C0",X"27",X"0A",
		X"81",X"C0",X"27",X"06",X"AD",X"9F",X"C8",X"EC",X"20",X"06",X"1F",X"10",X"58",X"48",X"1F",X"01",
		X"AD",X"9F",X"C8",X"EC",X"35",X"96",X"34",X"06",X"86",X"C9",X"1F",X"8B",X"35",X"06",X"DD",X"10",
		X"84",X"0F",X"97",X"16",X"96",X"10",X"84",X"F0",X"97",X"10",X"91",X"11",X"27",X"34",X"8B",X"80",
		X"CB",X"80",X"DD",X"12",X"91",X"13",X"25",X"04",X"1E",X"98",X"DD",X"12",X"D0",X"12",X"54",X"54",
		X"54",X"54",X"D7",X"14",X"1F",X"10",X"C4",X"0F",X"D0",X"16",X"4F",X"C4",X"0F",X"20",X"03",X"9B",
		X"14",X"5A",X"26",X"FB",X"97",X"15",X"D6",X"10",X"D1",X"11",X"2D",X"04",X"D0",X"15",X"20",X"02",
		X"DB",X"15",X"86",X"C8",X"1F",X"8B",X"39",X"7E",X"00",X"F6",X"00",X"00",X"00",X"48",X"00",X"82",
		X"00",X"9C",X"00",X"D1",X"00",X"ED",X"00",X"FC",X"00",X"00",X"00",X"A4",X"00",X"EA",X"01",X"20",
		X"01",X"7B",X"01",X"EB",X"02",X"06",X"00",X"00",X"00",X"65",X"00",X"85",X"00",X"A2",X"00",X"00",
		X"00",X"C1",X"00",X"EE",X"01",X"2A",X"D6",X"E6",X"F6",X"06",X"16",X"26",X"16",X"06",X"16",X"26",
		X"16",X"06",X"F6",X"E6",X"D6",X"C6",X"B6",X"A6",X"B6",X"A6",X"B6",X"A6",X"B6",X"C6",X"D6",X"E6",
		X"F6",X"06",X"16",X"06",X"F6",X"E6",X"C6",X"D6",X"E6",X"F6",X"16",X"26",X"16",X"06",X"F6",X"06",
		X"16",X"F6",X"D6",X"C6",X"B6",X"C6",X"D6",X"C6",X"E6",X"D6",X"C6",X"D6",X"B6",X"A6",X"B6",X"A6",
		X"B6",X"C6",X"B6",X"C6",X"D6",X"E6",X"D6",X"E6",X"C6",X"E6",X"F6",X"06",X"F6",X"E6",X"D6",X"C5",
		X"A5",X"A4",X"A3",X"C4",X"D3",X"D5",X"E3",X"C3",X"D4",X"B4",X"B3",X"A4",X"A3",X"D4",X"B4",X"C5",
		X"B4",X"B3",X"C4",X"C5",X"D4",X"D5",X"C4",X"D5",X"B4",X"C5",X"E6",X"E5",X"D4",X"B3",X"B4",X"B3",
		X"A3",X"A4",X"B4",X"B5",X"C5",X"D5",X"D6",X"D5",X"C5",X"C4",X"B3",X"A3",X"A4",X"A5",X"B6",X"B5",
		X"D4",X"C5",X"B5",X"A5",X"A4",X"B5",X"A5",X"C5",X"C6",X"A6",X"B6",X"C6",X"D6",X"C6",X"A6",X"B6",
		X"A6",X"C6",X"B6",X"C6",X"D6",X"C6",X"B6",X"A6",X"C6",X"D6",X"B6",X"A6",X"B6",X"A6",X"B6",X"D6",
		X"B6",X"A6",X"C6",X"46",X"36",X"46",X"26",X"46",X"56",X"46",X"56",X"46",X"36",X"26",X"16",X"06",
		X"16",X"06",X"26",X"16",X"36",X"26",X"46",X"36",X"56",X"46",X"36",X"46",X"26",X"36",X"16",X"26",
		X"06",X"16",X"06",X"16",X"26",X"36",X"46",X"56",X"46",X"56",X"36",X"46",X"56",X"36",X"56",X"36",
		X"56",X"46",X"56",X"46",X"36",X"26",X"36",X"36",X"34",X"35",X"45",X"35",X"34",X"35",X"45",X"F5",
		X"F0",X"B0",X"BC",X"B4",X"34",X"B4",X"BC",X"B2",X"12",X"14",X"34",X"24",X"23",X"B3",X"BC",X"B4",
		X"34",X"24",X"C6",X"C6",X"F6",X"36",X"F6",X"C6",X"F6",X"C6",X"46",X"46",X"46",X"C6",X"F6",X"C6",
		X"16",X"C6",X"C6",X"20",X"20",X"20",X"20",X"20",X"20",X"20",X"22",X"30",X"32",X"30",X"32",X"30",
		X"32",X"32",X"32",X"30",X"33",X"30",X"30",X"20",X"20",X"20",X"20",X"20",X"20",X"20",X"20",X"20",
		X"30",X"22",X"32",X"34",X"34",X"31",X"31",X"34",X"32",X"32",X"33",X"30",X"20",X"30",X"20",X"30",
		X"32",X"30",X"32",X"30",X"32",X"33",X"31",X"30",X"20",X"22",X"30",X"30",X"22",X"30",X"22",X"34",
		X"32",X"30",X"22",X"32",X"34",X"33",X"31",X"30",X"22",X"32",X"34",X"34",X"32",X"32",X"30",X"22",
		X"32",X"30",X"20",X"20",X"20",X"20",X"20",X"22",X"32",X"30",X"24",X"32",X"30",X"24",X"32",X"30",
		X"20",X"20",X"20",X"22",X"30",X"21",X"30",X"22",X"30",X"20",X"32",X"33",X"34",X"32",X"30",X"20",
		X"20",X"20",X"20",X"22",X"34",X"31",X"31",X"30",X"22",X"30",X"20",X"20",X"20",X"20",X"22",X"30",
		X"30",X"20",X"20",X"20",X"21",X"30",X"23",X"30",X"22",X"30",X"22",X"33",X"30",X"20",X"20",X"20",
		X"24",X"32",X"32",X"33",X"31",X"30",X"20",X"32",X"32",X"32",X"30",X"21",X"32",X"33",X"30",X"21",
		X"30",X"20",X"30",X"20",X"20",X"20",X"20",X"40",X"40",X"41",X"40",X"40",X"40",X"40",X"40",X"40",
		X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",X"40",
		X"40",X"40",X"40",X"40",X"40",X"40",X"42",X"51",X"42",X"41",X"50",X"43",X"53",X"40",X"42",X"40",
		X"42",X"50",X"40",X"40",X"40",X"43",X"42",X"50",X"43",X"50",X"42",X"41",X"40",X"40",X"48",X"40",
		X"40",X"40",X"42",X"51",X"40",X"40",X"42",X"52",X"52",X"54",X"53",X"50",X"40",X"60",X"62",X"70",
		X"60",X"60",X"60",X"60",X"60",X"72",X"71",X"72",X"70",X"60",X"60",X"60",X"61",X"70",X"60",X"60",
		X"70",X"60",X"62",X"70",X"63",X"71",X"70",X"60",X"60",X"60",X"61",X"74",X"70",X"73",X"73",X"74",
		X"71",X"72",X"72",X"70",X"60",X"70",X"63",X"73",X"70",X"71",X"70",X"70",X"60",X"68",X"60",X"70",
		X"61",X"73",X"70",X"80",X"80",X"90",X"90",X"90",X"82",X"82",X"90",X"82",X"82",X"80",X"82",X"80",
		X"82",X"92",X"82",X"91",X"82",X"92",X"82",X"90",X"80",X"90",X"83",X"90",X"90",X"90",X"82",X"80",
		X"82",X"80",X"82",X"80",X"82",X"80",X"90",X"82",X"90",X"90",X"82",X"80",X"82",X"80",X"82",X"80",
		X"82",X"80",X"82",X"90",X"90",X"93",X"91",X"93",X"92",X"92",X"80",X"80",X"93",X"80",X"90",X"80",
		X"90",X"92",X"80",X"90",X"92",X"92",X"82",X"80",X"92",X"90",X"82",X"81",X"80",X"82",X"80",X"82",
		X"80",X"82",X"80",X"82",X"80",X"91",X"90",X"91",X"90",X"91",X"8A",X"80",X"90",X"90",X"A0",X"A1",
		X"B1",X"A0",X"B0",X"B0",X"A1",X"B0",X"A0",X"B0",X"B0",X"A0",X"B1",X"B1",X"B1",X"A1",X"B0",X"B0",
		X"A0",X"B0",X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"B0",X"B1",X"B1",X"B1",X"A1",X"B1",X"B1",X"B0",
		X"B0",X"B0",X"A0",X"B0",X"B0",X"A0",X"B1",X"B1",X"B1",X"B1",X"B0",X"A0",X"B0",X"B0",X"B0",X"B0",
		X"B0",X"A0",X"B0",X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"B0",
		X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"A0",X"B0",X"A0",X"B0",X"B0",
		X"A0",X"B0",X"A0",X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"B0",X"B0",
		X"B0",X"B0",X"B0",X"A0",X"B0",X"B0",X"B0",X"B0",X"B0",X"A0",X"A0",X"B0",X"B0",X"A0",X"C0",X"C0",
		X"C0",X"C0",X"C0",X"D0",X"C0",X"D0",X"D0",X"C0",X"C0",X"C0",X"C0",X"C0",X"C0",X"D0",X"D0",X"C0",
		X"D0",X"D5",X"D0",X"D0",X"D0",X"C0",X"D0",X"C0",X"D0",X"C0",X"D0",X"0D",X"BE",X"0D",X"B5",X"0D",
		X"D1",X"0D",X"E7",X"0D",X"FD",X"0E",X"0B",X"0E",X"1B",X"0E",X"31",X"0E",X"44",X"03",X"03",X"FD",
		X"FD",X"0E",X"57",X"0D",X"BE",X"0E",X"5E",X"0E",X"77",X"0E",X"87",X"0E",X"B0",X"0E",X"9A",X"0E",
		X"C0",X"0E",X"AA",X"0E",X"B7",X"0D",X"BE",X"0D",X"AD",X"0D",X"A5",X"0D",X"A9",X"0E",X"07",X"0D",
		X"A3",X"0D",X"A7",X"08",X"08",X"08",X"08",X"08",X"04",X"06",X"08",X"06",X"07",X"07",X"08",X"07",
		X"05",X"0D",X"A9",X"0D",X"A9",X"00",X"00",X"D0",X"FF",X"18",X"08",X"FF",X"00",X"F0",X"FF",X"20",
		X"00",X"FF",X"00",X"28",X"FF",X"E0",X"00",X"FF",X"00",X"E0",X"00",X"00",X"10",X"FF",X"E8",X"08",
		X"01",X"00",X"00",X"EC",X"FF",X"40",X"F4",X"FF",X"C0",X"F4",X"FF",X"0C",X"F0",X"FF",X"0C",X"1C",
		X"FF",X"F4",X"1C",X"FF",X"F4",X"F0",X"01",X"00",X"00",X"C8",X"FF",X"30",X"00",X"FF",X"00",X"30",
		X"FF",X"D0",X"00",X"FF",X"30",X"D0",X"00",X"00",X"30",X"FF",X"D0",X"D0",X"01",X"FF",X"20",X"08",
		X"FF",X"00",X"30",X"FF",X"E0",X"08",X"01",X"04",X"08",X"04",X"04",X"00",X"20",X"C0",X"FF",X"20",
		X"20",X"FF",X"E0",X"20",X"FF",X"E0",X"E0",X"FF",X"20",X"E0",X"01",X"00",X"E0",X"0C",X"FF",X"40",
		X"F4",X"FF",X"C0",X"F4",X"FF",X"0C",X"28",X"FF",X"0C",X"E4",X"FF",X"F4",X"E4",X"FF",X"F4",X"28",
		X"01",X"00",X"28",X"F4",X"FF",X"B0",X"10",X"00",X"14",X"24",X"FF",X"34",X"B4",X"00",X"04",X"3C",
		X"FF",X"BC",X"C0",X"01",X"00",X"28",X"E8",X"FF",X"B0",X"3C",X"00",X"04",X"D0",X"FF",X"4C",X"10",
		X"00",X"D8",X"D4",X"FF",X"0C",X"50",X"01",X"FF",X"10",X"00",X"FF",X"00",X"FC",X"01",X"FF",X"E8",
		X"F8",X"FF",X"00",X"F4",X"FF",X"24",X"F4",X"FF",X"F4",X"50",X"FF",X"F4",X"B0",X"FF",X"24",X"0C",
		X"FF",X"00",X"0C",X"FF",X"E8",X"08",X"01",X"00",X"00",X"20",X"FF",X"10",X"0C",X"FF",X"F0",X"34",
		X"FF",X"F0",X"CC",X"FF",X"10",X"F4",X"01",X"00",X"F4",X"F0",X"FF",X"14",X"F0",X"FF",X"08",X"20",
		X"FF",X"F8",X"20",X"FF",X"EC",X"F0",X"FF",X"00",X"E0",X"01",X"FF",X"04",X"18",X"FF",X"08",X"E4",
		X"FF",X"10",X"F8",X"FF",X"00",X"F4",X"FF",X"E8",X"14",X"01",X"FF",X"18",X"EC",X"FF",X"F4",X"F8",
		X"FF",X"00",X"F8",X"FF",X"F4",X"20",X"01",X"FF",X"F4",X"E8",X"FF",X"00",X"F8",X"FF",X"0C",X"04",
		X"FF",X"0C",X"FC",X"FF",X"F4",X"20",X"01",X"BD",X"F5",X"33",X"8E",X"C8",X"67",X"C6",X"0C",X"BD",
		X"F5",X"3F",X"86",X"80",X"97",X"77",X"CC",X"0D",X"0D",X"DD",X"42",X"97",X"44",X"C6",X"38",X"D7",
		X"45",X"39",X"BD",X"0E",X"D6",X"96",X"67",X"85",X"10",X"26",X"12",X"85",X"08",X"27",X"05",X"CC",
		X"00",X"20",X"20",X"04",X"DC",X"6D",X"27",X"08",X"C3",X"00",X"02",X"20",X"03",X"CC",X"00",X"00",
		X"DD",X"6D",X"DD",X"49",X"96",X"67",X"85",X"20",X"26",X"06",X"0D",X"71",X"27",X"20",X"20",X"02",
		X"0F",X"71",X"D6",X"71",X"C1",X"10",X"27",X"16",X"C1",X"08",X"2C",X"09",X"CA",X"08",X"C5",X"02",
		X"27",X"01",X"5F",X"C4",X"09",X"58",X"58",X"58",X"58",X"4F",X"0C",X"71",X"20",X"05",X"CC",X"00",
		X"00",X"0F",X"71",X"DD",X"4B",X"96",X"67",X"84",X"44",X"27",X"0A",X"C6",X"FF",X"D7",X"77",X"84",
		X"40",X"97",X"69",X"20",X"04",X"96",X"77",X"2B",X"5C",X"0C",X"77",X"96",X"77",X"D6",X"69",X"26",
		X"17",X"81",X"12",X"22",X"17",X"D6",X"67",X"C5",X"02",X"26",X"4A",X"44",X"44",X"88",X"0F",X"97",
		X"42",X"86",X"1C",X"97",X"45",X"7E",X"0F",X"F7",X"81",X"3F",X"23",X"07",X"C6",X"80",X"D7",X"77",
		X"7E",X"0F",X"F7",X"81",X"10",X"23",X"02",X"86",X"10",X"8B",X"30",X"97",X"78",X"BD",X"F5",X"17",
		X"84",X"7F",X"91",X"78",X"23",X"F7",X"5F",X"1C",X"FE",X"46",X"56",X"46",X"56",X"46",X"56",X"DD",
		X"47",X"C3",X"00",X"05",X"DD",X"49",X"96",X"77",X"44",X"44",X"44",X"88",X"0F",X"97",X"43",X"97",
		X"42",X"0F",X"44",X"20",X"52",X"96",X"67",X"85",X"01",X"26",X"38",X"98",X"68",X"84",X"03",X"27",
		X"09",X"86",X"40",X"97",X"76",X"CC",X"05",X"00",X"20",X"39",X"0A",X"76",X"0A",X"76",X"2B",X"F1",
		X"96",X"76",X"81",X"0A",X"23",X"08",X"C6",X"1A",X"3D",X"C3",X"00",X"94",X"20",X"25",X"CC",X"00",
		X"00",X"20",X"20",X"00",X"2E",X"5C",X"8A",X"B8",X"B8",X"8A",X"5C",X"2E",X"00",X"00",X"00",X"2E",
		X"5C",X"8A",X"B8",X"D6",X"6A",X"C4",X"0F",X"4F",X"1F",X"01",X"E6",X"89",X"0F",X"D3",X"59",X"49",
		X"C3",X"01",X"6C",X"DD",X"74",X"DD",X"47",X"96",X"67",X"84",X"03",X"97",X"68",X"0F",X"67",X"39");
begin
process(clk)
begin
	if rising_edge(clk) then
		data <= rom_data(to_integer(unsigned(addr)));
	end if;
end process;
end architecture;
