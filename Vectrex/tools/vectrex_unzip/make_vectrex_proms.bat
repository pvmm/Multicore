
make_vhdl_prom 	exec_rom.bin vectrex_exec_prom.vhd
make_vhdl_prom 	scramble.bin vectrex_scramble_prom.vhd
make_vhdl_prom 	berzerk.bin vectrex_berzerk_prom.vhd
make_vhdl_prom 	frogger.bin vectrex_frogger_prom.vhd
make_vhdl_prom 	spacewar.bin vectrex_spacewar_prom.vhd
make_vhdl_prom 	polepos.bin vectrex_polepos_prom.vhd
make_vhdl_prom 	ripoff.bin vectrex_ripoff_prom.vhd
make_vhdl_prom 	spike.bin vectrex_spike_prom.vhd
make_vhdl_prom 	startrek.bin vectrex_startrek_prom.vhd
make_vhdl_prom 	vecmania1.bin vectrex_vecmania1_prom.vhd
make_vhdl_prom 	webwars.bin vectrex_webwars_prom.vhd
make_vhdl_prom 	wotr.bin vectrex_wotr_prom.vhd
make_vhdl_prom 	clean_sweep.bin vectrex_clean_sweep_prom.vhd
