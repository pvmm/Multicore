

module mc2_toplevel (

	// Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [3:0]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram2_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram2_data_io	= 8'bzzzzzzzz,
	output wire	sram2_we_n_o		= 1'b1,
	output wire	sram2_oe_n_o		= 1'b1,

	output wire	[18:0]sram3_addr_o	= 18'b0000000000000000000,
	inout wire	[7:0]sram3_data_io	= 8'bzzzzzzzz,
	output wire	sram3_we_n_o		= 1'b1,
	output wire	sram3_oe_n_o		= 1'b1,
		
	// SDRAM	(H57V256)
	output wire	[12:0]sdram_ad_o,
	inout wire	[15:0]sdram_da_io,
	output wire	[1:0]sdram_ba_o,
	output wire	[1:0]sdram_dqm_o,
	output wire	sdram_ras_o,
	output wire	sdram_cas_o,
	output wire	sdram_cke_o,
	output wire	sdram_clk_o,
	output wire	sdram_cs_o,
	output wire	sdram_we_o,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  	= 1'bz,
	inout wire	ps2_mouse_data_io 	= 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output wire	dac_l_o				= 1'b0,
	output wire	dac_r_o				= 1'b0,
	input wire	ear_i,
	output wire	mic_o				= 1'b0,

		// VGA
	output wire	[3:0]vga_r_o,
	output wire	[3:0]vga_g_o,
	output wire	[3:0]vga_b_o,
	output wire	vga_hsync_n_o,
	output wire	vga_vsync_n_o,

		// HDMI
	output wire	[7:0]tmds_o				= 8'b00000000,

		//STM32
	input wire	smt_rx_i,
	output wire	smt_tx_o,
	output wire	smt_rst_o			= 1'b0,
		
	inout wire	smt_b8_io,
	inout wire	smt_b9_io,
	inout wire	smt_b12_io,
	inout wire	smt_b13_io,
	inout wire	smt_b14_io,
	inout wire	smt_b15_io
		
);



////////////////////////////////////////
// internal signals                   //
////////////////////////////////////////

// clock
wire           pll_in_clk;
wire           clk_114;
wire           clk_28;
wire           pll_locked;
wire           clk_7;
wire           c1;
wire           c3;
wire           cck;
wire [ 10-1:0] eclk;
wire           clk_50;
wire				sysclk;
wire				memclk;

// reset
wire           pll_rst;
wire           sdctl_rst;
wire           rst_50;
wire           rst_minimig;

// ctrl
wire           rom_status;
wire           ram_status;
wire           reg_status;

// tg68
wire           tg68_rst;
wire [ 16-1:0] tg68_dat_in;
wire [ 16-1:0] tg68_dat_out;
wire [ 32-1:0] tg68_adr;
wire [  3-1:0] tg68_IPL;
wire           tg68_dtack;
wire           tg68_as;
wire           tg68_uds;
wire           tg68_lds;
wire           tg68_rw;
wire           tg68_ena7RD;
wire           tg68_ena7WR;
wire           tg68_enaWR;
wire [ 16-1:0] tg68_cout;
wire           tg68_cpuena;
wire [  2-1:0] cpu_config;
wire [  6-1:0] memcfg;
wire [ 32-1:0] tg68_cad;
wire [  6-1:0] tg68_cpustate;
wire           tg68_cdma;
wire           tg68_clds;
wire           tg68_cuds;

// minimig
wire [ 16-1:0] ram_data;      // sram data bus
wire [ 16-1:0] ramdata_in;    // sram data bus in
wire [ 22-1:1] ram_address;   // sram address bus
wire           _ram_bhe;      // sram upper byte select
wire           _ram_ble;      // sram lower byte select
wire           _ram_we;       // sram write enable
wire           _ram_oe;       // sram output enable
wire           _15khz;        // scandoubler disable
wire           joy_emu_en;    // joystick emulation enable
wire           sdo;           // SPI data output
wire [ 15-1:0] ldata;         // left DAC data
wire [ 15-1:0] rdata;         // right DAC data
wire [15:0]    audio_left;
wire [15:0]    audio_right;
wire           floppy_fwr;
wire           floppy_frd;
wire           hd_fwr;
wire           hd_frd;

// sdram
wire           reset_out;
wire [  4-1:0] sdram_cs;
wire [  2-1:0] sdram_dqm;
wire [  2-1:0] sdram_ba;

// audio
wire           audio_lr_switch;
wire           audio_lr_mix;

// ctrl
wire [ 16-1:0] SRAM_DAT_W;
wire [ 16-1:0] SRAM_DAT_R;
wire [  8-1:0] FL_DAT_W;
wire [  8-1:0] FL_DAT_R;
wire [  4-1:0] SPI_CS_N;
wire           SPI_DI;
wire           rst_ext;
wire           boot_sel;
wire [  4-1:0] ctrl_cfg;
wire [  4-1:0] ctrl_status;

// indicators
wire [  8-1:0] track;

//audio
wire  audio_LR;


////////////////////////////////////////
// toplevel assignments               //
////////////////////////////////////////

// PS/2 keyboard
wire PS2K_DAT_IN=ps2_data_io;
wire PS2K_DAT_OUT;
assign ps2_data_io = (PS2K_DAT_OUT == 1'b0) ? 1'b0 : 1'bz;
wire PS2K_CLK_IN=ps2_clk_io;
wire PS2K_CLK_OUT;
assign ps2_clk_io = (PS2K_CLK_OUT == 1'b0) ? 1'b0 : 1'bz;

// PS/2 Mouse
wire PS2M_DAT_IN=ps2_mouse_data_io;
wire PS2M_DAT_OUT;
assign ps2_mouse_data_io = (PS2M_DAT_OUT == 1'b0) ? 1'b0 : 1'bz;
wire PS2M_CLK_IN=ps2_mouse_clk_io;
wire PS2M_CLK_OUT;
assign ps2_mouse_clk_io = (PS2M_CLK_OUT == 1'b0) ? 1'b0 : 1'bz;





wire [7:0] vga_red;
wire [7:0] vga_green;
wire [7:0] vga_blue;

assign vga_r_o = {vga_red[7:5], 1'b0};
assign vga_g_o = {vga_green[7:5], 1'b0};
assign vga_b_o = {vga_blue[7:5], 1'b0};
 

//// generated clocks ////

pll mypll
(
	.inclk0(clock_50_i),
	.c0(sysclk),
	.c1(memclk),
	.c2(sdram_clk_o)
);

wire  [15:0]lpf1_wave_L;
wire  [15:0]lpf5_wave_L;
wire  [15:0]lpf1_wave_R;
wire  [15:0]lpf5_wave_R;

INTERPO #(.msbi (15)) u_interpo_L
(
	  .clk21m  (sysclk),
	  .reset   (1'b0),
	  .clkena  (1'b1),
	  .idata   ({ 2'b00,audio_left[15:2]}),
	  .odata   (lpf1_wave_L)
);

lpf2 #(.msbi (15)) u_lpf2_L
(
	  .clk21m  (sysclk),
	  .reset   (1'b0),
	  .clkena  (1'b1),  
	  .idata   (lpf1_wave_L),
	  .odata   (lpf5_wave_L)
);

INTERPO #(.msbi (15)) u_interpo_R
(
	  .clk21m  (sysclk),
	  .reset   (1'b0),
	  .clkena  (1'b1),
	  .idata   ({ 2'b00,audio_right[15:2]}),
	  .odata   (lpf1_wave_R)
);

lpf2 #(.msbi (15)) u_lpf2_R
(
	  .clk21m  (sysclk),
	  .reset   (1'b0),
	  .clkena  (1'b1),  
	  .idata   (lpf1_wave_R),
	  .odata   (lpf5_wave_R)
);
	 

dac #( .msbi_g(15) ) dacL
(
	.clk_i   (sysclk),
	.res_n_i (1'b1),
	.dac_i   (lpf5_wave_L),
	.dac_o   (dac_l_o)
);

dac #( .msbi_g(15) ) dacR
(
	.clk_i   (sysclk),
	.res_n_i (1'b1),
	.dac_i   (lpf5_wave_R),
	.dac_o   (dac_r_o)
);




defparam myvt.rowAddrBits = 13;
defparam myvt.colAddrBits = 9;

Virtual_Toplevel myvt
(
	.reset(btn_n_i[0]),
	.CLK(sysclk),
	.SDR_CLK(memclk),
	
	//.SW(SW),
	
	.DRAM_ADDR(sdram_ad_o),
	.DRAM_DQ(sdram_da_io),
	.DRAM_BA_1(sdram_ba_o[1]),
	.DRAM_BA_0(sdram_ba_o[0]),
	.DRAM_CKE(sdram_cke_o),
	.DRAM_UDQM(sdram_dqm_o[1]),
	.DRAM_LDQM(sdram_dqm_o[0]),
	.DRAM_CS_N(sdram_cs_o),
	.DRAM_WE_N(sdram_we_o),
	.DRAM_CAS_N(sdram_cas_o),
	.DRAM_RAS_N(sdram_ras_o),
	
	.DAC_LDATA(audio_left),
	.DAC_RDATA(audio_right),
	
	.VGA_R(vga_red),
	.VGA_G(vga_green),
	.VGA_B(vga_blue),
	.VGA_VS(vga_vsync_n_o),
	.VGA_HS(vga_hsync_n_o),

	.RS232_RXD(smt_rx_o),
	.RS232_TXD(smt_tx_i),
	
	// PS/2
	.ps2k_clk_in(PS2K_CLK_IN),
	.ps2k_dat_in(PS2K_DAT_IN),
	.ps2k_clk_out(PS2K_CLK_OUT),
	.ps2k_dat_out(PS2K_DAT_OUT),
	
	// Joystick
	.joya({2'b11,joy1_p9_i,joy1_p6_i,joy1_right_i,joy1_left_i,joy1_down_i,joy1_up_i}),
	.joyb({2'b11,joy2_p9_i,joy2_p6_i,joy2_right_i,joy2_left_i,joy2_down_i,joy2_up_i}),

	// SD card
	.spi_cs(sd_cs_n_o),
	.spi_miso(sd_miso_i),
	.spi_mosi(sd_mosi_o),
	.spi_clk(sd_sclk_o)

);

endmodule

