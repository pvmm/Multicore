

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;


entity C16_multicore is
port (
-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture rtl of C16_multicore is

-------------
-- Signals
-------------

	component C16 is
	port
	(
		CLK28 		: 	in 	std_logic;
		RESET			: 	in 	std_logic;
		CSYNC			:	out	std_logic;
		HSYNC			:	out	std_logic;
		VSYNC			:	out	std_logic;
		RED			:	out	std_logic_vector(3 downto 0);
		GREEN			:	out	std_logic_vector(3 downto 0);
		BLUE			:	out	std_logic_vector(3 downto 0);
		RAS			:	out	std_logic;
		CAS			:	out	std_logic;
		RW				:	out	std_logic;
		A				:	out	std_logic_vector(7 downto 0);
		D				:	inout	std_logic_vector(7 downto 0);
		PS2DAT 		: 	in 	std_logic;
		PS2CLK 		: 	in 	std_logic;
		IEC_DATAOUT	:	out	std_logic;
		IEC_DATAIN 	: 	in 	std_logic;
		IEC_CLKOUT	:	out	std_logic;
		IEC_CLKIN 	: 	in 	std_logic;
		IEC_ATNOUT	:	out	std_logic;
		IEC_RESET	:	out	std_logic;
		AUDIO_L		:	out	std_logic;
		AUDIO_R		:	out	std_logic;
		RS232_TX		:	out	std_logic;
		RGBS			:	out	std_logic
	);
	end component;


signal clock_s      	:   std_logic;
signal reset_s      	:   std_logic;

signal vga_r_s       :   std_logic_vector(3 downto 0);
signal vga_g_s       :   std_logic_vector(3 downto 0);
signal vga_b_s       :   std_logic_vector(3 downto 0);
signal vga_hs_s      :   std_logic;
signal vga_vs_s      :   std_logic;

signal c16_a_s      	:   std_logic_vector(7 downto 0);
signal c16_d_s      	:   std_logic_vector(7 downto 0);
signal c16_ras_s     :   std_logic;
signal c16_cas_s     :   std_logic;
signal c16_rw_s      :   std_logic;
signal c16_a_hi_s    :   std_logic_vector(7 downto 0);
signal c16_a_low_s   :   std_logic_vector(7 downto 0);

begin

	C16_inst : C16 
	port map
	(
		CLK28 			=> clock_s,
		RESET				=> reset_s,
		CSYNC				=> open,
		HSYNC				=> vga_hs_s,
		VSYNC				=>	vga_vs_s,	
		RED				=>	vga_r_s,	
		GREEN				=> vga_g_s,
		BLUE				=> vga_b_s,
		RAS				=> c16_ras_s,
		CAS				=> c16_cas_s,
		RW					=> c16_rw_s,
		A					=> c16_a_s,
		D					=> sram_data_io,
		PS2DAT 			=> ps2_data_io,
		PS2CLK 			=> ps2_clk_io,
		IEC_DATAOUT		=> open,
		IEC_DATAIN 		=> '0',
		IEC_CLKOUT		=> open,
		IEC_CLKIN 		=> '0',
		IEC_ATNOUT		=> open,
		IEC_RESET		=> open,
		AUDIO_L			=> dac_l_o,
		AUDIO_R			=> dac_r_o,
		RS232_TX			=> open,
		RGBS				=> open
	);


	pll: entity work.pll1
	port map
   (
			inclk0 => clock_50_i,
			c0 => clock_s --28
	 );
	 
	 
	 
	 -- latch dram address
	process (c16_ras_s)
	begin
		if falling_edge (c16_ras_s) then
			c16_a_low_s <= c16_a_s;
		end if;
	end process;
	
	process (c16_cas_s)
	begin
		if falling_edge (c16_cas_s) then
			c16_a_hi_s <= c16_a_s;
		end if;
	end process;
	 
	 
	 
	 
	 reset_s 		<= not btn_n_i(1);
	 
	 vga_r_o 		<= vga_r_s(3 downto 0) & '0';
	 vga_g_o 		<= vga_g_s(3 downto 0) & '0';
	 vga_b_o 		<= vga_b_s(3 downto 0) & '0';
	 vga_hsync_n_o <= vga_hs_s;
	 vga_vsync_n_o <= vga_vs_s;


	sram_addr_o <= "000" & c16_a_hi_s & c16_a_low_s;
	
	sram_we_n_o <= c16_cas_s or c16_rw_s;
	sram_oe_n_o <= c16_cas_s or (not c16_rw_s);

	
	---------------------------------------------------------------
	
	
	
end architecture;
