`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:32:20 04/23/2013
// Design Name:   cpu65xx_en
// Module Name:   C:/Users/ishe/Documents/Cloud/FPGAdev/TED/cputestbench.v
// Project Name:  TED
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: cpu65xx_en
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module C16_testbench;

	reg clk28=0;
	reg sreset=0;
	reg [7:0] port_in=0;
	wire [7:0] port_out;
	wire [7:0] datain;
	wire aec;
	wire rdy;
	reg enable=0;
	wire cpuclk;

	wire irq_n;
   wire [7:0] dataout;
	wire [15:0] addr;
	wire rw;
	wire [6:0] color;
	wire [3:0] red,green,blue;
	wire csync;
	wire RAS,CAS,mux,cs0,cs1;
	wire [7:0] kbus;

	
	wire [15:0] c16_addr,ted_addr,cpu_addr;
	wire [7:0]  c16_data,ted_data,ram_data,A,cpu_data,basic_data,kernal_data;
	wire [7:0] keyboard_row,keyport_data;
	reg  [7:0] c16_datalatch=0;
	reg [7:0] keyscancode;
	wire [6:0] c16_color;
	reg [12:0] resetcounter=0;
	reg [15:0] c16_addrlatch=0;
	reg keyreceived=0;
	wire cpuenable;
	
	// 8501 CPU
	mos8501 cpu (
		.clk(clk28), 
		.reset(sreset), 
		.enable(cpuenable),  
		.irq_n(irq_n), 
		.data_in(c16_data), 
		.data_out(cpu_data), 
		.address(cpu_addr),
		.gate_in(mux),
		.rw(rw),							// rw=high read, rw=low write
		.port_in(port_in),
		.port_out(port_out),
		.rdy(rdy),
		.aec(aec)
	);

  // simulated 64k DRAM
  ram cpuram (
		.address(A),
		.ras(RAS),
		.cas(CAS),
		.data_in(c16_data),
		.data_out(ram_data),
		.rw(rw)
		);

 // TED
	ted mos8360(
		.clk(clk28),
		.addr_in(c16_addr),
		.addr_out(ted_addr),
		.data_in(c16_data),
		.data_out(ted_data),
		.rw(rw),
		.cpuclk(cpuclk),
		.color(color),
		.csync(csync),
		.irq(irq_n),
		.ba(rdy),
		.mux(mux),
		.ras(RAS),
		.cas(CAS),
		.cs0(cs0),
		.cs1(cs1),
		.aec(aec),
		.k(kbus),
		.snd(sound),
		.cpuenable(cpuenable)
		);

// Kernal rom
	kernal_rom kernal(
		.clk(clk28),
		.address_in(c16_addr[13:0]),
		.data_out(kernal_data),
		.cs(cs1)
		);

// Basic rom
	basic_rom basic(
		.clk(clk28),
		.address_in(c16_addr[13:0]),
		.data_out(basic_data),
		.cs(cs0)
		);
		
// Color decoder to 12bit RGB	
colors_to_rgb colordecode(
		.clk(clk28),
		.color(color),
		.red(red),
		.green(green),
		.blue(blue));

// keyboard part

/*
ps2receiver ps2rcv(
    .clk(clk28),
    .ps2_clk(ps2clk),
    .ps2_data(ps2dat),
    .rx_done(keyreceived),
    .ps2scancode(keyscancode)
    );
*/

c16_keymatrix keyboard(
	 .clk(clk28),
    .scancode(keyscancode),
    .receiveflag(keyreceived),
	 .row(keyboard_row),
    .kbus(kbus)
    );

mos6529 keyport(
	 .clk(clk28),
    .data_in(c16_data),
    .data_out(keyport_data),
    .port_in(8'hff),
    .port_out(keyboard_row),
    .rw(rw),
    .cs(keyboardio)
    );


assign c16_addr=(~mux)?c16_addrlatch:cpu_addr&ted_addr;
assign c16_data=(mux)?c16_datalatch:cpu_data&ted_data&ram_data&kernal_data&basic_data&keyport_data;
assign keyboardio=(c16_addr[15:4]==12'hfd3)?1'b1:1'b0;

assign A=(~mux)?c16_addr[15:8]:c16_addr[7:0];	//  DRAM address multiplexer	


always @(posedge clk28)
	begin
	c16_datalatch<=c16_data;
	c16_addrlatch<=c16_addr;
	end
	
initial begin
		// Initialize Inputs
		clk28=0;
		keyscancode=7'hff;
		keyreceived=1;
		
		// Wait for global reset to finish
		#590;
		port_in=8'hff;
		
		// Add stimulus here
						
	end

always @(posedge clk28)
	begin
	if(resetcounter==13'd100)
		sreset<=0;
	else begin
		resetcounter<=resetcounter+1;
		sreset<=1;
		end
	
	end


// 28.288 Mhz	
always
		begin
#17.675	clk28<=~clk28;
		end

always @(c16_addr)
	begin
	if (c16_addr == 16'he6e2)
		$finish;
	end

endmodule
