`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//  Copyright 2013-2016 Istvan Hegedus
//
//  FPGATED is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  FPGATED is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
//
// Create Date:    
// Design Name: 
// Module Name:    	dram.v
// Project Name: 		FPGATED
// Description:		This module is only used when TED ram is implemented inside FPGA sram
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module dram(
	 input wire clk, 
    input wire [13:0] address,
	 input wire ras,
	 input wire cas,
    input wire [7:0] data_in,
    output wire [7:0] data_out,
    input wire rw
    );

(* RAM_STYLE="BLOCK" *)
reg [7:0] memory[16383:0];
reg [7:0] q;
reg [13:0] ramaddress=0;
reg cas_prev=1;
wire enable;

always @(posedge clk)
	begin
	if(enable)
		ramaddress<=address;
	else if(~cas)
		begin
			if (~rw)
				memory[ramaddress]<=data_in;
			else 
				q<=memory[ramaddress];
		end
	end

always @(posedge clk)
	begin
		cas_prev<=cas;
	end
		
assign data_out=(~cas & rw)?q:8'hff;
assign enable=cas_prev & ~cas;

endmodule
