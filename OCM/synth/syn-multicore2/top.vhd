

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port 
	(
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram2_addr_o		: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram2_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram2_we_n_o		: out   std_logic								:= '1';
		sram2_oe_n_o		: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: inout std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: inout std_logic;

		-- Joysticks
		joy1_up_i			: inout    std_logic;
		joy1_down_i			: inout    std_logic;
		joy1_left_i			: inout    std_logic;
		joy1_right_i		: inout    std_logic;
		joy1_p6_i			: inout    std_logic;
		joy1_p9_i			: inout    std_logic;
		joy2_up_i			: inout    std_logic;
		joy2_down_i			: inout    std_logic;
		joy2_left_i			: inout    std_logic;
		joy2_right_i		: inout    std_logic;
		joy2_p6_i			: inout    std_logic;
		joy2_p9_i			: inout    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture Behavior of top is
	
	-- clocks	
	signal clk_hdmi			: std_logic;		
	signal clk_sdram			: std_logic;		
	

	-- Reset signal
	signal reset_s				: std_logic;		-- Reset geral
	signal power_on_s			: std_logic_vector(25 downto 0)	:= (others => '1');
	signal power_on_reset 	: std_logic := '1';
	
	-- DIPS: similar to Zemmix
	-- bit 7 - 0 = SD enable, 1 = disable
	-- bit 6 - 0 = 2048kb mapper, 1 = 4096kb mapper 
	-- bit 5-4 - config slot 2 "00" - Cart in Slot 2; "10" -SCC2; "01" -ASC8K; "11" -ASC16K
	-- bit 3 - config slot 1 = "0" = cart in slot 1, '1' = megaSCC+ 1024kb
	-- bit 2-1 - video 
	-- bit 0 - cpu speed
	
	
	signal dip_s				: std_logic_vector(7 downto 0) := "01100000";		
	
	-- VGA
	signal vga_r_s				: std_logic_vector(5 downto 0)	:= (others => '0');
	signal vga_g_s				: std_logic_vector(5 downto 0)	:= (others => '0');
	signal vga_b_s				: std_logic_vector(5 downto 0)	:= (others => '0');
	signal vga_hsync_n_s		: std_logic								:= '1';
	signal vga_vsync_n_s		: std_logic								:= '1';
	signal blank_sig			: std_logic;
	
	--audio
	signal SL_s					: std_logic_vector(5 downto 0)	:= (others => '0');
	signal SR_s					: std_logic_vector(5 downto 0)	:= (others => '0');
	
	

	
begin

 	reset_s <= btn_n_i(1) or power_on_reset;
  
--	U00 : work.pll
--	  port map(
--		inclk0   => clock_50_i,              
--		c0       => clk21m,                 -- 21.48MHz internal
--		c1       => memclk,                 -- 85.92MHz = 21.48MHz x 4
--		c2       => pMemClk,                -- 85.92MHz external
--		c3			=> clk_hdmi						-- 107.40Mhz = 21.48MHz x 5
--	  );

	ocm: work.emsx_top
	port map
	(
        -- Clock, Reset ports
        pClk21m         => clock_50_i,
        pExtClk         => '0',
        pCpuClk         => open,

        pSltRst_n       => reset_s,

        -- SD-RAM ports
		  pMemClk			=> clk_sdram,		-- SD-RAM Clock
        pMemCke         => sdram_cke_o,   -- SD-RAM Clock enable
        pMemCs_n        => sdram_cs_o,    -- SD-RAM Chip select
        pMemRas_n       => sdram_ras_o,   -- SD-RAM Row/RAS
        pMemCas_n       => sdram_cas_o,   -- SD-RAM /CAS
        pMemWe_n        => sdram_we_o,    -- SD-RAM /WE
        pMemUdq         => sdram_dqm_o(1),-- SD-RAM UDQM
        pMemLdq         => sdram_dqm_o(0),-- SD-RAM LDQM
        pMemBa1         => sdram_ba_o(1), -- SD-RAM Bank select address 1
        pMemBa0         => sdram_ba_o(0), -- SD-RAM Bank select address 0
        pMemAdr         => sdram_ad_o,		-- SD-RAM Address
        pMemDat         => sdram_da_io,   -- SD-RAM Data
		
        -- PS/2 keyboard ports
        pPs2Clk         => ps2_clk_io,
        pPs2Dat         => ps2_data_io,
		
        -- Joystick ports (Port_A, Port_B)
		  pJoyA(5)			=>	joy1_p9_i,
		  pJoyA(4)			=> joy1_p6_i,
		  pJoyA(3)			=>	joy1_right_i,
		  pJoyA(2)			=>	joy1_left_i,
		  pJoyA(1)			=>	joy1_down_i,
		  pJoyA(0)			=>	joy1_up_i,
	
		  pJoyB(5)			=>	joy2_p9_i,
		  pJoyB(4)			=> joy2_p6_i,
		  pJoyB(3)			=>	joy2_right_i,
		  pJoyB(2)			=>	joy2_left_i,
		  pJoyB(1)			=>	joy2_down_i,
		  pJoyB(0)			=>	joy2_up_i,
		
        pStrA           => open,
        pStrB           => open,
	  
        -- SD/MMC slot ports
		  pSd_Ck          => sd_sclk_o,             	-- pin 5 Clock
        pSd_Cm          => sd_mosi_o,             	-- pin 2 Datain
		  pSd_Dt(3)			=> sd_cs_n_o,					-- pin 1 CS
		  pSd_Dt(2)			=> open,
		  pSd_Dt(1)			=> open,
		  pSd_Dt(0)			=> sd_miso_i,					-- pin 7 Dataout


        -- DIP switch, Lamp ports
        pDip            => dip_s,

        -- Video, Audio/CMT ports
        pDac_VR         => vga_r_s,
        pDac_VG         => vga_g_s,
        pDac_VB         => vga_b_s,
		  
        pDac_SL   	   => SL_s,
        pDac_SR	      => SR_s,

        pVideoHS_n      => vga_hsync_n_s,
        pVideoVS_n      => vga_vsync_n_s,
		  
		  --oBlank				=> blank_sig,
		  
		  --others
		  pSltClk			=> '0',
		  pIopRsv14       => '0',
        pIopRsv15       => '0',
        pIopRsv16       => '0',
        pIopRsv17       => '0',
        pIopRsv18       => '0',
        pIopRsv19       => '0',
        pIopRsv20       => '0',
        pIopRsv21       => '0'
    );
	 
	sdram_clk_o <= clk_sdram;
	
	vga_r_o				<= vga_r_s(5 downto 1);
	vga_g_o				<= vga_g_s(5 downto 1);
	vga_b_o				<= vga_b_s(5 downto 1);
	vga_hsync_n_o		<= vga_hsync_n_s;
	vga_vsync_n_o		<= vga_vsync_n_s;
	
	dac_l_o <= SL_s(0);
	dac_r_o <= SR_s(0);
	
	
--	U20: entity work.hdmi
--	port map (
--	I_CLK_TMDS	=> clk_hdmi,
--	I_CLK_PIXEL	=> clk21m,
--	I_RED			=> vga_r_s & "00",
--	I_GREEN		=> vga_g_s & "00",
--	I_BLUE		=> vga_b_s & "00",
--	I_BLANK		=> blank_sig,
--	I_HSYNC		=> vga_hsync_n_s,
--	I_VSYNC		=> vga_vsync_n_s,
--	O_TMDS		=> tmds_o
--	);

		-- power on reset
		process (clk_sdram)
		begin
			if rising_edge(clk_sdram) then
				if power_on_s /= x"00" then
					power_on_s <= power_on_s - 1;
					power_on_reset <= '1';
				else
					power_on_reset <= '0';
				end if;
				
			end if;
		end process;
	

end architecture;
