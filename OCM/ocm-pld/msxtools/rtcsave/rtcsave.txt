RTC save 2.2 for One Chip MSX
Made By: NYYRIKKI 2008/KdL 2017

In case someone is interested, there is a bit improved BIOS for One Chip MSX.
You can save RTC settings on SD-CARD by executing RTCSAVE.COM


Installation:
- Convert an appropriate BIOS from hex-file in binary, OCM-SDBIOS Pack is suggested.
- Format your SD-Card into FAT16 and copy 'ocm-bios.dat' as first file in SD-Card.
- Copy 'rtcsave.com' to somewhere and run it to save the RTC settings.

Revision 1.0 (NYYRIKKI)
- First public release.

Revision 2.2 (KdL)
- Auto-scanning for RTC patch on any "custom BIOS" up to 1024 kB.
- Changed the string UNEXPECTED ERROR! into UNSUPPORTED KERNEL FOUND!

__________
2017.09.18
