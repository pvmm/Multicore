;
; rtcsave.*
;
;   ESE MSX-SYSTEM3 / MSX clone on a Cyclone FPGA (ALTERA)
;   Revision 1.00
; 
; Copyright (c) 2006 Kazuhiro Tsujikawa (ESE Artists' factory)
; Copyright (c) 2006 D4 Enterprise,Inc.
; Copyright (c) 2006 MSX association
; All rights reserved.
; 
; Redistribution and use of this source code or any derivative works, are 
; permitted provided that the following conditions are met:
; 
; 1. Redistributions of source code must retain the above copyright notice, 
;    this list of conditions and the following disclaimer.
; 2. Redistributions in binary form must reproduce the above copyright 
;    notice, this list of conditions and the following disclaimer in the 
;    documentation and/or other materials provided with the distribution.
; 3. Redistributions may not be sold, nor may they be used in a commercial 
;    product or activity without specific prior written permission.
; 
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
; TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
; OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
; ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
; ----------------------------------------
;  Prog:    RTC save 2.2 for One Chip MSX
;  Made By: NYYRIKKI 2008/KdL 2017
;  Date:    2017.09.18
;  Coded in TWZ'CA3 w/ TASM80 v3.2ud
; ----------------------------------------
;

           .ORG   $0100

DATA       .EQU   $3FC6
SIZE       .EQU   1024                    ; SDBIOS or ESE-RAM size (max 1024kB)
BLKS       .EQU   SIZE/16                 ; amount of 16kB blocks

; ----------------------------------------
startProg:
           LD     DE,STARTXT              ; msg: 'RTC save 2.0 for One Chip MSX'
                                          ;      'Made By: NYYRIKKI 2008/KdL 2017'
           CALL   PRINT

           LD     HL,$3E00
           LD     DE,0
           LD     BC,$1F0
           XOR    A
           SCF
           CCF                            ; Read sector
           RST    30
           .DB    %10001011               ; SLOT 3-2
           .DW    $4010
      
FINDROMFILE:
           LD     IX,$3E00
           LD     L,(IX+$0E)              ; Reserved sectors
           LD     H,(IX+$0F)
           LD     E,(IX+$11)              ; Root entries
           LD     D,(IX+$12)
           LD     A,E
           AND    $0F
           LD     B,$04
_F1C2:
           SRL    D
           RR     E
           DJNZ   _F1C2
           OR     A
           JR     Z,_F1CC
           INC    DE
_F1CC:
           PUSH   DE
           LD     B,(IX+$10)              ; Number of FATs
           LD     E,(IX+$16)              ; Sectors / FAT
           LD     D,(IX+$17)
_F1D7:
           ADD    HL,DE
           DJNZ   _F1D7
           POP    DE
           ADD    HL,DE

           PUSH   HL
; ----------------------------------------
           LD     DE,(BLKS-1)*32+31       ; OFFSET TO CORRECT PLACE                              
; ----------------------------------------
           ADD    HL,DE
           LD     (RTCSECTOR),HL
           POP    DE

           LD     HL,$3E00
           LD     BC,$01F0
           XOR    A
           SCF
           CCF                            ; Read sector
           RST    30
           .DB    %10001011               ; SLOT 3-2
           .DW    $4010
           LD     DE,ERROR                ; msg: 'INCOMPATIBLE KERNEL FOUND! (This program is for OCM only)'
           JP     C,PRINT

           LD     HL,($3E00)
           LD     DE,$4241                ; ROM header 'AB'
           OR     A
           SBC    HL,DE

           LD     DE,NOBIOS               ; msg: 'MSX-BIOS not found from MMC/SD, please intall BIOS first'
           JP     NZ,PRINT
IDSCAN:
           LD     HL,$3E00
           LD     DE,(RTCSECTOR)
           LD     BC,$01F0
           XOR    A
           SCF
           CCF                            ; Read sector
           RST    30
           .DB    %10001011               ; SLOT 3-2
           .DW    $4010
           LD     DE,ERROR                ; msg: 'INCOMPATIBLE KERNEL FOUND! (This program is for OCM only)'
           JP     C,PRINT

           LD     HL,$3FE0
           LD     DE,IDTXT                ; str: 'RTC CODE'
IDLOOP:
           LD     A,(DE)
           AND    A
           JR     Z,IDFOUND
           CP     (HL)
           JP     NZ,CHGBLK
           INC    HL
           INC    DE
           JR     IDLOOP

CHGBLK:                                   ; Auto-scanning
           LD     A,(CURBLK)
           DEC    A                       ; Current block - 1
           CP     $00
           JR     Z,NOID
           LD     (CURBLK),A
           LD     HL,(RTCSECTOR)
           LD     DE,$0020                ; 32 sectors by 512 bytes = 1 block
           SBC    HL,DE
           LD     (RTCSECTOR),HL
           JR     IDSCAN

; ----------------------------------------
IDFOUND:
           LD     HL,DATA
           LD     C,%00010000
rLOOP:
           CALL   RRTC
           AND    $0F
           PUSH   AF
           INC    C
           CALL   RRTC
           INC    C
           AND    $0F
           RRCA
           RRCA
           RRCA
           RRCA
           LD     B,A
           POP    AF
           OR     B
           LD     (HL),A
           INC    HL
           LD     A,C
           AND    $0F
           CP     $0E
           JR     NZ,rLOOP
           INC    C
           INC    C
        
           LD     A,%01000000
           CP     C
           JR     NZ,rLOOP
           
           LD     HL,$3E00
           LD     DE,(RTCSECTOR)
           LD     BC,$01F0
           XOR    A
           SCF                            ; Write sector
           RST    30
           .DB    %10001011               ; SLOT 3-2
           .DW    $4010
           LD     DE,WRTERR               ; msg: 'WRITE ERROR!'
           JP     C,PRINT
           LD     DE,OK                   ; msg: 'RTC saved'
        
PRINT:
           LD     C,$09
           JP     $0005

; ----------------------------------------
NOID:
           LD     DE,NOIDTXT              ; msg: 'No custom BIOS found!'
           JP     PRINT
                                          ; GENERAL SAVE HERE
           RET

RRTC:
           RST    30
           .DB    %10000111               ; SLOT 3-1
           .DW    $01F5
           RET

; ----------------------------------------
STARTXT:
           .DB    "RTC save 2.2 for One Chip MSX",10,13
           .DB    "Made By: NYYRIKKI 2008/KdL 2017",10,10,13,"$"
IDTXT:
           .DB    "RTC CODE",0
ERROR:
           .DB    "UNSUPPORTED KERNEL FOUND! (This program is for OCM only)",10,13,"$"
WRTERR:
           .DB    "WRITE ERROR!",10,13,"$"
OK:
           .DB    "RTC saved",10,13,"$"
NOBIOS:
           .DB    "MSX-BIOS not found from MMC/SD, please intall BIOS first",10,13,"$"
NOIDTXT:
           .DB    "No custom BIOS found!",10,13,"$"
CURBLK:
           .DB    BLKS
RTCSECTOR:
           .DW    $0000

; ----------------------------------------
endProg:                                  ; Fill 512 bytes (optional)
           .DB    $FF,$FF,$FF,$FF,$FF,$FF

           .END

;
; -----------------------------------------------------------------------------
; APPENDIX
; -----------------------------------------------------------------------------
;
; ---| MSX-BIOS Configuration (OCM-PLD v3.4 or later) |------------------------
; 
; 3-2 (4000h)  128kB  MEGASDHC.ROM + NULL64KB.ROM / NEXTOR16.ROM   blocks 01-08
; 0-0 (0000h)   32kB  MSX2P   .ROM / MSXTR   .ROM                  blocks 09-10
; 3-3 (4000h)   16kB  XBASIC2 .ROM / XBASIC21.ROM                  block  11
; 0-2 (4000h)   16kB  MSX2PMUS.ROM / MSXTRMUS.ROM                  block  12
; 3-1 (0000h)   16kB  MSX2PEXT.ROM / MSXTREXT.ROM                  block  13
; 3-1 (4000h)   32kB  MSXKANJI.ROM                                 blocks 14-15
; 0-3 (4000h)   16kB  FREE16KB.ROM / MSXTROPT.ROM                  block  16
; I/O          128kB  JIS1    .ROM                                 blocks 17-24
; I/O          128kB  JIS2    .ROM                        (512kB)  blocks 25-32
; -----------------------------------------------------------------------------
; Note: Slot0-1 has been replaced with Slot3-3 since OCM-PLD v3.5
; -----------------------------------------------------------------------------
;
; ---| MSX-BIOS Configuration (OCM-PLD v3.0 to v3.3.3) |-----------------------
; 
; 3-2 (4000h)   64kB  MEGASDHC.ROM                                 blocks 01-04
; 0-0 (0000h)   32kB  MSX2P   .ROM                                 blocks 05-06
; 3-1 (0000h)   16kB  MSX2PEXT.ROM (patch S0-1 is required)        block  07
; 0-2 (4000h)   16kB  MSX2PMUS.ROM                                 block  08
; I/O          128kB  JIS1    .ROM                                 blocks 09-16
; 0-1 (0000h)   16kB  FREE16KB.ROM                                 block  17
; 0-1 (4000h)   32kB  MSXKANJI.ROM                                 blocks 18-19
; 0-1 (C000h)   16kB  FREE16KB.ROM                                 block  20
; 0-3 (0000h)   16kB  FREE16KB.ROM                                 block  21
; 0-3 (4000h)   16kB  XBASIC2 .ROM                                 block  22
; 0-3 (8000h)   16kB  FREE16KB.ROM                                 block  23
; 0-3 (C000h)   16kB  FREE16KB.ROM                                 block  24
; N/A          128kB  FREE16KB.ROM * 8                    (512kB)  blocks 25-32
; -----------------------------------------------------------------------------
;
; DISKIO (#4010)  Sector(s) read/write
; -----------------------------------------------------------------------------
; Input:  [F]     carry set for write
;                 carry reset for read
;         [A]     Drive number (0=A:)
;         [B]     Number of sectors to write
;         [C]     Media descriptor
;         [DE]    Logical sector number (starts at 0)
;         [HL]    Transfer address
; 
; Output: [F]     carry set on error
;                 carry reset on success
;         [A]     If error: errorcode
;         [B]     Number of sectors transferred (always)
; 
; Error codes in [A] can be:
;         0       Write protected
;         2       Not ready
;         4       Data (CRC) error
;         6       Seek error
;         8       Record not found
;         10      Write fault
;         12      Other errors
; -----------------------------------------------------------------------------
;
; Offset(h) 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F
;
; 00003E00  3E EC D5 CD 3F 3E D1 7B FE 55 C8 3E 4C 18 2D FE  >�i-?>�{�U+>L.-�
; 00003E10  59 20 06 CD 3C 3E 7B 18 0C FE 57 20 0D CD 3C 3E  Y .-<>{..�W .-<>
; 00003E20  7B FE 49 28 E6 FE 45 C0 18 E1 FE 56 20 0C 3E 55  {�I(��E+.ߦV .>U
; 00003E30  D5 CD 39 3E CD D7 3D 18 CD 5F 3E 51 CD 7C 3D 21  i-9>-�=.-_>Q-|=!
; 00003E40  FC FA CB 7E 28 0E FE E0 38 04 D6 20 18 06 FE A0  ��-~(.��8.� ..��
; 00003E50  30 02 C6 20 2A F8 F3 77 23 7D FE 18 20 03 21 F0  0.� *��w#}�. .!�
; 00003E60  FB 3A FA F3 BD C8 22 F8 F3 C9 3A DB F3 A7 C8 3A  �:���+"��+:���+:
; 00003E70  D9 FB A7 C0 3E 0F 32 D9 FB F3 D3 AB 3E 0A 3D 20  +��+>.2+��˽>.= 
; 00003E80  FD 3E 0E D3 AB FB C9 21 FC FA CB 46 28 05 CB 86  �>.˽�+!��-F(.-�
; 00003E90  AF 18 19 3A AC FC 3C 28 10 3A EB FB 0F 38 08 AF  �..:��<(.:ٹ.8.�
; 00003EA0  32 F9 FA CB C6 18 05 3E FF 32 AC FC F5 3E 0F D3  2��-�..>�2���>.�
; 00003EB0  A0 DB A2 E6 7F 47 F1 B7 3E 80 28 01 AF B0 D3 A1  ��.G��>�(.����
; 00003EC0  C9 2A 3A 5D 5F 3F 3E 7B 7D 5C 40 3E 3F 5B 3C 7B  +*:]_?>{}\@>?[<{
; 00003ED0  7D B0 DE A1 A5 DF A4 A2 A3 0A 15 25 28 23 18 40  }���ѯ���..%(#.@
; 00003EE0  21 05 16 4B 01 0A 06 04 19 28 80 08 02 03 17 40  !..K.....(�....@
; 00003EF0  49 8C 47 22 FD F4 92 F5 92 F6 F7 F8 F9 FA FB FC  I�G"��Ƨ��������
; 00003F00  93 93 93 86 87 88 89 8A 8B 8C 88 8D 8A 8E FF FF  �����������.�Ġ�
; 00003F10  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F20  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F30  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F40  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F50  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F60  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F70  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F80  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003F90  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; 00003FA0  21 C6 3F 0E 10 7E 23 F5 CD 9E 1C F1 0F 0F 0F 0F  !�?..~#�-�.�....
; 00003FB0  0C CD 9E 1C 0C 79 E6 0F FE 0E 20 E9 0C 0C 3E 40  .-�..y�.�. �..>@
; 00003FC0  B9 20 E2 C3 8E 05 FF FF FF FF FF 0F 9F 0A 00 50  � �+�.�����.�..P
; 00003FD0  4F 17 02 A0 00 00 00 00 00 00 B0 FF FF FF FF FF  O..�......������
; 00003FE0  52 54 43 20 43 4F 44 45 FF FF FF FF FF FF FF FF  RTC CODE��������
; 00003FF0  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  ����������������
; -----------------------------------------------------------------------------
;
