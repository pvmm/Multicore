-------------------------------------------------------------------------------
--
-- CP/M 3 FPGA project
--
-- Copyright (c) 2016, Fabio Belavenuto (belavenuto@gmail.com)
--
-- All rights reserved
--
-- Some files is copyright by Grant Searle 2014
-- You are free to use this files in your own projects but must never charge for it nor use it without
-- acknowledgement.
-- Please ask permission from Grant Searle before republishing elsewhere.
-- If you use this file or any part of it, please add an acknowledgement to myself and
-- a link back to my main web site http://searle.hostei.com/grant/    
-- and to the "multicomp" page at http://searle.hostei.com/grant/Multicomp/index.html
--
-- Please check on the above web pages to see if there are any updates before using this file.
-- If for some reason the page is no longer available, please search for "Grant Searle"
-- on the internet to see if I have moved to another web hosting service.
--
-- Grant Searle
-- eMail address available on my main web page link above.
-------------------------------------------------------------------------------
-- altera message_off 10540 10541

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE1 board
entity de1_top is
	port (
		-- Clocks
		CLOCK_24       : in    std_logic_vector(1 downto 0);
		CLOCK_27       : in    std_logic_vector(1 downto 0);
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(9 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		-- Red LEDs
		LEDR           : out   std_logic_vector(9 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(7 downto 0)		:= (others => '0');

		-- VGA
		VGA_R          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_HS         : out   std_logic									:= '0';
		VGA_VS         : out   std_logic									:= '0';

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= '1';
		PS2_DAT        : inout std_logic									:= '1';

		-- I2C
		I2C_SCLK       : inout std_logic									:= '1';
		I2C_SDAT       : inout std_logic									:= '1';

		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '0');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '0');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => '0');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SD card (SPI mode)
		SD_nCS         : out   std_logic									:= '1';
		SD_MOSI        : out   std_logic									:= '1';
		SD_SCLK        : out   std_logic									:= '1';
		SD_MISO        : in    std_logic;

		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => 'Z');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => 'Z')
	);
end entity;

architecture rtl of de1_top is

	signal clock_master		: std_logic;
	signal pll_locked			: std_logic;
	signal reset				: std_logic;
	signal sramAddr			: std_logic_vector(16 downto 0);
	signal sramWE_n			: std_logic;
	signal sramCS_n			: std_logic;
	signal sramOE_n			: std_logic;
	signal serRX				: std_logic;
	signal serTX				: std_logic;
	signal serRTS				: std_logic;
	signal hsync_n				: std_logic;
	signal vsync_n				: std_logic;
	signal vgar					: std_logic_vector(1 downto 0);
	signal vgag					: std_logic_vector(1 downto 0);
	signal vgab					: std_logic_vector(1 downto 0);
	signal led					: std_logic;
	signal cpu_a				: std_logic_vector(15 downto 0);
	signal d_cpu_a				: std_logic_vector(15 downto 0);
	signal d_cpu_wr			: std_logic;
	signal d_cpu_rd			: std_logic;
	signal d_cpu_ioreq		: std_logic;
	signal d_cpu_mreq			: std_logic;
	signal d_cpu_rfsh			: std_logic;

begin

	pll_inst: entity work.pll1
	port map (
		inclk0	=> CLOCK_50,
		c0			=> clock_master,		-- 50 MHz
		locked	=> pll_locked
	);

	-- Virtual TOP
	v_top: entity work.virtual_top
	port map (
		n_reset			=> not reset,				--: in std_logic;
		clk				=> clock_master,			--: in std_logic;
		sramData			=> SRAM_DQ(7 downto 0),	--: inout std_logic_vector(7 downto 0);
		sramAddress		=> sramAddr,				--: out std_logic_vector(15 downto 0);
		n_sRamWE			=> sramWE_n,				--: out std_logic;
		n_sRamCS			=> sramCS_n,				--: out std_logic;
		n_sRamOE			=> sramOE_n,				--: out std_logic;
		rxd1				=> '0',						--: in std_logic;
		txd1				=> open,						--: out std_logic;
		rts1				=> open,						--: out std_logic;
		rxd2				=> serRX,					--: in std_logic;
		txd2				=> serTX,					--: out std_logic;
		rts2				=> serRTS,					--: out std_logic;
		videoSync		=> open,						--: out std_logic;
		video				=> open,						--: out std_logic;
		videoR0			=> vgar(0),					--: out std_logic;
		videoG0			=> vgag(0),					--: out std_logic;
		videoB0			=> vgab(0),					--: out std_logic;
		videoR1			=> vgar(1),					--: out std_logic;
		videoG1			=> vgag(1),					--: out std_logic;
		videoB1			=> vgab(1),					--: out std_logic;
		hSync				=> hsync_n,					--: out std_logic;
		vSync				=> vsync_n,					--: out std_logic;
		ps2Clk			=> PS2_CLK,					--: inout std_logic;
		ps2Data			=> PS2_DAT,					--: inout std_logic;
		sdCS				=> SD_nCS,					--: out std_logic;
		sdMOSI			=> SD_MOSI,					--: out std_logic;
		sdMISO			=> SD_MISO,					--: in std_logic;
		sdSCLK			=> SD_SCLK,					--: out std_logic;
		driveLED			=> led,						--: out std_logic :='1'	
		cpu_a				=> d_cpu_a,
		cpu_rd			=> d_cpu_rd,
		cpu_wr			=> d_cpu_wr,
		cpu_ioreq		=> d_cpu_ioreq,
		cpu_mreq			=> d_cpu_mreq,
		cpu_rfsh			=> d_cpu_rfsh
	);

	-- Glue
	reset		<= not pll_locked or not KEY(0);

	SRAM_ADDR	<= "0" & sramAddr;
	SRAM_DQ(15 downto 8)	<= (others => 'Z');
	SRAM_LB_N	<= '0';
	SRAM_CE_N	<= sramCS_n;
	SRAM_WE_N	<= sramWE_n;
	SRAM_OE_N	<= sramOE_n;
--	serRX1		<= UART_RXD;
--	UART_TXD		<= serTX1;
	serRX			<= GPIO_0(29);
	GPIO_0(27)	<= serTX;
	GPIO_0(31)	<= serRTS;
	VGA_R			<= vgar & "00";
	VGA_G			<= vgag & "00";
	VGA_B			<= vgab & "00";
	VGA_HS		<= hsync_n;
	VGA_VS		<= vsync_n;
	
	LEDG(0)		<= reset;
	LEDG(7)		<= led;

	-- debug
	LEDG(1)		<= d_cpu_rd;
	LEDG(2)		<= d_cpu_wr;
	LEDG(3)		<= d_cpu_ioreq;
	LEDG(4)		<= d_cpu_mreq;
	LEDG(5)		<= d_cpu_rfsh;
	
	ld3: entity work.seg7
	port map(
		D		=> d_cpu_a(15 downto 12),
		Q		=> HEX3
	);

	ld2: entity work.seg7
	port map(
		D		=> d_cpu_a(11 downto 8),
		Q		=> HEX2
	);

	ld1: entity work.seg7
	port map(
		D		=> d_cpu_a(7 downto 4),
		Q		=> HEX1
	);

	ld0: entity work.seg7
	port map(
		D		=> d_cpu_a(3 downto 0),
		Q		=> HEX0
	);

end;