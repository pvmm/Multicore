;
; Oliver Schmidt, 2009-09-15
; Fabio Belavenuto, 2016-08-17
;
; Startup code for cc65 (MPF2 version)
;

        .export         return
        .export         __STARTUP__ : absolute = 1      ; Mark as startup

        .import         zerobss, callmain

        .include        "zeropage.inc"
        .include        "mpf2.inc"

; ------------------------------------------------------------------------

        .segment        "STARTUP"

        ldx     #$FF
        txs                     ; Init stack pointer
        jsr     init
        ; Clear the BSS data.
        jsr     zerobss
        ; Push the command-line arguments; and, call main().
        jsr     callmain
exit:
		; We're done
done:	jmp		done

init:
		lda     #$FF
		ldx     #$9F

        ; Set up the C stack.
        sta     sp
        stx     sp+1

        ldx     #<exit
        lda     #>exit
        jsr     reset           ; Setup RESET vector

        rts

; ------------------------------------------------------------------------

        .code

        ; Set up the RESET vector.
reset:  stx     SOFTEV
        sta     SOFTEV+1
        eor     #$5A
        sta     PWREDUP
return: rts

; ------------------------------------------------------------------------
