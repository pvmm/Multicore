/*
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "vdp.h"
#include "mmc.h"
#include "fat.h"

/* Definicoes */
static const char ROMFILES[2][12] = {
	"TK2000  ROM",
	"MPFII   ROM"
};

/* Funcoes internas */

/*******************************************************************************/
static void printCenter(unsigned char y, unsigned char *msg)
{
	unsigned char x;

	x = 20 - strlen(msg)/2;
	vdp_gotoxy(x, y);
	vdp_putstring(msg);
}

/*******************************************************************************/
static void erro(unsigned char *erro)
{
	printCenter(12, erro);
	for(;;);
}

/* Funcoes publicas */

/*******************************************************************************/
void main()
{
	unsigned char *pbios = 0xC000;
	unsigned char i, bi, keys;
	char msg[32];
	char *romfilename		= NULL;
	fileTYPE file;

	POKE(0xC05A, 0);
	vdp_cls();
	printCenter(0, "TK2000 ROM LOADER");
	keys = PEEK(SD_STATUS);
	if ((keys & 0x02) == 0) {
		bi = 0;
	} else {
		bi = 1;
	}
	romfilename = (char *)ROMFILES[bi];
	
	strcpy(msg, "Loading ");
	strcat(msg, romfilename);
	printCenter(9, msg);

	if (disk_initialize()) {
		erro("Error on SD card initialization!");
	}
	if (!FindDrive()) {
		erro("Error monting SD card!");
	}
	if (!FileOpen(&file, romfilename)) {
		erro("ROM file not found!");
	}
	if (file.size != 16384) {
		erro("ROM file size is not 16384 bytes!");
	}
	for (i = 0; i < 32; i++) {								// Ler 32 blocos de 512 bytes = 16384 bytes
		if (!FileRead(&file, pbios)) {
			erro("Error reading ROM file!");
		}
		pbios += 512;
	}
	printCenter(20, "Booting...");
	POKE(SD_IPL, 0);
}
