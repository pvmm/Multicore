/*
Copyright (c) 2016 Fabio Belavenuto

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "vdp.h"
#include "font.h"

/* Variables */
unsigned int faddr;
unsigned int vaddr;
unsigned char cx, cy;

/* Private functions */

//------------------------------------------------------------------------------
static void vdp_cursorinc()
{
	++cx;
	if (cx > 39) {
		cx = 0;
		++cy;
		if (cy > 23) {
			cy = 23;
//			vdp_rolatela();
		}
	}
}

/* Public functions */

//------------------------------------------------------------------------------
void vdp_cls(void)
{
	POKE(0xC051, 0);
	POKE(0xC054, 0);
	__asm__("LDA	#$00");
	__asm__("STA	$FE");
	__asm__("LDA	#$20");
	__asm__("STA	$FF");
	__asm__("LDY	#$00");
	__asm__("L2: TYA");
	__asm__("L: STA	($FE), Y");
	__asm__("INY");
	__asm__("BNE	L");
	__asm__("INC	$FF");
	__asm__("LDA	$FF");
	__asm__("CMP	#$40");
	__asm__("BNE	L2");
	__asm__("RTS");
	cx = 0;
	cy = 0;
}

//------------------------------------------------------------------------------
void vdp_gotoxy(unsigned char x, unsigned char y)
{
	cx = x;
	if (cx > 39) {
		cx = 39;
	}
	cy = y;
	if (cy > 23) {
		cy = 23;
	}
}

//------------------------------------------------------------------------------
void vdp_putcharxy(unsigned char x, unsigned char y, unsigned char c)
{
	vdp_gotoxy(x, y);
	vdp_putchar(c);
}

//------------------------------------------------------------------------------
void vdp_putchar(unsigned char c)
{
	unsigned char i;

//	faddr = (c-32)*8;
	faddr = c << 3;
	vaddr = VRAM_ADDR + (cy & 7)*0x80 + (cy / 8)*0x28 + cx;

	for (i=0; i < 8; i++) {
		POKE(vaddr, font[faddr]);
		vaddr += 0x400;
		++faddr;
	}
	vdp_cursorinc();
}

//------------------------------------------------------------------------------
void vdp_putstring(char *s)
{
	do {
		vdp_putchar(*s);
	} while(*++s);
}
