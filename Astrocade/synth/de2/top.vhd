

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

-- Generic top-level entity for Altera DE1 board
entity top is
	port (
		-- Clocks
		CLOCK_27       : in    std_logic;
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(17 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX4           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX5           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX6           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX7           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		
		-- Red LEDs
		LEDR           : out   std_logic_vector(17 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(8 downto 0)		:= (others => '0');

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';

		-- IRDA
		IRDA_RXD       : in    std_logic;
		IRDA_TXD       : out   std_logic									:= '0';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => 'Z');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		--	ISP1362 Interface	
		OTG_ADDR       : out   std_logic_vector(1 downto 0)		:= (others => '0');	--	ISP1362 Address 2 Bits
		OTG_DATA       : inout std_logic_vector(15 downto 0)		:= (others => 'Z');	--	ISP1362 Data bus 16 Bits
		OTG_CS_N       : out   std_logic									:= '1';					--	ISP1362 Chip Select
		OTG_RD_N       : out   std_logic									:= '1';					--	ISP1362 Write
		OTG_WR_N       : out   std_logic									:= '1';					--	ISP1362 Read
		OTG_RST_N      : out   std_logic									:= '1';					--	ISP1362 Reset
		OTG_FSPEED     : out   std_logic									:= 'Z';					--	USB Full Speed,	0 = Enable, Z = Disable
		OTG_LSPEED     : out   std_logic									:= 'Z';					--	USB Low Speed, 	0 = Enable, Z = Disable
		OTG_INT0       : in    std_logic;															--	ISP1362 Interrupt 0
		OTG_INT1       : in    std_logic;															--	ISP1362 Interrupt 1
		OTG_DREQ0      : in    std_logic;															--	ISP1362 DMA Request 0
		OTG_DREQ1      : in    std_logic;															--	ISP1362 DMA Request 1
		OTG_DACK0_N    : out   std_logic									:= '1';					--	ISP1362 DMA Acknowledge 0
		OTG_DACK1_N    : out   std_logic									:= '1';					--	ISP1362 DMA Acknowledge 1
		
		--	LCD Module 16X2		
		LCD_ON         : out   std_logic									:= '0';					--	LCD Power ON/OFF, 0 = Off, 1 = On
		LCD_BLON       : out   std_logic									:= '0';					--	LCD Back Light ON/OFF, 0 = Off, 1 = On
		LCD_DATA       : inout std_logic_vector(7 downto 0)		:= (others => '0');	--	LCD Data bus 8 bits
		LCD_RW         : out   std_logic									:= '1';					--	LCD Read/Write Select, 0 = Write, 1 = Read
		LCD_EN         : out   std_logic									:= '1';					--	LCD Enable
		LCD_RS         : out   std_logic									:= '1';					--	LCD Command/Data Select, 0 = Command, 1 = Data
		
		--	SD_Card Interface	
		SD_DAT         : inout std_logic									:= 'Z';					--	SD Card Data (SPI MISO)
		SD_DAT3        : inout std_logic									:= 'Z';					--	SD Card Data 3 (SPI /CS)
		SD_CMD         : inout std_logic									:= 'Z';					--	SD Card Command Signal (SPI MOSI)
		SD_CLK         : out   std_logic									:= '1';					--	SD Card Clock (SPI SCLK)
		
		-- I2C
		I2C_SCLK       : inout std_logic									:= 'Z';
		I2C_SDAT       : inout std_logic									:= 'Z';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= 'Z';
		PS2_DAT        : inout std_logic									:= 'Z';

		-- VGA
		VGA_R          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_HS         : out   std_logic									:= '0';
		VGA_VS         : out   std_logic									:= '0';
		VGA_BLANK		: out   std_logic									:= '1';				
		VGA_SYNC			: out   std_logic									:= '0';	
		VGA_CLK		   : out   std_logic									:= '0';	
		
		-- Ethernet Interface	
		ENET_CLK       : out   std_logic									:= '0';					--	DM9000A Clock 25 MHz
		ENET_DATA      : inout std_logic_vector(15 downto 0)		:= (others => 'Z');	--	DM9000A DATA bus 16Bits
		ENET_CMD       : out   std_logic									:= '0';					--	DM9000A Command/Data Select, 0 = Command, 1 = Data
		ENET_CS_N      : out   std_logic									:= '1';					--	DM9000A Chip Select
		ENET_WR_N      : out   std_logic									:= '1';					--	DM9000A Write
		ENET_RD_N      : out   std_logic									:= '1';					--	DM9000A Read
		ENET_RST_N     : out   std_logic									:= '1';					--	DM9000A Reset
		ENET_INT       : in    std_logic;															--	DM9000A Interrupt
	               
		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- TV Decoder		
		TD_DATA        : in    std_logic_vector(7 downto 0);									--	TV Decoder Data bus 8 bits
		TD_HS          : in    std_logic;															--	TV Decoder H_SYNC
		TD_VS          : in    std_logic;															--	TV Decoder V_SYNC
		TD_RESET       : out   std_logic									:= '1';					--	TV Decoder Reset
	
		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => 'Z');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => 'Z')
		
		
	);
end entity;

architecture RTL of top is

    signal I_RESET_L        : std_logic;
    signal reset            : std_logic;
    signal reset_l          : std_logic;
    signal sw_reg           : std_logic_vector(3 downto 0);
    --
    signal ena_x2           : std_logic;
    signal ena              : std_logic;
    signal clk_14           : std_logic;
    signal clk_ref          : std_logic;
    --
    signal switch_col       : std_logic_vector(7 downto 0);
    signal switch_row       : std_logic_vector(7 downto 0);
    signal ps2_1mhz_ena     : std_logic;
    signal ps2_1mhz_cnt     : std_logic_vector(5 downto 0);
    --
    signal video_r          : std_logic_vector(3 downto 0);
    signal video_g          : std_logic_vector(3 downto 0);
    signal video_b          : std_logic_vector(3 downto 0);
    signal hsync            : std_logic;
    signal vsync            : std_logic;
    signal fpsync           : std_logic;
    --
    signal video_r_x2       : std_logic_vector(3 downto 0);
    signal video_g_x2       : std_logic_vector(3 downto 0);
    signal video_b_x2       : std_logic_vector(3 downto 0);
    signal hsync_x2         : std_logic;
    signal vsync_x2         : std_logic;
    --
    signal audio            : std_logic_vector(7 downto 0);
    signal audio_pwm        : std_logic;

    signal exp_addr         : std_logic_vector(15 downto 0);
    signal exp_data_out     : std_logic_vector(7 downto 0);
    signal exp_data_in      : std_logic_vector(7 downto 0);
    signal exp_oe_l         : std_logic;

    signal exp_m1_l         : std_logic;
    signal exp_mreq_l       : std_logic;
    signal exp_iorq_l       : std_logic;
    signal exp_wr_l         : std_logic;
    signal exp_rd_l         : std_logic;
    --
    signal check_cart_msb   : std_logic_vector(3 downto 0);
    signal check_cart_lsb   : std_logic_vector(7 downto 4);
    --
    signal cas_addr         : std_logic_vector(12 downto 0);
    signal cas_data         : std_logic_vector( 7 downto 0);
    signal cas_cs_l         : std_logic;
	 
	 signal clock_master_s 		: std_logic;
	 signal pll_locked_s 		: std_logic;
 
begin

	-- PLL
	pll_1: entity work.pll1
	port map (
		inclk0	=> CLOCK_50,
		c0			=> clock_master_s,		
		locked	=> pll_locked_s
	);

  --
--  I_RESET_L <= not pll_locked_s and KEY(0);
 I_RESET_L <=  KEY(0);
  --
  u_clocks : entity work.BALLY_CLOCKS
    port map (
       I_CLK_REF  => clock_master_s,
       I_RESET_L  => I_RESET_L,
       --
       O_CLK_REF  => clk_ref,
       --
       O_ENA_X2   => ena_x2,
       O_ENA      => ena,
       O_CLK      => clk_14, -- ~14 MHz
       O_RESET    => reset
     );

  p_ena1mhz : process
  begin
    wait until rising_edge(clk_14);
    -- divide by 14
    ps2_1mhz_ena <= '0';
    if (ps2_1mhz_cnt = "001101") then
      ps2_1mhz_cnt <= "000000";
      ps2_1mhz_ena <= '1';
    else
      ps2_1mhz_cnt <= ps2_1mhz_cnt + '1';
    end if;
  end process;

  reset_l <= not reset;

  u_bally : entity work.BALLY
    port map (
      O_AUDIO        => audio,
      --
      O_VIDEO_R      => video_r,
      O_VIDEO_G      => video_g,
      O_VIDEO_B      => video_b,

      O_HSYNC        => hsync,
      O_VSYNC        => vsync,
      O_COMP_SYNC_L  => open,
      O_FPSYNC       => fpsync,
      --
      -- cart slot
      O_CAS_ADDR     => cas_addr,
      O_CAS_DATA     => open,
      I_CAS_DATA     => cas_data,
      O_CAS_CS_L     => cas_cs_l,

      -- exp slot (subset for now)
      O_EXP_ADDR     => exp_addr,
      O_EXP_DATA     => exp_data_out,
      I_EXP_DATA     => exp_data_in,
      I_EXP_OE_L     => exp_oe_l,

      O_EXP_M1_L     => exp_m1_l,
      O_EXP_MREQ_L   => exp_mreq_l,
      O_EXP_IORQ_L   => exp_iorq_l,
      O_EXP_WR_L     => exp_wr_l,
      O_EXP_RD_L     => exp_rd_l,
      --
      O_SWITCH_COL   => switch_col,
      I_SWITCH_ROW   => switch_row,
      I_RESET_L      => reset_l,
      ENA            => ena,
      CLK            => clk_14
      );

  u_ps2 : entity work.BALLY_PS2_IF
    port map (

      I_PS2_CLK         => PS2_CLK,
      I_PS2_DATA        => PS2_DAT,

      I_COL             => switch_col,
      O_ROW             => switch_row,

      I_RESET_L         => reset_l,
      I_1MHZ_ENA        => ps2_1mhz_ena,
      CLK               => clk_14
      );

  --u_check_cart : entity work.BALLY_CHECK_CART
    --port map (
      --I_EXP_ADDR         => exp_addr,
      --I_EXP_DATA         => exp_data_out,
      --O_EXP_DATA         => exp_data_in,
      --O_EXP_OE_L         => exp_oe_l,

      --I_EXP_M1_L         => exp_m1_l,
      --I_EXP_MREQ_L       => exp_mreq_l,
      --I_EXP_IORQ_L       => exp_iorq_l,
      --I_EXP_WR_L         => exp_wr_l,
      --I_EXP_RD_L         => exp_rd_l,
      ----
      --O_CHAR_MSB         => check_cart_msb,
      --O_CHAR_LSB         => check_cart_lsb,
      ----
      --I_RESET_L          => reset_l,
      --ENA                => ena,
      --CLK                => clk_14
      --);

  -- if no expansion cart
  exp_data_in <= x"ff";
  exp_oe_l <= '1';
  --
  -- scan doubler
  --
  u_dblscan : entity work.BALLY_DBLSCAN
    port map (
      I_R               => video_r,
      I_G               => video_g,
      I_B               => video_b,
      I_HSYNC           => hsync,
      I_VSYNC           => vsync,
      --
      I_FPSYNC          => fpsync,
      --
      O_R               => video_r_x2,
      O_G               => video_g_x2,
      O_B               => video_b_x2,
      O_HSYNC           => hsync_x2,
      O_VSYNC           => vsync_x2,
      --
      I_RESET           => reset,
      ENA_X2            => ena_x2,
      ENA               => ena,
      CLK               => clk_14
    );
  --
 -- p_video_ouput : process
 -- begin
 --   wait until rising_edge(clk_14);
    -- switch is on (up) use scan converter and light led
  --  sw_reg <= SW(3 DOWNTO 0);
	sw_reg <= "0001";

   -- if (sw_reg(0) = '1') then
      LEDR(0) <= '1';
      VGA_R(9 downto 7) <= video_r_x2(3 downto 1);
      VGA_G(9 downto 7) <= video_g_x2(3 downto 1);
      VGA_B(9 downto 7) <= video_b_x2(3 downto 1);
      VGA_HS   <= hSync_X2;
      VGA_VS   <= vSync_X2;
		
		VGA_CLK <= clk_14;
		VGA_BLANK <= '1';
		VGA_SYNC <= '0';
		
 --   else
 --     leds_n_o(0) <= '0';
 --     vga_r_o <= video_r(3 downto 1);
 --     vga_g_o <= video_g(3 downto 1);
 --     vga_b_o <= video_b(3 downto 1);
 --     vga_hsync_n_o   <= hSync;
 --     vga_vsync_n_o   <= vSync;
 --   end if;
 -- end process;

  --
  -- Audio
  --
  u_dac : entity work.dac
    generic map(
      msbi_g => 7
    )
    port  map(
      clk_i   => clk_ref,
      res_n_i => reset_l,
      dac_i   => audio,
      dac_o   => audio_pwm
    );

  --dac_l_o <= audio_pwm;
  --dac_r_o <= audio_pwm;
  
  
  
  
  --
  -- cart slot
  --
 -- p_flash : process
 -- begin
 --   wait until rising_edge(clk_14);
 --   O_LED(3 downto 1) <= sw_reg(3 downto 1);
--
 --   O_STRATAFLASH_CE_L <= '1';
 --   if (sw_reg(1) = '0') then -- unplug card
 --     O_STRATAFLASH_CE_L <= cas_cs_l;
 --   end if;
 --   O_STRATAFLASH_OE_L <= '0';
 --   O_STRATAFLASH_WE_L <= '1';
 --   O_STRATAFLASH_BYTE <= '0';
--
 --   O_STRATAFLASH_ADDR(23 downto 15) <= (others => '0');
--
 --   O_STRATAFLASH_ADDR(14 downto 13) <= sw_reg(3 downto 2);
 --   O_STRATAFLASH_ADDR(12 downto  0) <= cas_addr(12 downto 0);
 --   B_STRATAFLASH_DATA <= (others => 'Z');
 --   -- should really sample and latch this at the correct point, but it seems to work
 --   cas_data <= B_STRATAFLASH_DATA;
 -- end process;
	
	
	
	cart_inst : ENTITY work.cart 
	PORT MAP
	(
		address	=> cas_addr(12 downto 0),
		clock		=> clk_14,
		q			=> cas_data 
	);
	
	GPIO_0(0) <= clk_14;
	GPIO_0(1) <= clock_master_s;
	GPIO_0(2) <= ena;
	GPIO_0(3) <= vsync;
	GPIO_0(4) <= hsync;
	
	


end architecture;
