-------------------------------------------------------------------------------
--
-- FPGA Videopac
--
-- $Id: jop_vp.vhd,v 1.11 2007/04/10 21:29:02 arnim Exp $
-- $Name: videopac_rel_1_0 $
--
-- Toplevel of the Cyclone port for JOP.design's cycore board.
--   http://jopdesign.com/
--
-------------------------------------------------------------------------------
--
-- Copyright (c) 2007, Arnim Laeuger (arnim.laeuger@gmx.net)
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Please report bugs to the author, but before you do so, please
-- make sure that this is not a derivative work and that
-- you have the latest version of this file.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity top_odyssey2 is

  port (
  
	-- Clocks
		clk_s					: in    std_logic;
		clk_en_o				: out    std_logic;
		
		reset_n   			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- PS2
		keyb_data      	: in    std_logic_vector(7 downto 0);
		keyb_valid     	: in    std_logic;
		osd_o		      	: out    std_logic_vector(7 downto 0);

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		snd_vec_o      	: out std_logic_vector(3 downto 0);


		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';
		color_index_o 		: out   std_logic_vector(3 downto 0);


		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1');
		
		-- cartridge
		rom_a_s				: out   std_logic_vector(12 downto 0);
      rom_d_s				: in   std_logic_vector(7 downto 0);
		cart_psen_n_o		: out   std_logic;
		
			  -- Counters
	 hpos_o     : out std_logic_vector(8 downto 0);
    vpos_o  	: out std_logic_vector(8 downto 0)

    
  );

end top_odyssey2;


library ieee;
use ieee.numeric_std.all;

library altera_mf;

use work.board_misc_comp_pack.dac;
use work.vp_console_comp_pack.vp_console;
use work.i8244_col_pack.all;

architecture struct of top_odyssey2 is

	




  component vp_por
    generic (
      delay_g     : integer := 4;
      cnt_width_g : integer := 2
    );
    port (
      clk_i   : in  std_logic;
      por_n_o : out std_logic
    );
  end component;

 



  -- CPU clock = PLL clock / 6
  constant cnt_cpu_c    : unsigned(2 downto 0) := to_unsigned(5, 3);
  -- VDC clock = PLL clock / 5
  constant cnt_vdc_c    : unsigned(2 downto 0) := to_unsigned(4, 3);
  signal cnt_cpu_q      : unsigned(2 downto 0);
  signal cnt_vdc_q      : unsigned(2 downto 0);
  signal clk_cpu_en_s,
         clk_vdc_en_s   : std_logic;

  signal pll_locked_s   : std_logic;
  signal reset_n_s      : std_logic;
  signal por_n_s        : std_logic;

  signal cart_a_s       : std_logic_vector(11 downto 0);
  --signal rom_a_s        : std_logic_vector(12 downto 0);
  signal cart_d_s : std_logic_vector( 7 downto 0);
   --      rom_d_s       
  signal cart_bs0_s,
         cart_bs1_s,
         cart_psen_n_s  : std_logic;

  signal keyb_dec_s     : std_logic_vector( 6 downto 1);
  signal keyb_enc_s     : std_logic_vector(14 downto 7);

  signal r_s,
         g_s,
         b_s,
         l_s            : std_logic;
  signal hsync_n_s,
         vsync_n_s      : std_logic;

  signal snd_s          : std_logic;


  signal joy_up_n_s,
         joy_down_n_s,
         joy_left_n_s,
         joy_right_n_s,
         joy_action_n_s : std_logic_vector( 1 downto 0);
  signal but_a_s,
         but_b_s,
         but_x_s,
         but_y_s,
         but_start_s,
         but_sel_s,
         but_tl_s,
         but_tr_s       : std_logic_vector( 1 downto 0);
  signal but_up_s,
         but_down_s,
         but_left_s,
         but_right_s    : std_logic_vector( 1 downto 0);

  signal dac_audio_s    : std_logic_vector( 7 downto 0);
  signal audio_s        : std_logic;
	signal snd_vec_s: std_logic_vector(3 downto 0);

  signal vdd_s          : std_logic;
  signal gnd_s          : std_logic;
  
  --PCM audio	
   signal pcm_lrclk			: std_logic;
	signal pcm_outl			: std_logic_vector(15 downto 0);
	signal pcm_outr			: std_logic_vector(15 downto 0);
	signal pcm_inl				: std_logic_vector(15 downto 0);

	signal keyb_out				: std_logic_vector(14 downto 7);
	
	signal reset_key_s		: std_logic := '1';

	
	signal hpos_s : std_logic_vector(8 downto 0);
	signal vpos_s : std_logic_vector(8 downto 0);
	
	signal col_i_s : std_logic_vector(3 downto 0);

begin

	-- Keyboard
	kb_vp: entity work.keyb_odyssey2 port map (
		CLK				=> clk_s,						
		nRESET			=> reset_n_s,						
		keyb_data		=> keyb_data,					
		keyb_valid		=> keyb_valid,						
		Key_dec     	=> keyb_dec_s,
		key_enc    		=> keyb_out,
		
		reset_key_n_o => reset_key_s,
		
		osd_o				=> osd_o
		           
	);
	
		 hpos_o <= hpos_s;
	 vpos_o <= vpos_s;


  vdd_s <= '1';
  gnd_s <= '0';


  por_b : vp_por
    generic map (
       delay_g     => 4,
       cnt_width_g => 2
    )
    port map (
       clk_i   => clk_s,
       por_n_o => por_n_s
    );


  reset_n_s <= ((but_tl_s(0) or but_tr_s(0))  and por_n_s) and reset_n and reset_key_s;




  -----------------------------------------------------------------------------
  -- Process clk_en
  --
  -- Purpose:
  --   Generates the CPU and VDC clock enables.
  --
  clk_en: process (clk_s, reset_n_s)
  begin
    if reset_n_s = '0' then
      cnt_cpu_q <= cnt_cpu_c;
      cnt_vdc_q <= cnt_vdc_c;
    elsif rising_edge(clk_s) then
      if clk_cpu_en_s = '1' then
        cnt_cpu_q <= cnt_cpu_c;
      else
        cnt_cpu_q <= cnt_cpu_q - 1;
      end if;
      --
      if clk_vdc_en_s = '1' then
        cnt_vdc_q <= cnt_vdc_c;
      else
        cnt_vdc_q <= cnt_vdc_q - 1;
      end if;
    end if;
  end process clk_en;
  --
  clk_cpu_en_s <= '1' when cnt_cpu_q = 0 else '0';
  clk_vdc_en_s <= '1' when cnt_vdc_q = 0 else '0';
  
  clk_en_o <= clk_cpu_en_s;
  --
  -----------------------------------------------------------------------------


  -----------------------------------------------------------------------------
  -- The Videopac console
  -----------------------------------------------------------------------------
  vp_console_b : vp_console
    generic map (
      is_pal_g => 1
    )
    port map (
      clk_i          => clk_s,
      clk_cpu_en_i   => clk_cpu_en_s,
      clk_vdc_en_i   => clk_vdc_en_s,
      res_n_i        => reset_n_s,
      cart_cs_o      => open,
      cart_cs_n_o    => open,
      cart_wr_n_o    => open,
      cart_a_o       => cart_a_s,
      cart_d_i       => cart_d_s,
      cart_bs0_o     => cart_bs0_s,
      cart_bs1_o     => cart_bs1_s,
      cart_psen_n_o  => cart_psen_n_s,
      cart_t0_i      => gnd_s,
      cart_t0_o      => open,
      cart_t0_dir_o  => open,
      -- idx = 0 : left joystick
      -- idx = 1 : right joystick
      joy_up_n_i     => joy_up_n_s,
      joy_down_n_i   => joy_down_n_s,
      joy_left_n_i   => joy_left_n_s,
      joy_right_n_i  => joy_right_n_s,
      joy_action_n_i => joy_action_n_s,
      keyb_dec_o     => keyb_dec_s,
      keyb_enc_i     => keyb_enc_s,
      r_o            => r_s,
      g_o            => g_s,
      b_o            => b_s,
      l_o            => l_s,
      hsync_n_o      => hsync_n_s,
      vsync_n_o      => vsync_n_s,
      hbl_o          => open,
      vbl_o          => open,
      snd_o          => snd_s,
      snd_vec_o      => snd_vec_s,
		hpos_o     		=> hpos_s,
		vpos_o  	  		=> vpos_s
    );
  --
  rgb: process (clk_s, reset_n_s)
   variable col_v : natural range 0 to 15;
  begin
    if reset_n_s = '0' then
      vga_r_o <= (others => '0');
      vga_g_o <= (others => '0');
      vga_b_o <= (others => '0');

    elsif rising_edge(clk_s) then
      col_v := to_integer(unsigned'(l_s & r_s & g_s & b_s));
      col_i_s <= std_logic_vector(to_unsigned(col_v, 4));
   --   vga_r_o <= std_logic_vector(to_unsigned(full_rgb_table_c(col_v)(r_c), 8))(7 downto 5) ;
   --   vga_g_o <= std_logic_vector(to_unsigned(full_rgb_table_c(col_v)(g_c), 8))(7 downto 5) ;
    --  vga_b_o <= std_logic_vector(to_unsigned(full_rgb_table_c(col_v)(b_c), 8))(7 downto 5) ;
    end if;
  end process rgb;
  --
 -- comp_sync_n_o <= hsync_n_s and vsync_n_s;

	vga_vsync_n_o <= not vsync_n_s;
	vga_hsync_n_o <= not hsync_n_s;
	
	color_index_o <= col_i_s;

  -----------------------------------------------------------------------------
  -- The cartridge ROM
  -----------------------------------------------------------------------------
  rom_a_s <= ( 0 => cart_a_s( 0),
               1 => cart_a_s( 1),
               2 => cart_a_s( 2),
               3 => cart_a_s( 3),
               4 => cart_a_s( 4),
               5 => cart_a_s( 5),
               6 => cart_a_s( 6),
               7 => cart_a_s( 7),
               8 => cart_a_s( 8),
               9 => cart_a_s( 9),
              10 => cart_a_s(11),
              11 => cart_bs0_s,
              12 => cart_bs1_s);
  --
  
  --
  cart_d_s <=   rom_d_s
              when cart_psen_n_s = '0' else
                (others => '1');

	cart_psen_n_o <= cart_psen_n_s;
  
  
  joy_up_n_s     <= (0 => joy1_up_i,     1 => joy2_up_i);
  joy_down_n_s   <= (0 => joy1_down_i,   1 => joy2_down_i);
  joy_left_n_s   <= (0 => joy1_left_i,   1 => joy2_left_i);
  joy_right_n_s  <= (0 => joy1_right_i,  1 => joy2_right_i);
  joy_action_n_s <= (0 => joy1_p6_i,     1 => joy2_p6_i);
  
--	keyb_enc_s( 7) <= btn_n_i(1);
--	keyb_enc_s( 8) <= btn_n_i(2);
--	keyb_enc_s( 9) <= btn_n_i(3);
--	keyb_enc_s( 10) <= btn_n_i(4);


  
  --keyb_enc_s( 7) <= '1';
  --keyb_enc_s( 8) <= keyb_dec_s(1) or but_tl_s(0) or but_a_s(0) OR KEY(2);
  --keyb_enc_s( 9) <= '1';
  --keyb_enc_s(10) <= '1';
  keyb_enc_s(14 downto 7) <= keyb_out(14 downto 7);
                   

	snd_vec_o      <= snd_vec_s;



end struct;
