

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


entity top is
	generic (
		hdmi_output_g	: boolean	:= false
	);
	port (
			-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
	
		
		
	);
end entity;

architecture Behavior of top is

	-- Reset signal
	signal reset_n				: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset			: std_logic;		-- Reset do PLL
	signal pll_locked			: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clk_28		: std_logic;
	signal odyssey_clk		: std_logic;
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	
	
	signal port_243b : std_logic_vector(7 downto 0);
	
	signal ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal ram_din				: std_logic_vector(15 downto 0);
	signal ram_dout			: std_logic_vector(15 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector(7 downto 0);
	
	signal from_sram			: std_logic_vector(15 downto 0);
	signal to_sram				: std_logic_vector(15 downto 0);
	
		-- ram
	signal loader_ram_a		: std_logic_vector(17 downto 0);		-- 512K
	signal loader_to_sram	: std_logic_vector(15 downto 0);
	signal loader_from_sram	: std_logic_vector(15 downto 0);
	signal loader_ram_data	: std_logic_vector(15 downto 0);
	signal loader_ram_cs		: std_logic;
	signal loader_ram_oe		: std_logic;
	signal loader_ram_we		: std_logic;

	signal a2601_ram_a		: std_logic_vector(11 downto 0);	
	signal a2601_ram_dout	: std_logic_vector(7 downto 0);
	

	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	
	
	--rgb
	signal rgb_loader_out		: std_logic_vector(7 downto 0);
	signal rgb_odyssey_out		: std_logic_vector(7 downto 0);
	
	signal hsync_loader_out		: std_logic;
	signal vsync_loader_out		: std_logic;
	
	signal hsync_odyssey_out	: std_logic;
	signal vsync_odyssey_out	: std_logic;
	
	signal cart_a        		: std_logic_vector(12 downto 0);
	signal cart_d 					: std_logic_vector( 7 downto 0);
  
	signal odyssey_reset_n_s 	: std_logic;
          
	-- PS/2
	signal keyb_data_s 			: std_logic_vector(7 downto 0);
	signal keyb_valid_s 			: std_logic;
	signal clk_keyb 				: std_logic;
	
	-- HDMI
	signal clk_module_vga		: std_logic;
	signal clk_vga					: std_logic;
	signal clk_dvi					: std_logic;
	signal color_s 				: std_logic_vector(3 downto 0);
	signal vga_color_s 			: std_logic_vector(3 downto 0);
	signal vga_hsync_n_s 		: std_logic;
	signal vga_vsync_n_s 		: std_logic;
	signal vga_blank_s 			: std_logic;
	
	signal cnt_hor_s 				: std_logic_vector(8 downto 0);
	signal cnt_ver_s 				: std_logic_vector(8 downto 0);
	signal sound_hdmi_s			: std_logic_vector(15 downto 0);
	signal tdms_s					: std_logic_vector( 7 downto 0);
	
	signal loader_hor_s 			: std_logic_vector(8 downto 0);
	signal loader_ver_s 			: std_logic_vector(8 downto 0);
	signal vp_hor_s 				: std_logic_vector(8 downto 0);
	signal vp_ver_s 				: std_logic_vector(8 downto 0);
	signal vp_color_index_s 	: std_logic_vector(3 downto 0);
	signal vga_rgb_s 				: std_logic_vector(7 downto 0);

	signal vga_rgb_out_s 		: std_logic_vector(7 downto 0);
	
	signal snd_vec_s				: std_logic_vector(3 downto 0);
	signal audio_s 				: std_logic;
	
begin

 	ps2 : work.ps2_intf port map 
	(
		clk_keyb,
		reset_n,
		ps2_clk_io,
		ps2_data_io,
		keyb_data_s,
		keyb_valid_s,
		open
	);
	
	clk_keyb <= clk_28 when odyssey_reset_n_s = '0' else sysclk;
  

	pll: work.pll_odyssey port map (
		areset		=> pll_reset,				-- PLL Reset
		inclk0		=> clock_50_i,				-- Clock 50 MHz externo
		c0				=> sysclk,					
		c1				=> vid_clk,					
		c2				=> clk_28,		
		c3				=> clk_vga,			
		c4				=> clk_dvi,			
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
	
	pll_reset	<= '1' when btn_n_i(3) = '0' and btn_n_i(4) = '0' else '0';
	reset_n		<= not (pll_reset or not pll_locked);	-- System is reset by external reset switch or PLL being out of lock
	
	

	
	
	loader : work.speccy48_top port map (
		
		clk_28      => clk_28,
		reset_n_i => reset_n,
	
		-- VGA
		VGA_R(3 downto 1) => rgb_loader_out (7 downto 5),    
		VGA_G(3 downto 1) => rgb_loader_out (4 downto 2),    
		VGA_B(3 downto 2) => rgb_loader_out (1 downto 0),   
		VGA_HS         	=> hsync_loader_out,  
		VGA_VS         	=> vsync_loader_out,   

		
		-- PS/2 Keyboar   -- PS/2 Ke
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  
   
   
		SRAM_ADDR      => loader_ram_a,
		FROM_SRAM      => loader_from_sram,
		TO_SRAM       	=> loader_to_sram,
		SRAM_CE_N      => loader_ram_cs,
		SRAM_OE_N      => loader_ram_oe,
		SRAM_WE_N      => loader_ram_we,
                  
				  
		SD_nCS         => sd_cs_n_o,
		SD_MOSI        => sd_mosi_o, 
		SD_SCLK        => sd_sclk_o, 
		SD_MISO        => sd_miso_i,
	
		PORT_243B      => port_243b,
		
		JOYSTICK			=> "000" & not (joy1_p6_i and joy2_p6_i) &  not (joy1_up_i and joy2_up_i) &  not (joy1_down_i and joy2_down_i) & not (joy1_left_i and joy2_left_i) & not (joy1_right_i and joy2_right_i),
		
		cnt_h_o      => loader_hor_s,
		cnt_v_o      => loader_ver_s
		
	);
	
	
	
	
	sram_addr_o   <= "0" & ram_a;
	sram_data_io  <= to_sram(7 downto 0) when ram_we = '0' else (others=>'Z');
	from_sram(7 downto 0) 	  <= sram_data_io;
	sram_ce_n_o(0)<= ram_cs;
   sram_oe_n_o   <= ram_oe;
	sram_we_n_o   <= ram_we;
	
	--------------------------------------------------------------------------------

	
	odyssey2: work.top_odyssey2 port map (
  
	-- Clocks
		clk_s			=> odyssey_clk,

		reset_n   => odyssey_reset_n_s,
		
		-- Buttons
		btn_n_i				=> btn_n_i,

		-- PS2
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  

		-- Joystick
		joy1_up_i			=> joy1_up_i,
		joy1_down_i			=> joy1_down_i,
		joy1_left_i			=> joy1_left_i,
		joy1_right_i		=> joy1_right_i,
		joy1_p6_i			=> joy1_p6_i,
		joy1_p7_o			=> joy1_p7_o,
		joy1_p9_i			=> joy1_p9_i,
		joy2_up_i			=> joy2_up_i,
		joy2_down_i			=> joy2_down_i,
		joy2_left_i			=> joy2_left_i,
		joy2_right_i		=> joy2_right_i,
		joy2_p6_i			=> joy2_p6_i,
		joy2_p7_o			=> joy2_p7_o,
		joy2_p9_i			=> joy2_p9_i,

		-- Audio
		snd_vec_o			=> snd_vec_s,


		-- VGA
		vga_r_o				=> rgb_odyssey_out(7 downto 5),
		vga_g_o				=> rgb_odyssey_out(4 downto 2),
		vga_b_o(2 downto 1)=> rgb_odyssey_out(1 downto 0),
		vga_hsync_n_o		=> hsync_odyssey_out,
		vga_vsync_n_o		=> vsync_odyssey_out,
		
		color_index_o => vp_color_index_s,


		-- Debug
		leds_n_o				=> open,
		
		-- cartridge
		rom_a_s => cart_a,
		rom_d_s => cart_d,
		
		hpos_o     => vp_hor_s,
		vpos_o  	  => vp_ver_s
    
  );

	  
  dac_b : work.dac
    generic map (
      msbi_g => 7
    )
    port map (
      clk_i   => clk_module_vga,
      res_n_i => reset_n,
      dac_i   => "0" & snd_vec_s & "000",
      dac_o   => audio_s
    );
  --
  dac_l_o <= audio_s;
  dac_r_o <= audio_s;
	

	---------------------------------------------------
	
	
	process(port_243b)
	begin
		if port_243b(7) = '1' then --magic bit to start 
			
			--ODYSSEY 2
			
			odyssey_clk 	<= sysclk;
			ram_a				<= "00100" & cart_a;
			to_sram			<= (others=>'Z');
			cart_d			<= from_sram(7 downto 0);
			ram_cs			<= '0';
			ram_oe			<= '0';
			ram_we			<= '1';
			
			--vga_r_s  <= rgb_odyssey_out (7 downto 5);
			--vga_g_s  <= rgb_odyssey_out (4 downto 2);
			--vga_b_c  <= rgb_odyssey_out (1 downto 0) & rgb_odyssey_out (0);
			color_s <=  vp_color_index_s; --"0" & rgb_odyssey_out(7)& rgb_odyssey_out(4)& rgb_odyssey_out(1);
		--	vga_hsync_n_s <= hsync_odyssey_out;
		--	vga_vsync_n_s <= vsync_odyssey_out;
			
			odyssey_reset_n_s <= '1';
			
			clk_module_vga <= sysclk;
			
			cnt_hor_s <= vp_hor_s;-- - 20;---25; 
			cnt_ver_s <= vp_ver_s;
			
			sound_hdmi_s <= "0000" & snd_vec_s & "00000000";
			
		else
			
			--LOADER

			ram_a				<= loader_ram_a;
			
			to_sram			<= loader_to_sram;
			loader_from_sram <= from_sram;
			
			ram_cs			<= loader_ram_cs;
			ram_oe			<= loader_ram_oe;
			ram_we			<= loader_ram_we;
			
			color_s  <= "0" & rgb_loader_out(7)& rgb_loader_out(4)& rgb_loader_out(1);
			--vga_hsync_n_s <= hsync_loader_out;
			--vga_vsync_n_s <= vsync_loader_out;
			
			odyssey_reset_n_s <= '0';
			
			clk_module_vga <= clk_28;
			cnt_hor_s <= loader_hor_s;
			cnt_ver_s <= loader_ver_s;
			
			sound_hdmi_s <=(others=>'0');
			
		end if;
	end process;
	
	
	
--	
	
	
	-- VGA framebuffer
	vga: entity work.vga
	port map (
		I_CLK			=> clk_module_vga,
		I_CLK_VGA	=> clk_vga,
		I_COLOR		=> color_s,
		I_HCNT		=> cnt_hor_s,
		I_VCNT		=> cnt_ver_s,
		O_HSYNC		=> vga_hsync_n_s,
		O_VSYNC		=> vga_vsync_n_s,
		O_COLOR		=> vga_color_s,
		O_HCNT		=> open,
		O_VCNT		=> open,
		O_H			=> open,
		O_BLANK		=> vga_blank_s
	);
	
	
	
	process (vga_color_s)
	begin
		case vga_color_s is
			when "0000" => vga_rgb_s <= "00000000"; --X"000000"; -- 000 000 000
			when "0001" => vga_rgb_s <= "00000111"; --X"0e3dd4"; -- 000 001 110
			when "0010" => vga_rgb_s <= "00010000"; --X"00981b"; -- 000 100 000
			when "0011" => vga_rgb_s <= "00010111"; --X"00bbd9"; -- 000 101 110
			when "0100" => vga_rgb_s <= "11000000"; --X"c70008"; -- 110 000 000
			when "0101" => vga_rgb_s <= "11000010"; --X"cc16b3"; -- 110 000 101
			when "0110" => vga_rgb_s <= "10010000"; --X"9d8710"; -- 100 100 000
			when "0111" => vga_rgb_s <= "11111011"; --X"e1dee1"; -- 111 110 111
			when "1000" => vga_rgb_s <= "01001101"; --X"5f6e6b"; -- 010 011 011
			when "1001" => vga_rgb_s <= "01110111"; --X"6aa1ff"; -- 011 101 111
			when "1010" => vga_rgb_s <= "00111101"; --X"3df07a"; -- 001 111 011
			when "1011" => vga_rgb_s <= "00111111"; --X"31ffff"; -- 001 111 111
			when "1100" => vga_rgb_s <= "11101001"; --X"ff4255"; -- 111 010 010
			when "1101" => vga_rgb_s <= "11110011"; --X"ff98ff"; -- 111 100 111
			when "1110" => vga_rgb_s <= "11010101"; --X"d9ad5d"; -- 110 101 010
			when "1111" => vga_rgb_s <= "11111111"; --X"ffffff"; -- 111 111 111
		end case;
	end process;
	

	

	
	
	
	uh: if hdmi_output_g generate
			-- HDMI
		inst_dvid: entity work.hdmi
		generic map (
			FREQ	=> 25000000,	-- pixel clock frequency 
			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
			CTS	=> 25000,		-- CTS = Freq(pixclk) * N / (128 * Fs)
			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
		) 
		port map (
			I_CLK_VGA		=> clk_vga,
			I_CLK_TMDS		=> clk_dvi,
			
			I_RED				=> vga_rgb_out_s(7 downto 5) & vga_rgb_out_s(7 downto 5) & vga_rgb_out_s(7 downto 6),
			I_GREEN			=> vga_rgb_out_s(4 downto 2) & vga_rgb_out_s(4 downto 2) & vga_rgb_out_s(4 downto 3),
			I_BLUE			=> vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(1 downto 0),
			
			I_BLANK			=> vga_blank_s,
			I_HSYNC			=> vga_hsync_n_s,
			I_VSYNC			=> vga_vsync_n_s,
			I_AUDIO_PCM_L 	=> sound_hdmi_s,
			I_AUDIO_PCM_R	=> sound_hdmi_s,
			O_TMDS			=> tdms_s
		);
		

		vga_hsync_n_o	<= tdms_s(7);	-- 2+		10
		vga_vsync_n_o	<= tdms_s(6);	-- 2-		11
		vga_b_o(2)		<= tdms_s(5);	-- 1+		144	
		vga_b_o(1)		<= tdms_s(4);	-- 1-		143
		vga_r_o(0)		<= tdms_s(3);	-- 0+		133
		vga_g_o(2)		<= tdms_s(2);	-- 0-		132
		vga_r_o(1)		<= tdms_s(1);	-- CLK+	113
		vga_r_o(2)		<= tdms_s(0);	-- CLK-	112
	end generate;


	nuh: if not hdmi_output_g generate
		vga_r_o			<= vga_rgb_out_s(7 downto 5);
		vga_g_o			<= vga_rgb_out_s(4 downto 2);
		vga_b_o			<= vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(0);
		vga_hsync_n_o	<= vga_hsync_n_s;
		vga_vsync_n_o	<= vga_vsync_n_s;
	end generate;
	
	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_n_i(1) or btn_n_i(2),
		result_o			=> btn_scan_s
	);
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	
	
	vga_rgb_out_s <= '0' & vga_rgb_s(6 downto 5) & '0' & vga_rgb_s(3 downto 2)& '0' & vga_rgb_s(0) when scanlines_en_s = '1' and odd_line_s = '1' else vga_rgb_s;
	
	
	
	process(vga_hsync_n_s,vga_vsync_n_s)
	begin
		if vga_vsync_n_s = '0' then
			odd_line_s <= '0';
		elsif rising_edge(vga_hsync_n_s) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;
	


end architecture;
