

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE1 board
entity top is
	port (
		-- Clocks
		CLOCK_24       : in    std_logic_vector(1 downto 0);
		CLOCK_27       : in    std_logic_vector(1 downto 0);
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(9 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);
		 
		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		-- Red LEDs
		LEDR           : out   std_logic_vector(9 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(7 downto 0)		:= (others => '0');
		 
		-- VGA
		VGA_R          : out   std_logic_vector(3 downto 0)		:= (others => '1');
		VGA_G          : out   std_logic_vector(3 downto 0)		:= (others => '1');
		VGA_B          : out   std_logic_vector(3 downto 0)		:= (others => '1');
		VGA_HS         : out   std_logic									:= '1';
		VGA_VS         : out   std_logic									:= '1';
		 
		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';
		 
		-- PS/2 Keyboard
		PS2_CLK        : in std_logic;
		PS2_DAT        : in std_logic;

		-- I2C
		I2C_SCLK       : inout std_logic;
		I2C_SDAT       : inout std_logic;

		-- Audio
		AUD_XCK        : out   std_logic									:= '1';
		AUD_BCLK       : out   std_logic									:= '1';
		AUD_ADCLRCK    : out   std_logic									:= '1';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '1';
		AUD_DACDAT     : out   std_logic									:= '1';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '1');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '1');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '1');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '1');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';
		 
		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '1');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => '1');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';
		 
		-- SD card (SPI mode)
		SD_nCS         : out   std_logic									:= '1';
		SD_MOSI        : out   std_logic									:= '1';
		SD_SCLK        : out   std_logic									:= '1';
		SD_MISO        : in    std_logic;
		 
		-- GPIO
		GPIO_0         : inout std_logic_vector(31 downto 0)		:= (others => '1');
		
		
				-- PS/2
		GPIO_PS2_CLK1        : inout std_logic;
		GPIO_PS2_DAT1        : inout std_logic;

				-- PS/2
		GPIO_PS2_CLK2        : in std_logic;
		GPIO_PS2_DAT2        : in std_logic;

		
		GPIO_1         : inout std_logic_vector(10 downto 0)		:= (others => '1');
		
		
		GPIO_D : inout std_logic_vector(7 downto 0)		:= (others => 'Z');
		GPIO_WR : out   std_logic									:= '1';
		GPIO_RD : out   std_logic									:= '1';
		GPIO_A0 : out   std_logic									:= '1';
		GPIO_CS : out   std_logic									:= '1';
		
		GPIO_INT : in   std_logic	
		
		
	);
end entity;

architecture Behavior of top is

	component lpm_rom
    generic (
      LPM_WIDTH           : positive;
      LPM_WIDTHAD         : positive;
      LPM_NUMWORDS        : natural := 0;
      LPM_ADDRESS_CONTROL : string  := "REGISTERED";
      LPM_OUTDATA         : string  := "REGISTERED";
      LPM_FILE            : string;
      LPM_TYPE            : string  := "LPM_ROM";
      LPM_HINT            : string  := "UNUSED"
    );
    port (
      ADDRESS  : in STD_LOGIC_VECTOR(LPM_WIDTHAD-1 downto 0);
      INCLOCK  : in STD_LOGIC := '0';
      MEMENAB  : in STD_LOGIC := '1';
      Q        : out STD_LOGIC_VECTOR(LPM_WIDTH-1 downto 0)
    );
  end component;
  

	-- Reset signal
	signal reset_n				: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset			: std_logic;		-- Reset do PLL
	signal pll_locked			: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clk_28		: std_logic;
	signal odyssey_clk		: std_logic;
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	
	
	signal port_243b : std_logic_vector(7 downto 0);
	
		signal ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal ram_din				: std_logic_vector(15 downto 0);
	signal ram_dout				: std_logic_vector(15 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector(7 downto 0);
	
	signal from_sram			: std_logic_vector(15 downto 0);
	signal to_sram			: std_logic_vector(15 downto 0);
	
		-- ram
	signal loader_ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal loader_to_sram			: std_logic_vector(15 downto 0);
	signal loader_from_sram			: std_logic_vector(15 downto 0);
	signal loader_ram_data			: std_logic_vector(15 downto 0);
	signal loader_ram_cs				: std_logic;
	signal loader_ram_oe				: std_logic;
	signal loader_ram_we				: std_logic;

	signal a2601_ram_a				: std_logic_vector(11 downto 0);	
	signal a2601_ram_dout			: std_logic_vector(7 downto 0);
	


	
	
	--rgb
	signal rgb_loader_out			: std_logic_vector(7 downto 0);
	signal rgb_odyssey_out				: std_logic_vector(7 downto 0);
	
	signal hsync_loader_out			: std_logic;
	signal vsync_loader_out			: std_logic;
	
	signal hsync_odyssey_out			: std_logic;
	signal vsync_odyssey_out			: std_logic;
	
	signal cart_a        : std_logic_vector(12 downto 0);
	signal cart_d : std_logic_vector( 7 downto 0);
  
	signal odyssey_reset_n_s : std_logic;
          
	-- PS/2
	signal keyb_data_s : std_logic_vector(7 downto 0);
	signal keyb_valid_s : std_logic;
	
begin

	ps2 : work.ps2_intf port map 
	(
		clk_28,
		reset_n,
		PS2_CLK,
		PS2_DAT,
		keyb_data_s,
		keyb_valid_s,
		open
	);

 
  

	pll: work.pll_odyssey port map (
		areset		=> pll_reset,				-- PLL Reset
		inclk0		=> CLOCK_50,				-- Clock 50 MHz externo
		c0				=> sysclk,					
	--	c1				=> vid_clk,					
		c2				=> clk_28,					
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
	pll_reset	<= not KEY(0);
	reset_n		<= not (pll_reset or not pll_locked);	-- System is reset by external reset switch or PLL being out of lock
	
	loader : work.speccy48_top port map (
		
		clk_28   => clk_28,
		reset_n 	=> reset_n,
	
		-- VGA
		VGA_R(3 downto 1) => rgb_loader_out (7 downto 5),    
		VGA_G(3 downto 1) => rgb_loader_out (4 downto 2),    
		VGA_B(3 downto 2) => rgb_loader_out (1 downto 0),   
		VGA_HS         	=> hsync_loader_out,  
		VGA_VS         	=> vsync_loader_out,   

		-- PS/2 Keyboar   -- PS/2 Ke
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  
   
		SRAM_ADDR      => loader_ram_a,
		FROM_SRAM      => loader_from_sram,
		TO_SRAM       	=> loader_to_sram,
		SRAM_CE_N      => loader_ram_cs,
		SRAM_OE_N      => loader_ram_oe,
		SRAM_WE_N      => loader_ram_we,
		  
		SD_nCS         => SD_nCS,
		SD_MOSI        => SD_MOSI, 
		SD_SCLK        => SD_SCLK, 
		SD_MISO        => SD_MISO,
		
		PORT_243B      => port_243b
		
	);
	
	
	
	
	SRAM_ADDR   <= ram_a;
	SRAM_DQ 		<= to_sram when ram_we = '0' else (others=>'Z');
	from_sram 	<= SRAM_DQ;
	SRAM_CE_N   <= ram_cs;
   SRAM_OE_N   <= ram_oe;
	SRAM_WE_N   <= ram_we;
   SRAM_UB_N   <= '1';
	SRAM_LB_N   <= '0';
	
	--------------------------------------------------------------------------------
	
--	cart_rom_b : lpm_rom
--   generic map (
--     LPM_WIDTH           =>  8,
--     LPM_WIDTHAD         => 13,
--     LPM_ADDRESS_CONTROL => "REGISTERED",
--     LPM_OUTDATA         => "UNREGISTERED",
--     LPM_FILE            => "cart.hex",
--     LPM_TYPE            => "LPM_ROM"
--   )
--   port map (
--     ADDRESS  => cart_a(12 downto 0),
--     INCLOCK  => odyssey_clk,
--     MEMENAB  => '1',
--     Q        => cart_d
--   );
--	 
	 ---------------------------------------------------------------
	
	odyssey2: work.top_odyssey2 port map (
  
	-- Clocks
		clk_s			=> sysclk,
		
		reset_n   => odyssey_reset_n_s,

		-- Buttons
		btn_n_i				=> KEY(3 downto 1)&'1',

		-- PS2
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  

		-- Joystick
		joy1_up_i			=> '0',
		joy1_down_i			=> '0',
		joy1_left_i			=> '0',
		joy1_right_i		=> '0',
		joy1_p6_i			=> '0',
		joy1_p7_o			=> open,
		joy1_p9_i			=> '0',
		joy2_up_i			=> '0',
		joy2_down_i			=> '0',
		joy2_left_i			=> '0',
		joy2_right_i		=> '0',
		joy2_p6_i			=> '0',
		joy2_p7_o			=> open,
		joy2_p9_i			=> '0',

		-- Audio
		dac_l_o				=> open,
		dac_r_o				=> open,


		-- VGA
		vga_r_o				=> rgb_odyssey_out(7 downto 5),
		vga_g_o				=> rgb_odyssey_out(4 downto 2),
		vga_b_o(2 downto 1)=> rgb_odyssey_out(1 downto 0),
		vga_hsync_n_o		=> hsync_odyssey_out,
		vga_vsync_n_o		=> vsync_odyssey_out,


		-- Debug
		leds_n_o				=> open,
		
		-- cartridge
		rom_a_s => cart_a,
		rom_d_s => cart_d
    
  );

	
	
	
	
	
	
	
	
	
	
	
	---------------------------------------------------
	
	
	process(port_243b)
	begin
		if port_243b(7) = '1' then --magic bit to start 
			
			--ODYSSEY 2
			
			--odyssey_clk 	<= sysclk;
			ram_a				<= "00100" & cart_a;
			to_sram			<= (others=>'Z');
			cart_d			<= from_sram(7 downto 0);
			ram_cs			<= '0';
			ram_oe			<= '0';
			ram_we			<= '1';
			
			VGA_R  <= rgb_odyssey_out (7 downto 5) & '0';
			VGA_G  <= rgb_odyssey_out (4 downto 2) & '0';
			VGA_B  <= rgb_odyssey_out (1 downto 0) & rgb_odyssey_out (0) &'0';
			VGA_HS <= hsync_odyssey_out;
			VGA_VS <= vsync_odyssey_out;
			
			odyssey_reset_n_s <= '1';
			
		else
			
			--LOADER
			

			ram_a				<= loader_ram_a;
			
			to_sram			<= loader_to_sram;
			loader_from_sram <= from_sram;
			
			ram_cs			<= loader_ram_cs;
			ram_oe			<= loader_ram_oe;
			ram_we			<= loader_ram_we;
			
			VGA_R  <= rgb_loader_out (7 downto 5) & '0';
			VGA_G  <= rgb_loader_out (4 downto 2) & '0';
			VGA_B  <= rgb_loader_out (1 downto 0) & rgb_loader_out (0) &'0';
			VGA_HS <= hsync_loader_out;
			VGA_VS <= vsync_loader_out;
			
			odyssey_reset_n_s <= '0';
			
		end if;
	end process;


	
	
	----------------------------
	-- debugs
	
	ledr(7 downto 0) <= port_243b;
	
		-- Led display
	ld3: work.seg7 port map(
		D		=> "0000",
		Q		=> HEX3
	);

	ld2: work.seg7 port map(
		D		=> "0000",
		Q		=> HEX2
	);

	ld1: work.seg7 port map(
		D		=> "0000",
		Q		=> HEX1
	);

	ld0: work.seg7 port map(
		D		=> "0000",
		Q		=> HEX0
	);
	
	

end architecture;
