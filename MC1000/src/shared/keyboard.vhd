--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- * Redistributions in synthesized form must reproduce the above copyright
--   notice, this list of conditions and the following disclaimer in the
--   documentation and/or other materials provided with the distribution.
--
-- * Neither the name of the author nor the names of other contributors may
--   be used to endorse or promote products derived from this software without
--   specific prior written agreement from the author.
--
-- * License is granted for non-commercial use only.  A fee may not be charged
--   for redistributions as source code or in synthesized/hardware form without
--   specific prior written agreement from the author.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--

-- PS/2 scancode to matrix conversion
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.keyscans.all;

entity keyboard is
	generic (
		clkfreq_g	: integer										-- This is the system clock value in kHz
	);
	port (
		clock_i		: in    std_logic;
		enable_i		: in    std_logic;
		reset_i		: in    std_logic;
		-- PS/2 interface
		ps2_clk_io	: inout std_logic;
		ps2_data_io	: inout std_logic;
		-- Row
		rows_i		: in    std_logic_vector(7 downto 0);
		-- Column
		cols_o		: out   std_logic_vector(7 downto 0);
		FKeys_o		: out   std_logic_vector(12 downto 1)		-- Function Keys
	);
end keyboard;

architecture rtl of keyboard is

	-- Interface to PS/2 block
	signal keyb_data_s		: std_logic_vector(7 downto 0);
	signal keyb_valid_s		: std_logic;

	-- Internal signals
	type key_matrix_t is array (7 downto 0) of std_logic_vector(7 downto 0);
	signal keys_s				: key_matrix_t;
	signal release_s			: std_logic;
	signal extended_s			: std_logic;
	signal k1_s, k2_s,
			k3_s, k4_s,
			k5_s, k6_s,
			k7_s, k8_s			: std_logic_vector(7 downto 0);
	signal idata_s				: std_logic_vector(7 downto 0);
	signal idata_rdy_s		: std_logic                     := '0';

begin

	-- PS/2 interface
	ps2 : entity work.ps2_iobase
	generic map (
		clkfreq			=> clkfreq_g
	)
	port map (
		enable			=> enable_i,
		clock				=> clock_i,
		reset				=> reset_i,
		ps2_clk			=> ps2_clk_io,
		ps2_data			=> ps2_data_io,
		idata_rdy		=> idata_rdy_s,
		idata				=> idata_s,
		send_rdy			=> open,
		odata_rdy		=> keyb_valid_s,
		odata				=> keyb_data_s
	);

	-- Mesclagem das linhas
	k1_s		<= keys_s(0) when rows_i(0) = '0' else (others => '1');
	k2_s		<= keys_s(1) when rows_i(1) = '0' else (others => '1');
	k3_s		<= keys_s(2) when rows_i(2) = '0' else (others => '1');
	k4_s		<= keys_s(3) when rows_i(3) = '0' else (others => '1');
	k5_s		<= keys_s(4) when rows_i(4) = '0' else (others => '1');
	k6_s		<= keys_s(5) when rows_i(5) = '0' else (others => '1');
	k7_s		<= keys_s(6) when rows_i(6) = '0' else (others => '1');
	k8_s		<= keys_s(7) when rows_i(7) = '0' else (others => '1');
	cols_o	<= k1_s and k2_s and k3_s and k4_s and k5_s and k6_s and k7_s and k8_s;
	
	process (reset_i, clock_i)
		variable keyb_valid_edge_v	: std_logic_vector(1 downto 0)	:= "00";
		variable sendresp_v			: std_logic := '0';
	begin
		if reset_i = '1' then
			keyb_valid_edge_v	:= "00";
			release_s 			<= '0';
			extended_s 			<= '0';

			keys_s(0) <= (others => '1');
			keys_s(1) <= (others => '1');
			keys_s(2) <= (others => '1');
			keys_s(3) <= (others => '1');
			keys_s(4) <= (others => '1');
			keys_s(5) <= (others => '1');
			keys_s(6) <= (others => '1');
			keys_s(7) <= (others => '1');

			FKeys_o	<= (others => '0');

		elsif rising_edge(clock_i) then
			keyb_valid_edge_v := keyb_valid_edge_v(0) & keyb_valid_s;
			if keyb_valid_edge_v = "01" then
				if keyb_data_s = X"AA" then
					sendresp_v := '1';
				elsif keyb_data_s = X"E0" then
					-- Extended key code follows
					extended_s <= '1';
				elsif keyb_data_s = X"F0" then
					-- Release code follows
					release_s <= '1';
				else
					-- Cancel extended/release flags for next time
					release_s	<= '0';
					extended_s	<= '0';

					if extended_s = '0' then
						-- Normal scancodes
						case keyb_data_s is
							when KEY_LSHIFT | KEY_RSHIFT	=> keys_s(0)(6) <= release_s;
																		keys_s(1)(6) <= release_s;
																		keys_s(2)(6) <= release_s;
																		keys_s(3)(6) <= release_s;
																		keys_s(4)(6) <= release_s;
																		keys_s(5)(6) <= release_s;
																		keys_s(6)(6) <= release_s;
																		keys_s(7)(6) <= release_s;
							when KEY_LCTRL			=> keys_s(0)(7) <= release_s;
															keys_s(1)(7) <= release_s;
															keys_s(2)(7) <= release_s;
															keys_s(3)(7) <= release_s;
															keys_s(4)(7) <= release_s;
															keys_s(5)(7) <= release_s;
															keys_s(6)(7) <= release_s;
															keys_s(7)(7) <= release_s;

							when KEY_Z      		=> keys_s(2)(3) <= release_s; -- Z
							when KEY_X 				=> keys_s(0)(3) <= release_s; -- X
							when KEY_C 				=> keys_s(3)(0) <= release_s; -- C
							when KEY_V 				=> keys_s(6)(2) <= release_s; -- V

							when KEY_A 				=> keys_s(1)(0) <= release_s; -- A
							when KEY_S 				=> keys_s(3)(2) <= release_s; -- S
							when KEY_D 				=> keys_s(4)(0) <= release_s; -- D
							when KEY_F 				=> keys_s(6)(0) <= release_s; -- F
							when KEY_G 				=> keys_s(7)(0) <= release_s; -- G

							when KEY_Q 				=> keys_s(1)(2) <= release_s; -- Q
							when KEY_W 				=> keys_s(7)(2) <= release_s; -- W
							when KEY_E 				=> keys_s(5)(0) <= release_s; -- E
							when KEY_R 				=> keys_s(2)(2) <= release_s; -- R
							when KEY_T 				=> keys_s(4)(2) <= release_s; -- T

							when KEY_1 | KEY_KP1	=> keys_s(1)(4) <= release_s; -- 1
							when KEY_2 | KEY_KP2 => keys_s(2)(4) <= release_s; -- 2
							when KEY_3 | KEY_KP3	=> keys_s(3)(4) <= release_s; -- 3
							when KEY_4 | KEY_KP4 => keys_s(4)(4) <= release_s; -- 4
							when KEY_5 | KEY_KP5 => keys_s(5)(4) <= release_s; -- 5
							when KEY_6 | KEY_KP6 => keys_s(6)(4) <= release_s; -- 6
							when KEY_7 | KEY_KP7 => keys_s(7)(4) <= release_s; -- 7
							when KEY_8 | KEY_KP8 => keys_s(0)(5) <= release_s; -- 8
							when KEY_9 | KEY_KP9 => keys_s(1)(5) <= release_s; -- 9
							when KEY_0 | KEY_KP0 => keys_s(0)(4) <= release_s; -- 0

							when KEY_P 				=> keys_s(0)(2) <= release_s; -- P
							when KEY_O 				=> keys_s(7)(1) <= release_s; -- O
							when KEY_I 				=> keys_s(1)(1) <= release_s; -- I
							when KEY_U 				=> keys_s(5)(2) <= release_s; -- U
							when KEY_Y 				=> keys_s(1)(3) <= release_s; -- Y

							when KEY_ENTER 		=> keys_s(3)(3) <= release_s; -- ENTER
							when KEY_L 				=> keys_s(4)(1) <= release_s; -- L
							when KEY_K 				=> keys_s(3)(1) <= release_s; -- K
							when KEY_J 				=> keys_s(2)(1) <= release_s; -- J
							when KEY_H 				=> keys_s(0)(1) <= release_s; -- H

							when KEY_SPACE 		=> keys_s(4)(3) <= release_s; -- SPACE
						
							when KEY_M 				=> keys_s(5)(1) <= release_s; -- M
							when KEY_N 				=> keys_s(6)(1) <= release_s; -- N
							when KEY_B 				=> keys_s(2)(0) <= release_s; -- B

							when KEY_BACKSPACE	=> keys_s(5)(3) <= release_s;
							when KEY_ESC			=> keys_s(7)(3) <= release_s;
							when KEY_BL				=> keys_s(6)(3) <= release_s; -- ' 

							when KEY_MINUS	| KEY_KPMINUS		=> keys_s(5)(5) <= release_s; -- -

							when KEY_EQUAL			=> keys_s(5)(5) <= release_s; -- = 
															keys_s(5)(6) <= release_s;

							when KEY_COMMA			=> keys_s(4)(5) <= release_s; -- , 

							when KEY_POINT	| KEY_KPCOMMA		=> keys_s(6)(5) <= release_s; -- . 

							when KEY_SLASH			=> keys_s(7)(5) <= release_s; -- / 
							when KEY_CCEDIL		=> keys_s(2)(5) <= release_s; -- ; 
							when KEY_TWOPOINT 	=> keys_s(3)(5) <= release_s; -- :
							when KEY_KPASTER		=> keys_s(2)(5) <= release_s; -- * 
															keys_s(2)(6) <= release_s;
							when KEY_KPPLUS		=> keys_s(3)(5) <= release_s; -- + 
															keys_s(3)(6) <= release_s;

							-- Teclas para o FPGA e nao para o Speccy
							when KEY_F1				=> FKeys_o(1)	<= not release_s;
							when KEY_F2				=> FKeys_o(2)	<= not release_s;
							when KEY_F3				=> FKeys_o(3)	<= not release_s;
							when KEY_F4				=> FKeys_o(4)	<= not release_s;
							when KEY_F5				=> FKeys_o(5)	<= not release_s;
							when KEY_F6				=> FKeys_o(6)	<= not release_s;
							when KEY_F7				=> FKeys_o(7)	<= not release_s;
							when KEY_F8				=> FKeys_o(8)	<= not release_s;
							when KEY_F9				=> FKeys_o(9)	<= not release_s;
							when KEY_F10			=> FKeys_o(10)	<= not release_s;
							when KEY_F11			=> FKeys_o(11)	<= not release_s;
							when KEY_F12			=> FKeys_o(12)	<= not release_s;

							when others =>
								null;
						end case;
					else
						-- Extended scancodes
						case keyb_data_s is

							when KEY_KPENTER 		=> keys_s(3)(3) <= release_s; -- ENTER

							-- Cursor keys
							when KEY_LEFT			=>	keys_s(1)(3) <= release_s;
							when KEY_DOWN			=>	keys_s(1)(2) <= release_s; 
							when KEY_UP				=>	keys_s(1)(1) <= release_s; 
							when KEY_RIGHT			=>	keys_s(1)(4) <= release_s; 
							when KEY_KPSLASH		=> keys_s(7)(5) <= release_s; -- Key pad / 

							when KEY_RCTRL			=> keys_s(0)(1) <= release_s; -- Right CTRL 
															keys_s(1)(7) <= release_s;
															keys_s(2)(7) <= release_s;
															keys_s(3)(7) <= release_s;
															keys_s(4)(7) <= release_s;
															keys_s(5)(7) <= release_s;
															keys_s(6)(7) <= release_s;
															keys_s(7)(7) <= release_s;

							when KEY_LWIN | KEY_RWIN => keys_s(0)(0) <= release_s; -- @

							when others =>
								null;
						end case;
					end if; -- extended = 0
				end if; -- keyb_data = F0
			else -- keyb_valid_edge = 01
				if sendresp_v = '1' then
					sendresp_v 	:= '0';
					idata_s		<= X"55";
					idata_rdy_s	<= '1';
				else
					idata_rdy_s	<= '0';
				end if;
			end if; -- keyb_valid_edge = 01
		end if; -- rising_edge
	end process;

end architecture;
