-- BBC Micro for Altera DE1
--
-- Copyright (c) 2011 Mike Stirling
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- * Redistributions in synthesized form must reproduce the above copyright
--   notice, this list of conditions and the following disclaimer in the
--   documentation and/or other materials provided with the distribution.
--
-- * Neither the name of the author nor the names of other contributors may
--   be used to endorse or promote products derived from this software without
--   specific prior written agreement from the author.
--
-- * License is granted for non-commercial use only.  A fee may not be charged
--   for redistributions as source code or in synthesized/hardware form without
--   specific prior written agreement from the author.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- MC6845 CRTC
--
-- Synchronous implementation for FPGA
--
-- (C) 2011 Mike Stirling
--
-- 2016/08/18 - Refactoring by Fabio Belavenuto

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mc6845 is
	port (
		clock_i			: in  std_logic;
		clock_en_i		: in  std_logic;
		reset_n_i		: in  std_logic;
		-- Bus interface
		cs_n_i			: in  std_logic;
		read_writen_i	: in  std_logic;
		reg_sel_i		: in  std_logic;
		data_i			: in  std_logic_vector( 7 downto 0);
		data_o			: out std_logic_vector( 7 downto 0);
		-- Display interface
		vsync_o			: out std_logic;
		hsync_o			: out std_logic;
		data_enable_o	: out std_logic;
		cursor_en_o		: out std_logic;
		lpstb_i			: in  std_logic;
		-- Memory interface
		ma_o				: out std_logic_vector(13 downto 0);
		ra_o				: out std_logic_vector( 4 downto 0)
	);
end entity;

architecture rtl of mc6845 is

	-- Host-accessible registers
	signal addr_reg_s						: std_logic_vector(4 downto 0);		-- Currently addressed register
	-- These are write-only
	signal r00_h_total_r					: unsigned(7 downto 0);					-- Horizontal total, chars
	signal r01_h_displayed_r			: unsigned(7 downto 0);					-- Horizontal active, chars
	signal r02_h_sync_pos_r				: unsigned(7 downto 0);					-- Horizontal sync position, chars
	signal r03_v_sync_width_r			: unsigned(3 downto 0);					-- Vertical sync width, scan lines (0=16 lines)
	signal r03_h_sync_width_r			: unsigned(3 downto 0);					-- Horizontal sync width, chars (0=no sync)
	signal r04_v_total_r					: unsigned(6 downto 0);					-- Vertical total, character rows
	signal r05_v_total_adj_r			: unsigned(4 downto 0);					-- Vertical offset, scan lines
	signal r06_v_displayed_r			: unsigned(6 downto 0);					-- Vertical active, character rows
	signal r07_v_sync_pos_r				: unsigned(6 downto 0);					-- Vertical sync position, character rows
	signal r08_interlace_r				: std_logic_vector(1 downto 0);
	signal r09_max_scan_line_addr_r	: unsigned(4 downto 0);
	signal r10_cursor_mode_r			: std_logic_vector(1 downto 0);
	signal r10_cursor_start_r			: unsigned(4 downto 0);					-- Cursor start, scan lines
	signal r11_cursor_end_r				: unsigned(4 downto 0);					-- Cursor end, scan lines
	signal r12_start_addr_h_r			: unsigned(5 downto 0);
	signal r13_start_addr_l_r			: unsigned(7 downto 0);
	-- These are read/write
	signal r14_cursor_h_r				: unsigned(5 downto 0);
	signal r15_cursor_l_r				: unsigned(7 downto 0);
	-- These are read-only
	signal r16_light_pen_h_r			: unsigned(5 downto 0);
	signal r17_light_pen_l_r			: unsigned(7 downto 0);


	-- Timing generation
	-- Horizontal counter counts position on line
	signal h_counter_s					: unsigned(7 downto 0);
	-- HSYNC counter counts duration of sync pulse
	signal h_sync_counter_s				: unsigned(3 downto 0);
	-- Row counter counts current character row
	signal row_counter_s					: unsigned(6 downto 0);
	-- Line counter counts current line within each character row
	signal line_counter_s				: unsigned(4 downto 0);
	-- VSYNC counter counts duration of sync pulse
	signal v_sync_counter_s				: unsigned(3 downto 0);
	-- Field counter counts number of complete fields for cursor flash
	signal field_counter_s				: unsigned(5 downto 0);

	-- Internal signals
	signal h_sync_start_s				: std_logic;
	signal h_half_way_s					: std_logic;
	signal h_display_s					: std_logic;
	signal hs								: std_logic;
	signal v_display_s					: std_logic;
	signal vs								: std_logic;
	signal odd_field_s					: std_logic;
	signal ma_s								: unsigned(13 downto 0);
--	signal ma_row_start_s 				: unsigned(13 downto 0); -- Start address of current character row
	signal cursor_s						: std_logic;
	signal lpstb_s							: std_logic;


begin
	hsync_o			<= hs;												-- External HSYNC driven directly from internal signal
	vsync_o			<= vs;												-- External VSYNC driven directly from internal signal
	data_enable_o	<= h_display_s and v_display_s;

	-- Cursor output generated combinatorially from the internal signal in
	-- accordance with the currently selected cursor mode
	cursor_en_o	<=	cursor_s										when r10_cursor_mode_r = "00" else
						'0'											when r10_cursor_mode_r = "01" else
						(cursor_s and field_counter_s(4))	when r10_cursor_mode_r = "10" else
						(cursor_s and field_counter_s(5));

	-- Synchronous register access.  Enabled on every clock.
	process(clock_i)
	begin
		if rising_edge(clock_i) then
			if cs_n_i = '0' and read_writen_i = '1' and reg_sel_i = '1' then
				-- Read
				case addr_reg_s is
					when "01110" =>
						data_o <= "00" & std_logic_vector(r14_cursor_h_r);
					when "01111" =>
						data_o <= std_logic_vector(r15_cursor_l_r);
					when "10000" =>
						data_o <= "00" & std_logic_vector(r16_light_pen_h_r);
					when "10001" =>
						data_o <= std_logic_vector(r17_light_pen_l_r);
					when others =>
						data_o <= (others => '0');
				end case;
			end if;
		end if;
	end process;

	--
	process(clock_i)
	begin
		if falling_edge(clock_i) then
			if reset_n_i = '0' then
				-- Reset registers to defaults
				addr_reg_s						<= (others => '0');

--				r00_h_total_r					<= (others => '0');
--				r01_h_displayed_r				<= (others => '0');
--				r02_h_sync_pos_r				<= (others => '0');
--				r03_v_sync_width_r			<= (others => '0');
--				r03_h_sync_width_r			<= (others => '0');
--				r04_v_total_r					<= (others => '0');
--				r05_v_total_adj_r				<= (others => '0');
--				r06_v_displayed_r				<= (others => '0');
--				r07_v_sync_pos_r				<= (others => '0');
--				r08_interlace_r				<= (others => '0');
--				r09_max_scan_line_addr_r	<= (others => '0');
--				r10_cursor_mode_r				<= (others => '0');
--				r10_cursor_start_r			<= (others => '0');
--				r11_cursor_end_r				<= (others => '0');
--				r12_start_addr_h_r			<= (others => '0');
--				r13_start_addr_l_r			<= (others => '0');
--				r14_cursor_h_r					<= (others => '0');
--				r15_cursor_l_r					<= (others => '0');

				r00_h_total_r					<= x"6D";
				r01_h_displayed_r				<= x"50";
				r02_h_sync_pos_r				<= x"53";
				r03_v_sync_width_r			<= x"6";
				r03_h_sync_width_r			<= x"D";
				r04_v_total_r					<= b"0011010";		--x"1a";
				r05_v_total_adj_r				<= b"00011";		--x"02";
				r06_v_displayed_r				<= b"0011000";		--x"18";
				r07_v_sync_pos_r				<= b"0011000";		-- x"19";
				r08_interlace_r				<= b"00";			--x"a0";
				r09_max_scan_line_addr_r	<= b"01011";		--x"0b";
				r10_cursor_mode_r				<= b"10";
				r10_cursor_start_r			<= b"01011";		-- x"0b";
				r11_cursor_end_r				<= b"01011";		-- x"0b";
				r12_start_addr_h_r			<= (others => '0');
				r13_start_addr_l_r			<= (others => '0');
				r14_cursor_h_r					<= (others => '0');
				r15_cursor_l_r					<= (others => '0');

				--; R0: Horizontal Total <-- #6D
				--; R1: Horizontal Displayed <- #50
				--; R2: Horizontal Sync Position #5C
				--; R3: Horizontal Sync Width #6D

				--; R4: Vertical Total <-- #1A
				--; R5: Vertical Total Adjust <#02
				--; R6: Vertical Displayed <-- #18
				--; R7: Vertical Sync Position <--#19

				--; R8: Interlace Mode <--#A0
				--; R9: Max Scan Line Address < #0B

				--; R10: Cursor Start  <-- scan line 11; + bit 5 = piscante#4B
				--; R11: Cursor End <-- sc#0B

				--; R12: Start Address (H)  #00
				--; R13: Start Address (L) #00
				--; R14: Cursor (H) <--  #00
				--; R15: Cursor (L) <--      #00

				--data_o <= (others => '0');

			end if;

			if cs_n_i = '0' and read_writen_i = '0' then
				if reg_sel_i = '0' then
					addr_reg_s <= data_i(4 downto 0);
				else
					case addr_reg_s is
						when "00000" =>
							r00_h_total_r <= unsigned(data_i);
						when "00001" =>
							r01_h_displayed_r <= unsigned(data_i);
						when "00010" =>
							r02_h_sync_pos_r <= unsigned(data_i);							-- tava comentado
						when "00011" =>
							r03_v_sync_width_r <= unsigned(data_i(7 downto 4));
							r03_h_sync_width_r <= unsigned(data_i(3 downto 0));
						when "00100" =>
							r04_v_total_r <= unsigned(data_i(6 downto 0));
						when "00101" =>
							r05_v_total_adj_r <= unsigned(data_i(4 downto 0));
						when "00110" =>
							r06_v_displayed_r <= unsigned(data_i(6 downto 0));
						when "00111" =>
							r07_v_sync_pos_r <= unsigned(data_i(6 downto 0));				-- tava comentado
						when "01000" =>
							r08_interlace_r <= data_i(1 downto 0);
						when "01001" =>
							r09_max_scan_line_addr_r <= unsigned(data_i(4 downto 0));
						when "01010" =>
							r10_cursor_mode_r <= data_i(6 downto 5);
							r10_cursor_start_r <= unsigned(data_i(4 downto 0));
						when "01011" =>
							r11_cursor_end_r <= unsigned(data_i(4 downto 0));
						when "01100" =>
							r12_start_addr_h_r <= unsigned(data_i(5 downto 0));
						when "01101" =>
							r13_start_addr_l_r <= unsigned(data_i(7 downto 0));
						when "01110" =>
							r14_cursor_h_r <= unsigned(data_i(5 downto 0));
						when "01111" =>
							r15_cursor_l_r <= unsigned(data_i(7 downto 0));
						when others =>
							null;
					end case;
				end if;
			end if;
		end if;
	end process; -- registers

	-- Horizontal, vertical and address counters
	process(clock_i, clock_en_i, reset_n_i)
		variable ma_row_start_s	: unsigned(13 downto 0);
		variable max_scan_line	: unsigned(4 downto 0);
	begin
		if reset_n_i = '0' then
			-- H
			h_counter_s			<= (others => '0');
			-- V
			line_counter_s		<= (others => '0');
			row_counter_s		<= (others => '0');
			odd_field_s			<= '0';
			-- Fields (cursor flash)
			field_counter_s	<= (others => '0');
			-- Addressing
			ma_row_start_s		:= (others => '0');
			ma_s					<= (others => '0');
		elsif rising_edge(clock_i) and clock_en_i = '1' then
			-- Horizontal counter increments on each clock, wrapping at
			-- h_total
			if h_counter_s = r00_h_total_r then
				-- h_total reached
				h_counter_s <= (others => '0');

				-- In interlace sync + video mode mask off the LSb of the
				-- max scan line address
				if r08_interlace_r = "11" then
					max_scan_line := r09_max_scan_line_addr_r(4 downto 1) & "0";
				else
					max_scan_line := r09_max_scan_line_addr_r;
				end if;

				-- Scan line counter increments, wrapping at max_scan_line_addr
				if line_counter_s = max_scan_line then
					-- Next character row
					-- FIXME: No support for v_total_adj yet
					line_counter_s <= (others => '0');
					if row_counter_s = r04_v_total_r then
						-- If in interlace mode we toggle to the opposite field.
						-- Save on some logic by doing this here rather than at the
						-- end of v_total_adj - it shouldn't make any difference to the
						-- output
						if r08_interlace_r(0) = '1' then
							odd_field_s <= not odd_field_s;
						else
							odd_field_s <= '0';
						end if;

						-- Address is loaded from start address register at the top of
						-- each field and the row counter is reset
						ma_row_start_s := r12_start_addr_h_r & r13_start_addr_l_r;
						row_counter_s <= (others => '0');

						-- Increment field counter
						field_counter_s <= field_counter_s + 1;
					else
						-- On all other character rows within the field the row start address is
						-- increased by h_displayed and the row counter is incremented
						ma_row_start_s := ma_row_start_s + r01_h_displayed_r;
						row_counter_s <= row_counter_s + 1;
					end if;
				else
					-- Next scan line.  Count in twos in interlaced sync+video mode
					if r08_interlace_r = "11" then
						line_counter_s <= line_counter_s + 2;
						line_counter_s(0) <= '0'; -- Force to even
					else
						line_counter_s <= line_counter_s + 1;
					end if;
				end if;

				-- Memory address preset to row start at the beginning of each
				-- scan line
				ma_s <= ma_row_start_s;
			else
				-- Increment horizontal counter
				h_counter_s <= h_counter_s + 1;
				-- Increment memory address
				ma_s <= ma_s + 1;
			end if;
		end if;
	end process;

	-- Signals to mark hsync and half way points for generating
	-- vsync in even and odd fields
	process(h_counter_s, r02_h_sync_pos_r)
	begin
		h_sync_start_s <= '0';
		h_half_way_s <= '0';

		if h_counter_s = r02_h_sync_pos_r then
			h_sync_start_s <= '1';
		end if;
		if h_counter_s = "0" & r02_h_sync_pos_r(7 downto 1) then
			h_half_way_s <= '1';
		end if;
	end process;

	-- Video timing and sync counters
	process(clock_i, clock_en_i, reset_n_i)
	begin
		if reset_n_i = '0' then
			-- H
			h_display_s			<= '0';
			hs						<= '0';
			h_sync_counter_s	<= (others => '0');
			-- V
			v_display_s			<= '0';
			vs						<= '0';
			v_sync_counter_s	<= (others => '0');
		elsif rising_edge(clock_i) and clock_en_i = '1' then
			-- Horizontal active video
			if h_counter_s = 0 then
				-- Start of active video
				h_display_s <= '1';
			end if;
			if h_counter_s = r01_h_displayed_r then
				-- End of active video
				h_display_s <= '0';
			end if;

			-- Horizontal sync
			if h_sync_start_s = '1' or hs = '1' then
				-- In horizontal sync
				hs <= '1';
				h_sync_counter_s <= h_sync_counter_s + 1;
			else
				h_sync_counter_s <= (others => '0');
			end if;
			if h_sync_counter_s = r03_h_sync_width_r then
				-- Terminate hsync after h_sync_width (0 means no hsync so this
				-- can immediately override the setting above)
				hs <= '0';
			end if;

			-- Vertical active video
			if row_counter_s = 0 then
				-- Start of active video
				v_display_s <= '1';
			end if;
			if row_counter_s = r06_v_displayed_r then
				-- End of active video
				v_display_s <= '0';
			end if;

			-- Vertical sync occurs either at the same time as the horizontal sync (even fields)
			-- or half a line later (odd fields)
			if (odd_field_s = '0' and h_sync_start_s = '1') or (odd_field_s = '1' and h_half_way_s = '1') then
				if (row_counter_s = r07_v_sync_pos_r and line_counter_s = 0) or vs = '1' then
					-- In vertical sync
					vs <= '1';
					v_sync_counter_s <= v_sync_counter_s + 1;
				else
					v_sync_counter_s <= (others => '0');
				end if;
				if v_sync_counter_s = r03_v_sync_width_r and vs = '1' then
					-- Terminate vsync after v_sync_width (0 means 16 lines so this is
					-- masked by 'vs' to ensure a full turn of the counter in this case)
					vs <= '0';
				end if;
			end if;
		end if;
	end process;

	-- Address generation
	process(clock_i, clock_en_i, reset_n_i)
	variable slv_line : std_logic_vector(4 downto 0);
	begin
		if reset_n_i = '0' then
			ra_o <= (others => '0');
			ma_o <= (others => '0');
		elsif rising_edge(clock_i) and clock_en_i = '1' then
			slv_line := std_logic_vector(line_counter_s);

			-- Character row address is just the scan line counter delayed by
			-- one clock to line up with the syncs.
			if r08_interlace_r = "11" then
				-- In interlace sync and video mode the LSb is determined by the
				-- field number.  The line counter counts up in 2s in this case.
				ra_o <= slv_line(4 downto 1) & (slv_line(0) or odd_field_s);
			else
				ra_o <= slv_line;
			end if;
			-- Internal memory address delayed by one cycle as well
			ma_o <= std_logic_vector(ma_s);
		end if;
	end process;

	-- Cursor control
	process(clock_i, clock_en_i, reset_n_i)
	variable cursor_line : std_logic;
	begin
		-- Internal cursor enable signal delayed by 1 clock to line up
		-- with address outputs
		if reset_n_i = '0' then
			cursor_s <= '0';
			cursor_line := '0';
		elsif rising_edge(clock_i) and clock_en_i = '1' then
			if h_display_s = '1' and v_display_s = '1' and ma_s = r14_cursor_h_r & r15_cursor_l_r then
				if line_counter_s = 0 then
					-- Suppress wrap around if last line is > max scan line
					cursor_line := '0';
				end if;
				if line_counter_s = r10_cursor_start_r then
					-- First cursor scanline
					cursor_line := '1';
				end if;

				-- Cursor output is asserted within the current cursor character
				-- on the selected lines only
				cursor_s <= cursor_line;

				if line_counter_s = r11_cursor_end_r then
					-- Last cursor scanline
					cursor_line := '0';
				end if;
			else
				-- Cursor is off in all character positions apart from the
				-- selected one
				cursor_s <= '0';
			end if;
		end if;
	end process;

	-- Light pen capture
	process(clock_i, clock_en_i, reset_n_i)
	begin
		if reset_n_i = '0' then
			lpstb_s <= '0';
			r16_light_pen_h_r <= (others => '0');
			r17_light_pen_l_r <= (others => '0');
		elsif rising_edge(clock_i) and clock_en_i = '1' then
			-- Register light-pen strobe input
			lpstb_s <= lpstb_i;

			if lpstb_i = '1' and lpstb_s = '0' then
				-- Capture address on rising edge
				r16_light_pen_h_r <= ma_s(13 downto 8);
				r17_light_pen_l_r <= ma_s(7 downto 0);
			end if;
		end if;
	end process;

end architecture;