--
-- Terasic DE1 top-level
--

-- altera message_off 10540 10541

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE1 board
entity de1_top is
	port (
		-- Clocks
		CLOCK_24       : in    std_logic_vector(1 downto 0);
		CLOCK_27       : in    std_logic_vector(1 downto 0);
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(9 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		-- Red LEDs
		LEDR           : out   std_logic_vector(9 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(7 downto 0)		:= (others => '0');

		-- VGA
		VGA_R          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_HS         : out   std_logic									:= '1';
		VGA_VS         : out   std_logic									:= '1';

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= '1';
		PS2_DAT        : inout std_logic									:= '1';

		-- I2C
		I2C_SCLK       : inout std_logic									:= '1';
		I2C_SDAT       : inout std_logic									:= '1';

		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '0';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => '0');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SD card (SPI mode)
		SD_nCS         : out   std_logic									:= '1';
		SD_MOSI        : out   std_logic									:= '1';
		SD_SCLK        : out   std_logic									:= '1';
		SD_MISO        : in    std_logic;

		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => 'Z');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => 'Z')
	);
end entity;

use work.mc6847_col_pack.all;

architecture Behavior of de1_top is

	-- Clocks
	signal clock_master_s	: std_logic;

	-- Reset
	signal pll_locked_s		: std_logic;
	signal reset_s				: std_logic;

	-- Memorias
	signal ram_addr_s			: std_logic_vector(15 downto 0);
	signal ram_data_from_s	: std_logic_vector( 7 downto 0);
	signal ram_data_to_s		: std_logic_vector( 7 downto 0);
	signal ram_cs_s			: std_logic;
	signal ram_oe_s			: std_logic;
	signal ram_we_s			: std_logic;
	signal rom_addr_s			: std_logic_vector(13 downto 0);
	signal rom_data_from_s	: std_logic_vector( 7 downto 0);
	-- VRAM
	signal vram_addr_s		: std_logic_vector(12 downto 0);
	signal vram_data_from_s	: std_logic_vector( 7 downto 0);
	signal vram_data_to_s	: std_logic_vector( 7 downto 0);
	signal vram_oe_s			: std_logic;
	signal vram_we_s			: std_logic;

	-- K7
	signal mic_s				: std_logic;
	signal ear_s				: std_logic;
	signal ear_e_s				: std_logic;

	-- Audio
	signal psg_s				: std_logic_vector( 7 downto 0);

	-- Video
	signal video_mode80_s	: std_logic;
	signal video_col_idx_s	: std_logic_vector( 3 downto 0);
	signal video_15khz_s		: std_logic_vector( 3 downto 0);
	signal video_31khz_s		: std_logic_vector( 3 downto 0);
	signal video_80_pixel_s	: std_logic;
--	signal video_r_s			: std_logic_vector( 7 downto 0);
--	signal video_g_s			: std_logic_vector( 7 downto 0);
--	signal video_b_s			: std_logic_vector( 7 downto 0);
	signal video_hs_s			: std_logic;
	signal video_vs_s			: std_logic;
--	signal video_hb_s			: std_logic;
--	signal video_vb_s			: std_logic;

	-- Joystick (Minimig standard)
	signal joy1_s				: std_logic_vector(4 downto 0);
	signal joy2_s				: std_logic_vector(4 downto 0);
	alias J1_UP					: std_logic						is GPIO_1(34);
	alias J1_DOWN				: std_logic						is GPIO_1(32);
	alias J1_LEFT				: std_logic						is GPIO_1(30);
	alias J1_RIGHT				: std_logic						is GPIO_1(28);
	alias J1_BTN				: std_logic						is GPIO_1(35);
--	alias J1_BTN2				: std_logic						is GPIO_1(29);
--	alias J1_MMB				: std_logic						is GPIO_1(26);
	alias J2_UP					: std_logic						is GPIO_1(24);
	alias J2_DOWN				: std_logic						is GPIO_1(22);
	alias J2_LEFT				: std_logic						is GPIO_1(20);
	alias J2_RIGHT				: std_logic						is GPIO_1(23);
	alias J2_BTN				: std_logic						is GPIO_1(25);
--	alias J2_BTN2				: std_logic						is GPIO_1(21);
--	alias J2_MMB				: std_logic						is GPIO_1(27);

	-- Debug
	signal D_cpu_addr_s		: std_logic_vector(15 downto 0);
	signal D_display_s		: std_logic_vector(15 downto 0);

begin

	-- PLL
	pll: entity work.pll1
	port map (
		inclk0	=> CLOCK_50,
		c0			=> clock_master_s,			-- 28.571429 MHz (28.636363)
		locked	=> pll_locked_s
	);

	mc1000_inst: entity work.mc1000
	port map (
		clock_i				=> clock_master_s,
		reset_i				=> reset_s,
		-- Options
		opt_ram64_i			=> SW(0),
		opt_graph_ch_i		=> SW(1),
		opt_keyboard_i		=> SW(2),
		-- RAM memory
		ram_addr_o			=> ram_addr_s,
		ram_data_from_i	=> ram_data_from_s,
		ram_data_to_o		=> ram_data_to_s,
		ram_cs_o				=> ram_cs_s,
		ram_oe_o				=> ram_oe_s,
		ram_we_o				=> ram_we_s,
		-- ROM memory
		rom_addr_o			=> rom_addr_s,
		rom_data_from_i	=> rom_data_from_s,
		-- VRAM memory
		vram_addr_o			=> vram_addr_s,
		vram_data_from_i	=> vram_data_from_s,
		vram_data_to_o		=> vram_data_to_s,
		vram_oe_o			=> vram_oe_s,
		vram_we_o			=> vram_we_s,
		-- PS/2 Keyboard
		ps2_clk_io			=> PS2_CLK,
		ps2_data_io			=> PS2_DAT,
		-- K7
		mic_o					=> mic_s,
		ear_i					=> ear_e_s,
		-- Joystick (order: Fire, Up, Down, Left, Right)
		joy1_i				=> joy1_s,
		joy2_i				=> joy2_s,
		-- Audio
		audio_psg_o			=> psg_s,
		-- Video
		video_mode80_o		=> video_mode80_s,
		video_col_idx_o	=> video_col_idx_s,
		video_80_pixel_o	=> video_80_pixel_s,
--		video_r_o			=> video_r_s,
--		video_g_o			=> video_g_s,
--		video_b_o			=> video_b_s,
		video_hs_o			=> video_hs_s,
		video_vs_o			=> video_vs_s,
		video_hb_o			=> open,--video_hb_s,
		video_vb_o			=> open,--video_vb_s,
		-- Debug
		D_clk_src_i			=> SW(9 downto 7),
		D_clk_manual_i		=> KEY(3),
--		D_rom_c000_o		=> LEDR(0),
--		D_rom_cs_o			=> LEDR(1),
--		D_rows_o				=> LEDG,
--		D_80col_en_o		=> LEDG(0),
--		D_crtc_vram_bank_o => LEDG(1),
		D_cpu_addr_o		=> D_cpu_addr_s
	);

	-- ROM 16K
	rom: entity work.mc1000_rom
	port map (
		clock			=> clock_master_s,
		address		=> rom_addr_s,
		q				=> rom_data_from_s
	);

	-- RAM
	ram: entity work.dpSRAM_25616
	port map (
		clock_i			=> clock_master_s,
		-- Port 0
		port0_addr_i	=> "100000" & vram_addr_s,
		port0_ce_i		=> '1',
		port0_oe_i		=> vram_oe_s,
		port0_we_i		=> vram_we_s,
		port0_d_i		=> vram_data_to_s,
		port0_d_o		=> vram_data_from_s,
		-- Port 1
		port1_addr_i	=> "000" & ram_addr_s,
		port1_ce_i		=> ram_cs_s,
		port1_oe_i		=> ram_oe_s,
		port1_we_i		=> ram_we_s,
		port1_d_i		=> ram_data_to_s,
		port1_d_o		=> ram_data_from_s,
		-- Output to SRAM
		sram_addr_o		=> SRAM_ADDR,
		sram_data_io	=> SRAM_DQ,
		sram_ub_n_o		=> SRAM_UB_N,
		sram_lb_n_o		=> SRAM_LB_N,
		sram_ce_n_o		=> SRAM_CE_N,
		sram_oe_n_o		=> SRAM_OE_N,
		sram_we_n_o		=> SRAM_WE_N
	);

	-- Audio
	audio: entity work.Audio_WM8731
	port map (
		clock_i			=> CLOCK_24(0),
		reset_i			=> reset_s,
		mic_i				=> mic_s,
		ear_o				=> ear_s,
		psg_i				=> psg_s,
		-- I2S
		i2s_xck_o		=> AUD_XCK,
		i2s_bclk_o		=> AUD_BCLK,
		i2s_adclrck_o	=> AUD_ADCLRCK,
		i2s_adcdat_i	=> AUD_ADCDAT,
		i2s_daclrck_o	=> AUD_DACLRCK,
		i2s_dacdat_o	=> AUD_DACDAT,
		-- I2C
		i2c_sda_io		=> I2C_SDAT,
		i2c_scl_io		=> I2C_SCLK,
		feedback_i		=> SW(3)
	);

	-- Scandoubler
	dblscan: entity work.scandoubler
	generic map (
		hSyncLength_g	=> 31,
		vSyncLength_g	=> 11,
		ramBits_g		=> 11,
		addr_width_g	=> 4,
		hsync_pol_g		=> '0',
		vsync_pol_g		=> '0'
	)
	port map (
		clock_i			=> clock_master_s,
		enable_i			=> '1',
		scanlines_en_i	=> SW(6),
		video_i			=> video_15khz_s,
		hsync_i			=> video_hs_s,
		vsync_i			=> video_vs_s,
		video_o			=> video_31khz_s,
		hsync_o			=> VGA_HS,
		vsync_o			=> VGA_VS
	);

	-- Glue
	reset_s <= '1' when pll_locked_s = '0' or KEY(0) = '0'	else '0';

	-- Joystick
	-- ordem: Fire, Up, Down, Left, Right
	joy1_s <= not (J1_BTN & J1_UP & J1_DOWN & J1_LEFT & J1_RIGHT);
	joy2_s <= not (J2_BTN & J2_UP & J2_DOWN & J2_LEFT & J2_RIGHT);

	-- Ear
	ear_e_s <= ear_s or not SW(3);

	-- Video
	video_15khz_s	<= video_col_idx_s	when video_mode80_s = '0'	else video_80_pixel_s & "000";
	
	process (clock_master_s)
		variable vga_col_v						: natural range 0 to 15;
		variable vga_r_v, vga_g_v, vga_b_v	: rgb_val_t;
	begin
		if rising_edge(clock_master_s) then
			vga_col_v	:= to_integer(unsigned(video_31khz_s));
--			vga_col_v	:= to_integer(unsigned(video_col_idx_s));
			vga_r_v	:= rgb_table_c(vga_col_v)(r_c);
			vga_g_v	:= rgb_table_c(vga_col_v)(g_c);
			vga_b_v	:= rgb_table_c(vga_col_v)(b_c);
			VGA_R		<= std_logic_vector(to_unsigned(vga_r_v, 8))(7 downto 4);
			VGA_G		<= std_logic_vector(to_unsigned(vga_g_v, 8))(7 downto 4);
			VGA_B		<= std_logic_vector(to_unsigned(vga_b_v, 8))(7 downto 4);
		end if;
	end process;
--	VGA_R		<= video_r_s(7 downto 4);
--	VGA_G		<= video_g_s(7 downto 4);
--	VGA_B		<= video_b_s(7 downto 4);
--	VGA_HS	<= video_hs_s;
--	VGA_VS	<= video_vs_s;

	----------------------------------------
	-- Debugs
	----------------------------------------

	LEDG(0)	<= video_mode80_s;

	D_display_s		<= D_cpu_addr_s;

	ld3: entity work.seg7
	port map(
		D		=> D_display_s(15 downto 12),
		Q		=> HEX3
	);

	ld2: entity work.seg7
	port map(
		D		=> D_display_s(11 downto 8),
		Q		=> HEX2
	);

	ld1: entity work.seg7
	port map(
		D		=> D_display_s(7 downto 4),
		Q		=> HEX1
	);

	ld0: entity work.seg7
	port map(
		D		=> D_display_s(3 downto 0),
		Q		=> HEX0
	);


end architecture;
