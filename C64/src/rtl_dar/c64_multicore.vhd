---------------------------------------------------------------------------------
-- Multicore 2 Top level for FPGA64_027 
--
-- FPGA64 is Copyrighted 2005-2008 by Peter Wendrich (pwsoft@syntiac.com)
-- http://www.syntiac.com/fpga64.html
--
-- Main features
--  15KHz(TV) / 31Khz(VGA) : board switch(0)
--  PAL(50Hz) / NTSC(60Hz) : board switch(1) and F12 key
--  PS2 keyboard input with portA / portB joystick emulation : F11 key
--  wm8731 sound output
--  64Ko of board SRAM used
--  External IEC bus available at gpio_1 (for drive 1541 or IEC/SD ...)
--   activated by switch(5) (activated with no hardware will stuck IEC bus)
--
--  Internal emulated 1541 on raw SD card : D64 images start at 256KB boundaries
--  Use hexidecimal disk editor such as HxD (www.mh-nexus.de) to build SD card.
--  Cut D64 file and paste at 0x00000 (first), 0x40000 (second), 0x80000 (third),
--  0xC0000(fourth), 0x100000(fith), 0x140000 (sixth) and so on.
--  BE CAREFUL NOT WRITING ON YOUR OWN HARDDRIVE
--
-- Uses only one pll for 32MHz and 18MHz generation from 50MHz
-- DE2, DE1 and DE0 nano Top level also available
--     
---------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.ALL;
use IEEE.numeric_std.all;

entity c64_multicore is
port(
-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
);
end c64_multicore;

architecture struct of c64_multicore is

	signal c64_iec_atn_i  : std_logic;
	signal c64_iec_clk_o  : std_logic;
	signal c64_iec_data_o : std_logic;
	signal c64_iec_atn_o  : std_logic;
	signal c64_iec_data_i : std_logic;
	signal c64_iec_clk_i  : std_logic;

	signal c1541_iec_atn_i  : std_logic;
	signal c1541_iec_clk_o  : std_logic;
	signal c1541_iec_data_o : std_logic;
	signal c1541_iec_atn_o  : std_logic;
	signal c1541_iec_data_i : std_logic;
	signal c1541_iec_clk_i  : std_logic;

	--alias tv15Khz_mode : std_logic is sw(0);
	signal tv15Khz_mode   : std_logic;
	signal ntsc_init_mode : std_logic;

	--alias sram_addr_int : unsigned is unsigned(sram_addr_o);
	alias sram_dq_int   : unsigned is unsigned(sram_data_io(7 downto 0));
	
	signal sram_addr_int : unsigned (18 downto 0);

-- DE1/DE2 numbering
--	alias ext_iec_atn_i  : std_logic is gpio_1(32);
--	alias ext_iec_clk_o  : std_logic is gpio_1(33);
--	alias ext_iec_data_o : std_logic is gpio_1(34);
--	alias ext_iec_atn_o  : std_logic is gpio_1(35);
--	alias ext_iec_data_i : std_logic is gpio_1(2);
--	alias ext_iec_clk_i  : std_logic is gpio_1(0);

-- DE0 nano numbering
--	alias iec_atn_i  : std_logic is gpio_0(30);
--	alias iec_clk_o  : std_logic is gpio_0(31);
--	alias iec_data_o : std_logic is gpio_0(32);
--	alias iec_atn_o  : std_logic is gpio_0(33);
--	alias iec_data_i : std_logic is gpio_0_in(1);
--	alias iec_clk_i  : std_logic is gpio_0_in(0);

	signal clk32 		: std_logic;
	signal clk18 		: std_logic;
	signal clkc2 		: std_logic;
	signal clk_info 	: std_logic := '0';
	
	signal ram_ce : std_logic;
	signal ram_we : std_logic;
	signal r : std_logic_vector(7 downto 0);
	signal g : std_logic_vector(7 downto 0);
	signal b : std_logic_vector(7 downto 0);
	signal hsync : std_logic;
	signal vsync : std_logic;
	signal csync : std_logic;

	signal audio_data 		: std_logic_vector(17 downto 0);
	signal audio_dac_s  		: std_logic;

	signal dbg_adr_fetch    : std_logic_vector(15 downto 0);
	signal dbg_cpu_irq      : std_logic;
	signal dbg_track_dbl    : std_logic_vector(6 downto 0);
	signal dbg_sync_n       : std_logic;
	signal dbg_byte_n       : std_logic;
	signal dbg_sd_busy      : std_logic;
	signal dbg_sd_state     : std_logic_vector(7 downto 0);
	signal dbg_read_sector  : std_logic_vector(4 downto 0); 
	
	signal reset_counter    : std_logic_vector(7 downto 0);
	signal reset_n          : std_logic;
	
	signal disk_num         : std_logic_vector(7 downto 0) 	:= (others=>'0');
	signal disk_num_prev    : std_logic_vector(7 downto 0) 	:= (others=>'0');
	signal dbg_num          : std_logic_vector(2 downto 0)	:= (others=>'0');
	signal led_disk         : std_logic_vector(7 downto 0);
	
	--OSD
	signal OSDBit_s         	: std_logic;
	signal videoConfigDim 		: std_logic := '1';
	signal videoConfigTimeout 	: unsigned(23 downto 0) := (others=>'0');
	signal videoConfigShow 		: std_logic := '0';
	
	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	signal vga_r_s					: std_logic_vector( 2 downto 0);
	signal vga_g_s					: std_logic_vector( 2 downto 0);
	signal vga_b_s					: std_logic_vector( 2 downto 0);	
	

begin

	tv15Khz_mode <= '0';--sw(0);
	ntsc_init_mode <= '1';--sw(1); --starts in NTSC mode


	clk_32_18 : entity work.pll50_to_32_and_18
	port map(
		inclk0 => clock_50_i,
		c0 => clk32,
		c1 => clk18,
		c2 => clkc2
	);
	
	process(clkc2)
	begin
		if rising_edge(clkc2) then
			clk_info <= not clk_info;
		end if;
	end process;
	
	process(clk32, btn_n_i(3))
	begin
		if rising_edge(clk32) then
			reset_n <= '0';
			if btn_n_i(3)='0' then
				reset_counter <= (others => '0');
			else
			  if reset_counter = X"FF" then
					reset_n <= '1';
				else
					reset_counter <= std_logic_vector(unsigned(reset_counter)+1);
				end if;
			end if;
		end if;
	end process;

	fpga64 : entity work.fpga64_sid_iec
	port map
	(
		sysclk => clock_50_i,
		clk32 => clk32,
		reset_n => reset_n,
		kbd_clk => ps2_clk_io,
		kbd_dat => ps2_data_io,
		ramAddr => sram_addr_int(15 downto 0),
		ramData => sram_dq_int,
		ramCE => ram_ce,
		ramWe => ram_we,
		tv15Khz_mode => tv15Khz_mode,
		ntscInitMode => ntsc_init_mode,
		hsync => hsync,
		vsync => vsync,
		r => r,
		g => g,
		b => b,
		game => '1',
		exrom => '1',
		irq_n => '1',
		nmi_n => '1',
		dma_n => '1',
		ba => open,
		dot_clk => open,
		cpu_clk => open,
		joyA => "0" & not joy1_p6_i & not joy1_right_i & not joy1_left_i & not joy1_down_i & not joy1_up_i,
		joyB => "0" & not joy2_p6_i & not joy2_right_i & not joy2_left_i & not joy2_down_i & not joy2_up_i,
		serioclk => open,
		ces => open,
		SIDclk => open,
		still => open,
		audio_data => audio_data,
		iec_data_o => c64_iec_data_o,
		iec_atn_o  => c64_iec_atn_o,
		iec_clk_o  => c64_iec_clk_o,
		iec_data_i => not c64_iec_data_i,
		iec_clk_i  => not c64_iec_clk_i,
		iec_atn_i  => not c64_iec_atn_i,
		disk_num => disk_num,
		dbg_num => dbg_num

	);

	-- 
--  c64_iec_atn_i  <= not ((not c64_iec_atn_o)  and (not c1541_iec_atn_o) ) or (ext_iec_atn_i  and sw(5));
 -- c64_iec_data_i <= not ((not c64_iec_data_o) and (not c1541_iec_data_o)) or (ext_iec_data_i and sw(5));
--	c64_iec_clk_i  <= not ((not c64_iec_clk_o)  and (not c1541_iec_clk_o) ) or (ext_iec_clk_i  and sw(5));
	
	c64_iec_atn_i  <= not ((not c64_iec_atn_o)  and (not c1541_iec_atn_o) ) ;
   c64_iec_data_i <= not ((not c64_iec_data_o) and (not c1541_iec_data_o)) ;
	c64_iec_clk_i  <= not ((not c64_iec_clk_o)  and (not c1541_iec_clk_o) ) ;
	
	c1541_iec_atn_i  <= c64_iec_atn_i;
	c1541_iec_data_i <= c64_iec_data_i;
	c1541_iec_clk_i  <= c64_iec_clk_i;
	
--	ext_iec_atn_o  <= c64_iec_atn_o   or c1541_iec_atn_o;
--	ext_iec_data_o <= c64_iec_data_o  or c1541_iec_data_o;
--	ext_iec_clk_o  <= c64_iec_clk_o   or c1541_iec_clk_o;
	
	c1541_sd : entity work.c1541_sd
	port map
	(
		clk32 => clk32,
		clk18 => clk18,
		reset => not reset_n,

		--disk_num => (sw(17 downto 16) & disk_num),
		disk_num => ("00" & disk_num),

		iec_atn_i  => c1541_iec_atn_i,
		iec_data_i => c1541_iec_data_i,
		iec_clk_i  => c1541_iec_clk_i,

		iec_atn_o  => c1541_iec_atn_o,
		iec_data_o => c1541_iec_data_o,
		iec_clk_o  => c1541_iec_clk_o,

		sd_dat            => sd_miso_i,
		sd_dat3           => sd_cs_n_o,
		sd_cmd            => sd_mosi_o,
		sd_clk            => sd_sclk_o,

		dbg_adr_fetch   => dbg_adr_fetch,
		dbg_cpu_irq     => dbg_cpu_irq,
		dbg_track_dbl   => dbg_track_dbl,
		dbg_sync_n      => dbg_sync_n,
		dbg_byte_n      => dbg_byte_n,
		dbg_sd_busy     => dbg_sd_busy,
		dbg_sd_state    => dbg_sd_state,
		dbg_read_sector => dbg_read_sector, 

		led => led_disk
	);
	
	
	
	
	-- static memory
  BLK_SRAM : block
  
		signal btn_reset_s : std_logic := '1';
		signal count_clear : std_logic_vector(18 downto 0) := (others=>'0');
	
		begin
  
				btnres: entity work.debounce
				generic map (
					counter_size_g	=> 16
				)
				port map (
					clk_i				=> clk32,
					button_i			=> btn_n_i(3) or btn_n_i(4),
					result_o			=> btn_reset_s
				);
				

				
			process (clock_50_i, btn_reset_s)
			begin
		 	
		 		if btn_reset_s='0' then
		 		
		 			if rising_edge (clock_50_i)  then
		 			
		 				sram_addr_o <= count_clear;
						sram_oe_n_o <= '0';
		 				sram_we_n_o <= '0';
		 				count_clear <= count_clear + 1;
		 			
		 			end if;
		 			
		 		else
						
						sram_addr_o <= "000" & std_logic_vector(sram_addr_int(15 downto 0));
						sram_we_n_o <= ram_we;
						sram_oe_n_o <= not ram_we;
						
					end if;

				end process;
		
		 end block BLK_SRAM;
	
	


	displayVideoConfig: entity work.fpga64_hexy
		generic map 
		(
			yOffset => 50,
			xOffset => 200
		)
		port map (
			clk_i 		=> clk32,
			vSync_i 		=> vsync,
			hSync_i 		=> hsync,
			video_o 		=> OSDBit_s,
			dim_o 		=> videoConfigDim,
			
			disk_num_i 	=> disk_num,
			track_i 		=> dbg_track_dbl,
			sector_i 	=> dbg_read_sector
		);
		
	-- Video config display (disabled)
 	process(clk_info, disk_num, disk_num_prev)
 	begin
	
 		if rising_edge(clk_info) then
 			if disk_num /= disk_num_prev or led_disk(6) = '1' then --show the OSD when change disk or on 1541 LED activity
 				videoConfigTimeout <= (others => '1');
				videoConfigShow <= '1';
				disk_num_prev <= disk_num;
 			end if;
					
			if videoConfigTimeout > 0 then
				videoConfigTimeout <= videoConfigTimeout - 1;
				videoConfigShow <= '1';
			else
				videoConfigShow <= '0';
			end if;
		end if;		

 	end process;
		




	

	comp_sync : entity work.composite_sync
	port map(
		clk32 => clk32,
		hsync => hsync,
		vsync => vsync,
		csync => csync
	);

-- synchro composite/ synchro horizontale
	vga_hsync_n_o <= csync when tv15Khz_mode = '1' else hsync;
-- commutation rapide / synchro verticale
	vga_vsync_n_o <= '1'   when tv15Khz_mode = '1' else vsync;
	
	
	
		-- Audio
	audioout: entity work.dac
	generic map (
		msbi_g		=> 7
	)
	port map (
		clk_i		=> clk32,
		res_i		=> not reset_n,
		dac_i		=> audio_data(17 downto 10),
		dac_o		=> audio_dac_s
	);
	
	dac_l_o	<= audio_dac_s;
	dac_r_o	<= audio_dac_s;
	
	btnscl: entity work.debounce
		generic map (
			counter_size_g	=> 16
		)
		port map (
			clk_i				=> clk32,
			button_i			=> btn_n_i(1) or btn_n_i(2),
			result_o			=> btn_scan_s
		);
		
		process (btn_scan_s)
		begin
			if falling_edge(btn_scan_s) then
				scanlines_en_s <= not scanlines_en_s;
			end if;
		end process;
		
		process(hsync,vsync)
		begin
			if vsync = '0' then
				odd_line_s <= '0';
			elsif rising_edge(hsync) then
				odd_line_s <= not odd_line_s;
			end if;
		end process;
		
		process(videoConfigDim)
		begin

			if videoConfigShow = '1' and videoConfigDim = '1' then
				vga_r_s <= OSDBit_s & r(7 downto 6);
				vga_g_s <= OSDBit_s & g(7 downto 6);
				vga_b_s <= OSDBit_s & b(7 downto 6);
			else
				vga_r_s <= r(7 downto 5);
				vga_g_s <= g(7 downto 5);
				vga_b_s <= b(7 downto 5);
			end if;				

		end process;
		
--		vga_r_o <=  vga_r_s - 2 when vga_r_s > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_r_s;
--		vga_g_o <=  vga_g_s - 2 when vga_g_s > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_g_s;
--		vga_b_o <=  vga_b_s - 2 when vga_b_s > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_b_s;
	
		vga_r_o <=  '0' & vga_r_s(2 downto 0) &'0' when scanlines_en_s = '1' and odd_line_s = '1' else vga_r_s & "00";
		vga_g_o <=  '0' & vga_g_s(2 downto 0) &'0' when scanlines_en_s = '1' and odd_line_s = '1' else vga_g_s & "00";
		vga_b_o <=  '0' & vga_b_s(2 downto 0) &'0' when scanlines_en_s = '1' and odd_line_s = '1' else vga_b_s & "00";
	

	--sound_string <= audio_data(17 downto 2) & audio_data(17 downto 2);

--	wm8731_dac : entity work.wm8731_dac
--	port map(
--		clk18MHz => clk18,
--		sampledata => sound_string,
--		i2c_sclk => i2c_sclk,
--		i2c_sdat => i2c_sdat,
--		aud_bclk => aud_bclk,
--		aud_daclrck => aud_daclrck,
--		aud_dacdat => aud_dacdat,
--		aud_xck => aud_xck
--	); 

--	with dbg_num select
--	leds_n_o(7 downto 0) <= not(disk_num); -- when "000",
			----		led_disk when "001",
			--		"00"&dbg_track_dbl(6 downto 1) when "010",
			--		"000"&dbg_read_sector when "011",
			--		dbg_sd_state when "100",
			--		X"AA" when others;
				 

	-- debug de2	
--	gpio_0(15 downto 0) <= dbg_adr_fetch(15 downto 0);
--	gpio_0(16) <= dbg_sync_n;
--	gpio_0(17) <= dbg_byte_n;
--	gpio_0(18) <= not c1541_iec_atn_i;
--	gpio_0(19) <= not c1541_iec_data_i;
--	gpio_0(20) <= not c1541_iec_clk_i;
--	gpio_0(21) <= not c1541_iec_data_o;
--	gpio_0(22) <= not c1541_iec_clk_o;
--	gpio_0(23) <= dbg_sd_busy;
--	gpio_0(24) <= dbg_cpu_irq;
--	gpio_0(32 downto 25) <= dbg_sd_state;
	--gpio_0(29 downto 25) <= dbg_read_sector;


--	h0 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector(dbg_adr_fetch(3 downto 0)), do=>hex0);
--	h1 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector(dbg_adr_fetch(7 downto 4)), do=>hex1);
--
--	h2 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector(dbg_adr_fetch(11 downto 8)), do=>hex2);
--	h3 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector(dbg_adr_fetch(15 downto 12)), do=>hex3);
--
--	h4 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector(dbg_track_dbl(4 downto 1)), do=>hex4);
--	h5 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector("00" & dbg_track_dbl(6 downto 5)), do=>hex5);
--
--	h6 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector(dbg_read_sector(3 downto 0)), do=>hex6);
--	h7 : entity work.decodeur_7_segments
--	port map(di=>std_logic_vector("000" & dbg_read_sector(4 downto 4)), do=>hex7);

end struct;
