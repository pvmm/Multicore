

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture Behavior of top is



	-- Reset signal
	signal reset_n				: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset			: std_logic;		-- Reset do PLL
	signal pll_locked			: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clk_28		: std_logic;
	signal clk_timeout		: std_logic;
	signal clk_dpc : std_logic;
	signal memory_clock		: std_logic;
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	signal atari_clk: std_logic;
	
	signal ram_a				: std_logic_vector(17 downto 0);	
	signal ram_din				: std_logic_vector(15 downto 0);
	signal ram_dout			: std_logic_vector(15 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);	
	signal rom_dout			: std_logic_vector(7 downto 0);
	
	signal from_sram			: std_logic_vector(15 downto 0);
	signal to_sram			: std_logic_vector(15 downto 0);
	
		-- ram
	signal loader_ram_a				: std_logic_vector(17 downto 0);		
	signal loader_to_sram			: std_logic_vector(15 downto 0);
	signal loader_from_sram			: std_logic_vector(15 downto 0);
	signal loader_ram_data			: std_logic_vector(15 downto 0);
	signal loader_ram_cs				: std_logic;
	signal loader_ram_oe				: std_logic;
	signal loader_ram_we				: std_logic;

	signal a2601_ram_a				: std_logic_vector(13 downto 0);	
	signal a2601_ram_dout			: std_logic_vector(7 downto 0);
	
	signal port_243b : std_logic_vector(7 downto 0);

	-- A2601
	signal audio: std_logic := '0';	
	signal A2601_reset: std_logic := '0';
	signal p_l: std_logic := '0';
	signal p_r: std_logic := '0';
	signal p_a: std_logic := '0';
	signal p_u: std_logic := '0';
	signal p_d: std_logic := '0';
	signal p2_l: std_logic := '0';
	signal p2_r: std_logic := '0';
	signal p2_a: std_logic := '0';
	signal p2_u: std_logic := '0';
	signal p2_d: std_logic := '0';
	signal p_s: std_logic := '0';
	signal p_bs: std_logic;
	signal LED: std_logic_vector(2 downto 0);
	signal I_SW : std_logic_vector(3 downto 0) := (others => '0');
	
	signal cart_a: std_logic_vector(13 downto 0);
	signal cart_d : std_logic_vector(7 downto 0);
	
	signal int_colu 	: std_logic_vector(6 downto 0);
	signal vga_colu 	: std_logic_vector(6 downto 0);
	signal hsync		: std_logic;
	signal vsync		: std_logic;
	signal clk_tia_s		: std_logic;
	signal clk_tiax2_s		: std_logic;
	signal  rgbx2:  std_logic_vector(23 downto 0);
	
	
	--rgb
	signal rgb_loader_out			: std_logic_vector(7 downto 0);
	signal rgb_atari_out				: std_logic_vector(11 downto 0);
	
	signal hsync_loader_out			: std_logic;
	signal vsync_loader_out			: std_logic;
	
	signal hsync_atari_out			: std_logic;
	signal vsync_atari_out			: std_logic;
	
	signal bs_method			 		: std_logic_vector(7 downto 0);
	
	-- PS/2
	signal keyb_data_s 			: std_logic_vector(7 downto 0);
	signal keyb_valid_s 			: std_logic;
	
	signal clk_keyb 				: std_logic;
	
	------
	signal loader_hor_s 				: std_logic_vector(8 downto 0);
	signal loader_ver_s 				: std_logic_vector(8 downto 0);
	
	signal clk_module_vga					: std_logic;
	signal clk_vga					: std_logic;
	signal clk_dvi					: std_logic;
	
	signal vga_color_s 			: std_logic_vector(3 downto 0);
	signal vga_blank_s 			: std_logic;
	
	-- video
	signal vga_r_s  	: std_logic_vector(4 downto 0);
	signal vga_g_s  	: std_logic_vector(4 downto 0);
	signal vga_b_s  	: std_logic_vector(4	downto 0);
	
	signal vga_r_out_s 		: std_logic_vector(4 downto 0);
	signal vga_g_out_s 		: std_logic_vector(4 downto 0);
	signal vga_b_out_s 		: std_logic_vector(4 downto 0);
	signal vga_hsync_n_s : std_logic;
	signal vga_vsync_n_s : std_logic;
		
	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	
	--OSD
	signal OSDBit_s         	: std_logic;
	signal videoConfigDim 		: std_logic := '1';
	signal videoConfigTimeout 	: unsigned(23 downto 0) := (others=>'0');
	signal videoConfigShow 		: std_logic := '0';
	
	signal atari_flags 			: std_logic_vector(2 downto 0);
	
	signal btn_diffp1_s			: std_logic;
	signal btn_diffp2_s			: std_logic;
	
	-- joystick
	signal joy1_s			: std_logic_vector(11 downto 0) := (others => '1'); --  MXYZ SACB RLDU
	signal joy2_s			: std_logic_vector(11 downto 0) := (others => '1'); --  MXYZ SACB RLDU
	signal joyP7_s			: std_logic;
	
begin

 	ps2 : work.ps2_intf port map 
	(
		clk_keyb,
		reset_n,
		ps2_clk_io,
		ps2_data_io,
		keyb_data_s,
		keyb_valid_s,
		open
	);
	
	clk_keyb <= clk_28 when A2601_reset = '1' else vid_clk;
  
  
	-- 28 MHz master clock
	pll: work.pll_atari port map (
		areset		=> pll_reset,				-- PLL Reset
		inclk0		=> clock_50_i,				-- Clock 50 MHz externo
		c0				=> sysclk,		
		c1				=> vid_clk,
		c2				=> clk_28,			
		c3 			=> clk_vga,
		c4 			=> clk_dvi,		
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
		-- 28 MHz master clock
	pll_dpc: work.pll_dpc port map (
		inclk0		=> clock_50_i,				-- Clock 50 MHz externo
		c0				=> clk_dpc		

	);
	
	pll_reset	<= '1' when btn_n_i(3) = '0' and btn_n_i(4) = '0' else '0';
	reset_n		<= not (pll_reset or not pll_locked);	-- System is reset by external reset switch or PLL being out of lock
	
	loader : work.speccy48_top port map 
	(	
		clk_28   => clk_28,
		reset_n_i 	=> reset_n,
	
		VGA_R(3 downto 1) => rgb_loader_out (7 downto 5),    
		VGA_G(3 downto 1) => rgb_loader_out (4 downto 2),    
		VGA_B(3 downto 2) => rgb_loader_out (1 downto 0),   
		VGA_HS         	=> hsync_loader_out,  
		VGA_VS         	=> vsync_loader_out,   

		-- PS/2 Keyboard
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  
   
		SRAM_ADDR      => loader_ram_a,
		FROM_SRAM      => loader_from_sram,
		TO_SRAM       	=> loader_to_sram,
		SRAM_CE_N      => loader_ram_cs,
		SRAM_OE_N      => loader_ram_oe,
		SRAM_WE_N      => loader_ram_we ,
                  		  
		SD_nCS         => sd_cs_n_o,
		SD_MOSI        => sd_mosi_o, 
		SD_SCLK        => sd_sclk_o, 
		SD_MISO        => sd_miso_i,
		
		PORT_243B => port_243b,
		
		--joystick       => "000" & not joy1_p6_i &  not joy1_up_i &  not joy1_down_i & not joy1_left_i & not joy1_right_i,
		joystick => "000" & not (joy1_s(10) and joy1_s(9) and joy1_s(8) and joy1_s(6) and joy1_s(5) and joy1_s(4)) & not joy1_s(0)  & not joy1_s(1)  & not joy1_s(2)  & not joy1_s(3),  -- joy_s format MXYZ SACB RLDU 
		
		scandbl_en		=> '1'
	);
	
	
	sram_addr_o   <= "0" & ram_a;
	sram_data_io  <= to_sram(7 downto 0) when ram_we = '0' else (others=>'Z');
	from_sram(7 downto 0) 	  <= sram_data_io;
   sram_oe_n_o   <= ram_oe;
	sram_we_n_o   <= ram_we;


--cartridge

--	Inst_cart_rom: entity work.cart_enduro PORT MAP(
--		clock => CLOCK_50,
--		q => cart_d,
--		address => cart_a	
--	);	


-- -----------------------------------------------------------------------
-- A2601 core
-- -----------------------------------------------------------------------
	a2601Instance : entity work.A2601NoFlash port map 
		(
			vid_clk => vid_clk,
			audio => audio,
   --      O_VSYNC => vsync_atari_out,
    --     O_HSYNC => hsync_atari_out,
	--		O_VIDEO_R => rgb_atari_out(7 downto 5),
	--		O_VIDEO_G => rgb_atari_out(4 downto 2),
	--		O_VIDEO_B(2 downto 1) => rgb_atari_out(1 downto 0),
	
			colu => int_colu,
			Hsyn => hsync,
			Vsyn => vsync,
	
	
         res => A2601_reset,--not key(2),
			
  --     p_l => joy1_left_i,
  --     p_r => joy1_right_i,
  --     p_a => joy1_p6_i,
  --     p_u => joy1_up_i,
  --     p_d => joy1_down_i,
  --     p2_l => joy2_left_i,
  --     p2_r => joy2_right_i,
  --     p2_a => joy2_p6_i,
  --     p2_u => joy2_up_i,
  --     p2_d => joy2_down_i,
			
			-- joy_s format MXYZ SACB RLDU 
		
         p_u => joy1_s(0),
         p_d => joy1_s(1),
			p_l => joy1_s(2),
         p_r => joy1_s(3),
         p_a => joy1_s(10) and joy1_s(9) and joy1_s(8) and joy1_s(6) and joy1_s(5) and joy1_s(4),
			
         p2_u => joy2_s(0),
         p2_d => joy2_s(1),
			p2_l => joy2_s(2),
         p2_r => joy2_s(3),
         p2_a => joy2_s(10) and joy2_s(9) and joy2_s(8) and joy2_s(6) and joy2_s(5) and joy2_s(4),
        
			
			paddle_0 => (others=>'0'),
         paddle_1 => (others=>'0'),
         paddle_2 => (others=>'0'),
         paddle_3 => (others=>'0'),
			paddle_ena => '0',
			
         p_s => p_s,
         p_bs => open,
			LED => atari_flags,
			I_SW => I_SW,
         JOYSTICK_GND => open,
			JOYSTICK2_GND => open,
			
			cart_a => cart_a,
			cart_d => cart_d,
			bs_method => bs_method,
			
			clk_tia => clk_tia_s,
			clk_tiax2 => clk_tiax2_s,
			
			clk_dpc => clk_dpc
		);

		
	process(port_243b)
	begin
		if port_243b(7) = '1' then --magic bit to start 
			
			-- A2601

			ram_a				<= "0010" & cart_a;
			to_sram			<= (others=>'Z');
			cart_d			<= from_sram(7 downto 0);
			ram_cs			<= '0';
			ram_oe			<= '0';
			ram_we			<= '1';
			
	--		vga_r_o <= rgb_atari_out(7 downto 5);
	--		vga_g_o <= rgb_atari_out(4 downto 2);
	--		vga_b_o <= rgb_atari_out(1 downto 0) & rgb_atari_out(0);
	--		vga_hsync_n_o <= not hsync_atari_out; 
	--		vga_vsync_n_o <= not vsync_atari_out; 
			

			vga_r_s <= rgb_atari_out(11 downto 8) & '0';
			vga_g_s <= rgb_atari_out(7 downto 4) & '0';
			vga_b_s <= rgb_atari_out(3 downto 0) & '0';
			vga_hsync_n_s <= not hsync_atari_out; 
			vga_vsync_n_s <= not vsync_atari_out; 
			
			dac_l_o <= audio;
			dac_r_o <= audio;
			
			A2601_reset <= '0';	
			
			bs_method 		<= '0' & port_243b(6 downto 0);
			
		else
			
			--LOADER
			
		

			ram_a				<= loader_ram_a;
			
			to_sram			<= loader_to_sram;
			loader_from_sram <= from_sram;
			
			ram_cs			<= loader_ram_cs;
			ram_oe			<= loader_ram_oe;
			ram_we			<= loader_ram_we;
			
			vga_r_s  		<= rgb_loader_out(7 downto 5) & "00";
			vga_g_s  		<= rgb_loader_out(4 downto 2) & "00";
			vga_b_s  		<= rgb_loader_out(1 downto 0) & "000";
			vga_hsync_n_s 	<= hsync_loader_out;
			vga_vsync_n_s 	<= vsync_loader_out;
			
			A2601_reset		 <= '1';
			
		end if;
	end process;
	


			

	p_s  <= not btn_n_i(1); --start game switch
		
	I_SW(3) <= not btn_n_i(2); 						-- select
	I_SW(2) <= not (btn_diffp1_s or joy1_p6_i); 	-- colour/PB (quando o botao do joy esta pressionado)
	I_SW(0) <= not btn_diffp2_s; 						-- difficult P2
	I_SW(1) <= not (btn_diffp1_s or not joy1_p6_i); -- difficult P1 (quando o botao do joy esta solto)
	
	
	Inst_VGA_SCANDBL: work.VGA_SCANDBL PORT MAP(
		I => int_colu,
		I_HSYNC => hsync,
		I_VSYNC => vsync,
		O => vga_colu,
		O_HSYNC => hsync_atari_out,
		O_VSYNC => vsync_atari_out,
		CLK => clk_tia_s,
		CLK_X2 => clk_tiax2_s
	);	
	
	Inst_VGAColorTable: work.VGAColorTable PORT MAP(
		clk => clk_tiax2_s,
--		lum => '0' & vga_colu(2 downto 0),
		lum => vga_colu(2 downto 0) & '0',
		hue => vga_colu(6 downto 3),
		mode => "00", --'0' & pal,	-- 00 = NTSC, 01 = PAL
		outColor => rgbx2
	);	
	
	 rgb_atari_out <= rgbx2(23 downto 20) & rgbx2(15 downto 12)& rgbx2(7 downto 4);
	--rgb_atari_out <= rgbx2(23 downto 21) & rgbx2(19) & rgbx2(15 downto 13) &  rgbx2(11) & rgbx2(7 downto 5)& rgbx2(3);
	
		
	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_n_i(1) or btn_n_i(2),
		result_o			=> btn_scan_s
	);
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	
	vga_r_out_s <=  '0' & vga_r_s(4 downto 1) when scanlines_en_s = '1' and odd_line_s = '1' else vga_r_s;
	vga_g_out_s <=  '0' & vga_g_s(4 downto 1) when scanlines_en_s = '1' and odd_line_s = '1' else vga_g_s;
	vga_b_out_s <=  '0' & vga_b_s(4 downto 1) when scanlines_en_s = '1' and odd_line_s = '1' else vga_b_s;
	
	
	process(vga_hsync_n_s,vga_vsync_n_s)
	begin
		if vga_vsync_n_s = '0' then
			odd_line_s <= '0';
		elsif rising_edge(vga_hsync_n_s) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;
	
	-------------------------------------
	
	displayVideoConfig: entity work.hexy
		generic map 
		(
			yOffset => 50,
			xOffset => 200
		)
		port map (
			clk_i 		=> clk_28,
			vSync_i 		=> vga_vsync_n_s,
			hSync_i 		=> vga_hsync_n_s,
			video_o 		=> OSDBit_s,
			dim_o 		=> videoConfigDim,
			
			diff_p1_i => atari_flags(1),
			diff_p2_i => atari_flags(0),
			type_i 	 => atari_flags(2)
		);
		
		process(videoConfigShow, videoConfigDim)
		begin

			if videoConfigShow = '1' and videoConfigDim = '1' then
				vga_r_o  <= OSDBit_s & vga_r_out_s(3 downto 0);
				vga_g_o  <= OSDBit_s & vga_g_out_s(3 downto 0);
				vga_b_o  <= OSDBit_s & vga_b_out_s(3 downto 0);
				vga_hsync_n_o <= vga_hsync_n_s;
				vga_vsync_n_o <= vga_vsync_n_s;
			else
				vga_r_o  <= vga_r_out_s;
				vga_g_o  <= vga_g_out_s;
				vga_b_o  <= vga_b_out_s;
				vga_hsync_n_o <= vga_hsync_n_s;
				vga_vsync_n_o <= vga_vsync_n_s;
			end if;				

		end process;
		
		process(clk_28)
		begin		
			if rising_edge(clk_28) then
				clk_timeout <= not clk_timeout;
			end if;
		end process;
		
		process(clk_timeout, btn_n_i(3), btn_n_i(4))
		begin
	
 		if rising_edge(clk_timeout) then
 			if btn_n_i(3) = '0' or btn_n_i(4) = '0' then --show the OSD 
 				videoConfigTimeout <= (others => '1');
				videoConfigShow <= '1';
 			end if;
					
			if videoConfigTimeout > 0 then
				videoConfigTimeout <= videoConfigTimeout - 1;
				videoConfigShow <= '1';
			else
				videoConfigShow <= '0';
			end if;
		end if;		

 	end process;
	
	-- debounces
	
	btndiffP1: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_n_i(3),
		result_o			=> btn_diffp1_s
	);
	
	btndiffP2: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_n_i(4),
		result_o			=> btn_diffp2_s
	);

	--- Joystick read with sega 6 button support----------------------

	process(vga_hsync_n_s)
		variable state_v : unsigned(7 downto 0) := (others=>'0');
		variable j1_sixbutton_v : std_logic := '0';
		variable j2_sixbutton_v : std_logic := '0';
	begin
		if falling_edge(vga_hsync_n_s) then
		
			state_v := state_v + 1;
			
			case state_v is
				-- joy_s format MXYZ SACB RLDU
			
				when X"00" =>  
					joyP7_s <= '0';
					
				when X"01" =>
					joyP7_s <= '1';

				when X"02" => 
					joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
					joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
					joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
					joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B					
					joyP7_s <= '0';
					j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
					j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

				when X"03" =>
					joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
					joy2_s(7 downto 6) <= joy2_p9_i & joy2_p6_i; -- Start, A
					joyP7_s <= '1';
			
				when X"04" =>  
					joyP7_s <= '0';

				when X"05" =>
					if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
						j1_sixbutton_v := '1'; --it's a six button
					end if;
					
					if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
						j2_sixbutton_v := '1'; --it's a six button
					end if;
					
					joyP7_s <= '1';
					
				when X"06" =>
					if j1_sixbutton_v = '1' then
						joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
					end if;
					
					if j2_sixbutton_v = '1' then
						joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
					end if;
					
					joyP7_s <= '0';

				when others =>
					joyP7_s <= '1';
					
			end case;

		end if;
	end process;
	
	joyX_p7_o <= joyP7_s;
---------------------------
	
------ NO HDMI --------------------------
	no_hdmi_block : block 

			component no_hdmi is
			port
			(
				-- pixel clock
				pclk			: in std_logic;
				
				-- output to VGA screen
				hs	: out std_logic;
				vs	: out std_logic;
				pixel_o	: out std_logic;
				blank 	: out std_logic
			);
			end component;
			  
			signal no_hdmi_vs_s : std_logic;
			signal no_hdmi_hs_s : std_logic;
			signal no_hdmi_blank_s : std_logic;
			signal no_hdmi_pixel_s : std_logic;
			
			signal tdms_r_s			: std_logic_vector( 9 downto 0);
			signal tdms_g_s			: std_logic_vector( 9 downto 0);
			signal tdms_b_s			: std_logic_vector( 9 downto 0);
			signal hdmi_p_s			: std_logic_vector( 3 downto 0);
			signal hdmi_n_s			: std_logic_vector( 3 downto 0);
			
	begin 			
			no_hdmi1 : no_hdmi 
			port map
			(
				pclk     => clk_vga,
				
				hs    	=> no_hdmi_hs_s,
				vs    	=> no_hdmi_vs_s,
				pixel_o  => no_hdmi_pixel_s,
				blank 	=> no_hdmi_blank_s
			);
		
		
		---------
		
		-- HDMI
			no_hdmi_dvid: entity work.hdmi
			generic map (
				FREQ	=> 25200000,	-- pixel clock frequency 
				FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
				CTS	=> 25200,		-- CTS = Freq(pixclk) * N / (128 * Fs)
				N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
			) 
			port map (
				I_CLK_PIXEL		=> clk_vga,
					
				I_R				=> no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s,
				I_G				=> no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s,
				I_B				=> no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s,
				
				I_BLANK			=> no_hdmi_blank_s,
				I_HSYNC			=> no_hdmi_hs_s,
				I_VSYNC			=> no_hdmi_vs_s,
				-- PCM audio
				I_AUDIO_ENABLE	=> '1',
				I_AUDIO_PCM_L 	=> (others=>'0'),
				I_AUDIO_PCM_R	=> (others=>'0'),
				-- TMDS parallel pixel synchronous outputs (serialize LSB first)
				O_RED				=> tdms_r_s,
				O_GREEN			=> tdms_g_s,
				O_BLUE			=> tdms_b_s
			);
			

			no_hdmi_io: entity work.hdmi_out_altera
			port map (
				clock_pixel_i		=> clk_vga,
				clock_tdms_i		=> clk_dvi,
				red_i					=> tdms_r_s,
				green_i				=> tdms_g_s,
				blue_i				=> tdms_b_s,
				tmds_out_p			=> hdmi_p_s,
				tmds_out_n			=> hdmi_n_s
			);
			
			
			tmds_o(7)	<= hdmi_p_s(2);	-- 2+		
			tmds_o(6)	<= hdmi_n_s(2);	-- 2-		
			tmds_o(5)	<= hdmi_p_s(1);	-- 1+			
			tmds_o(4)	<= hdmi_n_s(1);	-- 1-		
			tmds_o(3)	<= hdmi_p_s(0);	-- 0+		
			tmds_o(2)	<= hdmi_n_s(0);	-- 0-	
			tmds_o(1)	<= hdmi_p_s(3);	-- CLK+	
			tmds_o(0)	<= hdmi_n_s(3);	-- CLK-	
		
	end block;
	
			

end architecture;