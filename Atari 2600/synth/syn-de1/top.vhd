--
-- Terasic DE1 top-level
--

-- altera message_off 10540

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE1 board
entity top is
	port (
		-- Clocks
		CLOCK_24       : in    std_logic_vector(1 downto 0);
		CLOCK_27       : in    std_logic_vector(1 downto 0);
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(9 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);
		 
		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		-- Red LEDs
		LEDR           : out   std_logic_vector(9 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(7 downto 0)		:= (others => '0');
		 
		-- VGA
		VGA_R          : out   std_logic_vector(3 downto 0)		:= (others => '1');
		VGA_G          : out   std_logic_vector(3 downto 0)		:= (others => '1');
		VGA_B          : out   std_logic_vector(3 downto 0)		:= (others => '1');
		VGA_HS         : out   std_logic									:= '1';
		VGA_VS         : out   std_logic									:= '1';
		 
		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';
		 
		-- PS/2 Keyboard
		PS2_CLK        : in std_logic;
		PS2_DAT        : in std_logic;

		-- I2C
		I2C_SCLK       : inout std_logic;
		I2C_SDAT       : inout std_logic;

		-- Audio
		AUD_XCK        : out   std_logic									:= '1';
		AUD_BCLK       : out   std_logic									:= '1';
		AUD_ADCLRCK    : out   std_logic									:= '1';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '1';
		AUD_DACDAT     : out   std_logic									:= '1';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '1');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '1');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '1');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '1');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';
		 
		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '1');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => '1');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';
		 
		-- SD card (SPI mode)
		SD_nCS         : out   std_logic									:= '1';
		SD_MOSI        : out   std_logic									:= '1';
		SD_SCLK        : out   std_logic									:= '1';
		SD_MISO        : in    std_logic;
		 
		-- GPIO
		GPIO_0         : inout std_logic_vector(31 downto 0)		:= (others => '1');
		
		
				-- PS/2
		GPIO_PS2_CLK1        : inout std_logic;
		GPIO_PS2_DAT1        : inout std_logic;

				-- PS/2
		GPIO_PS2_CLK2        : in std_logic;
		GPIO_PS2_DAT2        : in std_logic;

		
		GPIO_1         : inout std_logic_vector(10 downto 0)		:= (others => '1');
		
		
		GPIO_D : inout std_logic_vector(7 downto 0)		:= (others => 'Z');
		GPIO_WR : out   std_logic									:= '1';
		GPIO_RD : out   std_logic									:= '1';
		GPIO_A0 : out   std_logic									:= '1';
		GPIO_CS : out   std_logic									:= '1';
		
		GPIO_INT : in   std_logic	
		
		
	);
end entity;

architecture Behavior of top is

	-- Reset signal
	signal reset_n				: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset			: std_logic;		-- Reset do PLL
	signal pll_locked			: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clk_28		: std_logic;
	signal memory_clock		: std_logic;
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	signal atari_clk: std_logic;
	
	signal port_243b: std_logic_vector(7 downto 0);
	
		signal ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal ram_din				: std_logic_vector(15 downto 0);
	signal ram_dout				: std_logic_vector(15 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector(7 downto 0);
	
	signal from_sram			: std_logic_vector(15 downto 0);
	signal to_sram			: std_logic_vector(15 downto 0);
	
		-- ram
	signal loader_ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal loader_to_sram			: std_logic_vector(15 downto 0);
	signal loader_from_sram			: std_logic_vector(15 downto 0);
	signal loader_ram_data			: std_logic_vector(15 downto 0);
	signal loader_ram_cs				: std_logic;
	signal loader_ram_oe				: std_logic;
	signal loader_ram_we				: std_logic;

	signal a2601_ram_a				: std_logic_vector(13 downto 0);	
	signal a2601_ram_dout			: std_logic_vector(7 downto 0);
	

	-- A2601
	signal audio: std_logic := '0';	
	signal res: std_logic := '0';
	signal p_l: std_logic := '0';
	signal p_r: std_logic := '0';
	signal p_a: std_logic := '0';
	signal p_u: std_logic := '0';
	signal p_d: std_logic := '0';
	signal p2_l: std_logic := '0';
	signal p2_r: std_logic := '0';
	signal p2_a: std_logic := '0';
	signal p2_u: std_logic := '0';
	signal p2_d: std_logic := '0';
	signal p_s: std_logic := '0';
	signal p_bs: std_logic;
	signal LED: std_logic_vector(2 downto 0);
	signal I_SW : std_logic_vector(2 downto 0) := (others => '0');
	
	signal cart_a: std_logic_vector(13 downto 0);
	signal cart_d : std_logic_vector(7 downto 0);
	
	signal int_colu 	: std_logic_vector(6 downto 0);
	signal vga_colu 	: std_logic_vector(6 downto 0);
	signal hsync		: std_logic;
	signal vsync		: std_logic;
	signal clk_tia_s		: std_logic;
	signal clk_tiax2_s		: std_logic;
	signal  rgbx2:  std_logic_vector(23 downto 0);
	
	
	--rgb
	signal rgb_loader_out			: std_logic_vector(7 downto 0);
	signal rgb_atari_out				: std_logic_vector(7 downto 0);
	
	signal hsync_loader_out			: std_logic;
	signal vsync_loader_out			: std_logic;
	
	signal hsync_atari_out			: std_logic;
	signal vsync_atari_out			: std_logic;
	signal bs_method					: std_logic_vector(7 downto 0);
	signal A2601_reset 				: std_logic := '1';

	signal atari_running : std_logic := '0';
	
		-- PS/2
	signal keyb_data_s 			: std_logic_vector(7 downto 0);
	signal keyb_valid_s 			: std_logic;
	
	signal clk_keyb : std_logic;
	
	signal clk7			: std_logic;
	signal clk14		: std_logic;
	
	signal vga_r_s					: std_logic_vector(2 downto 0);
	signal vga_g_s					: std_logic_vector(2 downto 0);
	signal vga_b_s					: std_logic_vector(2 downto 0);

	signal vga_r_out_s			: std_logic_vector(2 downto 0);
	signal vga_g_out_s			: std_logic_vector(2 downto 0);
	signal vga_b_out_s			: std_logic_vector(2 downto 0);
	
	signal vga_hsync_n_s		: std_logic;
	signal vga_vsync_n_s		: std_logic;
	
		--OSD
	signal OSDBit_s         	: std_logic;
	signal videoConfigDim 		: std_logic := '1';
	signal videoConfigTimeout 	: unsigned(23 downto 0) := (others=>'0');
	signal videoConfigShow 		: std_logic := '1';
	
	signal atari_flags : std_logic_vector(2 downto 0);
	
begin

 	ps2 : work.ps2_intf port map 
	(
		clk_keyb,
		reset_n,
		PS2_CLK,
		PS2_DAT,
		keyb_data_s,
		keyb_valid_s,
		open
	);
	
	clk_keyb <= clk_28 when A2601_reset = '1' else vid_clk;

	-- 28 MHz master clock
	pll: work.pll_atari port map (
		areset		=> pll_reset,				-- PLL Reset
		inclk0		=> CLOCK_50,				-- Clock 50 MHz externo
		c0				=> sysclk,					
		c1				=> vid_clk,					
		c2				=> clk_28,					
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
	pll_reset	<= not KEY(0);
	reset_n		<= not (pll_reset or not pll_locked);	-- System is reset by external reset switch or PLL being out of lock
	
	loader : work.speccy48_top port map (
		
		clk_28      => clk_28,
		reset_n_i => reset_n,
	
		-- VGA
		VGA_R(3 downto 1)          => rgb_loader_out (7 downto 5) ,    
		VGA_G(3 downto 1)          => rgb_loader_out (4 downto 2)  ,    
		VGA_B(3 downto 2)          => rgb_loader_out (1 downto 0)   ,   
		VGA_HS         => hsync_loader_out  ,  
		VGA_VS         => vsync_loader_out ,   

		
		-- PS/2 Keyboar   -- PS/2 Ke
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,   
   
   
		SRAM_ADDR      => loader_ram_a ,
		FROM_SRAM      => loader_from_sram   ,
		TO_SRAM       	=> loader_to_sram   ,
		SRAM_CE_N      => loader_ram_cs ,
		SRAM_OE_N      => loader_ram_oe ,
		SRAM_WE_N      => loader_ram_we ,

                  
				  
		SD_nCS         => SD_nCS    ,
		SD_MOSI        => SD_MOSI  , 
		SD_SCLK        => SD_SCLK  , 
		SD_MISO        => SD_MISO   ,
		
		PORT_243B		=> port_243b,
		
		JOYSTICK       => "000" & SW(4 downto 0)
		

		
	);
	
	
	
	
	SRAM_ADDR   <= ram_a;
	SRAM_DQ 		<= to_sram when ram_we = '0' else (others=>'Z');
	from_sram 	<= SRAM_DQ;
	SRAM_CE_N   <= ram_cs;
   SRAM_OE_N   <= ram_oe;
	SRAM_WE_N   <= ram_we;
	SRAM_UB_N   <= '1';
	SRAM_LB_N   <= '0';

--cartridge

--	Inst_cart_rom: entity work.cart_enduro PORT MAP(
--		clock => CLOCK_50,
--		q => cart_d,
--		address => cart_a	
--	);	

	
--cart: entity work.cart_enduro 
--	PORT map
--	(
--		address		=> cart_a(11 downto 0),
--		clock		=>vid_clk,
--		q	=>cart_d
--	);

	

-- -----------------------------------------------------------------------
-- A2601 core
-- -----------------------------------------------------------------------
	a2601Instance : entity work.A2601NoFlash
		port map 
		(
			vid_clk => vid_clk,
			audio => audio,
         
			--O_VSYNC => vsync_atari_out,
         --O_HSYNC => hsync_atari_out,
			--O_VIDEO_R => rgb_atari_out(7 downto 5),
			--O_VIDEO_G => rgb_atari_out(4 downto 2),
			--O_VIDEO_B(2 downto 1) => rgb_atari_out(1 downto 0),
			
			colu => int_colu,
			Hsyn => hsync,
			Vsyn => vsync,
			
         res => A2601_reset or not key(2),
         p_l => p_l,
         p_r => p_r,
         p_a => p_a,
         p_u => p_u,
         p_d => p_d,
         p2_l => p2_l,
         p2_r => p2_r,
         p2_a => p2_a,
         p2_u => p2_u,
         p2_d => p2_d,
			
			paddle_0 => (others=>'Z'), --joy_a_0(15 downto 8),
			paddle_1 => (others=>'Z'), --joy_a_0(7 downto 0),
			paddle_2 => (others=>'Z'), --joy_a_1(15 downto 8),
			paddle_3 => (others=>'Z'), --joy_a_1(7 downto 0),
			paddle_ena => '0', 
		
         p_s => p_s,
         p_bs => open,
			LED => atari_flags,
			I_SW => SW(9 downto 6),
         JOYSTICK_GND => open,
			JOYSTICK2_GND => open,
			
			cart_a => cart_a,
			cart_d => cart_d,
			bs_method => bs_method,
			
			clk_tia => clk_tia_s,
			clk_tiax2 => clk_tiax2_s
			
		);

		
	process(port_243b)
	begin
			if port_243b(7) = '1' then --magic bit to start 
			
			-- A2601


			ram_a				<= "0010" & cart_a;
			to_sram			<= (others=>'Z');
			cart_d			<= from_sram(7 downto 0);
			ram_cs			<= '0';
			ram_oe			<= '0';
			ram_we			<= '1';
			
			vga_r_s <= rgb_atari_out(7 downto 5);
			vga_g_s <= rgb_atari_out(4 downto 2);
			vga_b_s <= rgb_atari_out(1 downto 0) & rgb_atari_out(0);
			vga_hsync_n_s <= not hsync_atari_out; 
			vga_vsync_n_s <= not vsync_atari_out; 
			--dac_l_o <= audio;
			--dac_r_o <= audio;
			
			A2601_reset <= '0';

			
		else
			
			--LOADER
		
			
			bs_method <= '0' & port_243b(6 downto 0);

			ram_a				<= loader_ram_a;
			
			to_sram			<= loader_to_sram;
			loader_from_sram <= from_sram;
			
			ram_cs			<= loader_ram_cs;
			ram_oe			<= loader_ram_oe;
			ram_we			<= loader_ram_we;
			
			vga_r_s  <= rgb_loader_out (7 downto 5);
			vga_g_s  <= rgb_loader_out (4 downto 2);
			vga_b_s  <= rgb_loader_out (1 downto 0) & rgb_loader_out (0);
			vga_hsync_n_s <= hsync_loader_out;
			vga_vsync_n_s <= vsync_loader_out;
			
			A2601_reset <= '1';
					
		end if;
	end process;
	
		--a entrada do scandoubler, ok para 15kkz.
	
		Inst_VGA_SCANDBL: work.VGA_SCANDBL PORT MAP(
		I => int_colu,
		I_HSYNC => hsync,
		I_VSYNC => vsync,
		O => vga_colu,
		O_HSYNC => hsync_atari_out,
		O_VSYNC => vsync_atari_out,
		CLK => clk_tia_s,
		CLK_X2 => clk_tiax2_s
	);	
	
	Inst_VGAColorTable: work.VGAColorTable PORT MAP(
		clk => clk_tiax2_s,
		lum => '0' & vga_colu(2 downto 0),
		hue => vga_colu(6 downto 3),
		mode => "00", --'0' & pal,	-- 00 = NTSC, 01 = PAL
		outColor => rgbx2
	);	
	
	 rgb_atari_out <= rgbx2(23 downto 21) & rgbx2(15 downto 13)& rgbx2(7 downto 6);
	
		

--	p_l <= reset_button_n;
--	p_r <= freeze_n;
--	p_u <= '1';
--	p_d <= '1';

	p_u <= '1';
	p_d <= '1';
	p_l <= '1';
	p_r <= '1';
	p_a <= '1';

	
	p2_u <= '1';
	p2_d <= '1';
	p2_l <= '1';
	p2_r <= '1';
	p2_a <= '1';

	p_s  <= not KEY(3); --start game

	
--	I_SW(2) <= not KEY(1); -- select
--	I_SW(1) <= not SW(2); -- difficult?
--	I_SW(0) <= not SW(3); -- difficult?
	
	----------------------------
	-- debugs
	
		-- Led display
	ld3: work.seg7 port map(
		D		=> cart_a(7 downto 4),
		Q		=> HEX3
	);

	ld2: work.seg7 port map(
		D		=> cart_a(3 downto 0),
		Q		=> HEX2
	);

	ld1: work.seg7 port map(
		D		=> bs_method(7 downto 4),
		Q		=> HEX1
	);

	ld0: work.seg7 port map(
		D		=> bs_method(3 downto 0),
		Q		=> HEX0
	);
	
	GPIO_0(0) <= not hsync;
	GPIO_0(1) <= not vsync;
	GPIO_0(2) <= not hsync_atari_out;
	GPIO_0(3) <= not vsync_atari_out;
	
	LEDr(9 downto 7) <= atari_flags;
	
	--------------------------------------------------
	
	displayVideoConfig: entity work.hexy
		generic map 
		(
			yOffset => 50,
			xOffset => 200
		)
		port map (
			clk_i 		=> clk_28,
			vSync_i 		=> vga_vsync_n_s,
			hSync_i 		=> vga_hsync_n_s,
			video_o 		=> OSDBit_s,
			dim_o 		=> videoConfigDim,
			
			diff_p1_i => atari_flags(0),
			diff_p2_i => atari_flags(1),
			type_i 	 => atari_flags(2)
		);
		
		process(videoConfigShow, videoConfigDim)
		begin

			if videoConfigShow = '1' and videoConfigDim = '1' then
				vga_r_out_s <= OSDBit_s & vga_r_s(2 downto 1);
				vga_g_out_s <= OSDBit_s & vga_g_s(2 downto 1);
				vga_b_out_s <= OSDBit_s & vga_b_s(2 downto 1);
			else
				vga_r_out_s <= vga_r_s(2 downto 0);
				vga_g_out_s <= vga_g_s(2 downto 0);
				vga_b_out_s <= vga_b_s(2 downto 0);
			end if;				

		end process;
	
	
			VGA_R <= vga_r_out_s(2 downto 0) & "0";
			VGA_G <= vga_g_out_s(2 downto 0) & "0";
			VGA_B <= vga_b_out_s(2 downto 0) & "0";
			VGA_HS <= vga_hsync_n_s; 
			VGA_VS <= vga_vsync_n_s; 
	

end architecture;
