

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port (
			-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
		
		
	);
end entity;

architecture Behavior of top is

	-- Reset signal
	signal reset_n				: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset			: std_logic;		-- Reset do PLL
	signal pll_locked			: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clk_28		: std_logic;
	signal clk_timeout		: std_logic;
	signal memory_clock		: std_logic;
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	signal atari_clk: std_logic;
	
	signal ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal ram_din				: std_logic_vector(15 downto 0);
	signal ram_dout			: std_logic_vector(15 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector(7 downto 0);
	
	signal from_sram			: std_logic_vector(15 downto 0);
	signal to_sram			: std_logic_vector(15 downto 0);
	
		-- ram
	signal loader_ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal loader_to_sram			: std_logic_vector(15 downto 0);
	signal loader_from_sram			: std_logic_vector(15 downto 0);
	signal loader_ram_data			: std_logic_vector(15 downto 0);
	signal loader_ram_cs				: std_logic;
	signal loader_ram_oe				: std_logic;
	signal loader_ram_we				: std_logic;

	signal a2601_ram_a				: std_logic_vector(13 downto 0);	
	signal a2601_ram_dout			: std_logic_vector(7 downto 0);
	
	signal port_243b : std_logic_vector(7 downto 0);

	-- A2601
	signal audio: std_logic := '0';	
	signal A2601_reset: std_logic := '0';
	signal p_l: std_logic := '0';
	signal p_r: std_logic := '0';
	signal p_a: std_logic := '0';
	signal p_u: std_logic := '0';
	signal p_d: std_logic := '0';
	signal p2_l: std_logic := '0';
	signal p2_r: std_logic := '0';
	signal p2_a: std_logic := '0';
	signal p2_u: std_logic := '0';
	signal p2_d: std_logic := '0';
	signal p_s: std_logic := '0';
	signal p_bs: std_logic;
	signal LED: std_logic_vector(2 downto 0);
	signal I_SW : std_logic_vector(3 downto 0) := (others => '0');
	
	signal cart_a: std_logic_vector(13 downto 0);
	signal cart_d : std_logic_vector(7 downto 0);
	
	signal int_colu 	: std_logic_vector(6 downto 0);
	signal vga_colu 	: std_logic_vector(6 downto 0);
	signal hsync		: std_logic;
	signal vsync		: std_logic;
	signal clk_tia_s		: std_logic;
	signal clk_tiax2_s		: std_logic;
	signal  rgbx2:  std_logic_vector(23 downto 0);
	
	
	--rgb
	signal rgb_loader_out			: std_logic_vector(7 downto 0);
	signal rgb_atari_out				: std_logic_vector(7 downto 0);
	
	signal hsync_loader_out			: std_logic;
	signal vsync_loader_out			: std_logic;
	
	signal hsync_atari_out			: std_logic;
	signal vsync_atari_out			: std_logic;
	
	signal bs_method			 		: std_logic_vector(7 downto 0);
	
	-- PS/2
	signal keyb_data_s 			: std_logic_vector(7 downto 0);
	signal keyb_valid_s 			: std_logic;
	
	signal clk_keyb 				: std_logic;
	
	------
	signal loader_hor_s 				: std_logic_vector(8 downto 0);
	signal loader_ver_s 				: std_logic_vector(8 downto 0);
	
	signal clk_module_vga					: std_logic;
	signal clk_vga					: std_logic;
	signal clk_dvi					: std_logic;
	
	signal vga_color_s 			: std_logic_vector(3 downto 0);
	signal vga_blank_s 			: std_logic;
	
	-- video
	signal vga_rgb_s  	: std_logic_vector(7 downto 0);
	signal vga_rgb_out_s 		: std_logic_vector(7 downto 0);
	signal vga_hsync_n_s : std_logic;
	signal vga_vsync_n_s : std_logic;
		
	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	
	--OSD
	signal OSDBit_s         	: std_logic;
	signal videoConfigDim 		: std_logic := '1';
	signal videoConfigTimeout 	: unsigned(23 downto 0) := (others=>'0');
	signal videoConfigShow 		: std_logic := '0';
	
	signal atari_flags 			: std_logic_vector(2 downto 0);
	
	signal btn_diffp1_s			: std_logic;
	signal btn_diffp2_s			: std_logic;
	
begin

 	ps2 : work.ps2_intf port map 
	(
		clk_keyb,
		reset_n,
		ps2_clk_io,
		ps2_data_io,
		keyb_data_s,
		keyb_valid_s,
		open
	);
	
	clk_keyb <= clk_28 when A2601_reset = '1' else vid_clk;
  
  
	-- 28 MHz master clock
	pll: work.pll_atari port map (
		areset		=> pll_reset,				-- PLL Reset
		inclk0		=> clock_50_i,				-- Clock 50 MHz externo
		c0				=> sysclk,		
		c1				=> vid_clk,
		c2				=> clk_28,			
		c3 			=> clk_vga,
		c4 			=> clk_dvi,		
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
	pll_reset	<= '1' when btn_n_i(3) = '0' and btn_n_i(4) = '0' else '0';
	reset_n		<= not (pll_reset or not pll_locked);	-- System is reset by external reset switch or PLL being out of lock
	
	loader : work.speccy48_top port map 
	(
		
		clk_28   => clk_28,
		reset_n_i 	=> reset_n,
	
		VGA_R(3 downto 1) => rgb_loader_out (7 downto 5),    
		VGA_G(3 downto 1) => rgb_loader_out (4 downto 2),    
		VGA_B(3 downto 2) => rgb_loader_out (1 downto 0),   
		VGA_HS         	=> hsync_loader_out,  
		VGA_VS         	=> vsync_loader_out,   

		-- PS/2 Keyboard
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  
   
		SRAM_ADDR      => loader_ram_a,
		FROM_SRAM      => loader_from_sram,
		TO_SRAM       	=> loader_to_sram,
		SRAM_CE_N      => loader_ram_cs,
		SRAM_OE_N      => loader_ram_oe,
		SRAM_WE_N      => loader_ram_we ,
                  		  
		SD_nCS         => sd_cs_n_o,
		SD_MOSI        => sd_mosi_o, 
		SD_SCLK        => sd_sclk_o, 
		SD_MISO        => sd_miso_i,
		
		PORT_243B => port_243b,
		
		joystick       => "000" & not joy1_p6_i &  not joy1_up_i &  not joy1_down_i & not joy1_left_i & not joy1_right_i
		

		
	);
	
	sram_addr_o   <= "0" & ram_a;
	sram_data_io  <= to_sram(7 downto 0) when ram_we = '0' else (others=>'Z');
	from_sram(7 downto 0) 	  <= sram_data_io;
	sram_ce_n_o(0)<= ram_cs;
   sram_oe_n_o   <= ram_oe;
	sram_we_n_o   <= ram_we;


--cartridge

--	Inst_cart_rom: entity work.cart_enduro PORT MAP(
--		clock => CLOCK_50,
--		q => cart_d,
--		address => cart_a	
--	);	


-- -----------------------------------------------------------------------
-- A2601 core
-- -----------------------------------------------------------------------
	a2601Instance : entity work.A2601NoFlash port map 
		(
			vid_clk => vid_clk,
			audio => audio,
   --      O_VSYNC => vsync_atari_out,
    --     O_HSYNC => hsync_atari_out,
	--		O_VIDEO_R => rgb_atari_out(7 downto 5),
	--		O_VIDEO_G => rgb_atari_out(4 downto 2),
	--		O_VIDEO_B(2 downto 1) => rgb_atari_out(1 downto 0),
	
			colu => int_colu,
			Hsyn => hsync,
			Vsyn => vsync,
	
	
         res => A2601_reset,--not key(2),
         p_l => joy1_left_i,
         p_r => joy1_right_i,
         p_a => joy1_p6_i,
         p_u => joy1_up_i,
         p_d => joy1_down_i,
         p2_l => joy2_left_i,
         p2_r => joy2_right_i,
         p2_a => joy2_p6_i,
         p2_u => joy2_up_i,
         p2_d => joy2_down_i,
			
			paddle_0 => (others=>'0'),
         paddle_1 => (others=>'0'),
         paddle_2 => (others=>'0'),
         paddle_3 => (others=>'0'),
			paddle_ena => '0',
			
         p_s => p_s,
         p_bs => open,
			LED => atari_flags,
			I_SW => I_SW,
         JOYSTICK_GND => open,
			JOYSTICK2_GND => open,
			
			cart_a => cart_a,
			cart_d => cart_d,
			bs_method => bs_method,
			
			clk_tia => clk_tia_s,
			clk_tiax2 => clk_tiax2_s
		);

		
	process(port_243b)
	begin
		if port_243b(7) = '1' then --magic bit to start 
			
			-- A2601

			ram_a				<= "0010" & cart_a;
			to_sram			<= (others=>'Z');
			cart_d			<= from_sram(7 downto 0);
			ram_cs			<= '0';
			ram_oe			<= '0';
			ram_we			<= '1';
			
	--		vga_r_o <= rgb_atari_out(7 downto 5);
	--		vga_g_o <= rgb_atari_out(4 downto 2);
	--		vga_b_o <= rgb_atari_out(1 downto 0) & rgb_atari_out(0);
	--		vga_hsync_n_o <= not hsync_atari_out; 
	--		vga_vsync_n_o <= not vsync_atari_out; 
			

			vga_rgb_s <= rgb_atari_out;
			vga_hsync_n_s <= not hsync_atari_out; 
			vga_vsync_n_s <= not vsync_atari_out; 
			
			dac_l_o <= audio;
			dac_r_o <= audio;
			
			A2601_reset <= '0';
			
		else
			
			--LOADER
			
			bs_method 		<= '0' & port_243b(6 downto 0);

			ram_a				<= loader_ram_a;
			
			to_sram			<= loader_to_sram;
			loader_from_sram <= from_sram;
			
			ram_cs			<= loader_ram_cs;
			ram_oe			<= loader_ram_oe;
			ram_we			<= loader_ram_we;
			
			vga_rgb_s  		<= rgb_loader_out;
			vga_hsync_n_s 	<= hsync_loader_out;
			vga_vsync_n_s 	<= vsync_loader_out;
			
			A2601_reset		 <= '1';
			
		end if;
	end process;
	


			

	p_s  <= not btn_n_i(1); --start game switch
		
	I_SW(3) <= not btn_n_i(2); 						-- select
	I_SW(2) <= not (btn_diffp1_s or joy1_p6_i); 	-- colour/PB (quando o botao do joy esta pressionado)
	I_SW(0) <= not btn_diffp2_s; 						-- difficult P2
	I_SW(1) <= not (btn_diffp1_s or not joy1_p6_i); -- difficult P1 (quando o botao do joy esta solto)
	
	
	Inst_VGA_SCANDBL: work.VGA_SCANDBL PORT MAP(
		I => int_colu,
		I_HSYNC => hsync,
		I_VSYNC => vsync,
		O => vga_colu,
		O_HSYNC => hsync_atari_out,
		O_VSYNC => vsync_atari_out,
		CLK => clk_tia_s,
		CLK_X2 => clk_tiax2_s
	);	
	
	Inst_VGAColorTable: work.VGAColorTable PORT MAP(
		clk => clk_tiax2_s,
		lum => '0' & vga_colu(2 downto 0),
		hue => vga_colu(6 downto 3),
		mode => "00", --'0' & pal,	-- 00 = NTSC, 01 = PAL
		outColor => rgbx2
	);	
	
	 rgb_atari_out <= rgbx2(23 downto 21) & rgbx2(15 downto 13)& rgbx2(7 downto 6);
	
		
	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_n_i(1) or btn_n_i(2),
		result_o			=> btn_scan_s
	);
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	
	vga_rgb_out_s(7 downto 5) <=  '0' & vga_rgb_s(7 downto 6) when scanlines_en_s = '1' and odd_line_s = '1' else vga_rgb_s(7 downto 5);
	vga_rgb_out_s(4 downto 2) <=  '0' & vga_rgb_s(4 downto 3) when scanlines_en_s = '1' and odd_line_s = '1' else vga_rgb_s(4 downto 2);
	vga_rgb_out_s(1 downto 0) <=  '0' & vga_rgb_s(1) 			 when scanlines_en_s = '1' and odd_line_s = '1' else vga_rgb_s(1 downto 0);
	
	
	process(vga_hsync_n_s,vga_vsync_n_s)
	begin
		if vga_vsync_n_s = '0' then
			odd_line_s <= '0';
		elsif rising_edge(vga_hsync_n_s) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;
	
	-------------------------------------
	
	displayVideoConfig: entity work.hexy
		generic map 
		(
			yOffset => 50,
			xOffset => 200
		)
		port map (
			clk_i 		=> clk_28,
			vSync_i 		=> vga_vsync_n_s,
			hSync_i 		=> vga_hsync_n_s,
			video_o 		=> OSDBit_s,
			dim_o 		=> videoConfigDim,
			
			diff_p1_i => atari_flags(1),
			diff_p2_i => atari_flags(0),
			type_i 	 => atari_flags(2)
		);
		
		process(videoConfigShow, videoConfigDim)
		begin

			if videoConfigShow = '1' and videoConfigDim = '1' then
				vga_r_o  <= OSDBit_s & vga_rgb_out_s(7 downto 6);
				vga_g_o  <= OSDBit_s & vga_rgb_out_s(4 downto 3);
				vga_b_o  <= OSDBit_s & vga_rgb_out_s(1 downto 0);
				vga_hsync_n_o <= vga_hsync_n_s;
				vga_vsync_n_o <= vga_vsync_n_s;
			else
				vga_r_o  <= vga_rgb_out_s(7 downto 5);
				vga_g_o  <= vga_rgb_out_s(4 downto 2);
				vga_b_o  <= vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(0);
				vga_hsync_n_o <= vga_hsync_n_s;
				vga_vsync_n_o <= vga_vsync_n_s;
			end if;				

		end process;
		
		process(clk_28)
		begin		
			if rising_edge(clk_28) then
				clk_timeout <= not clk_timeout;
			end if;
		end process;
		
		process(clk_timeout, btn_n_i(3), btn_n_i(4))
		begin
	
 		if rising_edge(clk_timeout) then
 			if btn_n_i(3) = '0' or btn_n_i(4) = '0' then --show the OSD when change disk or on 1541 LED activity
 				videoConfigTimeout <= (others => '1');
				videoConfigShow <= '1';
 			end if;
					
			if videoConfigTimeout > 0 then
				videoConfigTimeout <= videoConfigTimeout - 1;
				videoConfigShow <= '1';
			else
				videoConfigShow <= '0';
			end if;
		end if;		

 	end process;
	
	-- debounces
	
	btndiffP1: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_n_i(3),
		result_o			=> btn_diffp1_s
	);
	
	btndiffP2: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_n_i(4),
		result_o			=> btn_diffp2_s
	);

	

	
			

end architecture;
