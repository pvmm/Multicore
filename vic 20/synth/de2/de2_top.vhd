
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE2 board
entity de2_top is
	port (
		-- Clocks
		CLOCK_27       : in    std_logic;
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(17 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX4           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX5           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX6           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX7           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		
		-- Red LEDs
		LEDR           : out   std_logic_vector(17 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(8 downto 0)		:= (others => '0');

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';

		-- IRDA
		IRDA_RXD       : in    std_logic;
		IRDA_TXD       : out   std_logic									:= '0';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => 'Z');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		--	ISP1362 Interface	
		OTG_ADDR       : out   std_logic_vector(1 downto 0)		:= (others => '0');	--	ISP1362 Address 2 Bits
		OTG_DATA       : inout std_logic_vector(15 downto 0)		:= (others => 'Z');	--	ISP1362 Data bus 16 Bits
		OTG_CS_N       : out   std_logic									:= '1';					--	ISP1362 Chip Select
		OTG_RD_N       : out   std_logic									:= '1';					--	ISP1362 Write
		OTG_WR_N       : out   std_logic									:= '1';					--	ISP1362 Read
		OTG_RST_N      : out   std_logic									:= '1';					--	ISP1362 Reset
		OTG_FSPEED     : out   std_logic									:= 'Z';					--	USB Full Speed,	0 = Enable, Z = Disable
		OTG_LSPEED     : out   std_logic									:= 'Z';					--	USB Low Speed, 	0 = Enable, Z = Disable
		OTG_INT0       : in    std_logic;															--	ISP1362 Interrupt 0
		OTG_INT1       : in    std_logic;															--	ISP1362 Interrupt 1
		OTG_DREQ0      : in    std_logic;															--	ISP1362 DMA Request 0
		OTG_DREQ1      : in    std_logic;															--	ISP1362 DMA Request 1
		OTG_DACK0_N    : out   std_logic									:= '1';					--	ISP1362 DMA Acknowledge 0
		OTG_DACK1_N    : out   std_logic									:= '1';					--	ISP1362 DMA Acknowledge 1
		
		--	LCD Module 16X2		
		LCD_ON         : out   std_logic									:= '0';					--	LCD Power ON/OFF, 0 = Off, 1 = On
		LCD_BLON       : out   std_logic									:= '0';					--	LCD Back Light ON/OFF, 0 = Off, 1 = On
		LCD_DATA       : inout std_logic_vector(7 downto 0)		:= (others => '0');	--	LCD Data bus 8 bits
		LCD_RW         : out   std_logic									:= '1';					--	LCD Read/Write Select, 0 = Write, 1 = Read
		LCD_EN         : out   std_logic									:= '1';					--	LCD Enable
		LCD_RS         : out   std_logic									:= '1';					--	LCD Command/Data Select, 0 = Command, 1 = Data
		
		--	SD_Card Interface	
		SD_DAT         : inout std_logic									:= 'Z';					--	SD Card Data (SPI MISO)
		SD_DAT3        : inout std_logic									:= 'Z';					--	SD Card Data 3 (SPI /CS)
		SD_CMD         : inout std_logic									:= 'Z';					--	SD Card Command Signal (SPI MOSI)
		SD_CLK         : out   std_logic									:= '1';					--	SD Card Clock (SPI SCLK)
		
		-- I2C
		I2C_SCLK       : inout std_logic									:= 'Z';
		I2C_SDAT       : inout std_logic									:= 'Z';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= 'Z';
		PS2_DAT        : inout std_logic									:= 'Z';

		-- VGA
		VGA_R          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_H          : out   std_logic									:= '0';
		VGA_V          : out   std_logic									:= '0';
		VGA_BLANK		: out   std_logic									:= '1';				
		VGA_SYNC			: out   std_logic									:= '0';	
		VGA_CLK		   : out   std_logic									:= '0';	
		
		-- Ethernet Interface	
		ENET_CLK       : out   std_logic									:= '0';					--	DM9000A Clock 25 MHz
		ENET_DATA      : inout std_logic_vector(15 downto 0)		:= (others => 'Z');	--	DM9000A DATA bus 16Bits
		ENET_CMD       : out   std_logic									:= '0';					--	DM9000A Command/Data Select, 0 = Command, 1 = Data
		ENET_CS_N      : out   std_logic									:= '1';					--	DM9000A Chip Select
		ENET_WR_N      : out   std_logic									:= '1';					--	DM9000A Write
		ENET_RD_N      : out   std_logic									:= '1';					--	DM9000A Read
		ENET_RST_N     : out   std_logic									:= '1';					--	DM9000A Reset
		ENET_INT       : in    std_logic;															--	DM9000A Interrupt
	               
		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- TV Decoder		
		TD_DATA        : in    std_logic_vector(7 downto 0);									--	TV Decoder Data bus 8 bits
		TD_HS          : in    std_logic;															--	TV Decoder H_SYNC
		TD_VS          : in    std_logic;															--	TV Decoder V_SYNC
		TD_RESET       : out   std_logic									:= '1';					--	TV Decoder Reset
	
		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => 'Z');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => 'Z')
	);
end entity;


architecture behavior of de2_top is

	-- Clocks
	signal clock_master_s	: std_logic;
	signal clock_audio_s		: std_logic;

	-- Reset
	signal pll_locked_s		: std_logic;
	signal reset_s				: std_logic;

	-- Memorias
	signal ram_addr_s			: std_logic_vector(15 downto 0);
	signal ram_data_from_s	: std_logic_vector( 7 downto 0);
	signal ram_data_to_s		: std_logic_vector( 7 downto 0);
	signal ram_cs_n_s			: std_logic;
	signal ram_we_n_s			: std_logic;
	
	-- VRAM
	signal vram_addr_s		: std_logic_vector(12 downto 0);
	signal vram_data_from_s	: std_logic_vector( 7 downto 0);
	signal vram_data_to_s	: std_logic_vector( 7 downto 0);
	signal vram_oe_s			: std_logic;
	signal vram_we_s			: std_logic;

	-- K7
	signal mic_s				: std_logic;
	signal ear_s				: std_logic;
	signal ear_e_s				: std_logic;

	-- Audio
	signal psg_s				: std_logic_vector( 7 downto 0);

	-- Video
	signal video_mode80_s	: std_logic;
	signal video_col_idx_s	: std_logic_vector( 3 downto 0);
	signal video_15khz_s		: std_logic_vector( 3 downto 0);
	signal video_31khz_s		: std_logic_vector( 3 downto 0);
	signal video_80_pixel_s	: std_logic;
	
	signal video_r_s			: std_logic_vector( 3 downto 0);
	signal video_g_s			: std_logic_vector( 3 downto 0);
	signal video_b_s			: std_logic_vector( 3 downto 0);
	signal video_hsync_s		: std_logic;
	signal video_vsync_s		: std_logic;
	signal video_csync_s		: std_logic;
	signal video_hsync_n_s	: std_logic;
	signal video_vsync_n_s	: std_logic;
	signal video_blank_s		: std_logic;


	-- Debug
	signal D_cpu_addr_s		: std_logic_vector(15 downto 0);
	signal D_display_s		: std_logic_vector(31 downto 0);
	signal D_80col_en_s		: std_logic;
	signal dbg_signals		: std_logic_vector(7 downto 0);

begin

	--------------------------------
	-- PLL
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0		=> CLOCK_50,
		c0				=> clock_master_s,		--35.44mhz		
		locked		=> pll_locked_s
	);

	

	vic20_int : work.VIC20 
	port map (
    PS2_CLK         => PS2_CLK,
    PS2_DATA        => PS2_DAT,

    AUDIO_OUT       => open,
    VIDEO_R_OUT     => video_r_s,
    VIDEO_G_OUT     => video_g_s,
    VIDEO_B_OUT     => video_b_s,
    HSYNC_OUT       => video_hsync_s, --positive sync!
    VSYNC_OUT       => video_vsync_s, -- positive sync!
    COMP_SYNC_L_OUT => video_csync_s,
	 VIDEO_BLANK     => video_blank_s,
	 
	 SCANDOUBLER    => '1',--SW(0),
	 
    ROM_DATA        => open, 
    ROM_ADDR        => open, 
    ROM_WE_L        => open, 
    ROM_OE_L        => open, 
    ROM_CE_L        => open, 

    RESET_L_N_i      => not reset_s,
    CLK_40_i         => clock_master_s,
	 
	 dbg_o				=> dbg_signals,
	 
	 
	 ram_addr_o			=> ram_addr_s, 
	 ram_to_o		 	=> ram_data_to_s,
	 ram_from_i			=> ram_data_from_s,
	 ram_cs_n_o			=> ram_cs_n_s,
	 ram_we_n_o			=> ram_we_n_s
	 
	 
    );

	 video_hsync_n_s <= not video_hsync_s;
	 video_vsync_n_s <= not video_vsync_s;
	 
	 
	 
	 
	 
	 	SRAM_ADDR  <= "000" & ram_addr_s(15 downto 1);
		SRAM_CE_N  <= ram_cs_n_s;
		SRAM_WE_N  <= ram_we_n_s;
		SRAM_OE_N  <= '0';
		SRAM_UB_N  <= not ram_addr_s(0); -- UB = 0 ativa bits 15..8
		SRAM_LB_N  <= ram_addr_s(0); -- LB = 0 ativa bits 7..0
		
		SRAM_DQ	 <= (others=>'Z') when ram_we_n_s = '1' else "ZZZZZZZZ" &  ram_data_to_s when ram_addr_s(0) = '0' else ram_data_to_s & "ZZZZZZZZ";
		ram_data_from_s  <= SRAM_DQ(7 downto 0) when ram_addr_s(0) = '0' else SRAM_DQ(15 downto 8);
	 
	 
	 


--	-- RAM
--	ram: entity work.dpSRAM_25616
--	port map (
--		clock_i			=> clock_master_s,
--		-- Port 0
--		port0_addr_i	=> "100000" & vram_addr_s,
--		port0_ce_i		=> '1',
--		port0_oe_i		=> vram_oe_s,
--		port0_we_i		=> vram_we_s,
--		port0_d_i		=> vram_data_to_s,
--		port0_d_o		=> vram_data_from_s,
--		-- Port 1
--		port1_addr_i	=> "000" & ram_addr_s,
--		port1_ce_i		=> ram_cs_s,
--		port1_oe_i		=> ram_oe_s,
--		port1_we_i		=> ram_we_s,
--		port1_d_i		=> ram_data_to_s,
--		port1_d_o		=> ram_data_from_s,
--		-- Output to SRAM
--		sram_addr_o		=> SRAM_ADDR,
--		sram_data_io	=> SRAM_DQ,
--		sram_ub_n_o		=> SRAM_UB_N,
--		sram_lb_n_o		=> SRAM_LB_N,
--		sram_ce_n_o		=> SRAM_CE_N,
--		sram_oe_n_o		=> SRAM_OE_N,
--		sram_we_n_o		=> SRAM_WE_N
--	);
--
--	-- Audio
--	audio: entity work.Audio_WM8731
--	port map (
--		clock_i			=> clock_audio_s,
--		reset_i			=> reset_s,
--		mic_i				=> mic_s,
--		ear_o				=> ear_s,
--		psg_i				=> psg_s,
--		-- I2S
--		i2s_xck_o		=> AUD_XCK,
--		i2s_bclk_o		=> AUD_BCLK,
--		i2s_adclrck_o	=> AUD_ADCLRCK,
--		i2s_adcdat_i	=> AUD_ADCDAT,
--		i2s_daclrck_o	=> AUD_DACLRCK,
--		i2s_dacdat_o	=> AUD_DACDAT,
--		-- I2C
--		i2c_sda_io		=> I2C_SDAT,
--		i2c_scl_io		=> I2C_SCLK,
--		feedback_i		=> SW(3)
--	);
--

	-- Glue
	reset_s <= '1' when pll_locked_s = '0' or KEY(0) = '0'	else '0';


	VGA_R <= video_r_s & video_r_s & "00";
	VGA_G <= video_g_s & video_g_s & "00";
	VGA_B <= video_b_s & video_b_s & "00";
	VGA_H <= video_hsync_n_s;
	VGA_V <= video_vsync_n_s;


	VGA_CLK	<= clock_master_s;
	VGA_BLANK <= '1';
   VGA_SYNC <= '0';

	-- Debug
	
	LEDG(0) <= not KEY(0);
	
	
	
	GPIO_0(0) <= video_hsync_n_s;
	GPIO_0(1) <= video_vsync_n_s;
	GPIO_0(2) <= dbg_signals(0);

	D_display_s(31 downto 24) <= X"80" when '1' = video_mode80_s else X"32";
	D_display_s(23 downto 16) <= (others => '0');
	D_display_s(15 downto 0) <= D_cpu_addr_s;

	ld7: entity work.seg7
	port map(
		D		=> D_display_s(31 downto 28),
		Q		=> HEX7
	);

	ld6: entity work.seg7
	port map(
		D		=> D_display_s(27 downto 24),
		Q		=> HEX6
	);

	ld5: entity work.seg7
	port map(
		D		=> D_display_s(23 downto 20),
		Q		=> HEX5
	);

	ld4: entity work.seg7
	port map(
		D		=> D_display_s(19 downto 16),
		Q		=> HEX4
	);

	ld3: entity work.seg7
	port map(
		D		=> D_display_s(15 downto 12),
		Q		=> HEX3
	);

	ld2: entity work.seg7
	port map(
		D		=> video_r_s(3 downto 0),
		Q		=> HEX2
	);

	ld1: entity work.seg7
	port map(
		D		=> video_g_s(3 downto 0),
		Q		=> HEX1
	);

	ld0: entity work.seg7
	port map(
		D		=> video_b_s(3 downto 0),
		Q		=> HEX0
	);

end architecture;
