
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE2 board
entity multicore_top is
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
		
	);
end entity;


architecture behavior of multicore_top is

	-- Clocks
	signal clock_master_s	: std_logic;
	signal clock_audio_s		: std_logic;

	-- Reset
	signal pll_locked_s		: std_logic;
	signal reset_s				: std_logic;

	-- Memorias
	signal ram_addr_s			: std_logic_vector(15 downto 0);
	signal ram_data_from_s	: std_logic_vector( 7 downto 0);
	signal ram_data_to_s		: std_logic_vector( 7 downto 0);
	signal ram_cs_n_s			: std_logic;
	signal ram_we_n_s			: std_logic;
	
	-- VRAM
	signal vram_addr_s		: std_logic_vector(12 downto 0);
	signal vram_data_from_s	: std_logic_vector( 7 downto 0);
	signal vram_data_to_s	: std_logic_vector( 7 downto 0);
	signal vram_oe_s			: std_logic;
	signal vram_we_s			: std_logic;

	-- K7
	signal mic_s				: std_logic;
	signal ear_s				: std_logic;
	signal ear_e_s				: std_logic;

	-- Audio
	signal audio_s				: std_logic_vector( 7 downto 0);
	signal dac_s				: std_logic;

	-- Video
	signal video_mode80_s	: std_logic;
	signal video_col_idx_s	: std_logic_vector( 3 downto 0);
	signal video_15khz_s		: std_logic_vector( 3 downto 0);
	signal video_31khz_s		: std_logic_vector( 3 downto 0);
	signal video_80_pixel_s	: std_logic;
	
	signal video_r_s			: std_logic_vector( 3 downto 0);
	signal video_g_s			: std_logic_vector( 3 downto 0);
	signal video_b_s			: std_logic_vector( 3 downto 0);
	signal video_hsync_s		: std_logic;
	signal video_vsync_s		: std_logic;
	signal video_csync_s		: std_logic;
	signal video_hsync_n_s	: std_logic;
	signal video_vsync_n_s	: std_logic;
	signal video_blank_s		: std_logic;


	-- Debug
	signal D_cpu_addr_s		: std_logic_vector(15 downto 0);
	signal D_display_s		: std_logic_vector(31 downto 0);
	signal D_80col_en_s		: std_logic;
	signal dbg_signals		: std_logic_vector(7 downto 0);

begin

	--------------------------------
	-- PLL
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0		=> clock_50_i,
		c0				=> clock_master_s,		--35.44mhz		
		locked		=> pll_locked_s
	);

	

	vic20_int : work.VIC20 
	port map (
    PS2_CLK         => ps2_clk_io,
    PS2_DATA        => ps2_data_io,

    AUDIO_OUT       => audio_s(7 downto 4),
    VIDEO_R_OUT     => video_r_s,
    VIDEO_G_OUT     => video_g_s,
    VIDEO_B_OUT     => video_b_s,
    HSYNC_OUT       => video_hsync_s, --positive sync!
    VSYNC_OUT       => video_vsync_s, -- positive sync!
    COMP_SYNC_L_OUT => video_csync_s,
	 VIDEO_BLANK     => video_blank_s,
	 
	 SCANDOUBLER    => '1',--SW(0),
	 
    ROM_DATA        => open, 
    ROM_ADDR        => open, 
    ROM_WE_L        => open, 
    ROM_OE_L        => open, 
    ROM_CE_L        => open, 

    RESET_L_N_i      => not reset_s,
    CLK_40_i         => clock_master_s,
	 
	 dbg_o				=> dbg_signals,
	 
	 
	 ram_addr_o			=> ram_addr_s, 
	 ram_to_o		 	=> ram_data_to_s,
	 ram_from_i			=> ram_data_from_s,
	 ram_cs_n_o			=> ram_cs_n_s,
	 ram_we_n_o			=> ram_we_n_s
	 
	 
    );

	 video_hsync_n_s <= not video_hsync_s;
	 video_vsync_n_s <= not video_vsync_s;
	 
	 
	 
	 
	 
	 	sram_addr_o  <= "000" & ram_addr_s(15 downto 0);
		sram_ce_n_o(0)  <= ram_cs_n_s;
		sram_we_n_o  <= ram_we_n_s;
		sram_oe_n_o  <= '0';
		
		sram_data_io <= (others=>'Z') when ram_we_n_s = '1' else ram_data_to_s;
		ram_data_from_s  <= sram_data_io;
	 
	 
	 
	inst_dacl : entity work.dac
	port map(
		clk_i   => clock_master_s,
		res_i   => reset_s,
		dac_i   => audio_s,
		dac_o   => dac_s
	);
	
	dac_l_o <= dac_s;
	dac_r_o <= dac_s;

	-- Glue
	reset_s <= '1' when pll_locked_s = '0' or btn_n_i(1) = '0'	else '0';


	vga_r_o <= video_r_s(3 downto 1);
	vga_g_o <= video_g_s(3 downto 1);
	vga_b_o <= video_b_s(3 downto 1);
	vga_hsync_n_o <= video_hsync_n_s;
	vga_vsync_n_o <= video_vsync_n_s;





	

end architecture;
