--
-- Copyright (c) 2015 Fabio Belavenuto
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity intf_floppy is
	port (
		clock_i				: in  std_logic;								-- Clock >= 20 MHz
		clock_ena_i			: in  std_logic;								-- Clock enable 20Mhz if Clock > 20MHz
		clock_cpu_i			: in  std_logic;								-- CPU Clock (/Wait generation)
		enable_i				: in  std_logic;
		reset_n_i			: in  std_logic;
		disk_in_n_i			: in  std_logic;
		disk_out_n_i		: in  std_logic;
		data_i				: in  std_logic_vector( 7 downto 0);
		data_o				: out std_logic_vector( 7 downto 0);
		address_i			: in  std_logic_vector( 1 downto 0);
		WrNmiMaskReg_en_i	: in  std_logic;								-- Write I/O $E4-$E7
		RdNmiStatus_en_i	: in  std_logic;								-- Read  I/O $E4-$E7
		DrvSel_en_i			: in  std_logic;								-- Write I/O $F4
		nmi_n_o				: out std_logic;
		wait_n_o				: out std_logic;
		-- SD card
		image_num_i			: in  std_logic_vector( 9 downto 0) := "0000000000";
		sd_cs_n_o			: out std_logic;
		sd_miso_i			: in  std_logic;
		sd_mosi_o			: out std_logic;
		sd_sclk_o			: out std_logic;
		-- Debug
		D_track_num_o		: out std_logic_vector( 5 downto 0);
		D_track_ram_addr_o: out std_logic_vector(12 downto 0);
		D_track_ram_we_o	: out std_logic;
		D_error_o			: out std_logic
	);
end entity;

architecture Behavior of intf_floppy is

	signal reset_s				: std_logic;
	signal clk_1M_ena			: std_logic := '0';
	-- Regs
	signal WrNmiMaskReg_r	: std_logic_vector(7 downto 6);
	signal DrvSel_r			: std_logic_vector(7 downto 0);
	signal RdNmiStatus_r		: std_logic_vector(7 downto 0);
	-- Motor
	signal motor_on_s			: std_logic;
	signal motor_on_set_s	: std_logic;
	-- FDC
	signal fdc_drq				: std_logic;
	signal fdc_irq				: std_logic;
	signal fdc_step			: std_logic;
	signal fdc_dirc			: std_logic;
	signal fdc_rclk			: std_logic;
	signal fdc_raw_read_n	: std_logic;
	signal fdc_wg				: std_logic;
	signal fdc_wd				: std_logic;
	signal fdc_tr00_n			: std_logic;
	signal fdc_ip_n			: std_logic;
	signal fdc_wprt_n			: std_logic;
	signal fdc_enas			: std_logic_vector(4 downto 1) := (others => '0');
	 alias fdc_sels			: std_logic_vector(4 downto 1) is DrvSel_r(3 downto 0);
	signal fdc_data_out		: std_logic_vector(7 downto 0);
	-- SPI
	signal track_num_s		: std_logic_vector( 5 downto 0);
	signal track_offset_s	: std_logic_vector(12 downto 0);
	signal track_data_s		: std_logic_vector( 7 downto 0);
	signal track_ram_addr_s	: std_logic_vector(12 downto 0);
	signal track_ram_data_s	: std_logic_vector( 7 downto 0);
	signal track_ram_we_s	: std_logic;
	-- Storage for one track
	type track_ram_t is array(0 to 6271) of std_logic_vector(7 downto 0);
	-- Double-ported RAM for holding a track
	signal track_memory_q : track_ram_t;


begin

	reset_s		<= not reset_n_i;

	floppy: entity work.wd179x
	port map (
		clk			=> clock_i,				-- Clock 20 MHz
		clk_20M_ena	=> clock_ena_i,
		reset			=> reset_s,
		mr_n			=> enable_i,
		cs_n			=> '0',
		re_n			=> disk_in_n_i,
		we_n			=> disk_out_n_i,
		address		=> address_i,
		dal_i			=> data_i,
		dal_o			=> fdc_data_out,
		drq			=> fdc_drq,
		intrq			=> fdc_irq,
		step			=> fdc_step,
		dirc			=> fdc_dirc,
		rclk			=> fdc_rclk,
		raw_read_n	=> fdc_raw_read_n,
		write_gate	=> fdc_wg,
		raw_write_n	=> fdc_wd,				-- 200ns (MFM) or 500ns (FM) pulse
		ready			=> '1',
		tr00_n		=> fdc_tr00_n,
		ip_n			=> fdc_ip_n,
		wprt_n		=> '0',--fdc_wprt_n,
		wr_dat_o		=> open
	);
	
	floppy_if_inst: entity work.floppy_if
	generic map (
		num_tracks_g      => 40
	)
	port map (
		clock_i			=> clock_i,
		clock_en_i		=> clock_ena_i,
		reset_i			=> reset_s,
		-- drive select lines
		drv_ena_i		=> fdc_enas,
		drv_sel_i		=> fdc_sels,
		step_i			=> fdc_step,
		dirc_i			=> fdc_dirc,
		rclk_o			=> fdc_rclk,
		raw_read_n_o	=> fdc_raw_read_n,
		write_gate_i	=> fdc_wg,
		raw_write_n_i	=> fdc_wd,
		tr00_n_o			=> fdc_tr00_n,
		ip_n_o			=> fdc_ip_n,
		-- media interface
		track_num_o		=> track_num_s,
		offset_o			=> track_offset_s,
		data_i			=> track_data_s,
		data_o			=> open,
		write_ready_o	=> open,
		-- fifo control
		fifo_rd_o		=> open,
		fifo_flush_o	=> open
	);

	-- SPI controller
	spictl: entity work.spi_controller
	generic map (
		BLOCK_SIZE_g		=> 512,
		BLOCK_BITS_g		=> 9
	)
	port map (
		-- System Interface
		clock_i				=> clock_i,
		reset_i				=> reset_s,
		-- Track buffer Interface
		track_num_i			=> track_num_s,
		image_num_i			=> image_num_i,
		ram_write_addr_o	=> track_ram_addr_s,
		ram_data_o			=> track_ram_data_s,
		ram_we_o				=> track_ram_we_s,
		-- Card Interface
		sd_cs_n_o			=> sd_cs_n_o,
		sd_mosi_o			=> sd_mosi_o,
		sd_miso_i			=> sd_miso_i,
		sd_sclk_o			=> sd_sclk_o,
		-- Debug
		D_error_o			=> D_error_o
	);

	-- Glue

	-- Dual-ported RAM holding the contents of the track
	track_storage : process (clock_i)
	begin
		if rising_edge(clock_i) then
			if track_ram_we_s = '1' then
				track_memory_q(to_integer(unsigned(track_ram_addr_s))) <= track_ram_data_s;
			end if;
			track_data_s <= track_memory_q(to_integer(unsigned(track_offset_s)));
		end if;
	end process;



	fdc_enas(1) <= '1';

	-- FDC NMI
	nmi_n_o		<= not ( (fdc_irq and WrNmiMaskReg_r(7)) or (not motor_on_s and WrNmiMaskReg_r(6)) );

	-- Registro de status
	RdNmiStatus_r		<= (not fdc_irq) & motor_on_s & reset_n_i & "11111";				-- 0 = acontecendo NMI

	-- Escrita nas portas I/O
	-- WrNmiMaskReg
	process (reset_s, WrNmiMaskReg_en_i)
	begin
		if reset_s = '1' then
			WrNmiMaskReg_r	<= (others => '0');
		elsif falling_edge(WrNmiMaskReg_en_i) then
			WrNmiMaskReg_r	<= data_i(7 downto 6);
		end if;
	end process;

	-- DrvSel e gerador wait
	process (reset_s, clock_cpu_i, fdc_irq, fdc_drq, motor_on_s)
		subtype count_t is integer range 0 to 3999; -- 2ms watchdog
		variable count : count_t := count_t'high;
	begin
		if reset_s = '1' then
			DrvSel_r			<= (others => '0');
			count				:= count_t'high;
			motor_on_set_s	<= '0';
		elsif falling_edge(clock_cpu_i) then
			motor_on_set_s	<= '0';
			if DrvSel_en_i = '1' then
				DrvSel_r			<= data_i;
				count				:= 0;
				motor_on_set_s	<= '1';
			elsif count /= count_t'high then
				count				:= count + 1;
			end if;
		end if;
		if (fdc_irq or fdc_drq) = '1' or (count = count_t'high) then	-- async reset
			DrvSel_r(6)		<= '0';
		end if;
		if motor_on_s = '0' then													-- async reset
			DrvSel_r(4 downto 0) <= (others => '0');
			DrvSel_r(7)				<= '0';
		end if;
	end process;

	wait_n_o			<= not DrvSel_r(6);
--	wait_n_o			<= '1';

	-- Motor ON/OFF
	process (reset_s, clock_i, clock_ena_i)
		subtype count_t is integer range 0 to 40000000; -- 2s watchdog
		variable count : count_t := count_t'high;
	begin
		if reset_s = '1' then
			motor_on_s	<= '0';
			count			:= count_t'high;
		elsif rising_edge(clock_i) and clock_ena_i = '1' then
			if motor_on_set_s = '1' then
				motor_on_s	<= '1';
				count		:= 0;
			else
				if count /= count_t'high then
					count := count + 1;
				else
					motor_on_s <= '0';
				end if;
			end if;
		end if;
	end process;
	
	-- MUX de dados de saida
	data_o	<= fdc_data_out	when disk_in_n_i      = '0' and enable_i = '1'	else
	            RdNmiStatus_r	when RdNmiStatus_en_i = '1' and enable_i = '1'	else
					(others => '1');

	-- Debug
	D_track_num_o			<= track_num_s;
	D_track_ram_addr_o	<= track_ram_addr_s;
	D_track_ram_we_o		<= track_ram_we_s;

end architecture;
