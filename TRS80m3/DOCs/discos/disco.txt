Formato TRSDOS:

Densidade dupla, Lado simples, Setores de 256 bytes, 18 setores por trilha, 40 trilhas

40 * 18 * 256 = 184320 = 180K

256 * 18 = 4608 bytes por trilha

VHDL usa 6272 bytes por trilha => Gaps e IDs junto com os dados

Formato de um setor (em MFM):

GAP II				=> 22 x $4E, 12 x $00, 3 x $A1
IDAM				=> $FE
TRACK NUMBER		=> $xx
SIDE NUMBER			=> $xx
SECTOR NUMBER		=> $xx
SECTOR LENGTH		=> $xx
CRC1				=> $xx
CRC2				=> $xx

GAP III				=> 22 x $4E, 8 x $00, 3 x $A1
DATA AM				=> $xx ($F8 ou $FB)
DATA FIELD			=> 256 x $xx
CRC 1				=> $xx
CRC 2				=> $xx


IBM - 40 tracks
 =====================================
   1 x $FC        Index mark
  50 x $4E        Post index mark
 -------------------------------------
  9 times
   -----------------------------------
   |  12 x $00    Pre address mark
   |   3 x $A1    Sync.
   |   1 x $FE    Address mark
   |   1 x $xx    Track number  (0-39)
   |   1 x $xx    Side number (0/1)
   |   1 x $xx    Sector number (1-9)
   |   1 x $02    Sector length
   |   2 x $xx    CRC value
   |  22 x $4E    Post address mark
   |  12 x $00
   |   3 x $A1    Sync.
   |   1 x $FB    Data address mark
   | 512 x $xx    --sector data--
   |   2 x $xx    CRC value
   |  80 x $4E    Post data mark
 =====================================