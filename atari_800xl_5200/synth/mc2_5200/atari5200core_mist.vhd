--------------------------------------------------------------------------- -- (c) 2013 mark watson
-- I am happy for anyone to use this for non-commercial use.
-- If my vhdl files are used commercially or otherwise sold,
-- please contact me for explicit permission at scrameta (gmail).
-- This applies for source and binary form and derived works.
---------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

LIBRARY work;

ENTITY atari5200core_mist IS 
	GENERIC
	(
		TV : integer := 0;  -- 1 = PAL, 0=NTSC
		VIDEO : integer := 2; -- 1 = RGB, 2 = VGA
		COMPOSITE_SYNC : integer := 0; --0 = no, 1 = yes!
		SCANDOUBLE : integer := 1 -- 1 = YES, 0=NO, (+ later scanlines etc)
	);
	PORT
	(
			-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram2_addr_o		: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram2_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram2_we_n_o		: out   std_logic								:= '1';
		sram2_oe_n_o		: out   std_logic								:= '1';

		sram3_addr_o		: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram3_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram3_we_n_o		: out   std_logic								:= '1';
		sram3_oe_n_o		: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(3 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(3 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(3 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		smt_rx_i				: in std_logic;
		smt_tx_o				: out std_logic;
		smt_rst_o			: out std_logic;
		
		smt_b8_io			: inout std_logic;
		smt_b9_io			: inout std_logic;
		smt_b12_io			: inout std_logic;
		smt_b13_io			: inout std_logic;
		smt_b14_io			: inout std_logic;
		smt_b15_io			: inout std_logic	
	);
END atari5200core_mist;

ARCHITECTURE vhdl OF atari5200core_mist IS 

component hq_dac
port (
  reset :in std_logic;
  clk :in std_logic;
  clk_ena : in std_logic;
  pcm_in : in std_logic_vector(19 downto 0);
  dac_out : out std_logic
);
end component;
-- SYSTEM
	SIGNAL CLK : STD_LOGIC;
	SIGNAL CLK_SDRAM : STD_LOGIC;
	SIGNAL RESET_N : STD_LOGIC;
	signal SDRAM_RESET_N : std_logic;
	SIGNAL PLL_LOCKED : STD_LOGIC;

	-- GTIA
	signal CONSOL_OUT : std_logic_vector(3 downto 0);
	signal CONSOL_IN : std_logic_vector(3 downto 0);
	signal GTIA_TRIG : std_logic_vector(3 downto 0);
	
	SIGNAL TRIG : STD_LOGIC_VECTOR(1 downto 0);
	
	-- CARTRIDGE ACCESS
	--SIGNAL	CART_RD4 :  STD_LOGIC;
	--SIGNAL	CART_RD5 :  STD_LOGIC;
	--SIGNAL	CART_S4_n :  STD_LOGIC;
	--SIGNAL	CART_S5_n :  STD_LOGIC;
	--SIGNAL	CART_CCTL_n :  STD_LOGIC;
	
	-- PBI
	SIGNAL PBI_WRITE_DATA : std_logic_vector(31 downto 0);
	SIGNAL PBI_WIDTH_32BIT_ACCESS : std_logic;
	SIGNAL PBI_WIDTH_16BIT_ACCESS : std_logic;
	SIGNAL PBI_WIDTH_8BIT_ACCESS : std_logic;
	
	-- INTERNAL ROM/RAM
	SIGNAL	RAM_ADDR :  STD_LOGIC_VECTOR(18 DOWNTO 0);
	SIGNAL	RAM_DO :  STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL	RAM_REQUEST :  STD_LOGIC;
	SIGNAL	RAM_REQUEST_COMPLETE :  STD_LOGIC;
	SIGNAL	RAM_WRITE_ENABLE :  STD_LOGIC;
	
	SIGNAL	ROM_ADDR :  STD_LOGIC_VECTOR(21 DOWNTO 0);
	SIGNAL	ROM_DO :  STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL	ROM_REQUEST :  STD_LOGIC;
	SIGNAL	ROM_REQUEST_COMPLETE :  STD_LOGIC;

	-- SDRAM
	signal SDRAM_REQUEST : std_logic;
	signal SDRAM_REQUEST_COMPLETE : std_logic;
	signal SDRAM_READ_ENABLE :  STD_LOGIC;
	signal SDRAM_WRITE_ENABLE : std_logic;
	signal SDRAM_ADDR : STD_LOGIC_VECTOR(22 DOWNTO 0);
	signal SDRAM_DO : STD_LOGIC_VECTOR(31 DOWNTO 0);
	
	signal SDRAM_REFRESH : std_logic;
	
	-- pokey keyboard
	SIGNAL KEYBOARD_SCAN : std_logic_vector(5 downto 0);
	SIGNAL KEYBOARD_RESPONSE : std_logic_vector(1 downto 0);
	
	-- SIO
	SIGNAL SIO_RXD : std_logic;
	SIGNAL SIO_COMMAND : std_logic;
	SIGNAL SIO_TXD : std_logic;

	SIGNAL GPIO_SIO_RXD : std_logic;

	-- VIDEO
	signal VGA_VS_RAW : std_logic;
	signal VGA_HS_RAW : std_logic;
	signal VGA_CS_RAW : std_logic;

	-- AUDIO
	signal AUDIO_LEFT : std_logic_vector(15 downto 0);
	signal AUDIO_RIGHT : std_logic_vector(15 downto 0);

	-- dma/virtual drive
	signal DMA_ADDR_FETCH : std_logic_vector(23 downto 0);
	signal DMA_WRITE_DATA : std_logic_vector(31 downto 0);
	signal DMA_FETCH : std_logic;
	signal DMA_32BIT_WRITE_ENABLE : std_logic;
	signal DMA_16BIT_WRITE_ENABLE : std_logic;
	signal DMA_8BIT_WRITE_ENABLE : std_logic;
	signal DMA_READ_ENABLE : std_logic;
	signal DMA_MEMORY_READY : std_logic;
	signal DMA_MEMORY_DATA : std_logic_vector(31 downto 0);

	signal ZPU_ADDR_ROM : std_logic_vector(15 downto 0);
	signal ZPU_ROM_DATA :  std_logic_vector(31 downto 0);

	signal ZPU_OUT1 : std_logic_vector(31 downto 0);
	signal ZPU_OUT2 : std_logic_vector(31 downto 0);
	signal ZPU_OUT3 : std_logic_vector(31 downto 0);
	signal ZPU_OUT4 : std_logic_vector(31 downto 0);

	signal zpu_pokey_enable : std_logic;
	signal zpu_sio_txd : std_logic;
	signal zpu_sio_rxd : std_logic;
	signal zpu_sio_command : std_logic;

	SIGNAL FKEYS : std_logic_vector(11 downto 0);

	-- system control from zpu
	signal reset_atari : std_logic;
	signal pause_atari : std_logic;
	SIGNAL speed_6502 : std_logic_vector(5 downto 0);

	-- GPIO
	signal GPIO_0_DIR_OUT : std_logic_vector(35 downto 0);
	signal GPIO_0_OUT : std_logic_vector(35 downto 0);
	signal GPIO_1_DIR_OUT : std_logic_vector(35 downto 0);
	signal GPIO_1_OUT : std_logic_vector(35 downto 0);
	signal TRIGGERS : std_logic_vector(3 downto 0);

	-- POT
	signal POT_RESET : std_logic;
	signal POT_IN : std_logic_vector(7 downto 0);

	-- scandoubler
	signal half_scandouble_enable_reg : std_logic;
	signal half_scandouble_enable_next : std_logic;
	signal VIDEO_B : std_logic_vector(7 downto 0);
	
	--
	signal SLOW_PS2_CLK : std_logic;
	
	signal cycle_length : integer := 16; -- or 32...
	constant min_lines : integer := 15;
	constant max_lines : integer := 210;
	
	signal JOY1_N : std_logic_vector(4 downto 0); -- FRLDU, 0=pressed
	signal JOY2_N : std_logic_vector(4 downto 0); -- FRLDU, 0=pressed
	
	signal PS2_KEYS : STD_LOGIC_VECTOR(511 downto 0);

BEGIN 


	JOY1_N <= joy1_p6_i & joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i;
	JOY2_N <= joy2_p6_i & joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i;

sram1 : entity work.sram
PORT MAP(WREN => RAM_WRITE_ENABLE,
		 clk => CLK,
		 reset_n => RESET_N,
		 request => RAM_REQUEST,
		 width_16bit => PBI_WIDTH_16BIT_ACCESS,
		 ADDRESS => RAM_ADDR,
		 DIN => PBI_WRITE_DATA(15 DOWNTO 0),

		 sram2_addr_o	=> sram2_addr_o,	
		 sram2_data_io	=> sram2_data_io,	
		 sram2_we_n_o	=> sram2_we_n_o,	
		 sram2_oe_n_o	=> sram2_oe_n_o,	
		                
		 sram3_addr_o	=> sram3_addr_o,	
		 sram3_data_io	=> sram3_data_io,	
		 sram3_we_n_o	=> sram3_we_n_o,	
		 sram3_oe_n_o	=> sram3_oe_n_o,	
		 
		 
		 
		 
		 
		 
		 complete => RAM_REQUEST_COMPLETE,
		 DOUT => RAM_DO);

sdram_adaptor : entity work.sdram_statemachine
GENERIC MAP(ADDRESS_WIDTH => 22,
			AP_BIT => 10,
			COLUMN_WIDTH => 8,
			ROW_WIDTH => 12
			)
PORT MAP(CLK_SYSTEM => CLK,
		 CLK_SDRAM => CLK_SDRAM,
		 RESET_N =>  RESET_N,
		 READ_EN => SDRAM_READ_ENABLE,
		 WRITE_EN => SDRAM_WRITE_ENABLE,
		 REQUEST => SDRAM_REQUEST,
		 BYTE_ACCESS => PBI_WIDTH_8BIT_ACCESS,
		 WORD_ACCESS => PBI_WIDTH_16BIT_ACCESS,
		 LONGWORD_ACCESS => PBI_WIDTH_32BIT_ACCESS,
		 REFRESH => SDRAM_REFRESH,
		 ADDRESS_IN => SDRAM_ADDR,
		 DATA_IN => PBI_WRITE_DATA(31 downto 0),
		 SDRAM_DQ => sdram_da_io,
		 COMPLETE => SDRAM_REQUEST_COMPLETE,
		 SDRAM_BA0 => sdram_ba_o(0),
		 SDRAM_BA1 => sdram_ba_o(1),
		 SDRAM_CKE => sdram_cke_o,
		 SDRAM_CS_N => sdram_cs_o,
		 SDRAM_RAS_N => sdram_ras_o,
		 SDRAM_CAS_N => sdram_cas_o,
		 SDRAM_WE_N => sdram_we_o,
		 SDRAM_ldqm => sdram_dqm_o(0),
		 SDRAM_udqm => sdram_dqm_o(1),
		 DATA_OUT => SDRAM_DO,
		 SDRAM_ADDR => sdram_ad_o(11 downto 0),
		 reset_client_n => SDRAM_RESET_N
		 );


sdram_ad_o(12) <= '0';

		 
-- GTIA triggers
--GTIA_TRIG <= CART_RD5&"1"&JOY2_n(4)&JOY1_n(4);
--GTIA_TRIG <= TRIGGERS(3 downto 0);

-- Internal rom/ram
internalromram1 : entity work.internalromram
	GENERIC MAP
	(
		internal_rom => 0,
		internal_ram => 0
	)
	PORT MAP (
 		clock   => CLK,
		reset_n => RESET_N,

		ROM_ADDR => ROM_ADDR,
		ROM_REQUEST_COMPLETE => ROM_REQUEST_COMPLETE,
		ROM_REQUEST => ROM_REQUEST,
		ROM_DATA => ROM_DO,
		
		RAM_ADDR => RAM_ADDR,
		RAM_WR_ENABLE => RAM_WRITE_ENABLE,
		RAM_DATA_IN => PBI_WRITE_DATA(7 downto 0),
		RAM_REQUEST_COMPLETE => open,
		RAM_REQUEST => RAM_REQUEST,
		RAM_DATA => open
	);


--gpio0_gen:
--   for I in 0 to 35 generate
--		gpio_0(I) <= gpio_0_out(I) when gpio_0_dir_out(I)='1' else 'Z';
--   end generate gpio0_gen;
--
--gpio1_gen:
--   for I in 0 to 35 generate
--		gpio_1(I) <= gpio_1_out(I) when gpio_1_dir_out(I)='1' else 'Z';
--   end generate gpio1_gen;
--
--gpio1 : entity work.gpio
--PORT MAP(clk => CLK,
--		 gpio_enable => '1',
--		 pot_reset => pot_reset,
--		 pbi_write_enable => '0',
--		 cart_request => '0',
--		 cart_complete => open,
--		 cart_data_read => open,
--		 s4_n => '0',
--		 s5_n => '0',
--		 cctl_n => '0',
--		 cart_data_write => x"00",
--		 GPIO_0_IN => GPIO_0,
--		 GPIO_0_OUT => GPIO_0_OUT,
--		 GPIO_0_DIR_OUT => GPIO_0_DIR_OUT,
--		 GPIO_1_IN => GPIO_1,
--		 GPIO_1_OUT => GPIO_1_OUT,
--		 GPIO_1_DIR_OUT => GPIO_1_DIR_OUT,		 
--		 keyboard_scan => KEYBOARD_SCAN, --5200
--		 pbi_addr_out => X"0000",
--		 porta_out => (others=>'0'),
--		 porta_output => (others=>'0'),
--		 lightpen => open,
--		 rd4 => open,
--		 rd5 => open,
--		 keyboard_response => KEYBOARD_RESPONSE, -- 5200
--		 porta_in => open,
--		 pot_in => pot_in,
--		 trig_in => TRIGGERS, -- 5200
--		 CA2_DIR_OUT => '0',
--		 CA2_OUT => '0',
--		 CA2_IN => open,
--		 CB2_DIR_OUT => '0',
--		 CB2_OUT => '0',
--		 CB2_IN => open,
--		 SIO_IN => GPIO_SIO_RXD,
--		 SIO_OUT => SIO_TXD,
--		CONSOL => CONSOL_OUT
--		 );

	process(clk,RESET_N,SDRAM_RESET_N,reset_atari)
	begin
		if ((RESET_N and SDRAM_RESET_N and not(reset_atari))='0') then
			half_scandouble_enable_reg <= '0';
		elsif (clk'event and clk='1') then
			half_scandouble_enable_reg <= half_scandouble_enable_next;
		end if;
	end process;

	half_scandouble_enable_next <= not(half_scandouble_enable_reg);

scandoubler : entity work.scandoubler
GENERIC MAP
(
	video_bits=>4
)
PORT MAP(CLK => CLK,
		 RESET_N => RESET_N and SDRAM_RESET_N and not(reset_atari),
		 VGA => '1',
		 COMPOSITE_ON_HSYNC => '0',
		 colour_enable => half_scandouble_enable_reg,
		 doubled_enable => '1',
	 	 scanlines_on => '0',
		 vsync_in => VGA_VS_RAW,
		 hsync_in => VGA_HS_RAW,
		 csync_in => VGA_CS_RAW,
		 pal => '0',
		 colour_in => VIDEO_B,
		 VSYNC => vga_vsync_n_o,
		 HSYNC => vga_hsync_n_o,
		 B => vga_b_o,
		 G => vga_g_o,
		 R => vga_r_o);



mist_pll : entity work.pll_ntsc
PORT MAP(inclk0 => clock_50_i,
		 c0 => CLK_SDRAM,
		 c1 => CLK,
		 c2 => sdram_clk_o,
		 c3 => SLOW_PS2_CLK,
		 locked => PLL_LOCKED);
		 
--pll : entity work.pll
--PORT MAP(inclk0 => CLOCK_50,
--		 c0 => CLK_SDRAM,
--		 c1 => CLK,
--		 c2 => DRAM_CLK,
--		 locked => PLL_LOCKED);

--gen_ntsc_pll : if tv=0 generate
--pll : entity work.pll_ntsc
--PORT MAP(inclk0 => CLOCK_27(0),
--		 c0 => CLK_SDRAM,
--		 c1 => CLK,
--		 c2 => DRAM_CLK,
--		 locked => PLL_LOCKED);
--end generate;
--
--gen_pal_pll : if tv=1 generate
--pll : entity work.pll_pal
--PORT MAP(inclk0 => CLOCK_27(0),
--		 c0 => CLK_SDRAM,
--		 c1 => CLK,
--		 c2 => DRAM_CLK,
--		 locked => PLL_LOCKED);
--end generate;

RESET_N <= PLL_LOCKED;

-- PS2 to pokey
keyboard_map1 : entity work.ps2_to_atari5200
	PORT MAP
	( 
		CLK => clk,
		RESET_N => reset_n,
		PS2_CLK => ps2_clk_io,
		PS2_DAT => ps2_data_io,
		
		KEYBOARD_SCAN => KEYBOARD_SCAN,
		KEYBOARD_RESPONSE => KEYBOARD_RESPONSE,

		FIRE2 => '0' & '0' & not joy2_p6_i & not joy1_p6_i, --(others=>'0'),
		CONTROLLER_SELECT => CONSOL_OUT(1 downto 0),--(others=>'0'),

		FKEYS => FKEYS,
		
		PS2_KEYS => PS2_KEYS
		
	);
	
-- SIO
--UART_TXD <= SIO_TXD;


SIO_COMMAND <= 'Z'; -- no PIA...
zpu_sio_command <= SIO_COMMAND;
zpu_sio_rxd <= SIO_TXD;
SIO_RXD <= zpu_sio_txd;-- and UART_RXD;

-- VIDEO
CONSOL_IN <= "1000";

atari5200 : entity work.atari5200core
	GENERIC MAP
	(
		cycle_length => 32,
		video_bits => 8,
		palette => 0
	)
	PORT MAP
	(
		CLK => CLK,
		RESET_N => RESET_N and SDRAM_RESET_N and not(reset_atari),

		VIDEO_VS => VGA_VS_RAW,
		VIDEO_HS => VGA_HS_RAW,
		VIDEO_CS => VGA_CS_RAW,
		VIDEO_B => VIDEO_B,
		VIDEO_G => open,
		VIDEO_R => open,

		AUDIO_L => AUDIO_LEFT,
		AUDIO_R => AUDIO_RIGHT,

		KEYBOARD_RESPONSE => KEYBOARD_RESPONSE,
		KEYBOARD_SCAN => KEYBOARD_SCAN,

		POT_IN => POT_IN,
		POT_RESET => POT_RESET,
		
		PBI_ADDR => open,
		PBI_WRITE_ENABLE => open,
		PBI_SNOOP_DATA => open,
		PBI_WRITE_DATA => PBI_WRITE_DATA,
		PBI_WIDTH_8bit_ACCESS => PBI_WIDTH_8bit_ACCESS,
		PBI_WIDTH_16bit_ACCESS => PBI_WIDTH_16bit_ACCESS,
		PBI_WIDTH_32bit_ACCESS => PBI_WIDTH_32bit_ACCESS,

		PBI_ROM_DO => "11111111",
		PBI_REQUEST => open,
		PBI_REQUEST_COMPLETE => '1',

		SIO_RXD => '1',--SIO_RXD,
		SIO_TXD => open,--SIO_TXD,

		CONSOL_OUT => CONSOL_OUT,
		CONSOL_IN => CONSOL_IN,
		GTIA_TRIG => GTIA_TRIG,
		
		SDRAM_REQUEST => SDRAM_REQUEST,
		SDRAM_REQUEST_COMPLETE => SDRAM_REQUEST_COMPLETE,
		SDRAM_READ_ENABLE => SDRAM_READ_ENABLE,
		SDRAM_WRITE_ENABLE => SDRAM_WRITE_ENABLE,
		SDRAM_ADDR => SDRAM_ADDR,
		SDRAM_DO => SDRAM_DO,

		ANTIC_REFRESH => SDRAM_REFRESH,

		RAM_ADDR => RAM_ADDR,
		RAM_DO => RAM_DO,
		RAM_REQUEST => RAM_REQUEST,
		RAM_REQUEST_COMPLETE => RAM_REQUEST_COMPLETE,
		RAM_WRITE_ENABLE => RAM_WRITE_ENABLE,
		
		ROM_ADDR => ROM_ADDR,
		ROM_DO => ROM_DO,
		ROM_REQUEST => ROM_REQUEST,
		ROM_REQUEST_COMPLETE => ROM_REQUEST_COMPLETE,

		DMA_FETCH => dma_fetch,
		DMA_READ_ENABLE => dma_read_enable,
		DMA_32BIT_WRITE_ENABLE => dma_32bit_write_enable,
		DMA_16BIT_WRITE_ENABLE => dma_16bit_write_enable,
		DMA_8BIT_WRITE_ENABLE => dma_8bit_write_enable,
		DMA_ADDR => dma_addr_fetch,
		DMA_WRITE_DATA => dma_write_data,
		MEMORY_READY_DMA => dma_memory_ready,
		--DMA_MEMORY_DATA => dma_memory_data,  
		PBI_SNOOP_DATA => DMA_MEMORY_DATA,

		USE_SDRAM => '1', --SW(9), -- TODO
		ROM_IN_RAM => '1',
		HALT => pause_atari,
		THROTTLE_COUNT_6502 => speed_6502
	);

zpu: entity work.zpucore
	GENERIC MAP
	(
		platform => 1,
		spi_clock_div => 1 -- 28MHz/2. Max for SD cards is 25MHz...
	)
	PORT MAP
	(
		-- standard...
		CLK => CLK,
		RESET_N => RESET_N and sdram_reset_n,

		-- dma bus master (with many waitstates...)
		ZPU_ADDR_FETCH => dma_addr_fetch,
		ZPU_DATA_OUT => dma_write_data,
		ZPU_FETCH => dma_fetch,
		ZPU_32BIT_WRITE_ENABLE => dma_32bit_write_enable,
		ZPU_16BIT_WRITE_ENABLE => dma_16bit_write_enable,
		ZPU_8BIT_WRITE_ENABLE => dma_8bit_write_enable,
		ZPU_READ_ENABLE => dma_read_enable,
		ZPU_MEMORY_READY => dma_memory_ready,
		ZPU_MEMORY_DATA => dma_memory_data, 

		-- rom bus master
		-- data on next cycle after addr
		ZPU_ADDR_ROM => zpu_addr_rom,
		ZPU_ROM_DATA => zpu_rom_data,

		-- spi master
		-- Too painful to bit bang spi from zpu, so we have a hardware master in here
		ZPU_SD_DAT0 => sd_miso_i, --sd_data,
		ZPU_SD_CLK => sd_sclk_o, --sd_clk,
		ZPU_SD_CMD => sd_mosi_o, --sd_cmd,
		ZPU_SD_DAT3 => sd_cs_n_o, --sd_three,

		-- SIO
		-- Ditto for speaking to Atari, we have a built in Pokey
		ZPU_POKEY_ENABLE => zpu_pokey_enable,
		ZPU_SIO_TXD => zpu_sio_txd,
		ZPU_SIO_RXD => zpu_sio_rxd,
		ZPU_SIO_COMMAND => zpu_sio_command,

		-- external control
		-- switches etc. sector DMA blah blah.
		--ZPU_IN1 => X"00000"&FKEYS,
		
		ZPU_IN1 => X"000"&
			"00"&(ps2_keys(16#76#)& ps2_keys(16#5A#)&not ps2_keys(16#16b#)&not ps2_keys(16#174#)&not ps2_keys(16#175#)&not ps2_keys(16#172#))& -- (esc)FLRDU
			FKEYS,
				
		ZPU_IN2 => X"00000000",
		ZPU_IN3 => X"00000000",
		ZPU_IN4 => X"00000000",

		-- ouputs - e.g. Atari system control, halt, throttle, rom select
		ZPU_OUT1 => zpu_out1,
		ZPU_OUT2 => zpu_out2,
		ZPU_OUT3 => zpu_out3,
		ZPU_OUT4 => zpu_out4
	);

	pause_atari <= zpu_out1(0);
	reset_atari <= zpu_out1(1);
	speed_6502 <= zpu_out1(7 downto 2);

	zpu_rom1: entity work.zpu_rom
	port map(
	        clock => clk,
	        address => zpu_addr_rom(13 downto 2),
	        q => zpu_rom_data
	);

	enable_179_clock_div_zpu_pokey : entity work.enable_divider
	generic map (COUNT=>32) -- cycle_length
	port map
	(
		clk=>clk,reset_n=>reset_n,enable_in=>'1',enable_out=>zpu_pokey_enable
	);
	
	dac_left : hq_dac
	port map
	(
	  reset => not(reset_n),
	  clk => clk,
	  clk_ena => '1',
	  pcm_in => AUDIO_LEFT&"0000",
	  dac_out => dac_l_o
	);

	dac_right : hq_dac
	port map
	(
	  reset => not(reset_n),
	  clk => clk,
	  clk_ena => '1',
	  pcm_in => AUDIO_RIGHT&"0000",
	  dac_out => dac_r_o
	);
	-----------------------------------------
	
	
-- GTIA triggers
GTIA_TRIG <= "11"&JOY2_n(4)&JOY1_n(4);

-- pots 
pot0 : entity work.pot_from_signed
	GENERIC MAP
	(
		cycle_length=>16,
		min_lines=>15,
		max_lines=>210
	)
	PORT MAP
	(
		CLK => CLK,
		RESET_N => RESET_N,
		ENABLED => CONSOL_OUT(2),
		POT_RESET => POT_RESET,
		POS => (others=>'0'),
		FORCE_LOW => NOT(JOY1_N(2)),
		FORCE_HIGH => NOT(JOY1_N(3)),
		POT_HIGH => POT_IN(0)
	);
pot1 : entity work.pot_from_signed
	GENERIC MAP
	(
		cycle_length=>16,
		min_lines=>15,
		max_lines=>210
	)
	PORT MAP
	(
		CLK => CLK,
		RESET_N => RESET_N,
		ENABLED => CONSOL_OUT(2),
		POT_RESET => POT_RESET,
		POS => (others=>'0'),
		FORCE_LOW => NOT(JOY1_N(0)),
		FORCE_HIGH => NOT(JOY1_N(1)),
		POT_HIGH => POT_IN(1)
	);
pot2 : entity work.pot_from_signed
	GENERIC MAP
	(
		cycle_length=>16,
		min_lines=>15,
		max_lines=>210
	)
	PORT MAP
	(
		CLK => CLK,
		RESET_N => RESET_N,
		ENABLED => CONSOL_OUT(2),
		POT_RESET => POT_RESET,
		POS => (others=>'0'),
		FORCE_LOW => NOT(JOY2_N(2)),
		FORCE_HIGH => NOT(JOY2_N(3)),
		POT_HIGH => POT_IN(2)
	);
pot3 : entity work.pot_from_signed
	GENERIC MAP
	(
		cycle_length=>16,
		min_lines=>15,
		max_lines=>210
	)
	PORT MAP
	(
		CLK => CLK,
		RESET_N => RESET_N,
		ENABLED => CONSOL_OUT(2),
		POT_RESET => POT_RESET,
		POS => (others=>'0'),
		FORCE_LOW => NOT(JOY2_N(0)),
		FORCE_HIGH => NOT(JOY2_N(1)),
		POT_HIGH => POT_IN(3)
	);
POT_IN(7 downto 4) <= (others=>'0');

END vhdl;
