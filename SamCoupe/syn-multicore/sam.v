

module mc2_toplevel (

	// Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [3:0]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram_data_io	= 8'bzzzzzzzz,
	output wire	sram_we_n_o		= 1'b1,
	output wire	sram_oe_n_o		= 1'b1,
		
	// SDRAM	(H57V256)
	output wire	[12:0]sdram_ad_o,
	inout wire	[15:0]sdram_da_io,
	output wire	[1:0]sdram_ba_o,
	output wire	[1:0]sdram_dqm_o,
	output wire	sdram_ras_o,
	output wire	sdram_cas_o,
	output wire	sdram_cke_o,
	output wire	sdram_clk_o,
	output wire	sdram_cs_o,
	output wire	sdram_we_o,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  	= 1'bz,
	inout wire	ps2_mouse_data_io 	= 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output wire	dac_l_o				= 1'b0,
	output wire	dac_r_o				= 1'b0,
	input wire	ear_i,
	output wire	mic_o				= 1'b0,

		// VGA
	output wire	[4:0]vga_r_o,
	output wire	[4:0]vga_g_o,
	output wire	[4:0]vga_b_o,
	output wire	vga_hsync_n_o,
	output wire	vga_vsync_n_o,

		// HDMI
	output wire	[7:0]tmds_o				= 8'b00000000,

		//STM32
	input wire	stm_tx_i,
	output wire	stm_rx_o,
	output wire	stm_rst_o			= 1'b0,
		
	inout wire	stm_b8_io,
	inout wire	stm_b9_io,
	inout wire	stm_b12_io,
	inout wire	stm_b13_io,
	inout wire	stm_b14_io,
	inout wire	stm_b15_io
		
);

    // Interface with RAM
    wire [18:0] sram_addr_from_sam;
    wire sram_we_n_from_sam;
    
    // Audio and video
    wire [1:0] sam_r, sam_g, sam_b;
    wire sam_bright;
    
	 // scandoubler
	 wire hsync_pal, vsync_pal;
    wire [2:0] ri = {sam_r, sam_bright};
    wire [2:0] gi = {sam_g, sam_bright};
    wire [2:0] bi = {sam_b, sam_bright};
    
    wire clk24, clk12, clk6, clk8;

    reg [7:0] poweron_reset = 8'h00;
    reg [1:0] scandoubler_ctrl = 2'b00;
    always @(posedge clk6) begin
       poweron_reset <= {poweron_reset[6:0], 1'b1};
        if (poweron_reset[6] == 1'b0)
		  begin
//            	scandoubler_ctrl <= sram_data[1:0];
					scandoubler_ctrl[0] <= 1'b1; //scandoubler active
					scandoubler_ctrl[1] <= 1'b0; //scanlines active
			end
    end

    assign sram_addr_o = (poweron_reset[7] == 1'b0)? 21'h008FD5 : {2'b00, sram_addr_from_sam};
    assign sram_we_n_o = (poweron_reset[7] == 1'b0)? 1'b1 : sram_we_n_from_sam;
	 assign sram_oe_n_o = 1'b0;
	 


    pll los_relojes (
        .inclk0       (clock_50_i),      // IN
        .c0           (clk24),  // modulo multiplexor de SRAM
        .c1           (clk12),  // ASIC
        .c2           (clk6),   // CPU y teclado PS/2
        .c3           (clk8)    // SAA1099 y DAC
    );

    samcoupe maquina (
        .clk24(clk24),
        .clk12(clk12),
        .clk6(clk6),
        .clk8(clk8),
        .master_reset_n(poweron_reset[7]),
        // Video output
        .r(sam_r),
        .g(sam_g),
        .b(sam_b),
        .bright(sam_bright),
	    .hsync_pal(hsync_pal),
		 .vsync_pal(vsync_pal),
        // Audio output
        .ear(~ear_i),
        .audio_out_left(dac_l_o),
        .audio_out_right(dac_r_o),
        // PS/2 keyboard
        .clkps2(ps2_clk_io),
        .dataps2(ps2_data_io),
        // SRAM external interface
        .sram_addr(sram_addr_from_sam),
        .sram_data(sram_data_io),
        .sram_we_n(sram_we_n_from_sam)
    );        
	 
	vga_scandoubler #(.CLKVIDEO(12000)) salida_vga (
		.clkvideo(clk12),
		.clkvga(clk24),
		.enable_scandoubling(scandoubler_ctrl[0]),
        .disable_scaneffect(~scandoubler_ctrl[1]),
		.ri(ri),
		.gi(gi),
		.bi(bi),
		.hsync_ext_n(hsync_pal),
		.vsync_ext_n(vsync_pal),
		.ro(vga_r_o[4:2]),
		.go(vga_g_o[4:2]),
		.bo(vga_b_o[4:2]),
		.hsync(vga_hsync_n_o),
		.vsync(vga_vsync_n_o)
   );	 	

		assign vga_r_o[1:0] = 2'b0;
		assign vga_g_o[1:0] = 2'b0;
		assign vga_b_o[1:0] = 2'b0;
	 
endmodule
