library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

library work;
use work.pace_pkg.all;
use work.sdram_pkg.all;
use work.video_controller_pkg.all;
use work.maple_pkg.all;
use work.gamecube_pkg.all;
use work.project_pkg.all;
use work.platform_pkg.all;
use work.target_pkg.all;

entity target_top is
  port
  (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
  );

end target_top;

architecture SYN of target_top is

	constant DE1_HAS_BURCHED_PERIPHERAL   : boolean := false;
	constant DE1_TEST_BURCHED_LEDS        : boolean := false;
	constant DE1_TEST_BURCHED_DIPS        : boolean := false;
	constant DE1_TEST_BURCHED_7SEG        : boolean := false;

	signal init       	 : std_logic := '1';
	signal reset_loader_s : std_logic := '0';
  	signal end_load_s		 : std_logic := '0';
		
	signal clkrst_i       : from_CLKRST_t;
	signal buttons_i      : from_BUTTONS_t;
	signal switches_i     : from_SWITCHES_t;
	signal leds_o         : to_LEDS_t;
	signal inputs_i       : from_INPUTS_t;
	signal flash_i        : from_FLASH_t;
	signal flash_o        : to_FLASH_t;
	signal sram_i			 : from_SRAM_t;
	signal sram_o			 : to_SRAM_t;	
	signal sdram_i        : from_SDRAM_t;
	signal sdram_o        : to_SDRAM_t;
	signal video_i        : from_VIDEO_t;
	signal video_o        : to_VIDEO_t;
	signal audio_i        : from_AUDIO_t;
	signal audio_o        : to_AUDIO_t;
	signal ser_i          : from_SERIAL_t;
	signal ser_o          : to_SERIAL_t;
	signal project_i      : from_PROJECT_IO_t;
	signal project_o      : to_PROJECT_IO_t;
	signal platform_i     : from_PLATFORM_IO_t;
	signal platform_o     : to_PLATFORM_IO_t;
	signal target_i       : from_TARGET_IO_t;
	signal target_o       : to_TARGET_IO_t;
		
	-- cart
	signal cart_addr_s 		: std_logic_vector(11 downto 0) := (others=>'0');
	signal cart_q_s	 		: std_logic_vector(7 downto 0);

	signal last_data_s     	: std_logic_vector(7 downto 0);
	
	signal audio_dac_s 		: std_logic;
		
begin




  BLK_CLOCKING : block
  begin
  
    clkrst_i.clk_ref <= clock_50_i;
    

    
      pll_50_inst : entity work.pll1

        port map
        (
          inclk0  => clock_50_i,
          c0      => clkrst_i.clk(0),
          c1      => clkrst_i.clk(1),
			 c2      => clkrst_i.clk(2)
        );

  end block BLK_CLOCKING;
	
  -- FPGA STARTUP
	-- should extend power-on reset if registers init to '0'
	process (clock_50_i)
		variable count : unsigned(11 downto 0) := (others => '0');
	begin
		if rising_edge(clock_50_i) then
			if count = X"FFF" then
				init <= '0';
			else
				count := count + 1;
				init <= '1';
			end if;
		end if;
	end process;
	
	reset_loader_s <= not (btn_n_i(3) or btn_n_i(4));

  clkrst_i.arst <= init or not btn_n_i(1);-- or (not end_load_s);
	clkrst_i.arst_n <= not clkrst_i.arst;

  GEN_RESETS : for i in 0 to 3 generate

    process (clkrst_i.clk(i), clkrst_i.arst)
      variable rst_r : std_logic_vector(2 downto 0) := (others => '0');
    begin
      if clkrst_i.arst = '1' then
        rst_r := (others => '1');
      elsif rising_edge(clkrst_i.clk(i)) then
        rst_r := rst_r(rst_r'left-1 downto 0) & '0';
      end if;
      clkrst_i.rst(i) <= rst_r(rst_r'left);
    end process;

  end generate GEN_RESETS;
	
  -- buttons - active low
 -- buttons_i <= std_logic_vector(resize(unsigned(not key), buttons_i'length));
  -- switches - up = high
--  switches_i <= std_logic_vector(resize(unsigned(sw), switches_i'length));
  -- leds
  --ledr <= leds_o(ledr'range);
  
	-- inputs

  GEN_NO_BURCHED_PERIPHERAL : if not DE1_HAS_BURCHED_PERIPHERAL generate
    -- ps/2
    inputs_i.ps2_kclk <= ps2_clk_io;
    inputs_i.ps2_kdat <= ps2_data_io;
    inputs_i.ps2_mclk <= '0';
    inputs_i.ps2_mdat <= '0';

  end generate GEN_NO_BURCHED_PERIPHERAL;
  
  

		inputs_i.jamma_n.coin(1) <= '1';
		inputs_i.jamma_n.p(1).start <= '1';
		inputs_i.jamma_n.p(1).up <= joy1_up_i;
		inputs_i.jamma_n.p(1).down <= joy1_down_i;
		inputs_i.jamma_n.p(1).left <= joy1_left_i;
		inputs_i.jamma_n.p(1).right <= joy1_right_i;
		inputs_i.jamma_n.p(1).button <= "00" & joy1_p6_i & '1' & joy1_p9_i;

  
	-- not currently wired to any inputs
	inputs_i.jamma_n.coin_cnt <= (others => '1');
	inputs_i.jamma_n.coin(2) <= '1';
	inputs_i.jamma_n.p(2).start <= '1';
  inputs_i.jamma_n.p(2).up <= '1';
  inputs_i.jamma_n.p(2).down <= '1';
	inputs_i.jamma_n.p(2).left <= '1';
	inputs_i.jamma_n.p(2).right <= '1';
	inputs_i.jamma_n.p(2).button <= (others => '1');
	inputs_i.jamma_n.service <= '1';
	inputs_i.jamma_n.tilt <= '1';
	inputs_i.jamma_n.test <= '1';
		




  -- static memory
  BLK_SRAM : block
  begin
  
    GEN_SRAM : if PACE_HAS_SRAM generate
      sram_addr_o <= sram_o.a(sram_addr_o'range);
      sram_i.d <= std_logic_vector(resize(unsigned(sram_data_io), sram_i.d'length));
      sram_data_io <= sram_o.d(sram_data_io'range) when (sram_o.cs = '1' and sram_o.we = '1') else (others => 'Z');
      sram_ce_n_o(0) <= not sram_o.cs;
      sram_oe_n_o <= not sram_o.oe;
      sram_we_n_o <= not sram_o.we;
    end generate GEN_SRAM;
    
    GEN_NO_SRAM : if not PACE_HAS_SRAM generate
      sram_addr_o <= (others => 'Z');
      sram_i.d <= (others => '1');
      sram_data_io <= (others => 'Z');
      sram_ce_n_o(0) <= '1';
      sram_oe_n_o <= '1';
      sram_we_n_o <= '1';  
    end generate GEN_NO_SRAM;
    
  end block BLK_SRAM;

  

   
  BLK_VIDEO : block
  begin

		video_i.clk <= clkrst_i.clk(1);	-- by convention
		video_i.clk_ena <= '1';
    video_i.reset <= clkrst_i.rst(1);
    
    vga_r_o <= video_o.rgb.r(video_o.rgb.r'left downto video_o.rgb.r'left-2);
    vga_g_o <= video_o.rgb.g(video_o.rgb.g'left downto video_o.rgb.g'left-2);
    vga_b_o <= video_o.rgb.b(video_o.rgb.b'left downto video_o.rgb.b'left-2);
    vga_hsync_n_o <= video_o.hsync;
    vga_vsync_n_o <= video_o.vsync;

  end block BLK_VIDEO;

  

  

    -- enable each channel independantly for debugging
  --  dac_l_o <= audio_o.ldata; --when switches_i(9) = '0' else (others => '0');
   -- dac_r_o <= audio_o.rdata; --when switches_i(8) = '0' else (others => '0');

		-- Audio
	audioout: entity work.dac
	generic map (
		msbi_g		=> 7
	)
	port map (
		clk_i		=> clkrst_i.clk(0),
		res_i		=> clkrst_i.arst,
		dac_i		=> audio_o.ldata(15 downto 8),
		dac_o		=> audio_dac_s
	);
   
	dac_l_o	<= audio_dac_s;
	dac_r_o	<= audio_dac_s;
	
  
  pace_inst : entity work.pace                                            
    port map
    (
    	-- clocks and resets
	  	clkrst_i					=> clkrst_i,

      -- misc inputs and outputs
      buttons_i         => buttons_i,
      switches_i        => switches_i,
      leds_o            => leds_o,
      
      -- controller inputs
      inputs_i          => inputs_i,

     	-- external ROM/RAM
     	flash_i           => flash_i,
      flash_o           => flash_o,
      sram_i        		=> sram_i,
      sram_o        		=> sram_o,
     	sdram_i           => sdram_i,
     	sdram_o           => sdram_o,
  
      -- VGA video
      video_i           => video_i,
      video_o           => video_o,
      
      -- sound
      audio_i           => audio_i,
      audio_o           => audio_o,

      -- SPI (flash)
      spi_i.din         => '0',
      spi_o             => open,
  
      -- serial
      ser_i             => ser_i,
      ser_o             => ser_o,
      
      -- custom i/o
      project_i         => project_i,
      project_o         => project_o,
      platform_i        => platform_i,
      platform_o        => platform_o,
      target_i          => target_i,
      target_o          => target_o,
		
		cart_addr_o			=> cart_addr_s,
		cart_q_i				=> cart_q_s
		
		
    );

  




  
  -----------------------------------------------------------------------------
  -- CART ROM (4K)
  -----------------------------------------------------------------------------
  cart_rom : entity work.sprom
    generic map 
	(
		numwords_a		=> 4096,
      widthad_a     => 12,
     -- init_file     => "../../../../src/platform/adventurevision/roms/carts/defender.hex"
	   --init_file     => "../../../../src/platform/adventurevision/roms/carts/scobra.hex" 
	--	init_file     => "../../../../src/platform/adventurevision/roms/carts/turtles.hex" 
		init_file     => "../../../../src/platform/adventurevision/roms/carts/spacef.hex" 
    )
    port map 
		(
      clock    		=> clkrst_i.clk(2),
      address 			=> cart_addr_s,
      q       			=> open --cart_q_s
    );
	 
	 
	BLK_FILL_CART : block
	
		signal asmi_addr_s         : std_logic_vector(23 downto 0) := "000100000000000000000000";
		signal asmi_dout_s         : std_logic_vector(7 downto 0);
		
		signal asmi_data_valid_s 	: std_LOGIC := '0';
		signal asmi_busy_s 			: std_LOGIC := '0';
		
		signal load_ram_addr_s 		: std_logic_vector(11 downto 0) := (others=>'1');
		
		signal cart_ram_addr_s 		: std_logic_vector(11 downto 0) := (others=>'0');
		signal cart_ram_data_s     : std_logic_vector(7 downto 0);
		

		
		signal cart_ram_wr_s 		: std_LOGIC := '0';
		
		signal get_next_byte 		: std_LOGIC := '1';
		signal pulse_next_byte 		: std_LOGIC := '0';
		


		
		------
		signal btn_change_rom_s		: std_logic := '1';
		signal rom_counter			: std_logic_vector(1 downto 0) := "00";


		
  begin
	 

	
	
	asmi: entity work.asmi_parallel_altasmi_parallel_bke2
	 PORT map
	 ( 
		 addr	=> "0001000000" & rom_counter & asmi_addr_s(11 downto 0),
		 busy => asmi_busy_s,
		 clkin	=> clkrst_i.clk(2),
		 data_valid	=> asmi_data_valid_s,
		 dataout	=> asmi_dout_s,
		 rden	=> pulse_next_byte,
		 read	=> pulse_next_byte,
		 reset	=> clkrst_i.arst
	 );
	
	
	--gera o pulso de read e aguarda o proximo pulso
	get_data:process(clkrst_i.clk(2), asmi_busy_s)
	begin
		if rising_edge(clkrst_i.clk(2) ) then
			if asmi_busy_s = '0' and end_load_s = '0' then --se nao esta ocupado, gera o proximo pulso
				pulse_next_byte <= '1';  
			else
				pulse_next_byte <= '0';  
			end if;
	  end if;
	end process;
--	
	--quando o data_valid sobe, esta pronto para gravar
	process (asmi_data_valid_s)
	begin
	
		if reset_loader_s = '1' then
				load_ram_addr_s <= (others=>'1');
				asmi_addr_s <= "000100000000000000000000";
				end_load_s <= '0';
		elsif rising_edge(asmi_data_valid_s) then

			--write to ram
			cart_ram_data_s 	<= asmi_dout_s;	
			load_ram_addr_s 	<= load_ram_addr_s + 1;

			last_data_s <= asmi_dout_s;				
					
			if asmi_addr_s(15 downto 0) < X"0fff" then
			
				asmi_addr_s <= asmi_addr_s + 1;

			else
				--fim do fill cart
				end_load_s <= '1';
				
			end if;
			
		end if;

	end process;

		
		

	cart_ram_wr_s <= asmi_data_valid_s;

	cart_ram_addr_s <= load_ram_addr_s when asmi_data_valid_s = '1'  else cart_addr_s;
	
	leds_n_o(0) <= end_load_s;
	leds_n_o(7) <= asmi_data_valid_s;
	
	
	 
	-----------------------------------------------------------------------------
	-- CART ROM (4K)
	-----------------------------------------------------------------------------
	 cart_ram : entity work.spram
	 generic map 
	(
			numwords_a	=> 4096,
			widthad_a   => 12
	 )
	 port map 
	(
		clock    		=> clkrst_i.clk(2),
		address 			=> cart_ram_addr_s,
		data 				=> cart_ram_data_s,
		wren 				=> cart_ram_wr_s,
		q       			=> cart_q_s
	 );
	 
	 
	 
	 btnadd: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clkrst_i.clk(2), --18mhz
		button_i			=> btn_n_i(3) or btn_n_i(4),
		result_o			=> btn_change_rom_s
	);
	
	process (btn_change_rom_s)
	begin
		if falling_edge(btn_change_rom_s) then
			rom_counter <= rom_counter + 1;
		end if;
	end process;
--
--	btnEar: entity work.debounce
--	generic map (
--		counter_size_g	=> 16
--	)
--	port map (
--		clk_i				=> clkrst_i.clk(2), --18mhz
--		button_i			=> key(1),
--		result_o			=> btn_ear_s
--	);
--	
--	get_data:process(clkrst_i.clk(2), btn_ear_s)
--		variable idle : boolean;
--	begin
--		if btn_ear_s = '1' then
--			idle := true;
--		elsif rising_edge(clkrst_i.clk(2)) then
--			pulse_next_byte <= '0';     -- default action
--			if idle then
--			--	if flag = '1' then
--					pulse_next_byte <= '1';  -- overrides default FOR THIS CYCLE ONLY
--					idle := false;
--			--	end if;
--			--else
--			--	if flag = '0' then
--			--		idle := true;
--			--	end if;
--			end if;
--	  end if;
--	end process;
--	
--	
--	process (asmi_data_valid_s)
--	begin
--		if rising_edge(asmi_data_valid_s) then
--			last_data_s <= asmi_dout_s;
--		end if;
--	end process;
--
	end block BLK_FILL_CART;
  
end SYN;
