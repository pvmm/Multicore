-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BALLBOMB_ROM_3 is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(10 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of BALLBOMB_ROM_3 is


  type ROM_ARRAY is array(0 to 2047) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"0F",x"21",x"74",x"20",x"7E",x"FE",x"30",x"DC", -- 0x0000
    x"22",x"10",x"FE",x"E8",x"D4",x"2A",x"10",x"2B", -- 0x0008
    x"7E",x"FE",x"18",x"DA",x"31",x"10",x"21",x"71", -- 0x0010
    x"20",x"CD",x"A1",x"0D",x"2A",x"73",x"20",x"C3", -- 0x0018
    x"2B",x"0F",x"F5",x"E5",x"2A",x"73",x"20",x"C3", -- 0x0020
    x"4B",x"0F",x"E5",x"2A",x"73",x"20",x"C3",x"57", -- 0x0028
    x"0F",x"2A",x"73",x"20",x"CD",x"3E",x"0F",x"21", -- 0x0030
    x"6E",x"46",x"11",x"6E",x"20",x"C3",x"67",x"0F", -- 0x0038
    x"21",x"11",x"20",x"AF",x"BE",x"C8",x"36",x"00", -- 0x0040
    x"21",x"14",x"20",x"34",x"21",x"00",x"00",x"22", -- 0x0048
    x"16",x"20",x"2A",x"1A",x"20",x"CD",x"C3",x"08", -- 0x0050
    x"21",x"60",x"20",x"AF",x"BE",x"CA",x"B0",x"10", -- 0x0058
    x"21",x"65",x"20",x"CD",x"8F",x"10",x"D4",x"6C", -- 0x0060
    x"10",x"C3",x"B0",x"10",x"CD",x"96",x"10",x"DA", -- 0x0068
    x"73",x"10",x"C9",x"CD",x"9D",x"10",x"D2",x"7A", -- 0x0070
    x"10",x"C9",x"CD",x"A9",x"10",x"DA",x"81",x"10", -- 0x0078
    x"C9",x"E1",x"2A",x"65",x"20",x"22",x"93",x"20", -- 0x0080
    x"21",x"91",x"20",x"34",x"C3",x"5E",x"0F",x"3A", -- 0x0088
    x"1A",x"20",x"C6",x"08",x"BE",x"C9",x"F5",x"3E", -- 0x0090
    x"10",x"86",x"C3",x"A5",x"10",x"3A",x"1B",x"20", -- 0x0098
    x"23",x"F5",x"7E",x"D6",x"08",x"47",x"F1",x"B8", -- 0x00A0
    x"C9",x"F5",x"3E",x"10",x"86",x"C3",x"A5",x"10", -- 0x00A8
    x"21",x"67",x"20",x"AF",x"BE",x"CA",x"E7",x"10", -- 0x00B0
    x"21",x"6C",x"20",x"CD",x"8F",x"10",x"D4",x"C4", -- 0x00B8
    x"10",x"C3",x"E7",x"10",x"CD",x"96",x"10",x"DA", -- 0x00C0
    x"CB",x"10",x"C9",x"CD",x"9D",x"10",x"D2",x"D2", -- 0x00C8
    x"10",x"C9",x"CD",x"A9",x"10",x"DA",x"D9",x"10", -- 0x00D0
    x"C9",x"E1",x"2A",x"6C",x"20",x"22",x"93",x"20", -- 0x00D8
    x"21",x"91",x"20",x"34",x"C3",x"CB",x"0F",x"21", -- 0x00E0
    x"6E",x"20",x"AF",x"BE",x"CA",x"1E",x"11",x"21", -- 0x00E8
    x"73",x"20",x"CD",x"8F",x"10",x"D4",x"FB",x"10", -- 0x00F0
    x"C3",x"1E",x"11",x"CD",x"96",x"10",x"DA",x"02", -- 0x00F8
    x"11",x"C9",x"CD",x"9D",x"10",x"D2",x"09",x"11", -- 0x0100
    x"C9",x"CD",x"A9",x"10",x"DA",x"10",x"11",x"C9", -- 0x0108
    x"E1",x"2A",x"73",x"20",x"22",x"93",x"20",x"21", -- 0x0110
    x"91",x"20",x"34",x"C3",x"31",x"10",x"21",x"38", -- 0x0118
    x"20",x"22",x"75",x"20",x"0E",x"06",x"7E",x"A7", -- 0x0120
    x"CA",x"33",x"11",x"23",x"23",x"CD",x"8F",x"10", -- 0x0128
    x"D4",x"44",x"11",x"2A",x"75",x"20",x"11",x"04", -- 0x0130
    x"00",x"19",x"22",x"75",x"20",x"0D",x"C2",x"26", -- 0x0138
    x"11",x"C3",x"71",x"11",x"CD",x"96",x"10",x"DA", -- 0x0140
    x"4B",x"11",x"C9",x"CD",x"9D",x"10",x"D2",x"52", -- 0x0148
    x"11",x"C9",x"CD",x"A9",x"10",x"DA",x"59",x"11", -- 0x0150
    x"C9",x"F1",x"2B",x"2B",x"2B",x"36",x"00",x"CD", -- 0x0158
    x"6A",x"11",x"22",x"29",x"20",x"21",x"27",x"20", -- 0x0160
    x"34",x"C9",x"23",x"23",x"5E",x"23",x"56",x"EB", -- 0x0168
    x"C9",x"CD",x"07",x"13",x"22",x"77",x"20",x"0E", -- 0x0170
    x"06",x"E5",x"21",x"1F",x"20",x"34",x"E1",x"7E", -- 0x0178
    x"07",x"DC",x"9D",x"11",x"2A",x"77",x"20",x"11", -- 0x0180
    x"04",x"00",x"19",x"22",x"77",x"20",x"0D",x"C2", -- 0x0188
    x"79",x"11",x"21",x"1F",x"20",x"36",x"00",x"CD", -- 0x0190
    x"E2",x"14",x"C3",x"7D",x"15",x"CD",x"6A",x"11", -- 0x0198
    x"7C",x"FE",x"3C",x"D0",x"C5",x"CD",x"49",x"0D", -- 0x01A0
    x"22",x"79",x"20",x"C1",x"21",x"79",x"20",x"3A", -- 0x01A8
    x"1A",x"20",x"C6",x"0A",x"BE",x"D2",x"B9",x"11", -- 0x01B0
    x"C9",x"F5",x"3E",x"20",x"86",x"47",x"F1",x"B8", -- 0x01B8
    x"DA",x"C4",x"11",x"C9",x"3A",x"1B",x"20",x"23", -- 0x01C0
    x"F5",x"7E",x"D6",x"08",x"47",x"F1",x"B8",x"D2", -- 0x01C8
    x"D3",x"11",x"C9",x"F5",x"3E",x"23",x"86",x"47", -- 0x01D0
    x"F1",x"B8",x"DA",x"DE",x"11",x"C9",x"F1",x"E5", -- 0x01D8
    x"21",x"1D",x"20",x"34",x"2A",x"1A",x"20",x"01", -- 0x01E0
    x"00",x"F6",x"09",x"22",x"22",x"20",x"2A",x"77", -- 0x01E8
    x"20",x"7E",x"E6",x"0F",x"77",x"CD",x"6A",x"11", -- 0x01F0
    x"CD",x"89",x"08",x"E1",x"2B",x"3E",x"05",x"86", -- 0x01F8
    x"47",x"3A",x"1A",x"20",x"B8",x"D2",x"0E",x"12", -- 0x0200
    x"CD",x"67",x"1A",x"C3",x"0C",x"13",x"21",x"24", -- 0x0208
    x"20",x"34",x"CD",x"5F",x"1A",x"C3",x"0C",x"13", -- 0x0210
    x"2E",x"24",x"CD",x"52",x"1A",x"7E",x"FE",x"06", -- 0x0218
    x"D2",x"24",x"12",x"C9",x"E5",x"3A",x"1D",x"20", -- 0x0220
    x"A7",x"D3",x"06",x"C2",x"25",x"12",x"E1",x"36", -- 0x0228
    x"00",x"CD",x"0D",x"19",x"CD",x"4C",x"00",x"21", -- 0x0230
    x"7B",x"20",x"34",x"21",x"00",x"46",x"11",x"00", -- 0x0238
    x"20",x"06",x"31",x"CD",x"8B",x"03",x"21",x"35", -- 0x0240
    x"46",x"11",x"35",x"20",x"06",x"46",x"CD",x"8B", -- 0x0248
    x"03",x"21",x"81",x"46",x"11",x"81",x"20",x"06", -- 0x0250
    x"3F",x"CD",x"8B",x"03",x"21",x"14",x"20",x"34", -- 0x0258
    x"CD",x"1E",x"14",x"CD",x"52",x"1A",x"2E",x"26", -- 0x0260
    x"34",x"7E",x"47",x"0F",x"DA",x"71",x"12",x"2B", -- 0x0268
    x"34",x"78",x"FE",x"08",x"CC",x"13",x"13",x"FE", -- 0x0270
    x"0F",x"CC",x"13",x"13",x"CD",x"AD",x"12",x"CD", -- 0x0278
    x"07",x"13",x"23",x"23",x"5E",x"23",x"56",x"21", -- 0x0280
    x"09",x"20",x"73",x"23",x"72",x"3E",x"30",x"CD", -- 0x0288
    x"D5",x"14",x"21",x"05",x"20",x"34",x"7E",x"A7", -- 0x0290
    x"D3",x"06",x"C2",x"96",x"12",x"21",x"7B",x"20", -- 0x0298
    x"36",x"00",x"2E",x"2F",x"34",x"2E",x"14",x"36", -- 0x02A0
    x"00",x"CD",x"78",x"1A",x"C9",x"CD",x"52",x"1A", -- 0x02A8
    x"2E",x"25",x"7E",x"FE",x"0B",x"DA",x"BB",x"12", -- 0x02B0
    x"3E",x"03",x"77",x"47",x"FE",x"07",x"DA",x"C3", -- 0x02B8
    x"12",x"06",x"06",x"3E",x"18",x"D6",x"03",x"05", -- 0x02C0
    x"C2",x"C5",x"12",x"4F",x"CD",x"07",x"13",x"23", -- 0x02C8
    x"23",x"EB",x"69",x"26",x"27",x"CD",x"00",x"03", -- 0x02D0
    x"2E",x"25",x"CD",x"52",x"1A",x"7E",x"0F",x"0F", -- 0x02D8
    x"11",x"E8",x"44",x"DA",x"E9",x"12",x"11",x"F8", -- 0x02E0
    x"44",x"CD",x"07",x"13",x"CD",x"EF",x"02",x"CD", -- 0x02E8
    x"07",x"13",x"7E",x"0F",x"0F",x"01",x"20",x"00", -- 0x02F0
    x"DA",x"FE",x"12",x"01",x"E0",x"FF",x"21",x"28", -- 0x02F8
    x"21",x"71",x"23",x"70",x"C3",x"BC",x"00",x"2E", -- 0x0300
    x"04",x"C3",x"52",x"1A",x"2E",x"24",x"CD",x"52", -- 0x0308
    x"1A",x"34",x"C9",x"F5",x"3E",x"FF",x"CD",x"04", -- 0x0310
    x"02",x"3E",x"04",x"CD",x"FB",x"01",x"CD",x"C1", -- 0x0318
    x"00",x"3E",x"20",x"CD",x"FB",x"01",x"CD",x"83", -- 0x0320
    x"1A",x"CD",x"BC",x"00",x"CD",x"69",x"13",x"3E", -- 0x0328
    x"04",x"CD",x"04",x"02",x"2E",x"26",x"CD",x"52", -- 0x0330
    x"1A",x"7E",x"0F",x"DA",x"49",x"13",x"21",x"12", -- 0x0338
    x"30",x"11",x"14",x"40",x"06",x"04",x"C3",x"51", -- 0x0340
    x"13",x"21",x"12",x"2D",x"11",x"18",x"40",x"06", -- 0x0348
    x"0A",x"CD",x"60",x"02",x"CD",x"B6",x"00",x"CD", -- 0x0350
    x"4C",x"00",x"3E",x"80",x"CD",x"D5",x"14",x"CD", -- 0x0358
    x"92",x"13",x"21",x"30",x"20",x"36",x"00",x"F1", -- 0x0360
    x"C9",x"21",x"03",x"24",x"22",x"2D",x"21",x"21", -- 0x0368
    x"1C",x"40",x"22",x"2F",x"21",x"0E",x"71",x"C5", -- 0x0370
    x"2A",x"2D",x"21",x"CD",x"BB",x"13",x"22",x"2D", -- 0x0378
    x"21",x"2A",x"2F",x"21",x"CD",x"CF",x"13",x"22", -- 0x0380
    x"2F",x"21",x"C1",x"0D",x"C2",x"77",x"13",x"CD", -- 0x0388
    x"FC",x"00",x"0E",x"79",x"21",x"1C",x"33",x"22", -- 0x0390
    x"2D",x"21",x"21",x"03",x"32",x"22",x"2F",x"21", -- 0x0398
    x"C5",x"AF",x"2A",x"2D",x"21",x"CD",x"D1",x"13", -- 0x03A0
    x"22",x"2D",x"21",x"AF",x"2A",x"2F",x"21",x"CD", -- 0x03A8
    x"BD",x"13",x"22",x"2F",x"21",x"C1",x"0D",x"C2", -- 0x03B0
    x"A0",x"13",x"C9",x"3E",x"FF",x"06",x"1A",x"E5", -- 0x03B8
    x"77",x"23",x"CD",x"E3",x"13",x"05",x"C2",x"C0", -- 0x03C0
    x"13",x"E1",x"11",x"20",x"00",x"19",x"C9",x"3E", -- 0x03C8
    x"FF",x"06",x"1A",x"E5",x"77",x"2B",x"CD",x"E3", -- 0x03D0
    x"13",x"05",x"C2",x"D4",x"13",x"E1",x"11",x"E0", -- 0x03D8
    x"FF",x"19",x"C9",x"1E",x"20",x"D3",x"06",x"1D", -- 0x03E0
    x"C2",x"E5",x"13",x"C9",x"DB",x"02",x"E6",x"04", -- 0x03E8
    x"C8",x"3A",x"03",x"20",x"A7",x"C0",x"31",x"00", -- 0x03F0
    x"24",x"06",x"04",x"C5",x"CD",x"1E",x"14",x"C1", -- 0x03F8
    x"05",x"C2",x"FB",x"13",x"3E",x"01",x"32",x"03", -- 0x0400
    x"20",x"CD",x"B6",x"00",x"FB",x"11",x"22",x"40", -- 0x0408
    x"21",x"16",x"30",x"06",x"04",x"CD",x"60",x"02", -- 0x0410
    x"CD",x"C1",x"00",x"C3",x"18",x"00",x"21",x"03", -- 0x0418
    x"24",x"01",x"DF",x"1A",x"C5",x"E5",x"36",x"00", -- 0x0420
    x"23",x"05",x"C2",x"26",x"14",x"E1",x"11",x"20", -- 0x0428
    x"00",x"19",x"C1",x"0D",x"C2",x"24",x"14",x"C9", -- 0x0430
    x"CD",x"50",x"0C",x"E6",x"10",x"CA",x"4D",x"14", -- 0x0438
    x"21",x"14",x"20",x"AF",x"BE",x"C0",x"23",x"BE", -- 0x0440
    x"C0",x"34",x"23",x"34",x"C9",x"21",x"15",x"20", -- 0x0448
    x"36",x"00",x"C9",x"21",x"16",x"20",x"AF",x"BE", -- 0x0450
    x"C8",x"23",x"BE",x"C2",x"69",x"14",x"34",x"2A", -- 0x0458
    x"33",x"20",x"01",x"10",x"05",x"09",x"22",x"1A", -- 0x0460
    x"20",x"2A",x"1A",x"20",x"CD",x"C3",x"08",x"2A", -- 0x0468
    x"1A",x"20",x"3A",x"54",x"20",x"A7",x"7D",x"C2", -- 0x0470
    x"88",x"14",x"FE",x"E0",x"D2",x"8D",x"14",x"C6", -- 0x0478
    x"03",x"6F",x"22",x"1A",x"20",x"C3",x"CB",x"08", -- 0x0480
    x"FE",x"C0",x"C3",x"7C",x"14",x"2A",x"1A",x"20", -- 0x0488
    x"22",x"BD",x"20",x"21",x"BB",x"20",x"34",x"21", -- 0x0490
    x"16",x"46",x"11",x"16",x"20",x"06",x"07",x"C3", -- 0x0498
    x"8B",x"03",x"21",x"BB",x"20",x"AF",x"BE",x"C8", -- 0x04A0
    x"23",x"BE",x"C2",x"BD",x"14",x"34",x"2A",x"BD", -- 0x04A8
    x"20",x"11",x"CD",x"14",x"01",x"08",x"01",x"CD", -- 0x04B0
    x"76",x"03",x"C3",x"D5",x"01",x"AF",x"77",x"2B", -- 0x04B8
    x"77",x"2A",x"BD",x"20",x"01",x"08",x"01",x"CD", -- 0x04C0
    x"76",x"03",x"C3",x"8C",x"08",x"A9",x"5C",x"BE", -- 0x04C8
    x"7F",x"FE",x"7F",x"7E",x"95",x"32",x"00",x"20", -- 0x04D0
    x"3A",x"00",x"20",x"A7",x"D3",x"06",x"C2",x"D8", -- 0x04D8
    x"14",x"C9",x"21",x"82",x"20",x"AF",x"BE",x"C8", -- 0x04E0
    x"3A",x"1A",x"20",x"D6",x"08",x"21",x"83",x"20", -- 0x04E8
    x"BE",x"D0",x"C6",x"12",x"BE",x"D8",x"3A",x"1B", -- 0x04F0
    x"20",x"23",x"D6",x"03",x"BE",x"D0",x"C6",x"09", -- 0x04F8
    x"BE",x"D8",x"2A",x"83",x"20",x"22",x"8D",x"20", -- 0x0500
    x"CD",x"5B",x"0A",x"21",x"82",x"20",x"36",x"00", -- 0x0508
    x"21",x"8B",x"20",x"34",x"C9",x"21",x"82",x"20", -- 0x0510
    x"AF",x"BE",x"C8",x"23",x"5E",x"23",x"56",x"EB", -- 0x0518
    x"E5",x"CD",x"5B",x"0A",x"E1",x"11",x"F9",x"FF", -- 0x0520
    x"19",x"22",x"83",x"20",x"7D",x"FE",x"18",x"D2", -- 0x0528
    x"42",x"0A",x"22",x"87",x"20",x"21",x"85",x"20", -- 0x0530
    x"36",x"FF",x"21",x"82",x"20",x"36",x"00",x"C9", -- 0x0538
    x"21",x"85",x"20",x"AF",x"BE",x"C8",x"23",x"34", -- 0x0540
    x"7E",x"FE",x"01",x"CA",x"83",x"15",x"FE",x"04", -- 0x0548
    x"C2",x"59",x"15",x"11",x"24",x"44",x"C3",x"86", -- 0x0550
    x"15",x"FE",x"07",x"D8",x"11",x"3C",x"44",x"CD", -- 0x0558
    x"86",x"15",x"21",x"00",x"00",x"22",x"81",x"20", -- 0x0560
    x"22",x"85",x"20",x"CD",x"E0",x"15",x"21",x"88", -- 0x0568
    x"20",x"D2",x"7A",x"15",x"CD",x"E7",x"15",x"C3", -- 0x0570
    x"7D",x"15",x"CD",x"07",x"16",x"21",x"14",x"20", -- 0x0578
    x"36",x"00",x"C9",x"11",x"0C",x"44",x"2A",x"87", -- 0x0580
    x"20",x"CD",x"76",x"03",x"01",x"0C",x"02",x"C3", -- 0x0588
    x"D5",x"01",x"21",x"8B",x"20",x"AF",x"BE",x"C8", -- 0x0590
    x"23",x"34",x"7E",x"FE",x"01",x"CA",x"CE",x"15", -- 0x0598
    x"FE",x"04",x"C2",x"AB",x"15",x"11",x"B4",x"44", -- 0x05A0
    x"C3",x"D1",x"15",x"FE",x"07",x"D8",x"2A",x"8D", -- 0x05A8
    x"20",x"CD",x"76",x"03",x"01",x"10",x"02",x"CD", -- 0x05B0
    x"8C",x"08",x"21",x"00",x"00",x"22",x"8B",x"20", -- 0x05B8
    x"21",x"81",x"20",x"36",x"00",x"CD",x"6F",x"1A", -- 0x05C0
    x"CD",x"D7",x"15",x"C3",x"7D",x"15",x"11",x"94", -- 0x05C8
    x"44",x"2A",x"8D",x"20",x"C3",x"89",x"15",x"CD", -- 0x05D0
    x"E0",x"15",x"DA",x"ED",x"15",x"C3",x"0D",x"16", -- 0x05D8
    x"CD",x"07",x"13",x"7E",x"0F",x"0F",x"C9",x"AF", -- 0x05E0
    x"3A",x"34",x"20",x"BE",x"D0",x"21",x"80",x"20", -- 0x05E8
    x"34",x"7E",x"FE",x"03",x"DA",x"FF",x"15",x"36", -- 0x05F0
    x"00",x"3E",x"E8",x"2B",x"86",x"77",x"C9",x"FE", -- 0x05F8
    x"02",x"3E",x"F0",x"DA",x"FB",x"15",x"C9",x"AF", -- 0x0600
    x"3A",x"34",x"20",x"BE",x"D8",x"21",x"7E",x"20", -- 0x0608
    x"34",x"7E",x"FE",x"03",x"DA",x"1F",x"16",x"36", -- 0x0610
    x"00",x"3E",x"15",x"2B",x"86",x"77",x"C9",x"FE", -- 0x0618
    x"02",x"3E",x"0D",x"DA",x"1B",x"16",x"C9",x"21", -- 0x0620
    x"91",x"20",x"AF",x"BE",x"C8",x"23",x"34",x"7E", -- 0x0628
    x"FE",x"01",x"CA",x"BA",x"16",x"FE",x"04",x"CA", -- 0x0630
    x"C9",x"16",x"FE",x"07",x"D8",x"CD",x"CF",x"16", -- 0x0638
    x"21",x"00",x"00",x"22",x"91",x"20",x"CD",x"5F", -- 0x0640
    x"1A",x"21",x"95",x"20",x"34",x"7E",x"FE",x"03", -- 0x0648
    x"D2",x"5E",x"16",x"3E",x"50",x"32",x"C2",x"20", -- 0x0650
    x"CD",x"8C",x"02",x"C3",x"7D",x"15",x"CD",x"88", -- 0x0658
    x"16",x"21",x"C1",x"20",x"70",x"78",x"C6",x"20", -- 0x0660
    x"CD",x"2E",x"03",x"2A",x"93",x"20",x"CD",x"76", -- 0x0668
    x"03",x"CD",x"45",x"03",x"11",x"34",x"04",x"CD", -- 0x0670
    x"45",x"03",x"11",x"34",x"04",x"CD",x"45",x"03", -- 0x0678
    x"21",x"96",x"20",x"34",x"23",x"36",x"0A",x"C9", -- 0x0680
    x"3A",x"01",x"20",x"0F",x"0F",x"06",x"03",x"0F", -- 0x0688
    x"D8",x"06",x"05",x"0F",x"D8",x"06",x"07",x"0F", -- 0x0690
    x"D8",x"06",x"09",x"C9",x"21",x"96",x"20",x"AF", -- 0x0698
    x"BE",x"C8",x"23",x"BE",x"CA",x"A9",x"16",x"35", -- 0x06A0
    x"C9",x"CD",x"8C",x"02",x"01",x"18",x"01",x"CD", -- 0x06A8
    x"D2",x"16",x"21",x"96",x"20",x"36",x"00",x"C3", -- 0x06B0
    x"7D",x"15",x"11",x"94",x"44",x"2A",x"93",x"20", -- 0x06B8
    x"CD",x"76",x"03",x"01",x"10",x"02",x"C3",x"D5", -- 0x06C0
    x"01",x"11",x"B4",x"44",x"C3",x"BD",x"16",x"01", -- 0x06C8
    x"10",x"02",x"2A",x"93",x"20",x"CD",x"76",x"03", -- 0x06D0
    x"C3",x"8C",x"08",x"21",x"81",x"20",x"AF",x"BE", -- 0x06D8
    x"C0",x"CD",x"07",x"13",x"E5",x"7E",x"FE",x"FF", -- 0x06E0
    x"C2",x"ED",x"16",x"E1",x"C9",x"07",x"DA",x"F9", -- 0x06E8
    x"16",x"E1",x"23",x"23",x"23",x"23",x"C3",x"E4", -- 0x06F0
    x"16",x"E1",x"E5",x"CD",x"40",x"0D",x"4D",x"44", -- 0x06F8
    x"CD",x"E0",x"15",x"21",x"7F",x"20",x"DA",x"2F", -- 0x0700
    x"17",x"2E",x"7D",x"7E",x"B8",x"D2",x"F1",x"16", -- 0x0708
    x"C6",x"04",x"B8",x"DA",x"F1",x"16",x"21",x"83", -- 0x0710
    x"20",x"71",x"23",x"78",x"FE",x"30",x"DA",x"F1", -- 0x0718
    x"16",x"FE",x"F0",x"D2",x"F1",x"16",x"70",x"21", -- 0x0720
    x"FF",x"FF",x"22",x"81",x"20",x"E1",x"C9",x"7E", -- 0x0728
    x"B8",x"D2",x"F1",x"16",x"C6",x"06",x"B8",x"DA", -- 0x0730
    x"F1",x"16",x"C3",x"16",x"17",x"3A",x"AA",x"20", -- 0x0738
    x"A7",x"C8",x"21",x"A2",x"20",x"CD",x"F0",x"0D", -- 0x0740
    x"CD",x"5D",x"17",x"21",x"AE",x"20",x"35",x"7E", -- 0x0748
    x"A7",x"C2",x"57",x"17",x"C3",x"82",x"17",x"21", -- 0x0750
    x"A0",x"20",x"C3",x"A1",x"0D",x"7D",x"E6",x"07", -- 0x0758
    x"D3",x"02",x"CD",x"76",x"03",x"C5",x"E5",x"1A", -- 0x0760
    x"D3",x"04",x"DB",x"03",x"B6",x"77",x"23",x"13", -- 0x0768
    x"AF",x"D3",x"04",x"DB",x"03",x"B6",x"77",x"E1", -- 0x0770
    x"01",x"20",x"00",x"09",x"C1",x"05",x"C2",x"65", -- 0x0778
    x"17",x"C9",x"2A",x"A8",x"20",x"EB",x"1A",x"2A", -- 0x0780
    x"AF",x"20",x"2D",x"CA",x"BA",x"17",x"22",x"AF", -- 0x0788
    x"20",x"32",x"AE",x"20",x"13",x"1A",x"E6",x"0F", -- 0x0790
    x"07",x"01",x"BF",x"17",x"26",x"00",x"6F",x"09", -- 0x0798
    x"E5",x"C1",x"21",x"A0",x"20",x"0A",x"77",x"03", -- 0x07A0
    x"23",x"0A",x"77",x"13",x"1A",x"6F",x"13",x"1A", -- 0x07A8
    x"67",x"22",x"A2",x"20",x"EB",x"23",x"22",x"A8", -- 0x07B0
    x"20",x"C9",x"AF",x"32",x"AA",x"20",x"C9",x"00", -- 0x07B8
    x"02",x"02",x"02",x"02",x"00",x"02",x"FE",x"00", -- 0x07C0
    x"FE",x"FE",x"FE",x"FE",x"00",x"FE",x"02",x"01", -- 0x07C8
    x"02",x"02",x"01",x"02",x"FF",x"01",x"FE",x"FF", -- 0x07D0
    x"FE",x"FE",x"FF",x"FE",x"01",x"FF",x"02",x"FF", -- 0x07D8
    x"FF",x"24",x"06",x"D7",x"30",x"10",x"00",x"D7", -- 0x07E0
    x"30",x"04",x"07",x"D7",x"50",x"0A",x"06",x"CF", -- 0x07E8
    x"58",x"04",x"05",x"BC",x"58",x"04",x"07",x"B4", -- 0x07F0
    x"50",x"0A",x"06",x"AC",x"58",x"04",x"05",x"98"  -- 0x07F8
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;
