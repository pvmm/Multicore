

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port (
			-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
		
		
	);
end entity;

architecture Behavior of top is

	-- Reset signal
	signal reset_n			: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset		: std_logic;		-- Reset do PLL
	signal pll_locked		: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clock_s			: std_logic;
	signal clock2X_s		: std_logic;
	signal clock4X_s		: std_logic;
	

	
	
	--rgb
	signal rgb_loader_out			: std_logic_vector(7 downto 0);
	signal rgb_atari_out				: std_logic_vector(7 downto 0);
	
	signal hsync_loader_out			: std_logic;
	signal vsync_loader_out			: std_logic;
	
	signal hsync_atari_out			: std_logic;
	signal vsync_atari_out			: std_logic;
	
	signal bs_method			 		: std_logic_vector(7 downto 0);
	
	-- PS/2
	signal keyb_data_s 			: std_logic_vector(7 downto 0);
	signal keyb_valid_s 			: std_logic;
	
	signal clk_keyb 				: std_logic;
	
	------
	signal loader_hor_s 				: std_logic_vector(8 downto 0);
	signal loader_ver_s 				: std_logic_vector(8 downto 0);
	
	signal clk_module_vga					: std_logic;
	signal clk_vga					: std_logic;
	signal clk_dvi					: std_logic;
	
	signal vga_color_s 			: std_logic_vector(3 downto 0);
	signal vga_blank_s 			: std_logic;
	
	-- video
	signal vga_rgb_s  	: std_logic_vector(7 downto 0);
	signal vga_rgb_out_s 		: std_logic_vector(7 downto 0);
	signal vga_hsync_n_s : std_logic;
	signal vga_vsync_n_s : std_logic;
		
	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	
begin

 	

	pll: work.pll1
	port map 
	(
		areset		=> pll_reset,				-- PLL Reset
		inclk0		=> clock_50_i,				-- Clock 50 MHz externo
		c0				=> clock_s,		
		c1				=> clock2X_s,
		c2				=> clock4X_s,
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
	
	invaders: work.invaders_top 
	generic map
	(
		g_invaders 		=> false,
		g_invadersdlx 	=> false,
		g_lrescue 		=> false,
		g_spclaser 		=> false,
		g_searthin 		=> true,
		g_ballbomb 		=> false,
		g_outra 			=> false
	)
	port map
	(
		STRATAFLASH_OE => open,
		STRATAFLASH_CE => open,
		STRATAFLASH_WE => open,
		--
		I_PS2_CLK  	 	=> ps2_clk_io,
		I_PS2_DATA  	=> ps2_data_io,
		--
		O_VIDEO_R      => vga_r_o(2),
		O_VIDEO_G      => vga_g_o(2),
		O_VIDEO_B      => vga_b_o(1),
		O_HSYNC        => vga_hsync_n_o,
		O_VSYNC        => vga_vsync_n_o,
		--
		O_AUDIO_L      => dac_l_o,
		O_AUDIO_R      => dac_r_o,
		--
		I_RESET        => not btn_n_i(4),
		I_CLK      		=> clock_s,
		I_CLK_2X   		=> clock2X_s,
		I_CLK_4X   		=> clock4X_s,
		
		btn_n_i			=> btn_n_i,
		
		joy1_up_i	   => joy1_up_i,	
		joy1_down_i		=> joy1_down_i,	
		joy1_left_i		=> joy1_left_i,	
		joy1_right_i	=> joy1_right_i,
		joy1_p6_i		=> joy1_p6_i,
		joy1_p9_i		=> joy1_p9_i,
		joy2_up_i		=> joy2_up_i,
		joy2_down_i		=> joy2_down_i,	
		joy2_left_i		=> joy2_left_i,	
		joy2_right_i	=> joy2_right_i,
		joy2_p6_i		=> joy2_p6_i,
		joy2_p9_i		=> joy2_p9_i		
			
	);


end architecture;
