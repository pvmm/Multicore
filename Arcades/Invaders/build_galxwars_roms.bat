
set rom_path=roms\

copy /b %rom_path%galxwars\univgw3.0 + %rom_path%galxwars\univgw4.1 %rom_path%galxwars\galxwars.1
copy /b %rom_path%galxwars\univgw5.2 + %rom_path%galxwars\univgw6.3 %rom_path%galxwars\galxwars.2
copy /b %rom_path%galxwars\univgw1.4 + %rom_path%galxwars\univgw2.5 %rom_path%galxwars\galxwars.3
                  
romgen %rom_path%galxwars\galxwars.1 GALAXWARS_ROM_1 11 a r > %rom_path%galxwars\galaxwars_rom_1.vhd
romgen %rom_path%galxwars\galxwars.2 GALAXWARS_ROM_2 11 a r > %rom_path%galxwars\galaxwars_rom_2.vhd
romgen %rom_path%galxwars\galxwars.3 GALAXWARS_ROM_3 11 a r > %rom_path%galxwars\galaxwars_rom_3.vhd
pause
echo done
