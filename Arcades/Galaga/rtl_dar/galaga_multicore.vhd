---------------------------------------------------------------------------------
-- DE2-35 Top level for Galaga Midway by Dar (darfpga@aol.fr) (December 2016)
-- http://darfpga.blogspot.fr
---------------------------------------------------------------------------------
-- Educational use only
-- Do not redistribute synthetized file with roms
-- Do not redistribute roms whatever the form
-- Use at your own risk
---------------------------------------------------------------------------------
--
-- Main features :
--  PS2 keyboard input
--  Wm8731 sound output
--  NO board SRAM/Flash used
--
-- Uses 1 pll for 18MHz and 11MHz generation from 50MHz
--
-- Board key :
--      0 : reset
--
-- Keyboard inputs :
--   F3 : Add coin
--   F2 : Start 2 players
--   F1 : Start 1 player
--   SPACE       : Fire player 1 & 2
--   RIGHT arrow : Move right player 1 & 2
--   LEFT arrow  : Move left player 1 & 2
--
-- Dip switch and other details : see galaga.vhd

---------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

entity multicore_top is
port(
 	-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
		
	);
end;

architecture struct of multicore_top is

 signal clock_18  : std_logic;
 signal clock_11  : std_logic;

 signal r         : std_logic_vector(2 downto 0);
 signal g         : std_logic_vector(2 downto 0);
 signal b         : std_logic_vector(1 downto 0);
 signal video_clk : std_logic;
 signal csync     : std_logic;
 signal blankn    : std_logic;
 signal hsync     : std_logic;
 signal vsync     : std_logic;
 
 signal audio        : std_logic_vector(9 downto 0);
 signal sound_string : std_logic_vector(7 downto 0);
 signal reset        : std_logic;
 signal dac_s        : std_logic;
 
 alias  reset_n      : std_logic is btn_n_i(4);
 
 signal kbd_intr      : std_logic;
 signal kbd_scancode  : std_logic_vector(7 downto 0);
 signal joyPCFRLDU    : std_logic_vector(7 downto 0);

begin

reset <= not reset_n;
-- tv15Khz_mode <= sw();

clk_11_18 : entity work.pll50_to_11_and_18
port map(
 inclk0 => clock_50_i,
 c0 => clock_11,
 c1 => clock_18,
 locked => open --pll_locked
);

galaga : entity work.galaga
port map(
 clock_18     => clock_18,
 reset        => reset,
-- tv15Khz_mode => tv15Khz_mode,
 video_r      => r,
 video_g      => g,
 video_b      => b,
 video_csync  => csync,
 video_blankn => blankn,
 video_hs     => hsync,
 video_vs     => vsync,
 audio        => audio,
 
 b_test       => '1',
 b_svce       => '1', 
 coin         => joyPCFRLDU(7) or btn_n_i(3),
 start1       => joyPCFRLDU(5) or btn_n_i(1),
 left1        => joyPCFRLDU(2) or not joy1_left_i,
 right1       => joyPCFRLDU(3) or not joy1_right_i,
 fire1        => joyPCFRLDU(4) or not joy1_p6_i,
 start2       => joyPCFRLDU(6) or btn_n_i(2),
 left2        => joyPCFRLDU(2) or not joy1_left_i,
 right2       => joyPCFRLDU(3) or not joy1_right_i,
 fire2        => joyPCFRLDU(4) or not joy1_p6_i
);

--VGA_CLK   <= clock_18;
--VGA_SYNC  <= '0';
--VGA_BLANK <= blankn;

vga_r_o <= r when blankn='1' else (others=>'0');
vga_g_o <= g when blankn='1' else (others=>'0');
vga_b_o <= b & "0" when blankn='1' else (others=>'0');

-- synchro composite/ synchro horizontale
--vga_hs <= csync;
-- vga_hs <= csync when tv15Khz_mode = '1' else hsync;
-- commutation rapide / synchro verticale
--vga_vs <= '1';
-- vga_vs <= '1'   when tv15Khz_mode = '1' else vsync;

vga_hsync_n_o <= hsync;
vga_vsync_n_o <= vsync;


sound_string <= "0" & audio (9 downto 3);

--wm8731_dac : entity work.wm8731_dac
--port map(
-- clk18MHz => clock_18,
-- sampledata => sound_string,
-- i2c_sclk => i2c_sclk,
-- i2c_sdat => i2c_sdat,
-- aud_bclk => aud_bclk,
-- aud_daclrck => aud_daclrck,
-- aud_dacdat => aud_dacdat,
-- aud_xck => aud_xck
--); 

	inst_dacl : entity work.dac
	port map(
		clk_i   => clock_18,
		res_i   => reset,
		dac_i   => sound_string,
		dac_o   => dac_s
	);
	
	dac_l_o <= dac_s;
	dac_r_o <= dac_s;


-- get scancode from keyboard
keyboard : entity work.io_ps2_keyboard
port map (
  clk       => clock_11,
  kbd_clk   => ps2_clk_io,
  kbd_dat   => ps2_data_io,
  interrupt => kbd_intr,
  scancode  => kbd_scancode
);

-- translate scancode to joystick
joystick : entity work.kbd_joystick
port map (
  clk         => clock_11,
  kbdint      => kbd_intr,
  kbdscancode => std_logic_vector(kbd_scancode), 
  joyPCFRLDU  => joyPCFRLDU 
);

--d7s0 : entity work.dec_7_seg port map(addr_cpu_out( 3 downto  0),hex0);
--d7s1 : entity work.dec_7_seg port map(addr_cpu_out( 7 downto  4),hex1);
--d7s2 : entity work.dec_7_seg port map(addr_cpu_out(11 downto  8),hex2);
--d7s3 : entity work.dec_7_seg port map(addr_cpu_out(15 downto 12),hex3);

end struct;
