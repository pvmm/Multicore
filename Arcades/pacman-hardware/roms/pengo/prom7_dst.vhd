-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PROM7_DST is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(4 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of PROM7_DST is


  type ROM_ARRAY is array(0 to 31) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"00",x"F6",x"07",x"38",x"C9",x"F8",x"3F",x"EF", -- 0x0000
    x"6F",x"16",x"2F",x"7F",x"F0",x"36",x"DB",x"C6", -- 0x0008
    x"00",x"F6",x"D8",x"F0",x"F8",x"16",x"07",x"2F", -- 0x0010
    x"36",x"3F",x"7F",x"28",x"32",x"38",x"EF",x"C6"  -- 0x0018
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;
