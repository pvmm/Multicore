------------------------------------------------------------------------------
-- GALAXIAN TOP level implementation 
--
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;

entity multicore_top is
port(
 	-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
		
	);
end;

architecture RTL of multicore_top is
	signal W_CLK_24M          : std_logic := '0';
	signal W_CLK_18M          : std_logic := '0';
	signal W_CLK_12M          : std_logic := '0';
	signal W_CLK_6M           : std_logic := '0';

	signal P1_CSJUDLR         : std_logic_vector( 6 downto 0) := (others => '0'); -- player 1 controls
	signal P2_CSJUDLR         : std_logic_vector( 6 downto 0) := (others => '0'); -- player 2 controls

	signal W_H_SYNC           : std_logic := '0';
	signal W_V_SYNC           : std_logic := '0';
	signal W_CMPBLK           : std_logic := '0';
	signal W_SDAT_A           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_SDAT_B           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_VID              : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_R                : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_G                : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_B                : std_logic_vector( 2 downto 0) := (others => '0');

	signal W_VGA              : std_logic_vector( 7 downto 0) := (others => '0');

	signal ps2_codeready      : std_logic := '1';
	signal ps2_scancode       : std_logic_vector( 9 downto 0) := (others => '0');
	
	signal I_RESET					: std_logic := '0';
begin
	inst_clocks : entity work.CLOCKGEN
	port map(
		CLKIN_IN   => clock_50_i,
		RST_IN     => I_RESET,
		O_CLK_24M  => W_CLK_24M,
		O_CLK_18M  => W_CLK_18M,
		O_CLK_12M  => W_CLK_12M,
		O_CLK_06M  => W_CLK_6M
	);

	inst_galaxian : entity work.galaxian
	port map(
		W_CLK_18M  => W_CLK_18M,
		W_CLK_12M  => W_CLK_12M,
		W_CLK_6M   => W_CLK_6M,

		P1_CSJUDLR => P1_CSJUDLR,
		P2_CSJUDLR => P2_CSJUDLR,

		I_RESET    => I_RESET,
		W_R        => W_R,
		W_G        => W_G,
		W_B        => W_B,
		W_H_SYNC   => W_H_SYNC,
		W_V_SYNC   => W_V_SYNC,
		W_SDAT_A   => W_SDAT_A,
		W_SDAT_B   => W_SDAT_B,
		O_CMPBL    => W_CMPBLK
	);

	inst_vga : entity work.VGA_SCANCONV
	port map(
		CLK        => W_CLK_6M,
		CLK_X4     => W_CLK_24M,
		--	input
		I_VIDEO    => W_VID,
		I_HSYNC    => W_H_SYNC,
		I_VSYNC    => not W_V_SYNC,
		I_CMPBLK_N => W_CMPBLK,
		--	output
		O_VIDEO    => W_VGA,
		O_HSYNC    => vga_hsync_n_o,
		O_VSYNC    => vga_vsync_n_o,
		O_CMPBLK_N => open
	);

	W_VID <= W_R & W_G & W_B(2 downto 1);

	vga_r_o <= W_VGA(7 downto 5);
	vga_g_o <= W_VGA(4 downto 2);
	vga_b_o <= W_VGA(1 downto 0) & W_VGA(0);

 

--	vga_r_o <= W_R;
--	vga_g_o <= W_G;
--	vga_b_o <= W_B;	
--	vga_hsync_n_o <= W_H_SYNC;
--	vga_vsync_n_o <= W_V_SYNC;


	inst_dacl : entity work.dac
	port map(
		clk_i   => W_CLK_24M,
		res_i   => I_RESET,
		dac_i   => W_SDAT_A,
		dac_o   => dac_l_o
	);

	inst_dacr : entity work.dac
	port map(
		clk_i   => W_CLK_24M,
		res_i   => I_RESET,
		dac_i   => W_SDAT_B,
		dac_o   => dac_r_o
	);

	-----------------------------------------------------------------------------
	-- Keyboard - active low buttons
	-----------------------------------------------------------------------------
	inst_kbd : entity work.Keyboard
	port map (
		Reset     => I_RESET,
		Clock     => W_CLK_18M,
		PS2Clock  => ps2_clk_io,
		PS2Data   => ps2_data_io,
		CodeReady => ps2_codeready,  --: out STD_LOGIC;
		ScanCode  => ps2_scancode    --: out STD_LOGIC_VECTOR(9 downto 0)
	);

-- ScanCode(9)          : 1 = Extended  0 = Regular
-- ScanCode(8)          : 1 = Break     0 = Make
-- ScanCode(7 downto 0) : Key Code

	P1_CSJUDLR(6) <= not btn_n_i(1);
	P1_CSJUDLR(5) <= not btn_n_i(3);
	P2_CSJUDLR(5) <= not btn_n_i(4);
 
	P1_CSJUDLR(4) <= not joy1_p6_i;     -- P1 jump "I"
	P1_CSJUDLR(3) <= not joy1_up_i;     -- P1 up arrow
	P1_CSJUDLR(2) <= not joy1_down_i;   -- P1 down arrow
	P1_CSJUDLR(1) <= not joy1_left_i;   -- P1 left arrow
	P1_CSJUDLR(0) <= not joy1_right_i;  -- P1 right arrow
	
	P2_CSJUDLR(4) <= not joy2_p6_i;     -- P2 jump "I"
	P2_CSJUDLR(3) <= not joy2_up_i;     -- P2 up arrow
	P2_CSJUDLR(2) <= not joy2_down_i;     -- P2 down arrow
	P2_CSJUDLR(1) <= not joy2_left_i;     -- P2 left arrow
	P2_CSJUDLR(0) <= not joy2_right_i;     -- P2 right arrow
	
	
	
	
--	process(W_CLK_18M)
--	begin
--		if rising_edge(W_CLK_18M) then
--			if I_RESET = '1' then
--				P1_CSJUDLR <= (others=>'0');
--				P2_CSJUDLR <= (others=>'0');
--			elsif (ps2_codeready = '1') then
--				case (ps2_scancode(7 downto 0)) is
--			--		when x"05" =>	P1_CSJUDLR(6) <= not ps2_scancode(8);     -- P1 coin "F1"
--			--		when x"04" =>	P2_CSJUDLR(6) <= not ps2_scancode(8);     -- P2 coin "F3"
----
--			--		when x"06" =>	P1_CSJUDLR(5) <= not ps2_scancode(8);     -- P1 start "F2"
--			--		when x"0c" =>	P2_CSJUDLR(5) <= not ps2_scancode(8);     -- P2 start "F4"
--
--					when x"43" =>	P1_CSJUDLR(4) <= not ps2_scancode(8);     -- P1 jump "I"
--										P2_CSJUDLR(4) <= not ps2_scancode(8);     -- P2 jump "I"
--
--					when x"75" =>	P1_CSJUDLR(3) <= not ps2_scancode(8);     -- P1 up arrow
--										P2_CSJUDLR(3) <= not ps2_scancode(8);     -- P2 up arrow
--
--					when x"72" =>	P1_CSJUDLR(2) <= not ps2_scancode(8);     -- P1 down arrow
--										P2_CSJUDLR(2) <= not ps2_scancode(8);     -- P2 down arrow
--
--					when x"6b" =>	P1_CSJUDLR(1) <= not ps2_scancode(8);     -- P1 left arrow
--										P2_CSJUDLR(1) <= not ps2_scancode(8);     -- P2 left arrow
--
--					when x"74" =>	P1_CSJUDLR(0) <= not ps2_scancode(8);     -- P1 right arrow
--										P2_CSJUDLR(0) <= not ps2_scancode(8);     -- P2 right arrow
--
--					when others => null;
--				end case;
--			end if;
--		end if;
--	end process;

end RTL;
